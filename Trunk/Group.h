/************************************************************************/
/* Name     : Trunk\Group.h                                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 12 Oct 2010                                               */
/************************************************************************/
#pragma once
#include <vector>
#include <map>
#include <list>
#include <boost/shared_ptr.hpp>
#include "../Composite/Composite.h"


// methods
const CAtlString M_CIRCLE   = L"Circle"    ;
const CAtlString M_LINEAR   = L"Linear"    ;
const CAtlString M_RANDOM   = L"Random"    ;
const CAtlString M_LASTUSED = L"LeastUsed" ;
// directions
const CAtlString D_FORWARD  = L"Forward"   ;
const CAtlString D_REVERSE  = L"Reverse"   ;
// parity
const CAtlString P_BOTH     = L"Both"      ;
const CAtlString P_ODD      = L"Odd"       ;
const CAtlString P_EVEN     = L"Even"      ;
//
//enum TrunkMethods
//{
//	TM_CIRCLE = 0,
//	TM_LINEAR,
//	TM_RANDOM,
//	TM_LEASTUSED
//};
//
//enum TrunckDirection
//{
//	TD_FORWARD = 0,
//	TD_REVERSE
//};
//
//enum TruncParity
//{
//	TP_BOTH = 0,
//	TP_ODD,  // 1, 3, 5 ...
//	TP_EVEN  // 0, 2, 4 ...
//};

class CEnumerator;

//class CTrunkGroup
//{
//	struct DTI
//	{
//		DTI(const int& _board, const int& _timeslot)
//			:board  (_board),
//			timeslot(_timeslot),
//			address (0)
//		{
//		}
//
//		DTI(const int& _board, const int& _timeslot, const ULONGLONG&_address)
//			:board   (_board),
//			 timeslot(_timeslot),
//			 address (_address)
//		{
//		}
//		DTI(const DTI& rhs)
//		{
//			board    = rhs.board;
//			timeslot = rhs.timeslot;
//			address  = rhs.address;
//		}
//		~DTI()
//		{
//			int test = 0;
//		}
//		int board;
//		int timeslot;
//		ULONGLONG address;
//	};
//	//typedef std::vector<DTI> NameList;
//
//	class NameList:public std::vector<DTI>
//	{
//	public:
//		NameList::iterator Find(const int         & _board, const int         & _timeslot);
//		NameList::iterator Find(const std::wstring& _board, const std::wstring& _timeslot);
//	};
//
//public:
//	class CRemoveUnctiveTimeSlotPredicat
//	{
//	public:
//		bool operator()(CTrunkGroup::DTI& _dti)
//		{
//			if (!_dti.address)
//				return true;
//			return false;
//			//return (!_dti.address)?false:true;
//		}
//	};
//
//	class CRemoveParityTimeSlotPredicat
//	{
//	public:
//		CRemoveParityTimeSlotPredicat(bool _parity_ever = true):m_bParityEver(_parity_ever)
//		{}
//		bool operator()(const CTrunkGroup::DTI& _dti)
//		{
//			return (m_bParityEver)?((_dti.timeslot%2)==1):((_dti.timeslot%2)!=1);
//		}
//	private:
//		bool m_bParityEver;
//	};
//
//	class CSortTimeSlotPredicat
//	{
//	public:
//		bool operator()(CTrunkGroup::DTI& _dti1, CTrunkGroup::DTI& _dti2)
//		{
//			////return ( (_dti1.board > _dti2.board) || (_dti1.timeslot > _dti2.timeslot));
//			if (_dti1.board > _dti2.board)
//				return false;
//			if ((_dti1.board == _dti2.board) && (_dti1.timeslot > _dti2.timeslot))
//				return false;
//
//			return true;
//
//			//return ((_dti1.board < _dti2.board) || (_dti1.timeslot < _dti2.timeslot));
//		}
//	};
//
//
//
//public:
//	typedef boost::shared_ptr<CTrunkGroup> ITrunkGroup;
//	typedef std::vector<ITrunkGroup> GroupList;
//	typedef bool (*listcallbackfunc)(const std::wstring&, ITrunkGroup);
//	//typedef void (*checkuricallbackfunc)(const std::wstring&);
//	typedef std::vector<std::wstring>  DTINameList;
//	typedef std::vector<ULONGLONG   >  AddressList;
//	//typedef std::map <std::wstring, ULONGLONG> TDTIList;
//	struct TIList
//	{
//		TIList(const std::wstring& _name, const ULONGLONG& _address): name(_name), address(_address)
//		{
//		}
//		std::wstring name;
//		ULONGLONG address;
//	};
//
//	class CTSInListFinder
//	{
//	public:
//		CTSInListFinder(const std::wstring& _name):m_sName(_name)
//		{
//
//		}
//		bool operator()(TIList& ts)
//		{
//			return !m_sName.compare(ts.name);
//		}
//	private:
//		std::wstring m_sName;
//	};
//
//	typedef std::list <TIList> TDTIList;
//
//
//private:
//	CTrunkGroup(const std::wstring& _filename);
//
//	CTrunkGroup(const std::wstring& _name,
//				const std::wstring& _timeslots,
//				const std::wstring& _method, 
//				const std::wstring& _direction, 
//				const std::wstring& _parity, 
//				unsigned int& _max);
//
//
//	CTrunkGroup(const std::wstring& _name,
//		        const std::wstring& _dti,
//		        const std::wstring& _timeslot,
//		        const std::wstring& _method, 
//		        const std::wstring& _direction, 
//		        const std::wstring& _parity,
//		        const ULONGLONG& ullAddress
//		);
//
//public:
//		~CTrunkGroup();
//
//	//void DeleteDTI(const int&_board,
//	//			   const int&_timeslot);
//
//	void AddDTI(const std::wstring& _groupname,
//		const std::wstring& _board,
//		const std::wstring& _timeslot,
//		const ULONGLONG& _address);
//
//	//bool SetDTI(
//	//	const int& _board,
//	//	const int& _timeslot,
//	//	const ULONGLONG& _address);
//	//bool SetDTI(
//	//	const std::wstring& _board,
//	//	const std::wstring& _timeslot,
//	//	const ULONGLONG& _address);
//
//	void SetDTI(
//		const int& _board,
//		const int& _timeslot,
//		const ULONGLONG& _address);
//	void SetDTI(
//		const std::wstring& _board,
//		const std::wstring& _timeslot,
//		const ULONGLONG& _address);
//
//	void Add (ITrunkGroup _elem){m_childrens.push_back(_elem);}
//	void Clear(){m_childrens.clear();}
//	void AddProxyList(IList::SPtr _proxy){ m_pipoutbound = _proxy; }
//
//	void GetAddress(TDTIList &List);
//	//bool      GetAddressByGroupName(const std::wstring& _groupname, TDTIList &List, std::wstring& log);
//	//bool      IsEnoughOutcomingsLine(const std::wstring& _groupname)const;
//
//	bool      GetAddressByTimeSlot(const std::wstring& _dti_timeslot,
//								   const std::wstring& _method,
//								   const std::wstring& _direction,
//								   const std::wstring& _parity,
//								   TDTIList &_list,
//								   std::wstring& log);
//	void      GetAddressByParametrs(const NameList& _dti_list,
//									const std::wstring& _method,
//		                            const std::wstring& _direction,
//		                            const std::wstring& _parity,
//								    TDTIList &_address_list);
//	CTrunkGroup*  GetGroupByGroupName(const std::wstring& _groupname);
//
//	//ULONGLONG GetDTIAddress();
//	std::wstring GetGroupName ()const{return m_sName.GetString();}
//	std::wstring GetScriptName()const{return m_sFileName;}
//	IList::SPtr GetOutBoundNameByGroupName(const std::wstring& _groupName)const;
//
//	bool refresh(const std::wstring& _filename, listcallbackfunc callbackfunc, ITrunkGroup pParent);
//	//void LUStepList(const std::wstring& _board, const std::wstring& _timeslot);
//
//private:
//	ULONGLONG GetDTIAddress(const std::wstring& _board, const std::wstring& _timeslot);
//	//ULONGLONG GetDTIAddress();
//	void      GetDTIAddress(TDTIList &List);
//
//	void ExtractTimeSlots(const std::wstring& sTimeSlots, // [in ]
//		                  NameList& _list);               // [out]
//
//	//NameList::iterator Find(const int         & _board, const int         & _timeslot, bool active = false);
//	//NameList::iterator Find(const std::wstring& _board, const std::wstring& _timeslot, bool active = false);
//	//bool Convert(const std::wstring& sBoard, const std::wstring& sTimeSlot,// [in ]
//	//	                       int& iBoard, int& iTimeSlot);                             // [out]
//	void BuildList();
//	void StepList (/*bool bIncludeLeastUsed*/);
//	//void StepList (const int& _board,	const int& _timeslot);
//
//	template<class _Pr> inline
//	NameList Remove_if(const NameList& _list, _Pr _Pred);
//
//	void AddDTI(const int&_board,
//		const int&_timeslot,
//		const ULONGLONG& _address);
//
//	void AddDTI(const std::wstring& _board,
//		const std::wstring& _timeslot,
//		const ULONGLONG& _address);
//
//
//
//	CAtlString      m_sName;
//	NameList        m_TimeSlots;
//	NameList		m_ActiveTimeSlots;
//	CAtlString      m_method;
//	CAtlString      m_direction;
//	CAtlString      m_parity;
//
//	GroupList       m_childrens;
//	std::wstring    m_sFileName;
//
//	unsigned int    m_Counter;
//	unsigned int    m_ActiveSize;
//
//	bool            m_externalHost;
//
//	//CAtlString      m_ipoutbound;
//	IList::SPtr     m_pipoutbound;
//	unsigned int    m_MaxLines;
//	unsigned int    m_BusyLines;
//
//	boost::shared_ptr<CEnumerator> m_counter;
//
//};

/******************************* ABSTRACT *************************************/
class CParentTrunk;

class CAbstractTrunk
{
	friend class CParentTrunk;
public:
	typedef boost::shared_ptr<CAbstractTrunk> ITrunkGroup;
	//typedef bool(*listcallbackfunc)(const std::wstring&, ITrunkGroup);

	using truck_group_callback =  std::function<void()>;
	using listcallbackfunc = std::function<bool(const std::wstring&, ITrunkGroup, truck_group_callback)>;

	typedef std::vector<ULONGLONG   >  AddressList;
protected:

	typedef std::vector<ITrunkGroup> GroupList;


public:

	class DTI
	{
	public:
		//ctors
		DTI(const int& _board, const int& _timeslot)
			:m_board(_board),
			m_timeslot(_timeslot),
			m_address(0)
		{
		}

		DTI(const int& _board, const int& _timeslot, const ULONGLONG&_address)
			:m_board(_board),
			m_timeslot(_timeslot),
			m_address(_address)
		{
		}
		DTI(const DTI& rhs) //copy ctor
		{
			m_board = rhs.m_board;
			m_timeslot = rhs.m_timeslot;
			m_address = rhs.m_address;
		}
		~DTI() //dtor
		{
			int test = 0;
		}

		int GetBoard()const{ return m_board; }
		int GetTimeslot()const{ return m_timeslot; }
		ULONGLONG GetAddress()const { return m_address; }

		void SetBoard(const int& _board) { m_board = _board; }
		void SetTimeslot(const int& _timeslot) { m_timeslot = _timeslot; }
		void SetAddress(const ULONGLONG& _address) { m_address = _address; }

		std::wstring GetName()
		{ 
			if (m_name.empty())
			{
				CAtlString sTemplate((m_timeslot>9) ? L"DTI%i T#%i" : L"DTI%i T#0%i");
				CAtlString sName;
				sName.Format(sTemplate, m_board, m_timeslot);
				m_name = sName.GetString();
			}

			return m_name;
		}

	private:
		int m_board;
		int m_timeslot;
		ULONGLONG m_address;
		std::wstring m_name;
	};

public:
	class NameList :public std::vector<DTI>
	{
	public:
		NameList::iterator Find(const int         & _board, const int         & _timeslot);
		NameList::iterator Find(const std::wstring& _board, const std::wstring& _timeslot);
	};

	class CRemoveUnctiveTimeSlotPredicat
	{
	public:
		bool operator()(CAbstractTrunk::DTI& _dti)
		{
			if (!_dti.GetAddress())
				return true;
			return false;
			//return (!_dti.address)?false:true;
		}
	};

	class CRemoveParityTimeSlotPredicat
	{
	public:
		CRemoveParityTimeSlotPredicat(bool _parity_ever = true) :m_bParityEver(_parity_ever)
		{}
		bool operator()(const CAbstractTrunk::DTI& _dti)
		{
			return (m_bParityEver) ? ((_dti.GetTimeslot() % 2) == 1) : ((_dti.GetTimeslot() % 2) != 1);
		}
	private:
		bool m_bParityEver;
	};

	class CSortTimeSlotPredicat
	{
	public:
		bool operator()(CAbstractTrunk::DTI& _dti1, CAbstractTrunk::DTI& _dti2)
		{
			////return ( (_dti1.board > _dti2.board) || (_dti1.timeslot > _dti2.timeslot));
			if (_dti1.GetBoard() > _dti2.GetBoard())
				return false;
			if ((_dti1.GetBoard() == _dti2.GetBoard()) && (_dti1.GetTimeslot() > _dti2.GetTimeslot()))
				return false;

			return true;

			//return ((_dti1.board < _dti2.board) || (_dti1.timeslot < _dti2.timeslot));
		}
	};


public:

	CAbstractTrunk() = default;

	CAbstractTrunk(const std::wstring& _name,
		const std::wstring& _timeslots,
		const std::wstring& _direction,
		const std::wstring& _parity,
		unsigned int& _max);

	virtual bool AddDTI(const std::wstring& _groupname,
		const std::wstring& _board,
		const std::wstring& _timeslot,
		const ULONGLONG& _address);

	//void RebuildAll();

	static ITrunkGroup CreateGroup(
		const std::wstring& _name,
		const std::wstring& _timeslots,
		const std::wstring& _method,
		const std::wstring& _direction,
		const std::wstring& _parity,
		unsigned int& _max);

	void AddProxyList(IList::SPtr _proxy){ m_pipoutbound = _proxy; }
	virtual IList::SPtr GetOutBoundNameByGroupName(const std::wstring& _groupName)const;
	//void Add(ITrunkGroup _elem){ }
	bool refresh(const std::wstring& _filename, listcallbackfunc callbackfunc, ITrunkGroup pParent, CAbstractTrunk::truck_group_callback call_back);
	virtual CAbstractTrunk*  GetGroupByGroupName(const std::wstring& _groupname);
	bool      GetAddressByTimeSlot(const std::wstring& _dti_timeslot,
		const std::wstring& _method,
		const std::wstring& _direction,
		const std::wstring& _parity,
		NameList &_list,
		std::wstring& log);


protected:
	//bool AddDTI(const std::wstring& _groupname,
	//	const int& _board,
	//	const int& _timeslot,
	//	const ULONGLONG& _address);

	void SetDTI(
		const int& _board,
		const int& _timeslot,
		const ULONGLONG& _address);

	virtual void GetAddressByParametrs(const NameList& _dti_list,
		const std::wstring& _method,
		const std::wstring& _direction,
		const std::wstring& _parity,
		NameList &_address_list);

public:

	// Interface

	virtual void BuildList() = 0;
	virtual void StepList() = 0;
	virtual CAtlString GetMethod() = 0;
	virtual void GetAddress(NameList &List) = 0;
	virtual std::wstring Info()const = 0;


protected:

	CAtlString      m_direction;
	CAtlString      m_parity;

	NameList        m_TimeSlots;
	NameList		m_ActiveTimeSlots;

	unsigned int    m_Counter;
	unsigned int    m_ActiveSize;

	CAtlString      m_sName;

	/*unsigned int    m_MaxLines; ToDO!!!
	unsigned int    m_BusyLines;
	boost::shared_ptr<CEnumerator> m_counter;*/

	// proxy list
	IList::SPtr     m_pipoutbound;


};


//template <class T>
//void BuildList()
//{
//	m_ActiveTimeSlots.erase(std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), [](const CAbstractTrunk::DTI& _dti)
//	{
//		if (!_dti.GetAddress())
//			return true;
//		return false;
//	}
//	), m_ActiveTimeSlots.end());
//
//	if (!m_ActiveTimeSlots.empty())
//	{
//		std::sort(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), CAbstractTrunk::CSortTimeSlotPredicat());
//
//		if (!D_REVERSE.CollateNoCase(m_direction))
//			std::reverse(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end());
//
//		bool bParity = false;
//
//		auto removeParity = [bParity](const CAbstractTrunk::DTI& _dti)
//		{
//			return (bParity) ? ((_dti.GetTimeslot() % 2) == 1) : ((_dti.GetTimeslot() % 2) != 1);
//		};
//
//		if (!P_EVEN.CollateNoCase(m_parity))
//		{
//			bParity = true;
//			m_ActiveTimeSlots.erase(
//				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity),
//				m_ActiveTimeSlots.end());
//		}
//
//		if (!P_ODD.CollateNoCase(m_parity))
//		{
//			bParity = false;
//			m_ActiveTimeSlots.erase(
//				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity),
//				m_ActiveTimeSlots.end());
//		}
//
//		//if (!P_EVEN.CollateNoCase(m_parity))
//		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(true));
//
//		//if (!P_ODD.CollateNoCase(m_parity))
//		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(false));
//	}
//
//	m_Counter = 0;
//	m_ActiveSize = m_ActiveTimeSlots.size();
//}

/******************************* TRUNKS *************************************/

class CLinearTrunk : public CAbstractTrunk
{
public:
	CLinearTrunk(const std::wstring& _name,
		const std::wstring& _timeslots,
		const std::wstring& _direction,
		const std::wstring& _parity,
		unsigned int& _max);
public:
	void BuildList() override;
	void StepList() override;
	void GetAddress(NameList &List) override;
	CAtlString GetMethod() override { return M_LINEAR; }
	virtual std::wstring Info()const override;
};


class CCircleTrunk : public CAbstractTrunk
{
public:
	CCircleTrunk(const std::wstring& _name,
		const std::wstring& _timeslots,
		const std::wstring& _direction,
		const std::wstring& _parity,
		unsigned int& _max);
public:
	void BuildList() override;
	void StepList() override;
	void GetAddress(NameList &List) override;
	CAtlString GetMethod() override { return M_CIRCLE; }
	virtual std::wstring Info()const override;
};


class CRandomTrunk : public CAbstractTrunk
{
public:
	CRandomTrunk(const std::wstring& _name,
		const std::wstring& _timeslots,
		const std::wstring& _direction,
		const std::wstring& _parity,
		unsigned int& _max);
public:
	void BuildList() override;
	void StepList()override;
	void GetAddress(NameList &List) override;
	CAtlString GetMethod() override { return M_RANDOM; }
	virtual std::wstring Info()const override;
};


class CLastusedTrunk : public CAbstractTrunk
{
public:
	CLastusedTrunk(const std::wstring& _name,
		const std::wstring& _timeslots,
		const std::wstring& _direction,
		const std::wstring& _parity,
		unsigned int& _max);
public:
	void BuildList() override;
	void StepList() override;
	void GetAddress(NameList &List) override;
	CAtlString GetMethod() override { return M_LASTUSED; }
	virtual std::wstring Info()const override;
};


class CParentTrunk : public CAbstractTrunk
{
public:
	CParentTrunk(const std::wstring& _filename);

	bool AddDTI(const std::wstring& _groupname,
		const std::wstring& _board,
		const std::wstring& _timeslot,
		const ULONGLONG& _address);

	void Add(ITrunkGroup _elem){ m_childrens.push_back(_elem); }
	void Clear(){ m_childrens.clear(); }

	std::wstring GetScriptName()const{ return m_sFileName; }

	void AddNewGroupWithDefaultParams(const std::wstring& _groupname, const std::wstring& sBoard, const std::wstring& sTimeSlot, const ULONGLONG& ullAddress);
	IList::SPtr GetOutBoundNameByGroupName(const std::wstring& _groupName)const;
	CAbstractTrunk*  GetGroupByGroupName(const std::wstring& _groupname);

public:
	void BuildList() override;
	void StepList() override;
	void GetAddress(NameList &List) override;
	CAtlString GetMethod() override { return L""; }
	std::wstring Info()const override;

protected:
	void      GetAddressByParametrs(const NameList& _dti_list,
		const std::wstring& _method,
		const std::wstring& _direction,
		const std::wstring& _parity,
		NameList &_address_list);


private:
	GroupList	m_childrens;
	std::wstring    m_sFileName;
};
/******************************* eof *************************************/