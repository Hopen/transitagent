/************************************************************************/
/* Name     : Trunk\Group.cpp                                           */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 12 Oct 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include <algorithm>
#include "Group.h"
#include "..\TransitAgent\Enumerator.h"
#include "../utils.h"

const std::wstring ALPH = L"0123456789";

enum ExtractCurrentState
{
	ECS_UNKNOWN = 0,
	ECS_DTI,
	ECS_TS,
	ECS_ERROR
};

bool static Convert(const std::wstring& sBoard, const std::wstring& sTimeSlot, int& iBoard, int& iTimeSlot)
{
	int pos = sBoard.find(L"DTI");
	if (pos)
	{
		pos = sBoard.find(L"IPT");
	}
	if (pos)
		return false;
	std::wstring sDTINum;
	bool bError = false;
	for (unsigned int i=3;i<sBoard.size() && !bError;++i)
	{
		if (ALPH.find(sBoard[i]) == -1)
			bError = true;
		else sDTINum+=sBoard[i];
	}
	if (bError)
		return false;

	iBoard    = _wtoi(sDTINum.c_str());
	iTimeSlot = _wtoi(sTimeSlot.c_str());
	return true;
}

//void CTrunkGroup::ExtractTimeSlots(const std::wstring& sTimeSlots, NameList& _list)
//{
//	std::vector<int> dti;
//	std::vector<int> ts;
//	std::wstring sTemp, first_range;
//	bool bEnum  = false;// "true" when we've got enumeration
//	bool bError = false;
//	int iCurrentState = ECS_UNKNOWN;
//	for (unsigned int i=0;i<sTimeSlots.size() && !bError;++i)
//	{
//		WCHAR CurrentChar = sTimeSlots[i];
//		switch (iCurrentState)
//		{
//		case ECS_UNKNOWN:
//			{
//				if (CurrentChar=='[')
//					iCurrentState = ECS_DTI;
//				if (CurrentChar=='(')
//					iCurrentState = ECS_TS;
//				continue;
//				break;
//			}
//		case ECS_DTI:
//		case ECS_TS:
//			{
//				std::vector<int> &rLiast      = (iCurrentState==ECS_DTI)?dti:ts;
//				//WCHAR            &rFinishChar = (iCurrentState==ECS_DTI)?']':')';
//				if(CurrentChar==']' && iCurrentState == ECS_DTI || CurrentChar==')' && iCurrentState == ECS_TS)
//				{
//					if (bEnum)
//					{
//						int range1 = _wtoi(first_range.c_str()),
//							range2 = _wtoi(sTemp.c_str());
//						for (int i=range1;i<=range2;++i)
//						{
//							rLiast.push_back(i);
//						}
//						bEnum = false;
//					}
//					else
//						rLiast.push_back(_wtoi(sTemp.c_str()));
//					sTemp.clear();
//					iCurrentState = ECS_UNKNOWN;
//					continue;
//				}
//				if(CurrentChar==',')
//				{
//					if (bEnum)
//					{
//						int range1 = _wtoi(first_range.c_str()),
//							range2 = _wtoi(sTemp.c_str());
//						for (int i=range1;i<=range2;++i)
//						{
//							rLiast.push_back(i);
//						}
//						bEnum = false;
//					}
//					else
//						rLiast.push_back(_wtoi(sTemp.c_str()));
//					sTemp.clear();
//					continue;
//				}
//				if(CurrentChar=='-' && bEnum || 
//				   CurrentChar==',' && bEnum   )
//				{
//				    iCurrentState = ECS_ERROR;
//					continue;
//				}
//				if (CurrentChar == '-')
//				{
//					first_range = sTemp;
//					sTemp.clear();
//					bEnum = true;
//					continue;
//				}
//				if (ALPH.find(CurrentChar) == -1)
//				{
//					iCurrentState = ECS_ERROR;
//					continue;
//				}
//				sTemp+=CurrentChar;
//
//				break;
//			}
//		//case ECS_TS:
//		//	{
//		//		if(CurrentChar==')')
//		//		{
//		//			if (bEnum)
//		//			{
//		//				int range1 = _wtoi(first_range.c_str()),
//		//					range2 = _wtoi(sTemp.c_str());
//		//				for (int i=range1;i<=range2;++i)
//		//				{
//		//					ts.push_back(i);
//		//				}
//
//		//			}
//		//			else
//		//				ts.push_back(_wtoi(sTemp.c_str()));
//		//			iCurrentState = ECS_UNKNOWN;
//		//			continue;
//		//		}
//		//		if(CurrentChar==',')
//		//		{
//		//			if (bEnum)
//		//			{
//		//				int range1 = _wtoi(first_range.c_str()),
//		//					range2 = _wtoi(sTemp.c_str());
//		//				for (int i=range1;i<=range2;++i)
//		//				{
//		//					ts.push_back(i);
//		//				}
//
//		//			}
//		//			else
//		//				ts.push_back(_wtoi(sTemp.c_str()));
//		//			continue;
//		//		}
//		//		//if(CurrentChar=='-' && bEnum || 
//		//		//   CurrentChar==',' && bEnum   )
//		//		//{
//		//		//    iCurrentState = ECS_ERROR;
//		//		//	continue;
//		//		//}
//		//		if (CurrentChar == '-')
//		//		{
//		//			first_range = sTemp;
//		//			sTemp.clear();
//		//			bEnum = true;
//		//			continue;
//		//		}
//		//		sTemp+=CurrentChar;
//		//		break;
//		//	}
//		case ECS_ERROR:
//			{
//				bError = true;
//			}
//		};
//	}
//
//	if (iCurrentState == ECS_ERROR)
//	{
//		// swearing here
//		return;
//	}
//
//	for (unsigned int i=0;i<dti.size();++i)
//	{
//		for (unsigned int j=0;j<ts.size();++j)
//		{
//			_list.push_back(DTI(dti[i],ts[j]));
//		}
//	}
//}
//
//CTrunkGroup::CTrunkGroup(const std::wstring& _name,
//						 const std::wstring& _timeslots,
//						 const std::wstring& _method, 
//						 const std::wstring& _direction, 
//						 const std::wstring& _parity,
//						 /*const std::wstring& _ipoutbound*/
//						 unsigned int& _max )
//						 : m_method   ((!_method   .empty())?_method   .c_str():M_CIRCLE ),
//						   m_direction((!_direction.empty())?_direction.c_str():D_FORWARD),
//						   m_parity   ((!_parity   .empty())?_parity   .c_str():P_BOTH   ),
//						   m_sName(_name.c_str()), /*m_ipoutbound(_ipoutbound.c_str()),*/
//						   m_Counter  (0),
//						   m_ActiveSize(0),
//						   m_externalHost(false),
//						   m_MaxLines(_max), m_BusyLines(0)
//
//{
//	ExtractTimeSlots(_timeslots,m_TimeSlots);
//	if (m_MaxLines)
//	{
//		int _max_incoming = DEFAULT_MAX_INCOMING,
//			_max_scripts = DEFAULT_MAX_SCRIPTS,
//			_max_outcoming = _max;
//		
//		time_t _exp_time = DEFAULT_EXPIRATION_TIME;
//
//		m_counter.reset(new CEnumerator());
//		m_counter->SetLicenceParam(_max_incoming, _max_scripts, _max_outcoming, _exp_time);
//	}
//};
//
//CTrunkGroup::CTrunkGroup(const std::wstring& _name,
//						 const std::wstring& _dti,
//						 const std::wstring& _timeslot,
//						 const std::wstring& _method, 
//						 const std::wstring& _direction, 
//						 const std::wstring& _parity,
//						 const ULONGLONG& ullAddress)
//						 : m_method   ((!_method   .empty())?_method   .c_str():M_CIRCLE ),
//						 m_direction((!_direction.empty())?_direction.c_str():D_FORWARD),
//						 m_parity   ((!_parity   .empty())?_parity   .c_str():P_BOTH   ),
//						 m_sName(_name.c_str()),/* m_ipoutbound(_ipoutbound.c_str()),*/
//						 m_Counter  (0),
//						 m_ActiveSize(0),
//						 m_externalHost(true),
//						 m_MaxLines(999999), m_BusyLines(0)
//{
//	//int iBoard    = 0,
//	//	iTimeSlot = 0;
//	//if (Convert(_dti,_timeslot,iBoard,iTimeSlot))
//	//	m_TimeSlots.push_back(DTI(iBoard,iTimeSlot,ullAddress));
//	AddDTI(_dti,_timeslot,ullAddress);
//}
//
//CTrunkGroup::CTrunkGroup(const std::wstring& _filename) :
//m_sFileName(_filename), 
//m_Counter(0), 
//m_ActiveSize(0),
//m_externalHost(false), 
//m_MaxLines(999999),
//m_BusyLines(0)
//{
//
//}
//
//void CTrunkGroup::AddDTI(const int&_board, const int&_timeslot, const ULONGLONG& _address)
//{
//	for (unsigned int i=0;i<m_TimeSlots.size();++i)
//	{
//		if (m_TimeSlots[i].board    == _board   &&
//			m_TimeSlots[i].timeslot == _timeslot  )
//		{
//			return;
//		}
//
//	}
//	if (m_externalHost)
//	{
//		m_TimeSlots.push_back(DTI(_board,_timeslot,_address));
//		BuildList();
//	}
//	
//}
//
//CTrunkGroup::~CTrunkGroup()
//{
//	m_TimeSlots.clear();
//	int test = 0;
//}
//
//void CTrunkGroup::AddDTI(const std::wstring& _board, const std::wstring& _timeslot, const ULONGLONG& _address)
//{
//	int iBoard    = 0, 
//		iTimeSlot = 0;
//
//	if (Convert(_board,_timeslot,iBoard,iTimeSlot))
//		AddDTI(iBoard,iTimeSlot,_address);
//}
//
//void CTrunkGroup::AddDTI(const std::wstring& _groupname, const std::wstring& sBoard, const std::wstring& sTimeSlot, const ULONGLONG& ullAddress)
//{
//	SetDTI(sBoard,sTimeSlot,ullAddress);
//	CAtlString sGroupName(_groupname.c_str());
//	bool bNewGroup = true;
//	for (unsigned int i=0;i<m_childrens.size();++i)
//	{
//		if (!sGroupName.CompareNoCase(m_childrens[i]->GetGroupName().c_str()))
//		{
//			bNewGroup = false;
//			m_childrens[i]->AddDTI(sBoard,sTimeSlot,ullAddress);
//		}
//	}
//	//CAtlString sGroupName(_groupname.c_str());
//	//bool bNewGroup = true;
//	//for (unsigned int i=0;i<m_childrens.size();++i)
//	//{
//	//	m_childrens[i]->SetDTI(sBoard,sTimeSlot,ullAddress);
//	//	m_childrens[i]->AddDTI2(_groupname,sBoard,sTimeSlot,ullAddress);
//	//	if (!sGroupName.CompareNoCase(m_childrens[i]->GetGroupName().c_str()))
//	//	{
//	//		bNewGroup = false;
//	//		m_childrens[i]->AddDTI(sBoard,sTimeSlot,ullAddress);
//	//	}
//	//}
//	if (bNewGroup)
//	{
//		//m_groups.push_back(
//		Add (
//			CTrunkGroup::ITrunkGroup(
//			new CTrunkGroup(_groupname,
//			sBoard,
//			sTimeSlot,
//			M_CIRCLE.GetString(),
//			D_FORWARD.GetString(),
//			P_BOTH.GetString(),
//			ullAddress
//			)
//			)
//			);
//
//	}
//
//}
//
//CTrunkGroup::NameList::iterator CTrunkGroup::NameList::Find(const int& _board, const int& _timeslot)
//{
//	for (NameList::iterator it = begin();it!=end();++it)
//	{
//		if (it->board == _board && it->timeslot == _timeslot)
//			return it;
//	}
//
//	return end();
//}
//
//
//CTrunkGroup::NameList::iterator CTrunkGroup::NameList::Find(const std::wstring& _board, const std::wstring& _timeslot)
//{
//	int iBoard    = 0, 
//		iTimeSlot = 0;
//
//	if (Convert(_board,_timeslot,iBoard,iTimeSlot))
//		return Find(iBoard,iTimeSlot);
//	return end();
//}
//
////bool CTrunkGroup::SetDTI(const int& _board,	const int& _timeslot,const ULONGLONG& _address)
////{
////	NameList::iterator it = Find(_board,_timeslot);
////	if (it != m_TimeSlots.end())
////	{
////		it->address = _address;
////		BuildList();
////		return true;
////	}
////	return false;
////}
////
////bool CTrunkGroup::SetDTI(const std::wstring& _board, const std::wstring& _timeslot, const ULONGLONG& _address)
////{
////	int iBoard    = 0, 
////		iTimeSlot = 0;
////
////	if (Convert(_board,_timeslot,iBoard,iTimeSlot))
////		return SetDTI(iBoard,iTimeSlot,_address);
////
////	return false;
////}
//
//void CTrunkGroup::SetDTI(const int& _board, const int& _timeslot, const ULONGLONG& _address)
//{
//	if (m_sName.GetLength())
//	{
//		NameList::iterator it = m_TimeSlots.Find(_board,_timeslot);
//		if (it != m_TimeSlots.end())
//		{
//			//if (it->address)
//			//	it->address = _address;
//			//else // in null address case we should include timeslot into active timeslots list
//			//{
//				it->address = _address;
//				BuildList();
//			//}
//			
//		}
//	}
//	for (unsigned int i=0;i<m_childrens.size();++i)
//	{
//		m_childrens[i]->SetDTI(_board,_timeslot,_address);
//	}
//}
//
//void CTrunkGroup::SetDTI(const std::wstring& _board, const std::wstring& _timeslot, const ULONGLONG& _address)
//{
//	int iBoard    = 0, 
//		iTimeSlot = 0;
//
//	if (Convert(_board,_timeslot,iBoard,iTimeSlot))
//		SetDTI(iBoard,iTimeSlot,_address);
//}
//
////void CTrunkGroup::DeleteDTI(const int&_board, const int&_timeslot)
////{
////	NameList::iterator it = m_TimeSlots.Find(_board,_timeslot);
////	if (it != m_TimeSlots.end())
////		m_TimeSlots.erase(it);
////}
//
//ULONGLONG CTrunkGroup::GetDTIAddress(const std::wstring& _board, const std::wstring& _timeslot)
//{
//	NameList::iterator it = m_TimeSlots.Find(_board,_timeslot);
//	if (it!=m_TimeSlots.end())
//		return it->address;
//	return 0;
//}
//
////ULONGLONG CTrunkGroup::GetDTIAddress()
////{
////	ULONGLONG freeDtiAddress = 0;
////	if (m_ActiveTimeSlots.size())
////	{
////		freeDtiAddress = m_ActiveTimeSlots[0].address;
////		StepList();
////	}
////	return freeDtiAddress;
////}
//
//void CTrunkGroup::GetDTIAddress(TDTIList &_list)
//{
//	for (unsigned int i=0;i<m_ActiveTimeSlots.size();++i)
//	{
//		CAtlString sTemplate((m_ActiveTimeSlots[i].timeslot>9)?L"DTI%i T#%i":L"DTI%i T#0%i");
//		CAtlString sName;
//		sName.Format(sTemplate,m_ActiveTimeSlots[i].board,m_ActiveTimeSlots[i].timeslot);
//
//		////_list.Names    .push_back(sName.GetString());
//		////_list.Addresses.push_back(m_ActiveTimeSlots[i].address);
//		//_list[sName.GetString()] = m_ActiveTimeSlots[i].address;
//		_list.push_back(TIList(sName.GetString(),m_ActiveTimeSlots[i].address));
//	}
//}
//
//template<class _Pr>
//CTrunkGroup::NameList CTrunkGroup::Remove_if(const NameList& _list, _Pr _Pred)
//{
//	NameList ExistTimeSlots(_list);
//	NameList NewTimeSlots; NewTimeSlots.clear();
//	NameList::iterator it1 = std::remove_if(ExistTimeSlots.begin(),ExistTimeSlots.end(),_Pred);
//	NameList::iterator it2 = ExistTimeSlots.begin();
//	while (it2!=it1)
//	{
//		NewTimeSlots.push_back(*it2);
//		++it2;
//	}
//	return NewTimeSlots;
//}
//
//void CTrunkGroup::BuildList()
//{
//	//NameList NewTimeSlots(m_TimeSlots);
//	//m_ActiveTimeSlots.clear();
//	//NameList::iterator it1 = std::remove_if(NewTimeSlots.begin(),NewTimeSlots.end(),CTrunkGroup::CRemoveUnctiveTimeSlotPredicat());
//	//NameList::iterator it2 = NewTimeSlots.begin();
//	//while (it2!=it1)
//	//{
//	//	m_ActiveTimeSlots.push_back(*it2);
//	//	++it2;
//	//}
//
//
//	m_ActiveTimeSlots = Remove_if(m_TimeSlots,CTrunkGroup::CRemoveUnctiveTimeSlotPredicat());
//	if (!m_ActiveTimeSlots.empty())
//	{
//		std::sort(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), CTrunkGroup::CSortTimeSlotPredicat());
//
//		if (!M_RANDOM.CompareNoCase(m_method))
//			std::random_shuffle ( m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end() );
//		if (!D_REVERSE.CollateNoCase(m_direction))
//			std::reverse(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end() );
//
//		if (!P_EVEN.CollateNoCase(m_parity))
//			//std::remove_if(m_ActiveTimeSlots.begin(),m_ActiveTimeSlots.end(),CTrunkGroup::CRemoveParityTimeSlotPredicat(true));
//			m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots,CTrunkGroup::CRemoveParityTimeSlotPredicat(true));
//
//		if (!P_ODD.CollateNoCase(m_parity))
//			//std::remove_if(m_ActiveTimeSlots.begin(),m_ActiveTimeSlots.end(),CTrunkGroup::CRemoveParityTimeSlotPredicat(false));
//			m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots,CTrunkGroup::CRemoveParityTimeSlotPredicat(false));
//	}	
//
//	m_Counter = 0;
//
//}
//
//void CTrunkGroup::StepList(/*bool bIncludeLeastUsed*/)
//{
//	if (/*!bIncludeLeastUsed && */!M_LASTUSED.CollateNoCase(m_method))
//		return;
//
//	if (!m_ActiveTimeSlots.size() || m_ActiveTimeSlots.size() == 1)
//		return;
//	NameList::iterator it = m_ActiveTimeSlots.begin();
//	if (!M_CIRCLE.CollateNoCase(m_method) || !M_RANDOM.CollateNoCase(m_method))
//	{
//		DTI _dti = *it;
//		m_ActiveTimeSlots.erase(it);
//		m_ActiveTimeSlots.insert(m_ActiveTimeSlots.end(),_dti);
//
//		++m_Counter;
//		if (m_Counter >= m_ActiveTimeSlots.size())
//		{
//			BuildList();
//		}
//	}
//}
//
//void CTrunkGroup::GetAddress(TDTIList &_list)
//{
//	GetDTIAddress(_list);
//	if (!_list.empty())
//		StepList();
//}
//
//CTrunkGroup* CTrunkGroup::GetGroupByGroupName(const std::wstring& _groupname)
//{
//	CTrunkGroup* pRet = NULL;
//	if (!m_sName.CompareNoCase(_groupname.c_str()))
//	{
//		return this;
//	}
//	for (unsigned int i = 0; i<m_childrens.size(); ++i)
//	{
//		pRet = m_childrens[i]->GetGroupByGroupName(_groupname);
//		if (pRet)
//			break;
//	}
//	return pRet;
//}
//void CTrunkGroup::GetAddressByParametrs(const NameList& _dti_list, const std::wstring& _method, const std::wstring& _direction, const std::wstring& _parity, TDTIList &_address_list)
//{
//	if (!(!_method   .empty() && m_method   .CompareNoCase(_method   .c_str()) ||
//		  !_direction.empty() && m_direction.CompareNoCase(_direction.c_str()) ||
//		  !_parity   .empty() && m_parity   .CompareNoCase(_parity   .c_str())   ) )
//	{
//		for (unsigned int j=0;j<m_ActiveTimeSlots.size();++j)
//		{
//			for (unsigned int i=0;i<_dti_list.size();++i)
//			{
//				if (_dti_list[i].board == m_ActiveTimeSlots[j].board && _dti_list[i].timeslot == m_ActiveTimeSlots[j].timeslot)
//				{
//					CAtlString sTemplate((_dti_list[i].timeslot>9)?L"DTI%i T#%i":L"DTI%i T#0%i");
//					CAtlString sName;
//					sName.Format(sTemplate,_dti_list[i].board,_dti_list[i].timeslot);
//					CTSInListFinder finder(sName.GetString());
//					if (std::find_if(_address_list.begin(),_address_list.end(),finder) == _address_list.end())
//						_address_list.push_back(TIList(sName.GetString(),m_ActiveTimeSlots[j].address));
//				}
//			}
//		}
//	}
//	for (unsigned int i=0;i<m_childrens.size();++i)
//	{
//		m_childrens[i]->GetAddressByParametrs(_dti_list,_method,_direction,_parity,_address_list);
//	}
//}
//
//bool CTrunkGroup::GetAddressByTimeSlot(const std::wstring& _dti_timeslot, /*const std::wstring& _timeslot,*/ const std::wstring& _method, const std::wstring& _direction, const std::wstring& _parity, TDTIList &_list, std::wstring& _log)
//{
//	NameList WaitTimeSlots;
//	ExtractTimeSlots(_dti_timeslot,WaitTimeSlots);
//	if (!WaitTimeSlots.size())
//		return false;
//
//	GetAddressByParametrs(WaitTimeSlots,_method,_direction,_parity,_list );
//	if (_list.size())
//	{
//		_log+= L" Trunkgroup has chosen by method: ";
//		_log+= _method;
//		_log+= L", direction: ";
//		_log+= _direction;
//		_log+= L", parity: ";
//		_log+= _parity;
//
//		return true;
//	}
//	// still cannot find. Try to find with default parameters
//	GetAddressByParametrs(WaitTimeSlots,M_CIRCLE.GetString(),D_FORWARD.GetString(),P_BOTH.GetString(),_list);
//	if (_list.size())
//	{
//		_log+= L" Trunkgroup has chosen by default params like method: ";
//		_log+= M_CIRCLE.GetString();
//		_log+= L", direction: ";
//		_log+= D_FORWARD.GetString();
//		_log+= L", parity: ";
//		_log+= P_BOTH.GetString();
//
//		return true;
//	}
//
//	_log+= L" Trankgroup hasn't found with params method: ";
//	_log+= _method;
//	_log+= L", direction: ";
//	_log+= _direction;
//	_log+= L", parity: ";
//	_log+= _parity;
//
//	return false;
//}
//
//bool CTrunkGroup::refresh(const std::wstring& _filename, listcallbackfunc callbackfunc, ITrunkGroup pParent)
//{
//	// this is for advanced reload config change
//	//if(!m_sFileName.compare(_filename))
//	//{
//		return callbackfunc(_filename, pParent);
//	//	return; // children has already changed
//	//}
//	//for (unsigned int i=0;i<m_childrens.size();++i)
//	//{
//	//	m_childrens[i]->refresh(_filename,callbackfunc,m_childrens[i]);
//	//}
//
//		//if (!m_ipoutbound.IsEmpty() && m_pipoutbound.get())
//		//{
//		//	m_pipoutbound->refresh(_filename, callbackfunc, pParent);
//		//}
//}
//
//IList::SPtr CTrunkGroup::GetOutBoundNameByGroupName(const std::wstring& _groupName)const
//{
//	if (!m_sName.CompareNoCase(_groupName.c_str()))
//	{
//		return m_pipoutbound;
//	}
//	for (unsigned int i = 0; i<m_childrens.size(); ++i)
//	{
//		IList::SPtr pipoutbound = m_childrens[i]->GetOutBoundNameByGroupName(_groupName);
//		if (pipoutbound.get())
//			return pipoutbound;
//	}
//	return IList::SPtr((IList*)(NULL));
//}


/****************************************************************************/

//template<class _Pr>
//static CAbstractTrunk::NameList Remove_if(const CAbstractTrunk::NameList& _list, _Pr _Pred)
//{
//	CAbstractTrunk::NameList ExistTimeSlots(_list);
//	CAbstractTrunk::NameList NewTimeSlots; NewTimeSlots.clear();
//	CAbstractTrunk::NameList::iterator it1 = std::remove_if(ExistTimeSlots.begin(), ExistTimeSlots.end(), _Pred);
//	CAbstractTrunk::NameList::iterator it2 = ExistTimeSlots.begin();
//	while (it2 != it1)
//	{
//		NewTimeSlots.push_back(*it2);
//		++it2;
//	}
//	return NewTimeSlots;
//}

static void ExtractTimeSlots(const std::wstring& sTimeSlots, CAbstractTrunk::NameList& _list)
{
	std::vector<int> dti;
	std::vector<int> ts;
	std::wstring sTemp, first_range;
	bool bEnum = false;// "true" when we've got enumeration
	bool bError = false;
	int iCurrentState = ECS_UNKNOWN;
	for (unsigned int i = 0; i<sTimeSlots.size() && !bError; ++i)
	{
		WCHAR CurrentChar = sTimeSlots[i];
		switch (iCurrentState)
		{
		case ECS_UNKNOWN:
		{
			if (CurrentChar == '[')
				iCurrentState = ECS_DTI;
			if (CurrentChar == '(')
				iCurrentState = ECS_TS;
			continue;
			break;
		}
		case ECS_DTI:
		case ECS_TS:
		{
			std::vector<int> &rLiast = (iCurrentState == ECS_DTI) ? dti : ts;
			//WCHAR            &rFinishChar = (iCurrentState==ECS_DTI)?']':')';
			if (CurrentChar == ']' && iCurrentState == ECS_DTI || CurrentChar == ')' && iCurrentState == ECS_TS)
			{
				if (bEnum)
				{
					int range1 = _wtoi(first_range.c_str()),
						range2 = _wtoi(sTemp.c_str());
					for (int i = range1; i <= range2; ++i)
					{
						rLiast.push_back(i);
					}
					bEnum = false;
				}
				else
					rLiast.push_back(_wtoi(sTemp.c_str()));
				sTemp.clear();
				iCurrentState = ECS_UNKNOWN;
				continue;
			}
			if (CurrentChar == ',')
			{
				if (bEnum)
				{
					int range1 = _wtoi(first_range.c_str()),
						range2 = _wtoi(sTemp.c_str());
					for (int i = range1; i <= range2; ++i)
					{
						rLiast.push_back(i);
					}
					bEnum = false;
				}
				else
					rLiast.push_back(_wtoi(sTemp.c_str()));
				sTemp.clear();
				continue;
			}
			if (CurrentChar == '-' && bEnum ||
				CurrentChar == ',' && bEnum)
			{
				iCurrentState = ECS_ERROR;
				continue;
			}
			if (CurrentChar == '-')
			{
				first_range = sTemp;
				sTemp.clear();
				bEnum = true;
				continue;
			}
			if (ALPH.find(CurrentChar) == -1)
			{
				iCurrentState = ECS_ERROR;
				continue;
			}
			sTemp += CurrentChar;

			break;
		}

		case ECS_ERROR:
		{
			bError = true;
		}
		};
	}

	if (iCurrentState == ECS_ERROR)
	{
		// swearing here
		return;
	}

	for (unsigned int i = 0; i<dti.size(); ++i)
	{
		for (unsigned int j = 0; j<ts.size(); ++j)
		{
			_list.push_back(CAbstractTrunk::DTI(dti[i], ts[j]));
		}
	}
}

/******************************* ABSTRACT *************************************/

CAbstractTrunk::CAbstractTrunk(const std::wstring& _name,
	const std::wstring& _timeslots,
	const std::wstring& _direction,
	const std::wstring& _parity,
	unsigned int& _max)
	: 
	m_direction((!_direction.empty()) ? _direction.c_str() : D_FORWARD),
	m_parity((!_parity.empty()) ? _parity.c_str() : P_BOTH),
	m_sName(_name.c_str()),
	m_Counter(0),
	m_ActiveSize(0)
	//m_externalHost(false),
	//m_MaxLines(_max), m_BusyLines(0)

{
	ExtractTimeSlots(_timeslots, m_TimeSlots);
	/*if (m_MaxLines) ToDo!!!!
	{
		int _max_incoming = DEFAULT_MAX_INCOMING,
			_max_scripts = DEFAULT_MAX_SCRIPTS,
			_max_outcoming = _max;

		time_t _exp_time = DEFAULT_EXPIRATION_TIME;

		m_counter.reset(new CEnumerator());
		m_counter->SetLicenceParam(_max_incoming, _max_scripts, _max_outcoming, _exp_time);
	}*/
}

bool CAbstractTrunk::AddDTI(const std::wstring& _groupname, const std::wstring& sBoard, const std::wstring& sTimeSlot, const ULONGLONG& ullAddress)
{
	bool bGroupExist = false;
	if ((!_groupname.empty()) && (!CAtlString(_groupname.c_str()).CompareNoCase(m_sName)))
	{
		bGroupExist = true;
	}
	int iBoard = 0,
		iTimeSlot = 0;

	if (Convert(sBoard, sTimeSlot, iBoard, iTimeSlot))
	{
		SetDTI(iBoard, iTimeSlot, ullAddress);
		//return true;
	}

	return bGroupExist;
}

//bool CAbstractTrunk::AddDTI(const std::wstring& _groupname, const int& iBoard, const int& iTimeSlot, const ULONGLONG& ullAddress)
//{
//	bool bGroupExist = false;
//	if ((!_groupname.empty()) && (!CAtlString(_groupname.c_str()).CompareNoCase(m_sName)))
//	{
//		int iBoard = 0,
//			iTimeSlot = 0;
//
//		if (Convert(sBoard, sTimeSlot, iBoard, iTimeSlot))
//			return AddDTI(_groupname, iBoard, iTimeSlot, ullAddress);
//
//		SetDTI(iBoard, iTimeSlot, ullAddress);
//
//		bGroupExist = true;
//	}
//
//	for (unsigned int i = 0; i < m_childrens.size(); ++i)
//	{
//		if (m_childrens[i]->AddDTI(_groupname, iBoard, iTimeSlot, ullAddress))
//			bGroupExist = true;
//	}
//
//	return bGroupExist;
//}


void CAbstractTrunk::SetDTI(const int& _board, const int& _timeslot, const ULONGLONG& _address)
{
	NameList::iterator it = m_TimeSlots.Find(_board, _timeslot);
	if (it != m_TimeSlots.end())
	{
		it->SetAddress(_address);
	}
}

//void CAbstractTrunk::RebuildAll()
//{
//	this->BuildList();
//	for each (ITrunkGroup group in m_childrens)
//	{
//		group->RebuildAll();
//	}
//}

CAbstractTrunk::ITrunkGroup CAbstractTrunk::CreateGroup(
	const std::wstring& _name,
	const std::wstring& _timeslots,
	const std::wstring& _method,
	const std::wstring& _direction,
	const std::wstring& _parity,
	unsigned int& _max
)
{
	if (!M_CIRCLE.CompareNoCase(_method.c_str()))
	{
		return CAbstractTrunk::ITrunkGroup(new CCircleTrunk(_name, _timeslots, _direction, _parity, _max));
	}
	else if (!M_LINEAR.CompareNoCase(_method.c_str()))
	{
		return CAbstractTrunk::ITrunkGroup(new CLinearTrunk(_name, _timeslots, _direction, _parity, _max));
	}
	else if (!M_RANDOM.CompareNoCase(_method.c_str()))
	{
		return CAbstractTrunk::ITrunkGroup(new CRandomTrunk(_name, _timeslots, _direction, _parity, _max));
	}
	else if (!M_LASTUSED.CompareNoCase(_method.c_str()))
	{
		return CAbstractTrunk::ITrunkGroup(new CLastusedTrunk(_name, _timeslots, _direction, _parity, _max));
	}

	//default 
	return CAbstractTrunk::ITrunkGroup(new CLinearTrunk(_name, _timeslots, _direction, _parity, _max));
}

IList::SPtr CAbstractTrunk::GetOutBoundNameByGroupName(const std::wstring& _groupName)const
{
	if (!m_sName.CompareNoCase(_groupName.c_str()))
	{
		return m_pipoutbound;
	}
	return IList::SPtr((IList*)(NULL));
}

bool CAbstractTrunk::refresh(const std::wstring& _filename, listcallbackfunc callbackfunc, ITrunkGroup pParent, CAbstractTrunk::truck_group_callback call_back)
{
	// this is for advanced reload config change
	//if(!m_sFileName.compare(_filename))
	//{
	return callbackfunc(_filename, pParent, call_back);
	//	return; // children has already changed
	//}
	//for (unsigned int i=0;i<m_childrens.size();++i)
	//{
	//	m_childrens[i]->refresh(_filename,callbackfunc,m_childrens[i]);
	//}

	//if (!m_ipoutbound.IsEmpty() && m_pipoutbound.get())
	//{
	//	m_pipoutbound->refresh(_filename, callbackfunc, pParent);
	//}
}

CAbstractTrunk* CAbstractTrunk::GetGroupByGroupName(const std::wstring& _groupname)
{
	CAbstractTrunk* pRet = NULL;
	if (!m_sName.CompareNoCase(_groupname.c_str()))
	{
		return this;
	}
	return pRet;
}

bool CAbstractTrunk::GetAddressByTimeSlot(const std::wstring& _dti_timeslot, /*const std::wstring& _timeslot,*/ const std::wstring& _method, const std::wstring& _direction, const std::wstring& _parity, NameList &_list, std::wstring& _log)
{
	NameList WaitTimeSlots;
	ExtractTimeSlots(_dti_timeslot,WaitTimeSlots);
	if (!WaitTimeSlots.size())
		return false;

	GetAddressByParametrs(WaitTimeSlots,_method,_direction,_parity,_list );
	if (_list.size())
	{
		_log+= L" Trunkgroup has chosen by method: ";
		_log+= _method;
		_log+= L", direction: ";
		_log+= _direction;
		_log+= L", parity: ";
		_log+= _parity;

		return true;
	}
	//// still cannot find. Try to find with default parameters
	//GetAddressByParametrs(WaitTimeSlots,M_CIRCLE.GetString(),D_FORWARD.GetString(),P_BOTH.GetString(),_list);
	//if (_list.size())
	//{
	//	_log+= L" Trunkgroup has chosen by default params like method: ";
	//	_log+= M_CIRCLE.GetString();
	//	_log+= L", direction: ";
	//	_log+= D_FORWARD.GetString();
	//	_log+= L", parity: ";
	//	_log+= P_BOTH.GetString();

	//	return true;
	//}

	_log+= L" Trankgroup hasn't found with params method: ";
	_log+= _method;
	_log+= L", direction: ";
	_log+= _direction;
	_log+= L", parity: ";
	_log+= _parity;

	return false;
}

void CAbstractTrunk::GetAddressByParametrs(const NameList& _dti_list, const std::wstring& _method, const std::wstring& _direction, const std::wstring& _parity, NameList &_address_list)
{
	if (!(!_method   .empty() && GetMethod().CompareNoCase(_method   .c_str()) ||
		  !_direction.empty() && m_direction.CompareNoCase(_direction.c_str()) ||
		  !_parity   .empty() && m_parity   .CompareNoCase(_parity   .c_str())   ) )
	{
		
		//for (unsigned int j=0;j<m_ActiveTimeSlots.size();++j)
		//{
		//	for (unsigned int i=0;i<_dti_list.size();++i)
		//	{
		//		if (_dti_list[i].GetBoard() == m_ActiveTimeSlots[j].GetBoard() && _dti_list[i].GetTimeslot() == m_ActiveTimeSlots[j].GetTimeslot())
		//		{
		//			_address_list.push_back(m_ActiveTimeSlots[j]);
		//		}
		//	}
		//}

		for (unsigned int i = 0; i < _dti_list.size(); ++i)
		{
			CAbstractTrunk::NameList::const_iterator cit = m_ActiveTimeSlots.Find(_dti_list[i].GetBoard(), _dti_list[i].GetTimeslot());
			if (cit != m_ActiveTimeSlots.end())
			{
				_address_list.push_back(*cit);
			}
		}


	}
}


CAbstractTrunk::NameList::iterator CAbstractTrunk::NameList::Find(const int& _board, const int& _timeslot)
{
	//for (NameList::iterator it = begin();it!=end();++it)
	//{
	//	if (it->GetBoard() == _board && it->GetTimeslot() == _timeslot)
	//		return it;
	//}
	auto it = std::find_if(begin(), end(), [_board, _timeslot](const DTI& dti)
	{
		if (dti.GetBoard() == _board && dti.GetTimeslot() == _timeslot)
			return true;

		return false;
	}
	);

	return it;
}


CAbstractTrunk::NameList::iterator CAbstractTrunk::NameList::Find(const std::wstring& _board, const std::wstring& _timeslot)
{
	int iBoard    = 0, 
		iTimeSlot = 0;

	if (Convert(_board,_timeslot,iBoard,iTimeSlot))
		return Find(iBoard,iTimeSlot);
	return end();
}


/******************************* TRUNKS *************************************/
CLinearTrunk::CLinearTrunk(const std::wstring& _name,
	const std::wstring& _timeslots,
	const std::wstring& _direction,
	const std::wstring& _parity,
	unsigned int& _max)
	: CAbstractTrunk(_name, _timeslots, _direction, _parity, _max)
{

}

void CLinearTrunk::BuildList()
{
	//m_ActiveTimeSlots = Remove_if(m_TimeSlots, CAbstractTrunk::CRemoveUnctiveTimeSlotPredicat());
	//std::copy(m_TimeSlots.begin(), m_TimeSlots.end(), std::back_inserter(m_ActiveTimeSlots));
	m_ActiveTimeSlots = m_TimeSlots;
	m_ActiveTimeSlots.erase(std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), [](const CAbstractTrunk::DTI& _dti)
	{
		if (!_dti.GetAddress())
			return true;
		return false;
	}
	), m_ActiveTimeSlots.end());

	if (!m_ActiveTimeSlots.empty())
	{
		std::sort(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), CAbstractTrunk::CSortTimeSlotPredicat());

		if (!D_REVERSE.CollateNoCase(m_direction))
			std::reverse(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end());

		bool bParity = false;

		auto removeParity = [bParity](const CAbstractTrunk::DTI& _dti)
		{
			return (bParity) ? ((_dti.GetTimeslot() % 2) == 1) : ((_dti.GetTimeslot() % 2) != 1);
		};

		if (!P_EVEN.CollateNoCase(m_parity))
		{
			bParity = true;
			m_ActiveTimeSlots.erase(
				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity),
				m_ActiveTimeSlots.end());
		}

		if (!P_ODD.CollateNoCase(m_parity))
		{
			bParity = false;
			m_ActiveTimeSlots.erase(
				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity),
				m_ActiveTimeSlots.end());
		}

		//if (!P_EVEN.CollateNoCase(m_parity))
		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(true));

		//if (!P_ODD.CollateNoCase(m_parity))
		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(false));
	}

	m_Counter = 0;
	m_ActiveSize = m_ActiveTimeSlots.size();
}


void CLinearTrunk::StepList()
{
	//++m_Counter;
	//if (m_Counter >= m_ActiveSize)
	//{
	//	BuildList();
	//}
	// nothing to do
}

void CLinearTrunk::GetAddress(NameList &_list)
{
	_list = m_ActiveTimeSlots;
	//GetDTIAddress(_list);
	if (!_list.empty())
		StepList();
}

std::wstring CLinearTrunk::Info()const
{
	return std::wstring(m_sName.GetString()) + L" (" + Utils::toStr(m_ActiveSize) + L")";
}

CCircleTrunk::CCircleTrunk(const std::wstring& _name,
	const std::wstring& _timeslots,
	const std::wstring& _direction,
	const std::wstring& _parity,
	unsigned int& _max)
	: CAbstractTrunk(_name, _timeslots, _direction, _parity, _max)
{

}
void CCircleTrunk::BuildList()
{
	//m_ActiveTimeSlots = Remove_if(m_TimeSlots, CAbstractTrunk::CRemoveUnctiveTimeSlotPredicat());
	//std::copy(m_TimeSlots.begin(), m_TimeSlots.end(), std::back_inserter(m_ActiveTimeSlots));
	m_ActiveTimeSlots = m_TimeSlots;
	m_ActiveTimeSlots.erase(std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), [](const CAbstractTrunk::DTI& _dti)
	{
		if (!_dti.GetAddress())
			return true;
		return false;
	}
	), m_ActiveTimeSlots.end());

	if (!m_ActiveTimeSlots.empty())
	{
		std::sort(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), CAbstractTrunk::CSortTimeSlotPredicat());

		if (!D_REVERSE.CollateNoCase(m_direction))
			std::reverse(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end());

		bool bParity = false;

		auto removeParity = [bParity](const CAbstractTrunk::DTI& _dti)
		{
			return (bParity) ? ((_dti.GetTimeslot() % 2) == 1) : ((_dti.GetTimeslot() % 2) != 1);
		};

		if (!P_EVEN.CollateNoCase(m_parity))
		{
			bParity = true;
			m_ActiveTimeSlots.erase(
				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity), 
				m_ActiveTimeSlots.end());
		}

		if (!P_ODD.CollateNoCase(m_parity))
		{
			bParity = false;
			m_ActiveTimeSlots.erase(
				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity),
				m_ActiveTimeSlots.end());
		}
			
		//if (!P_EVEN.CollateNoCase(m_parity))
		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(true));

		//if (!P_ODD.CollateNoCase(m_parity))
		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(false));
	
		m_ActiveSize = m_ActiveTimeSlots.size();

		// doubling array
		m_ActiveTimeSlots.reserve(m_ActiveSize * 2);
		std::copy_n(m_ActiveTimeSlots.begin(), m_ActiveSize, std::back_inserter(m_ActiveTimeSlots));

	}
	else
	{
		m_ActiveSize = 0;
	}
	m_Counter = 0;

}
void CCircleTrunk::StepList()
{
	++m_Counter;
	if (m_Counter >= m_ActiveSize)
	{
		//BuildList();
		m_Counter = 0;
	}
}

void CCircleTrunk::GetAddress(NameList &_list)
{
	for (int i = m_Counter; (i < m_Counter + m_ActiveSize) && (i<m_ActiveTimeSlots.size()); ++i)
		_list.push_back(m_ActiveTimeSlots[i]);

	if (!_list.empty())
		StepList();
}

std::wstring CCircleTrunk::Info()const
{
	return std::wstring(m_sName.GetString()) + L" (" + Utils::toStr(m_ActiveSize) + L")";
}


CRandomTrunk::CRandomTrunk(const std::wstring& _name,
	const std::wstring& _timeslots,
	const std::wstring& _direction,
	const std::wstring& _parity,
	unsigned int& _max)
	: CAbstractTrunk(_name, _timeslots, _direction, _parity, _max)
{

}
void CRandomTrunk::BuildList()
{
	//m_ActiveTimeSlots = Remove_if(m_TimeSlots, CAbstractTrunk::CRemoveUnctiveTimeSlotPredicat());
	//std::copy(m_TimeSlots.begin(), m_TimeSlots.end(), std::back_inserter(m_ActiveTimeSlots));
	m_ActiveTimeSlots = m_TimeSlots;
	m_ActiveTimeSlots.erase(std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), [](const CAbstractTrunk::DTI& _dti)
	{
		if (!_dti.GetAddress())
			return true;
		return false;
	}
	), m_ActiveTimeSlots.end());

	if (!m_ActiveTimeSlots.empty())
	{
		//std::sort(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), CAbstractTrunk::CSortTimeSlotPredicat());
		std::random_shuffle(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end());

		if (!D_REVERSE.CollateNoCase(m_direction))
			std::reverse(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end());

		bool bParity = false;

		auto removeParity = [bParity](const CAbstractTrunk::DTI& _dti)
		{
			return (bParity) ? ((_dti.GetTimeslot() % 2) == 1) : ((_dti.GetTimeslot() % 2) != 1);
		};

		if (!P_EVEN.CollateNoCase(m_parity))
		{
			bParity = true;
			m_ActiveTimeSlots.erase(
				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity),
				m_ActiveTimeSlots.end());
		}

		if (!P_ODD.CollateNoCase(m_parity))
		{
			bParity = false;
			m_ActiveTimeSlots.erase(
				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity),
				m_ActiveTimeSlots.end());
		}
		//if (!P_EVEN.CollateNoCase(m_parity))
		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(true));

		//if (!P_ODD.CollateNoCase(m_parity))
		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(false));

		m_ActiveSize = m_ActiveTimeSlots.size();
		m_ActiveTimeSlots.reserve(m_ActiveSize * 2);
		std::copy_n(m_ActiveTimeSlots.begin(), m_ActiveSize, std::back_inserter(m_ActiveTimeSlots));

	}
	else
	{
		m_ActiveSize = 0;
	}

	//m_ActiveSize = m_ActiveTimeSlots.size();
	//// doubling array

	////NameList tmpArray;
	////tmpArray.reserve(m_ActiveSize * 2);
	////std::copy_n(m_ActiveTimeSlots.begin(), m_ActiveSize, std::back_inserter(tmpArray));
	////std::copy_n(m_ActiveTimeSlots.begin(), m_ActiveSize, std::back_inserter(tmpArray));
	////m_ActiveTimeSlots.swap(tmpArray);

	//m_ActiveTimeSlots.reserve(m_ActiveSize * 2);
	//std::copy_n(m_ActiveTimeSlots.begin(), m_ActiveSize, std::back_inserter(m_ActiveTimeSlots));

	m_Counter = 0;
}
void CRandomTrunk::StepList()
{
	++m_Counter;
	if (m_Counter >= m_ActiveSize)
	{
		//BuildList();
		m_Counter = 0;
	}
}

void CRandomTrunk::GetAddress(NameList &_list)
{
	for (int i = m_Counter; (i < m_Counter + m_ActiveSize) && (i<m_ActiveTimeSlots.size()); ++i)
		_list.push_back(m_ActiveTimeSlots[i]);

	if (!_list.empty())
		StepList();
}

std::wstring CRandomTrunk::Info()const
{
	return std::wstring(m_sName.GetString()) + L" (" + Utils::toStr(m_ActiveSize) + L")";
}

CLastusedTrunk::CLastusedTrunk(const std::wstring& _name,
	const std::wstring& _timeslots,
	const std::wstring& _direction,
	const std::wstring& _parity,
	unsigned int& _max)
	: CAbstractTrunk(_name, _timeslots, _direction, _parity, _max)
{

}
void CLastusedTrunk::BuildList()
{
	//m_ActiveTimeSlots = Remove_if(m_TimeSlots, CAbstractTrunk::CRemoveUnctiveTimeSlotPredicat());
	//std::copy(m_TimeSlots.begin(), m_TimeSlots.end(), std::back_inserter(m_ActiveTimeSlots));
	m_ActiveTimeSlots = m_TimeSlots;
	m_ActiveTimeSlots.erase(std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), [](const CAbstractTrunk::DTI& _dti)
	{
		if (!_dti.GetAddress())
			return true;
		return false;
	}
	), m_ActiveTimeSlots.end());

	if (!m_ActiveTimeSlots.empty())
	{
		std::sort(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), CAbstractTrunk::CSortTimeSlotPredicat());

		if (!D_REVERSE.CollateNoCase(m_direction))
			std::reverse(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end());
		bool bParity = false;

		auto removeParity = [bParity](const CAbstractTrunk::DTI& _dti)
		{
			return (bParity) ? ((_dti.GetTimeslot() % 2) == 1) : ((_dti.GetTimeslot() % 2) != 1);
		};

		if (!P_EVEN.CollateNoCase(m_parity))
		{
			bParity = true;
			m_ActiveTimeSlots.erase(
				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity),
				m_ActiveTimeSlots.end());
		}

		if (!P_ODD.CollateNoCase(m_parity))
		{
			bParity = false;
			m_ActiveTimeSlots.erase(
				std::remove_if(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end(), removeParity),
				m_ActiveTimeSlots.end());
		}

		//if (!P_EVEN.CollateNoCase(m_parity))
		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(true));

		//if (!P_ODD.CollateNoCase(m_parity))
		//	m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots, CAbstractTrunk::CRemoveParityTimeSlotPredicat(false));
	}

	m_Counter = 0;
}
void CLastusedTrunk::StepList()
{
	//nothing to do
}

void CLastusedTrunk::GetAddress(NameList &_list)
{
	_list = m_ActiveTimeSlots;
	if (!_list.empty())
		StepList();
}

std::wstring CLastusedTrunk::Info()const
{
	return std::wstring(m_sName.GetString()) + L" (" + Utils::toStr(m_ActiveSize) + L")";
}


void CParentTrunk::BuildList()
{
	for each (ITrunkGroup group in m_childrens)
	{
		group->BuildList();
	}
}
void CParentTrunk::StepList()
{

}

CParentTrunk::CParentTrunk(const std::wstring& _filename)
{
	m_sFileName = _filename;
}

bool CParentTrunk::AddDTI(const std::wstring& _groupname, const std::wstring& sBoard, const std::wstring& sTimeSlot, const ULONGLONG& ullAddress)
{
	bool bGroupExist = false;
	for (unsigned int i = 0; i < m_childrens.size(); ++i)
	{
		if (m_childrens[i]->AddDTI(_groupname, sBoard, sTimeSlot, ullAddress))
			bGroupExist = true;
	}

	return bGroupExist;
}

void CParentTrunk::AddNewGroupWithDefaultParams(const std::wstring& _groupname, const std::wstring& sBoard, const std::wstring& sTimeSlot, const ULONGLONG& ullAddress)
{
	unsigned int max = 9999;
	m_childrens.emplace_back(CreateGroup(_groupname, L"DTI[0-99](0-99)", M_CIRCLE.GetString(), D_FORWARD.GetString(), P_BOTH.GetString(), max));
	m_childrens.back()->AddDTI(_groupname, sBoard, sTimeSlot, ullAddress);
}

IList::SPtr CParentTrunk::GetOutBoundNameByGroupName(const std::wstring& _groupName)const
{
	for (unsigned int i = 0; i<m_childrens.size(); ++i)
	{
		IList::SPtr pipoutbound = m_childrens[i]->GetOutBoundNameByGroupName(_groupName);
		if (pipoutbound.get())
			return pipoutbound;
	}
	return IList::SPtr((IList*)(NULL));
}

CAbstractTrunk* CParentTrunk::GetGroupByGroupName(const std::wstring& _groupname)
{
	CAbstractTrunk* pRet = NULL;
	for (unsigned int i = 0; i<m_childrens.size(); ++i)
	{
		pRet = m_childrens[i]->GetGroupByGroupName(_groupname);
		if (pRet)
			break;
	}
	return pRet;
}

void CParentTrunk::GetAddressByParametrs(const NameList& _dti_list, const std::wstring& _method, const std::wstring& _direction, const std::wstring& _parity, NameList &_address_list)
{
	for (unsigned int i = 0; i<m_childrens.size(); ++i)
	{
		m_childrens[i]->GetAddressByParametrs(_dti_list, _method, _direction, _parity, _address_list);
	}
}


void CParentTrunk::GetAddress(NameList &_list)
{

}

std::wstring CParentTrunk::Info()const
{
	std::wstring sInfo;
	for each (ITrunkGroup group in m_childrens)
	{
		if (!sInfo.empty()) // second group
		{
			sInfo += L", ";
		}
		sInfo += group->Info();
	}

	return sInfo;
}

/******************************* eof *************************************/