#pragma once

#include "osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include "cryptlib.h"

#include "hex.h"

#include "filters.h"

#include "aes.h"
using CryptoPP::AES;

#include "modes.h"
using CryptoPP::CFB_Mode;

#include <vector>
using std::vector;

#include "string"
using std::string;

class CAesEncoder
{
private:
	AutoSeededRandomPool prng;
	vector<byte> key;
	vector<byte> iv;
public:
	CAesEncoder();
	CAesEncoder(vector<byte> _key);
	void GenerateKey();
	vector<byte> GetKey() { return key; }
	bool SetKey(vector<byte> _key);
	void GenerateIV();
	vector<byte> GetIV() { return iv; }
	bool SetIV(vector<byte> _iv);
	string Encode(string _data);
};