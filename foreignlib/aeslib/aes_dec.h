#pragma once

#include "osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include "cryptlib.h"

#include "hex.h"

#include "filters.h"

#include "aes.h"
using CryptoPP::AES;

#include "modes.h"
using CryptoPP::CFB_Mode;

#include <vector>
using std::vector;

#include "string"
using std::string;

class CAesDecoder
{
private:
	vector<byte> key;
public:
	CAesDecoder();
	CAesDecoder(vector<byte> _key);
	vector<byte> GetKey() { return key; }
	bool SetKey(vector<byte> _key);
	string Decode(string _data, vector<byte> _iv);
};