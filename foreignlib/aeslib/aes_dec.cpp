#include "aes_dec.h"

CAesDecoder::CAesDecoder()
{
	key.resize(AES::DEFAULT_KEYLENGTH);
}

CAesDecoder::CAesDecoder(vector<byte> _key)
{
	if (_key.size() == AES::DEFAULT_KEYLENGTH)
	{
		key = _key;
	}
	else
	{
		key.resize(0);
	}
}

bool CAesDecoder::SetKey(vector<byte> _key)
{
	if (_key.size() == AES::DEFAULT_KEYLENGTH)
	{
		key = _key;
		return true;
	}
	else
	{
		return false;
	}
}

string CAesDecoder::Decode(string _data, vector<byte> _iv)
{
	string ss = _data;
	CFB_Mode<AES>::Decryption cfbDecryption(&key[0], key.size(), &_iv[0]);
	cfbDecryption.ProcessData((byte*)&ss[0], (byte*)&_data[0], _data.size());
	return ss;
}