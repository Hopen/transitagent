#include "aes_enc.h"

CAesEncoder::CAesEncoder()
{
	key.resize(AES::DEFAULT_KEYLENGTH);
	iv.resize(AES::BLOCKSIZE);
}

CAesEncoder::CAesEncoder(vector<byte> _key)
{
	if (_key.size() == AES::DEFAULT_KEYLENGTH)
	{
		key = _key;
	}
	else
	{
		key.resize(0);
	}
	iv.resize(AES::BLOCKSIZE);
}


void CAesEncoder::GenerateKey()
{
	key.resize(AES::DEFAULT_KEYLENGTH);
	prng.GenerateBlock(&key[0], key.size());
}

bool CAesEncoder::SetKey(vector<byte> _key)
{
	if (_key.size() == AES::DEFAULT_KEYLENGTH)
	{
		key = _key;
		return true;
	}
	else
	{
		return false;
	}
}

bool CAesEncoder::SetIV(vector<byte> _iv)
{
	if (_iv.size() == AES::BLOCKSIZE)
	{
		iv=_iv;
		return true;
	}
	else
	{
		return false;
	}
}

void CAesEncoder::GenerateIV()
{
	iv.resize(AES::BLOCKSIZE);
	prng.GenerateBlock(&iv[0], iv.size());
}

string CAesEncoder::Encode(string _data)
{
	string ss = _data;
	CFB_Mode<AES>::Encryption cfbEncryption(&key[0], key.size(), &iv[0]);
	cfbEncryption.ProcessData((byte*)&ss[0], (byte*)&_data[0], _data.size());
	return ss;
}