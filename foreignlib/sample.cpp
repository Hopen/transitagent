//For AES_Encoding
#include "aes_enc.h"
//For AES_Decoding
#include "aes_dec.h"

#include <iostream>
using std::cout;
using std::endl;

int main()
{
	string data = "SomeVeryImportantMessage";
	cout << "Initial string: " << data << endl;
	CAesEncoder AesEncoder;
	AesEncoder.GenerateKey();
	//Key should be generated only once

	AesEncoder.GenerateIV();
	//Initialization Vector should be generated during every encoding

	string encoded = AesEncoder.Encode(data);

	cout << "Encoded string: " << encoded << endl;

	CAesDecoder AesDecoder(AesEncoder.GetKey());
	//Decoder should use the same Key and Initialization Vector
	string decoded = AesDecoder.Decode(encoded, AesEncoder.GetIV());

	cout << "Decoded string: " << decoded << endl;
	system("pause");
	return 0;
}