#pragma once
#include <memory>

template <typename T>
class IState;

template <typename T>
using State = std::shared_ptr<IState<T>>;

template <typename T>
class IState
{
public:
	IState()
		: mNext(nullptr)
	{}

	virtual ~IState() = default;
	virtual bool IsMatch(const T* aStartBuffer, const T* aEndBuffer, bool aAll) const = 0;
	void SetNext(const State<T>& aNextState) const
	{
		mNext = aNextState;
	}

protected:
	mutable State<T> mNext;
};

template <typename T>
class StateAny final : public IState<T>
{
public:
	bool IsMatch(const T* aStartBuffer, const T* aEndBuffer, bool aAll) const override
	{
		if (aStartBuffer == aEndBuffer)
		{
			return false;
		}

		return mNext->IsMatch(aStartBuffer + 1, aEndBuffer, false);
	}
};

template <typename T>
class StateAll final : public IState<T>
{
public:
	bool IsMatch(const T* aStartBuffer, const T* aEndBuffer, bool aAll) const override
	{
		if (aStartBuffer != aEndBuffer)
		{
			if (this->IsMatch(aStartBuffer + 1, aEndBuffer, true))
			{
				return true;
			}
		}
		return mNext->IsMatch(aStartBuffer, aEndBuffer, true);
	}
};

template <typename T>
class StateSymbol final : public IState<T>
{
public:
	StateSymbol(T aValue)
		: IState<T>()
		, mValue(aValue)
	{}

	bool IsMatch(const T* aStartBuffer, const T* aEndBuffer, bool aAll) const override
	{
		if (aStartBuffer == aEndBuffer || *aStartBuffer != mValue)
		{
			return false;
		}
		return mNext->IsMatch(aStartBuffer + 1, aEndBuffer, false);
	}


private:
	T mValue;
};

template <typename T>
class StateStart final : public IState<T>
{
public:
	bool IsMatch(const T* aStartBuffer, const T* aEndBuffer, bool aAll) const override
	{
		if (aStartBuffer == aEndBuffer)
		{
			return false;
		}

		return mNext->IsMatch(aStartBuffer, aEndBuffer, false);
	}
};

template <typename T>
class StateEnd final : public IState<T>
{
public:
	bool IsMatch(const T* aStartBuffer, const T* aEndBuffer, bool aAll) const override
	{
		if (aStartBuffer != aEndBuffer && !aAll)
		{
			return false;
		}

		return true;
	}
};

struct Factory
{
private:
	template <template<typename> class TState, class T, typename... TArgs>
	static State<T> Create(const State<T>& aPrev, TArgs&&... aArgs)
	{
		auto newState = std::make_shared<TState<T>>(std::forward<TArgs>(aArgs)...);
		if (aPrev)
		{
			aPrev->SetNext(newState);
		}
		return newState;
	}

public:
	template <typename T, typename... TArgs>
	static State<T> CreateSymbol(TArgs&&... aArgs)
	{
		return Create<StateSymbol, T>(std::forward<TArgs>(aArgs)...);
	}

	template <typename T, typename... TArgs>
	static State<T> CreateStart(TArgs&&... aArgs)
	{
		return Create<StateStart, T>(nullptr, std::forward<TArgs>(aArgs)...);
	}

	template <typename T, typename... TArgs>
	static State<T> CreateEnd(TArgs&&... aArgs)
	{
		return Create<StateEnd, T>(std::forward<TArgs>(aArgs)...);
	}

	template <typename T, typename... TArgs>
	static State<T> CreateAll(TArgs&&... aArgs)
	{
		return Create<StateAll, T>(std::forward<TArgs>(aArgs)...);
	}

	template <typename T, typename... TArgs>
	static State<T> CreateAny(TArgs&&... aArgs)
	{
		return Create<StateAny, T>(std::forward<TArgs>(aArgs)...);
	}

	template <typename T>
	static constexpr T GetAll()
	{
		return T;
	}

	template <typename T>
	static constexpr T GetAny()
	{
		return T;
	}

	template <>
	static constexpr char GetAll()
	{
		return '%';
	}

	template <>
	static constexpr char GetAny()
	{
		return '?';
	}

	template <>
	static constexpr wchar_t GetAll()
	{
		return L'%';
	}

	template <>
	static constexpr wchar_t GetAny()
	{
		return L'?';
	}

	template <typename T>
	static State<T> CreateAutomat(const T* aMask)
	{
		auto start = CreateStart<T>();

		auto last = start;

		const T* p = aMask;
		while (*p)
		{
			if (*p == GetAny<T>())
			{
				last = CreateAny<T>(last);
			}
			else if (*p == GetAll<T>())
			{
				last = CreateAll<T>(last);
			}
			else
			{
				last = CreateSymbol<T>(last, *p);
			}
			++p;
		}

		CreateEnd<T>(last);

		return start;
	}
};
