/************************************************************************/
/* Name     : TransitAgent\DebugListCollector.h                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 28 Oct 2010                                               */
/************************************************************************/
#pragma once
#include "Patterns/singleton.h"
#include "..\Composite\DebugList.h"

class CDebugListCollector
{
public:
	CDebugListCollector();

	IList::SPtr GetInstance(){return m_pDebugList;}

	void AddList(const std::wstring& _requestid,
		         const std::wstring& _runrequestid,
		         const std::wstring& _origination,
		         const std::wstring& _destination,
		         const CMessage& _message,
		         CLIENT_ADDRESS * _from//,
				 //std::wstring& sErrDescript // [out]
		);

	void RemoveListByID(const std::wstring& _requestid);
	void RemoveListByAddress(const DWORD& _address);
	void Remove(CDebugList* pItem);
	void load(const std::wstring&, IList::SPtr, IList::BlackListGroups& _blackListGroups);
	void refresh(const std::wstring&, IList::SPtr, IList::BlackListGroups& blackListGrpups);

private:
	DummyLoader dummyLoader;
	IList::SPtr m_pDebugList;

};

/******************************* eof *************************************/