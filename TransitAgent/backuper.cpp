/************************************************************************/
/* Name     : TransitAgent\backuper.cpp                                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Oct 2010                                               */
/************************************************************************/

#include "stdafx.h"

#include <3dparty/rapidxml/rapidxml.hpp>
#include <3dparty/rapidxml/rapidxml_utils.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include "backuper.h"
//#include "ce_xml.hpp"
#include "utils.h"
#include "Configuration/SystemConfiguration.h"
//#include "VXMLHTTPReader.h"

CBackUper::CBackUper()
{
	try
	{
		SystemConfig settings;
		try
		{
			//m_sCacheFolder = settings[_T("CacheFolder")].ToString();
			m_sCacheFolder = settings->get_config_value<std::wstring>(_T("CacheFolder"));
			boost::filesystem::create_directory(m_sCacheFolder);
		}
		catch( boost::exception & /*e*/ ) 
		{
			m_log->LogString(LEVEL_FINE, L"Error create directory: %s", m_sCacheFolder.c_str());
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading CacheFolder config key");
		}
	}	
	catch(core::configuration_error/*CConfigurationLoadException*/& _exception)
	{
		m_log->LogString(LEVEL_FINE,L"backuper starting failed: %s", stow(_exception.what()).c_str());
	}
	catch(...)
	{
		m_log->LogString(LEVEL_FINE, L"Unknown exception, when backuper try to start");
	}
}

CBackUper::~CBackUper()
{


}
//std::wstring CBackUper::GetNameFromURI(const std::wstring& _uri, bool bFileName)const
//{
//	int pos_left  = _uri.rfind(L'/');
//	int pos_rigth = _uri.rfind(L'\\');
//
//	int pos = (pos_left>pos_rigth)?pos_left:pos_rigth;
//
//	std::wstring sName(_uri);
//	if (pos >= 0) 
//	{
//		if (bFileName)
//			sName = _uri.substr(pos+1,_uri.length() - 1); // without "\"
//		else
//			sName = _uri.substr(0,pos+1); // with "\"
//	}
//
//	return sName;
//}
bool create_file( const std::wstring& ph, const std::wstring & contents )
{
	std::wofstream f(ph.c_str());
	if ( !f )
		return false;
	if ( !contents.empty() ) 
		f << contents;

	return true;
}

void CBackUper::GetHostAndParam(const std::wstring& _uri, std::wstring& _host, std::wstring& _params)
{
	std::wstring sHost(_uri), sParam;
	int pos = -1;
	pos =_uri.find(L"http://");
	if (pos == 0)
		sHost = _uri.substr(wcslen(L"http://"), _uri.length() - 1);


	pos = sHost.find(L'/');
	if (pos > 0)
	{
		sParam = sHost.substr(pos+1, sHost.length() -1);
		sHost  = sHost.substr(0 , pos);
	}
	_host   = sHost;
	_params = sParam;
}

bool CBackUper::HTTPGet(const std::wstring& uri, std::wstring& sFileText)
{
	//std::wstring test_uri = L"172.16.72.57";
	//CHTTPReader rd( test_uri.c_str(), FALSE);   
	////char buf[1024];
	//wchar_t frmdata[MAX_PATH];
	//
	//rd.Get(_uri.c_str(),frmdata);
	//DWORD size = 0;
	//wchar_t *data = rd.GetData(&size);
	try
	{
		CSyncClient client(L"get");
		std::wstring sHost, sParam;
		GetHostAndParam(uri,sHost,sParam);
		int stat = client.Send(sHost,sParam,sFileText);
		if (stat)
		{
			std::wstring reason;
			if (stat == 1)
			{
				reason = L"Couldn't receive document, internal error: Invalid response";
			}
			else
			{
				reason = L"server return value: ";
				std::wostringstream out;
				out << stat;
				reason += out.str();
			}

			m_log->LogString(LEVEL_FINE, L"Error receiving file from uri: %s, reason: %s", uri.c_str(), reason.c_str());
			return false;
		}
	}
	catch (std::runtime_error &e)
	{
		m_log->LogString(LEVEL_FINE, L"Error loading file %s : %s", uri.c_str(), e.what());
	}

	return true;
}

bool CBackUper::FileValid(const std::wstring& _uri)
{
	try
	{
		rapidxml::xml_document<> doc;
		rapidxml::file<> f(wtos(_uri).c_str());
		doc.parse<0>(f.data());

		//std::wifstream in(_uri.c_str());
		//boost::property_tree::wiptree propertyTree;
		//boost::property_tree::read_xml(in, propertyTree);
		//in.close();
		return true;
	}
	catch (std::exception& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Error loading file %s : %s", _uri.c_str(), e.what());
	}

	return false;
}

void CBackUper::Save(const std::wstring& _uri, bool bCheck)
{
	if (bCheck && !FileValid(_uri))
		return;

	bool bHttpFile = false;
	//if (_uri.find(L"http://") == 0)
	//	bHttpFile = true;

	WCHAR sNewURI[1024];
	PathCombineW(sNewURI, sNewURI, m_sCacheFolder.c_str());
	PathCombineW(sNewURI, sNewURI, Utils::GetFileNameFromURI(_uri).c_str());

	m_log->LogString(LEVEL_INFO, L"Caching config file: %s", sNewURI);
	try
	{
		if (!bHttpFile)
		{
			boost::filesystem::copy_file/*<boost::filesystem::wpath>*/(_uri, sNewURI, boost::filesystem::copy_option::overwrite_if_exists);
		}
		else
		{
			create_file(sNewURI,m_sFileText);
		}
	}
	catch( boost::exception & /*e*/ ) 
	{
		m_log->LogString(LEVEL_FINE, L"Error copy file");
	}
	
}

std::wstring CBackUper::LoadBackup(const std::wstring& _uri)
{
	m_log->LogString(LEVEL_INFO, L"Error loading file %s, trying to find it in cache folder", _uri.c_str());
	WCHAR sNewURI[1024];
	PathCombineW(sNewURI, sNewURI, m_sCacheFolder.c_str());
	PathCombineW(sNewURI, sNewURI, Utils::GetFileNameFromURI(_uri).c_str());
	if (FileValid(sNewURI))
		return std::wstring(sNewURI);

	return L"";
}

//std::wstring CBackUper::GetLocalRootFolder()const
//{
//	std::wstring sLocalRootFolder;
//	if (m_filenames.size())
//	{
//		StringSet::const_iterator cit = m_filenames.begin();//m_filenames.end(); --cit; // the last 
//		//sFile = m_filenames[m_filenames.size() - 1];
//		sLocalRootFolder = GetNameFromURI(*cit,false);
//	}
//	else
//		sLocalRootFolder = GetNameFromURI(CProcessHelper::GetCurrentModuleName().GetString(),false);
//
//	return sLocalRootFolder;
//
//}
static std::wstring _replace(const std::wstring& _string)
{
	std::wstring sNewString(_string);
	int pos  = -1;
	while ((pos = sNewString.find(L'/',++pos))>=0)
	{
		sNewString.replace(pos,1,L"\\");
	}
	return sNewString;
}

bool CBackUper::AddFolder(const std::wstring& _filename, const std::wstring& _root, std::wstring& uri)
{
	std::wstring sFolder, sFile, sURI, sFileName;
	bool bGet  = false,
		 bAbs  = false,
		 bHttp = false;
	WIN32_FIND_DATA fileData;
	ZeroMemory(&fileData,sizeof(fileData));
	if (!Utils::GetFile(_filename,&fileData)) // absolute URI
	{
		//m_filenames.push_back(_filename);		
		sURI = sFile = _filename;
		bAbs  = true;
		bGet  = true;
		sFileName = Utils::GetFileNameFromURI(sURI);
	}

	if (_filename.find(L"http") == 0)
	{
		bHttp = true;
		sURI = _filename;
	}
	//if (!bGet)
	//{
	//	sFileName = GetNameFromURI(_filename);
	//	if (m_filenames.size())
	//	{
	//		StringSet::const_iterator cit = m_filenames.begin();//m_filenames.end(); --cit; // the last 
	//		//sFile = m_filenames[m_filenames.size() - 1];
	//		sFile = *cit;
	//	}
	//	else
	//		sFile = CProcessHelper::GetCurrentModuleName();
	//}

	//sFolder = GetNameFromURI(sFile,false);
	sFolder = _root;

	if (bAbs || bHttp || !sFolder.empty() && !Utils::GetFile(sURI = sFolder + _filename,&fileData))
	{
		for (StringSet::const_iterator cit = m_filenames.begin();cit!= m_filenames.end();++cit)
		{
			std::wstring elem       = *cit;
			CAtlString elemfilename = Utils::GetFileNameFromURI(elem).c_str(); 
			CAtlString file         = sURI.c_str();
			//int pos = elem.rfind(L'/');
			//if (pos == -1)
			//{
			//	pos = elem.rfind(L'\\');
			//}
			//if (pos >= 0) 
			//{
			//	elemfilename = elem.substr(pos+1,elem.length() - 1).c_str(); // without "\"
			//}

			if (!elemfilename.CompareNoCase(sFileName.c_str()) && file.CompareNoCase(elem.c_str()))
			{
				m_log->LogString(LEVEL_INFO,L"Found file %s with exactly the same name but in another folder, collide with %s. File skiped", _filename.c_str(), elem.c_str());
				return false;
			}
		}
		if (!bHttp)
		{
		   sURI = _replace(sURI);
		   m_folders.insert(Utils::GetFolderNameFromURI(sURI));
		}
		m_filenames.insert(sURI);

		uri = sURI;
		return true;
	}
	return false;
}

bool CBackUper::Find(const std::wstring& _uri)
{
	return (m_filenames.find(_uri)!=m_filenames.end());
}

bool CBackUper::MatchFiles(const std::wstring& uri)
{
	std::wstring sFileName = Utils::GetFileNameFromURI(uri);
	WCHAR sCacheURI[1024];
	PathCombineW(sCacheURI, sCacheURI, m_sCacheFolder.c_str());
	PathCombineW(sCacheURI, sCacheURI, sFileName.c_str());

	std::wstring sCacheFileContent;
	std::wifstream cacheFile(sCacheURI);
	wchar_t buf[1024];
	while (cacheFile.read(buf, sizeof(buf)).gcount() > 0)
		sCacheFileContent.append(buf, cacheFile.gcount());

	cacheFile.close();

	//cacheFile >> sCacheFileContent;

	std::wstring SHttpFileContent;
	if (!HTTPGet(uri,SHttpFileContent))
		return false;

	if (!sCacheFileContent.compare(SHttpFileContent))
		return false;

	m_sFileTextName.clear();
	return true;
}

//void CBackUper::MatchHTTPFiles()
//{
//
//}
/******************************* eof *************************************/