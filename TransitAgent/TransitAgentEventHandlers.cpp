/************************************************************************/
/* Name     : TransitAgent\TransitAgentEventHandlers.cpp                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 17 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "TransitAgent.h"
#include "IPOBoundSingleton.h"
#include "sv_strutils.h"
//#include "../Router/types.h"
#include "boost/format.hpp"

//For AES_Encoding
#include "..\foreignlib\aeslib\aes_dec.h"
#include "utils.h"

static UINT s_uMessageCounter = 0;

enum TAErrors
{
	ERR_SCRIPT_DIDN_FOUND = 1,
	ERR_UNKNOWN_ACTION,
	ERR_TRUNCKGROUP_DIDN_FOUND,
	ERR_TIMESLOT_DIDN_FOUND,
	ERR_NOT_ENOUGH_OUTCOMING_LINES
};


static std::wstring GetErrorNameByCode(const int & _errorCode)
{
	std::wstring sError = L"Unknown error";
	switch (_errorCode)
	{
	case ERR_SCRIPT_DIDN_FOUND:
	{
		sError = L"ErrScriptDidntFound";
		break;
	}
	case ERR_UNKNOWN_ACTION:
	{
		sError = L"ErrUnknownAction";
		break;
	}
	case ERR_TRUNCKGROUP_DIDN_FOUND:
	{
		sError = L"ErrTrunckGroupDidntFound";
		break;
	}
	case ERR_TIMESLOT_DIDN_FOUND:
	{
		sError = L"ErrTimeSlotsDidntFound";
		break;
	}
	case ERR_NOT_ENOUGH_OUTCOMING_LINES:
	{
		sError = L"ErrNotEnoughOutcomingLines";
		break;
	}
	}

	return sError;
}


//template<typename Function, typename retValue, typename... Args>
//int64_t do_profiling(Function&& _func, retValue& retVal, const Args&...args)
//{
//	using namespace std::chrono;
//	auto start = high_resolution_clock::now();
//	retVal = _func(args...);
//	return duration_cast<milliseconds>(high_resolution_clock::now() - start).count();
//}


template<typename Function>
int64_t do_profiling(Function&& _func)
{
	using namespace std::chrono;
	auto start = high_resolution_clock::now();
	_func();
	return duration_cast<milliseconds>(high_resolution_clock::now() - start).count();
}


void CTransitAgent::OfferedEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	/************************************************************************/
	/* 
	Name: OFFERED; A = "9067837625"; B = "068125"; 
	Board = "DTI1"; CallID = 0x0006475C-19381AAB; 
	FileName = "C:\IS3\Scripts\ccxml_test\test10.ccxml"; 
	Host = "MS-IVRTEST"; InitialMessage = "OFFERED"; 
	MonitorDisplayedName = "CCXML_TEST"; RunRequestID = 0x00000000-5CFD6B76; 
	SigInfo = "0x0601100702200109010A020100040684906018520F0A070313097638675208018003047D0291811D038090A33F0A83973041150992995200"; TimeSlot = 15; SourceAddress = 0x050100B2-0006475C; ScriptID = 0x00053197-00139829 
	*/
	/************************************************************************/
	m_log->LogStringModule(LEVEL_FINEST, L"OFFERED", L"OfferedEventHandler start");

	CMessage response(_message);
	response.SetName(L"TA2D_RUNSCRIPT");
	std::wstring sScriptName;

	// log params
	std::wstring sLogString;

	__int64 iCallID = 0;

	if (_message == L"OFFERED")
	{
		if (_message.HasParam(L"A"))
		{
			sLogString += L"Origination: ";
			sLogString += _message.SafeReadParam(L"A", CMessage::CheckedType::String, L"").AsWideStr();
		}

		if (_message.HasParam(L"B"))
		{
			sLogString += L" Destination: ";
			sLogString += _message.SafeReadParam(L"B", CMessage::CheckedType::String, L"").AsWideStr();
		}

		iCallID = _message.SafeReadParam(L"CallID", CMessage::CheckedType::Int64, 0).AsInt64();
	}
	else if (_message == L"SIP2TA_SMPP_DELIVER_SM")
	{
		if (_message.HasParam(L"Orig_Address"))
		{
			sLogString+= L"Orig_Address: ";
			sLogString+= _message.SafeReadParam(L"Orig_Address", CMessage::CheckedType::String, L"").AsWideStr();
		}
		if (_message.HasParam(L"Dst_Address"))
		{
			sLogString+= L" Dst_Address: ";
			sLogString+= _message.SafeReadParam(L"Dst_Address", CMessage::CheckedType::String, L"").AsWideStr();
		}

		iCallID = _pFrom.GetClientID();
	}

	else if (_message == L"MCC2TA_NEWEMAIL")
	{
		if (_message.HasParam(L"From"))
		{
			sLogString += L"From: ";
			sLogString += _message.SafeReadParam(L"From", CMessage::CheckedType::String, L"").AsWideStr();
		}
		if (_message.HasParam(L"To"))
		{
			sLogString += L" To: ";
			sLogString += _message.SafeReadParam(L"To", CMessage::CheckedType::String, L"").AsWideStr();
		}
		iCallID = _message.SafeReadParam(L"MCCID", CMessage::CheckedType::Int64, 0).AsInt64();
	}
	else if (_message == L"MCC2TA_NEWCHAT")
	{
		if (_message.HasParam(L"CustomerCTN"))
		{
			sLogString += L"CustomerCTN: ";
			sLogString += _message.SafeReadParam(L"CustomerCTN", CMessage::CheckedType::String, L"").AsWideStr();
		}
		if (_message.HasParam(L"ChannelID"))
		{
			sLogString += L" ChannelID: ";
			sLogString += _message.SafeReadParam(L"ChannelID", CMessage::CheckedType::String, L"").AsWideStr();
		}

		iCallID = _message.SafeReadParam(L"MCCID", CMessage::CheckedType::Int64, 0).AsInt64();
	}

	
	std::wstring sCallID = format_wstring(L"%08X-%08X", HIDWORD(iCallID), LODWORD(iCallID));

	IList *script = NULL;
	m_log->LogStringModule(LEVEL_FINEST, L"OFFERED", L"Get debug script");
	if (1)
	{
		// if debug mode is turned on
		//boost::shared_lock<boost::shared_mutex> mylock(m_listMutex);
		script = m_pDebugList->GetScript(_message);
		if (script)
		{
			if (CDebugList* pDebugList = dynamic_cast<CDebugList*>(script))
			{
				response += pDebugList->GetDebugMessage();
				m_router->SendToAny(response);
				sLogString += L", debug message has sent";
				m_log->LogStringModule(LEVEL_INFO, _message.GetName().c_str(), sLogString.c_str());
				_debugListCollector->Remove(pDebugList);
				return;
			}

		}

		m_log->LogStringModule(LEVEL_FINEST, L"OFFERED", L"Get script");
		script = GetScript(_message);
		m_log->LogStringModule(LEVEL_FINEST, L"OFFERED", L"Get script done");
	}

	if (!script)
	{
		const auto alarmMsg = AlarmMsg(sCallID, L"OfferedEventHandler", GetErrorNameByCode(ERR_SCRIPT_DIDN_FOUND), sLogString, L"", ERR_SCRIPT_DIDN_FOUND);
		m_log->LogStringModule(LEVEL_INFO, alarmMsg.GetName().c_str(), sLogString.c_str());
		m_router->SendToAll(alarmMsg);
		return;
	}

	if (response.HasParam(L"A"))
		response[L"A"] = script->GetPrefix() + response.SafeReadParam(L"A", CMessage::CheckedType::String, L"").AsWideStr();
	if (response.HasParam(L"Orig_Address"))
		response[L"Orig_Address"] = script->GetPrefix() + response.SafeReadParam(L"Orig_Address", CMessage::CheckedType::String, L"").AsWideStr();

	sLogString+=L" -> ";
	sLogString+=script->log();

	auto scriptAction = script->GetAction();
	if (const auto blackList = script->GetBlackList())
	{
		if (auto blackListScript = blackList->GetScript(_message))
		{
			scriptAction = A_IGNORE;
			sLogString += L" -> ";
			sLogString += blackListScript->log();
		}
	}
		
	switch(scriptAction)
	{
	case A_UNKNOWN:
		{
			// here alarms to
			const auto alarmMsg = AlarmMsg(sCallID, L"OfferedEventHandler", GetErrorNameByCode(ERR_UNKNOWN_ACTION), sLogString, L"", ERR_UNKNOWN_ACTION);
			m_log->LogStringModule(LEVEL_INFO, alarmMsg.GetName().c_str(), sLogString.c_str());
			m_router->SendToAll(alarmMsg);
			break;
		}
	case A_RUNSCRIPT:
		{
			m_log->LogStringModule(LEVEL_FINEST, L"OFFERED", L"run script");
			m_log->LogStringModule(LEVEL_INFO, _message.GetName().c_str(), sLogString.c_str());

			response[L"FileName"] = script->GetScriptName();
			response[L"MonitorDisplayedName"] = script->GetMonitorName();
			response[L"RunRequestID"] = ((__int64)m_router->BoundAddress() << 32) | s_uMessageCounter++;
			auto params = script->GetOtherParams();
			if (!params.empty())
			{
				for (const auto paramPair : params)
				{
					response[paramPair.first] = paramPair.second;
				}
			}

			// send TA2D_RUNSCRIPT
			m_log->LogStringModule(LEVEL_FINEST, L"OFFERED", L"SendToAny");
			m_router->SendToAny(response);
			m_log->LogStringModule(LEVEL_FINEST, L"OFFERED", L"SendToAny done");

			break;
		}
	case A_REGION:
	{
			 response.SetName(L"OFFERED");
			 response[REGION_PARAM] = script->GetScriptName();
			 response[L"RunRequestID"] = ((__int64)m_router->BoundAddress() << 32) | s_uMessageCounter++;
			 m_log->LogStringModule(LEVEL_INFO, _message.GetName().c_str(), sLogString.c_str());
			 m_router->SendToRegion(script->GetScriptName(), response);
			 break;
	}
	case A_IGNORE:
		{
			CMessage dropmsg(L"DROP_CALL");
			dropmsg[L"CallID"]             = iCallID;//_message[L"CallID"].Value;
			dropmsg[L"DestinationAddress"] = iCallID;//_message[L"CallID"].Value;
			m_log->LogStringModule(LEVEL_INFO, _message.GetName().c_str(), sLogString.c_str());
			m_router->SendFromTo(dropmsg, _pTo.ChildID, _pFrom.ClientID, _pFrom.ChildID);
			break;
		}

	};

	
}

void CTransitAgent::TimeSlotStatusEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	/************************************************************************/
	/*                                                                      
	Name: TS2ANY_TIMESLOTSTATUS; StatusCode = 0x00000000-00000000; 
	StatusText = "IDLE"; StatusDescription = ""; 
	Host = "MTT-IVR001"; InService = true; Board = "DTI2"; TimeSlot = 4; 
	Blocked = false; Busy = false
	*/
	/************************************************************************/
	
	std::wstring sHost = _message[L"Host"].AsWideStr();
	std::wstring sBoard = _message[L"Board"].AsWideStr();
	std::wstring sTimeSlot = _message[L"TimeSlot"].AsWideStr();
	ULONGLONG    ullAddress = _pFrom.GetAsQWORD();

	CAtlString sLogFormat(
		TEXT("Incoming message: TS2ANY_TIMESLOTSTATUS,")
		TEXT(" host: %s, board: %s, timeslot: %s, address: %I64i")
		);
	CAtlString sLog;
	sLog.Format(sLogFormat, sHost.c_str(), sBoard.c_str(), sTimeSlot.c_str(), ullAddress);
	m_log->LogStringModule(LEVEL_INFO, L"TS2ANY_TIMESLOTSTATUS", sLog.GetString());

	CLIENT_ADDRESS ts_address;
	ts_address.SetAsQWORD(_pFrom.GetClientID());
	ts_address.ChildID = 0;

	if (_monitorStack.isNewServer(ts_address))
	{
		m_log->LogStringModule(LEVEL_INFO, L"TS2ANY_TIMESLOTSTATUS", L"Add new server: %d", _pFrom.GetClientID());
		_monitorStack.AddServer(/**_pFrom*/ts_address);
	}

	_trunkGroupCollector->AddTimeSlot(sHost, sBoard, sTimeSlot, ullAddress);
	BuildTrunkGroup();
}

void CTransitAgent::MakeCallEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	CAbstractTrunk::NameList TimeslotList; //output list

	std::wstring wsErrDescr;
	CMessage response(_message);

	__int64 iScriptID = _message.SafeReadParam(L"ScriptID", CMessage::CheckedType::Int64, 0).AsInt64();
	std::wstring sScriptID = format_wstring(L"%08X-%08X", HIDWORD(iScriptID), LODWORD(iScriptID));
	__int64 iCallbackID = _message.SafeReadParam(L"CallbackID", CMessage::CheckedType::Int64, 0).AsInt64();


	std::wstring sGroupName = _message.SafeReadParam(L"Group", CMessage::CheckedType::String, L"").AsWideStr();

	std::wstring sLogString;
	int iError = 0;

	std::wstring sA = _message.SafeReadParam(L"A", CMessage::CheckedType::String, L"").AsWideStr();
	std::wstring sB = _message.SafeReadParam(L"B", CMessage::CheckedType::String, L"").AsWideStr();

	sLogString = format_wstring(L"Incoming message: MAKE_CALL, origination: %s , destination: %s  -> ", sA.c_str(), sB.c_str());

	if (!m_counter.CanMakeOutcomingCall())
	{
		iError = ERR_NOT_ENOUGH_OUTCOMING_LINES;
	}

	if (!iError)
	{
		if (!sGroupName.empty())
		{

			/************************************************************************/
			/*  BY GROUP
			Name: MAKE_CALL; OriginationNumberType = "National";
			DestinationNumberType = "National"; OriginationPresentationRestrictions = "Restricted";
			Direction = "Forward"; A = "8003330515"; B = "9164657896";
			TimeOut = 60; Group = "default_trunkgroup"; Parity = "Both";
			DestinationAddress = 0x00000002-00000000; CallbackID =0x00000000-00000006;
			bSendMessageToTelServer = 1; ScriptID = 0x0001BBE9-00034426
			*/
			/************************************************************************/
			if (1) // redirect to region
			{
				std::wstring regionTG, nameTG;
				int pos = sGroupName.find(L".");
				if (pos >= 0)
				{
					regionTG = sGroupName.substr(0, pos);
					nameTG = sGroupName.substr(pos + 1, sGroupName.length() - 1); // without "."
				}

				if (!regionTG.empty() && !nameTG.empty())
				{
					/************************************************************************/
					/*  TO REGION
					Name: MAKE_CALL; ...
					... Group = "MOSCOW.DEFAULT"; ...
					...
					*/
					/************************************************************************/

					sLogString += L" has redirected to region: %s";
					response[REGION_PARAM] = regionTG.c_str();
					response[L"Group"] = nameTG.c_str();
					response[L"TransitAgentAddress"] = _pTo.ChildID;
					response[L"clientTA"] = _pTo.ChildID;
					m_log->LogStringModule(LEVEL_INFO, L"MAKE_CALL", sLogString.c_str(), regionTG.c_str());
					m_router->SendToRegion(regionTG, response);
					return; // EXIT
				}
			}

			// Get DTI/timeslot by group name
			//   thus, ignoring all other parameters
			response.Remove(L"Direction");
			response.Remove(L"Parity");
			response.Remove(L"Method");
			response.Remove(L"Timeslots");

			if (1)
			{
				CAbstractTrunk * group = NULL;
				boost::shared_lock<boost::shared_mutex> mylock(m_groupMutex);
				group = m_pTrunkGroups->GetGroupByGroupName(sGroupName);

				if (group)
				{
					group->GetAddress(TimeslotList);
				}
			}

			if (1)
			{
				bool bSip = sA.find(L"sip:") != -1;

				IList::SPtr outbound;
				if (!sGroupName.empty() && bSip)
				{
					// looking for proxy ip
					outbound = m_pTrunkGroups->GetOutBoundNameByGroupName(sGroupName);
				}
				if (outbound)
				{
					IList::SPtr proxy = outbound->get_front();

					CAtlString atlstr(sLogString.c_str());
					int pos = atlstr.Find(sA/*pA->AsWideStr()*/.c_str());
					if (pos != -1)
					{
						sLogString = atlstr.Left(pos + sA/*pA->AsWideStr()*/.length()) + L";" + proxy->log().c_str() + atlstr.Right(atlstr.GetLength() - pos - sA/*pA->AsWideStr()*/.length());
					}
					response[L"A"] = sA/*pA->AsWideStr()*/ + L";" + proxy->log();
					outbound->step();
				}
			}

			if (!TimeslotList.size())
			{
				wsErrDescr = format_wstring(L" trunkgroup \"%s\" doesn't exist ", sGroupName.c_str());
				iError = ERR_TRUNCKGROUP_DIDN_FOUND;
			}
			else
			{
				sLogString += L" TrunkGroup has chosen by groupname: ";
				sLogString += sGroupName;

			}
		}
		else
		{
			/************************************************************************/
			/* BY TIMESLOT
			Name: MAKE_CALL; A = "8003330515"; B = "9164657896";
			CallbackID = 0x00000000-00000006;
			DestinationAddress= 0x00000002-00000000;
			DestinationNumberType = "National"; OriginationNumberType = "National";
			OriginationPresentationRestrictions = "Restricted"; ScriptID = 0x0001BBE9-00034426;
			TimeOut = 60; Timeslots = "DTI[1,2,3](0-29)"; bSendMessageToTelServer = 1
			*/
			/************************************************************************/
			std::wstring sDirection = _message.SafeReadParam(L"Direction", CMessage::CheckedType::String, L"").AsWideStr(),
				sParity = _message.SafeReadParam(L"Parity", CMessage::CheckedType::String, L"").AsWideStr(),
				sMethod = _message.SafeReadParam(L"Method", CMessage::CheckedType::String, L"").AsWideStr(),
				sTimeslots = _message.SafeReadParam(L"Timeslots", CMessage::CheckedType::String, L"").AsWideStr();

			if (1)
			{
				boost::shared_lock<boost::shared_mutex> mylock(m_groupMutex);
				m_pTrunkGroups->GetAddressByTimeSlot(sTimeslots, sMethod, sDirection, sParity, TimeslotList, sLogString);
			}


			if (TimeslotList.empty())
			{
				wsErrDescr = format_wstring(L" timeslots \"%s\" doesn't exist ", sTimeslots.c_str());
				iError = ERR_TIMESLOT_DIDN_FOUND;
			}
		}
	}

	if (!iError) // OK - timeslots has found
	{
		unsigned int uTSSize = TimeslotList.size();
		std::wstring sTimeSlotsNames;
		unsigned int uLast = (uTSSize < 5) ? uTSSize : 5;
		unsigned int i = 0;
		for each (CAbstractTrunk::DTI ts in TimeslotList)
		{
			++i;
			sTimeSlotsNames += ts.GetName();
			if (i < uLast/* - 1*/)
				sTimeSlotsNames += L", ";
			else if (i == uLast)
				break;
		}

		sLogString += L": ";
		sLogString += sTimeSlotsNames;

		CAbstractTrunk::NameList::const_iterator cit = TimeslotList.begin();
		CLIENT_ADDRESS ts_address;
		ts_address.SetAsQWORD(cit->GetAddress());
		ts_address.ChildID = 0;

		CAbstractTrunk::AddressList RawData;
		while (cit != TimeslotList.end())
		{
			RawData.push_back((ULONGLONG(LODWORD(cit->GetAddress())) << 32) | HIDWORD(cit->GetAddress()));
			++cit;
		}
		//response[L"Timeslots"].SetRawData(&RawData.front(), RawData.size() * sizeof(RawData.front()));
		response[L"Timeslots"] = RawData;

		//ULONGLONG * ptr = (ULONGLONG *)response[L"Timeslots"].GetRawData();
		core::raw_data_type ptr = response.SafeReadParam(L"Timeslots", CMessage::CheckedType::RawData, core::raw_data_type()).GetRawData();

		sLogString += L", Router address: 0x%p";
		m_log->LogStringModule(LEVEL_INFO, L"MAKE_CALL", sLogString.c_str(), ts_address.ClientID);
		response[L"TransitAgentAddress"] = _pTo.ChildID;
		//m_router.SendFrom(response, _pFrom, ts_address.ClientID, ts_address.ChildID);
		m_router->SendFromTo(response, const_cast<CLIENT_ADDRESS&>(_pFrom), ts_address);
	}
	else // error
	{
		sLogString += L", reason: ";
		sLogString += wsErrDescr;
		m_log->LogString(LEVEL_INFO, sLogString.c_str());
		//m_router./*SendToAny*/SendToAll(AlarmMsg(iError, L"MAKE_CALL", sLogString));
		m_router->SendToAll(AlarmMsg(sScriptID, L"MakeCallEventHandler", GetErrorNameByCode(iError), sLogString, L"", iError));
		//m_router.SendTo(_pFrom, EMakeCallRejected(-1, (iError == ERR_NOT_ENOUGH_OUTCOMING_LINES ? L"NOT_ENOUGH_OUTCOMING_LINES" : L"INVALID_TIMESLOTS"), (PCWSTR)wsErrDescr.c_str()).Msg(), NULL);
		m_router->SendTo(EMakeCallRejected(iCallbackID, -1, (iError == ERR_NOT_ENOUGH_OUTCOMING_LINES ? L"NOT_ENOUGH_OUTCOMING_LINES" : L"INVALID_TIMESLOTS"), (PCWSTR)wsErrDescr.c_str()).Msg(), _pFrom);

		if (_message.HasParam(L"clientTA"))
		{
			CLIENT_ADDRESS ts_address;
			ts_address.SetAsQWORD(_message.SafeReadParam(L"clientTA",CMessage::CheckedType::Int64,0).AsInt64());
			m_router->SendTo(EMakeCallRejected(iCallbackID, -1, (iError == ERR_NOT_ENOUGH_OUTCOMING_LINES ? L"NOT_ENOUGH_OUTCOMING_LINES" : L"INVALID_TIMESLOTS"), (PCWSTR)wsErrDescr.c_str()).Msg(), ts_address);
		}

		if (m_uMaxTrunkGroupFail && iError == ERR_TRUNCKGROUP_DIDN_FOUND)
		{
			++m_uTrunkFailCounter;
			if (m_uTrunkFailCounter >= m_uMaxTrunkGroupFail)
			{
				m_log->LogString(LEVEL_INFO, L"WARNING!!! TrunkGroupFailed counter reached maximum value (%i)! Program will be terminated", m_uMaxTrunkGroupFail);
				::terminate();
			}
		}
	}

}

//void CTransitAgent::SIP2TAEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
//{
//
//}

void CTransitAgent::SG2TADebugRunEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	std::wstring sRequestID    = _message[L"RequestID"    ].AsWideStr();
	std::wstring sRunRequestID = _message[L"RunRequestID" ].AsWideStr();
	std::wstring sA            = _message[L"ANumMask"     ].AsWideStr();
	std::wstring sB            = _message[L"BNumMask"     ].AsWideStr();

	CMessage response(L"TA2SG_DEBUG_SCRIPT_ACK");
	response[L"RequestID"] = sRequestID;
	response[L"Succeeded"] = 1;

	CAtlString sTemplate = TEXT ("Incoming message: SG2TA_DEBUG_SCRIPT ", )
		                   TEXT ("RequestID: %s, ")
		                   TEXT ("ANumMask: %s, " )
						   TEXT ("BNumMask: %s " );
	CAtlString sLogString;
	sLogString.Format(sTemplate,sRequestID.c_str(),sA.c_str(),sB.c_str());
	try
	{
		m_log->LogString(LEVEL_INFO, sLogString.GetString());
		_debugListCollector->AddList(sRequestID,sRunRequestID,sA,sB,_message,&const_cast<CLIENT_ADDRESS&>(_pFrom));
	}
	catch (...)
	{
		response[L"Succeeded"  ] = 0;
		response[L"Description"] = L"Error compile masks";
	}

	m_router->SendFromTo(response, _pTo.ChildID, _pFrom.ClientID, _pFrom.ChildID);
}

void CTransitAgent::SG2TADebugCancelEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	std::wstring sRequestID    = _message[L"RequestID"    ].AsWideStr();
	CAtlString sTemplate = TEXT ("Incoming message: SG2TA_DEBUG_SCRIPT_CANCEL ", )
		TEXT ("RequestID: %s");
	CAtlString sLogString;
	sLogString.Format(sTemplate,sRequestID.c_str());

	m_log->LogString(LEVEL_INFO, sLogString.GetString());
	_debugListCollector->RemoveListByID(sRequestID);
}

void CTransitAgent::CallStatusEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{

	CAtlString sStatusText = _message.SafeReadParam(L"StatusText", CMessage::CheckedType::String, L"").AsWideStr().c_str();
	//if (CParam* param = _message.ParamByName(L"StatusText"))
	//	sStatusText = param->AsWideStr().c_str();
	
	if (!sStatusText.CompareNoCase(L"INCOMING") || !sStatusText.CompareNoCase(L"CALLING"))
	{
		//std::wstring sTimeSlot, sBoard;
		//if (CParam* param = _message.ParamByName(L"TimeSlot"))
		//	sTimeSlot = param->AsWideStr();
		//if (CParam* param = _message.ParamByName(L"Board"))
		//	sBoard    = param->AsWideStr();

		std::wstring sTimeSlot = _message.SafeReadParam(L"TimeSlot", CMessage::CheckedType::String, L"").AsWideStr();
		std::wstring sBoard    = _message.SafeReadParam(L"Board"   , CMessage::CheckedType::String, L"").AsWideStr();

		// log
		std::wstring logTimeSlot = (sTimeSlot.length()==1)?L"0"+sTimeSlot:sTimeSlot;
		CAtlString sTemplate = L"Call status has changed, %s";
		CAtlString sLogString;
		sLogString.Format(sTemplate,/*sBoard.c_str(),logTimeSlot.c_str(),*/sStatusText.GetString());
		CAtlString strTimeslot;
		strTimeslot.Format(L"%s T#%s", sBoard.c_str(),logTimeSlot.c_str());
		m_log->LogStringModule(LEVEL_FINE, strTimeslot.GetString(), sLogString.GetString());

		//boost::mutex::scoped_lock lock(m_stack->GetLocker());
//		boostex::unique_lock_ex lock(CMonitorStack::GetInstance()->GetLocker(), L"CallStatusEventHandler");
		if (1)
		{
			boost::unique_lock<boost::shared_mutex> mylock(m_groupMutex);
			//m_pTrunkGroups->LUStepList(sBoard, sTimeSlot);
		}
		
	}
}

void CTransitAgent::SG2TARunScriptEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	/************************************************************************/
	/* 
	Name: SG2TA_RUN_SCRIPT; DbgMsgStep = 1; DbgMsgCOM = 1; DbgMsgStepDisasm = 0; 
	DbgMsgExcDisasm = 1; FileName = Unknown Type 0; DebugFlag = 1; OfflineFlag = 1; 
	RunRequestID = 0x03D7B540-001A8D30; ScrGenAddr = 0x03D7B548-00000000; 
	ScrGenHost = "\MS-IVRTEST"; ScrGenUser = "MS-IVRTEST\Administrator"; MonitorDisplayedName = "ToString.sc3"
	*/
	/************************************************************************/
	//singleton_auto_pointer <CEnumerator> counter;
	if (!m_counter.CanRunScript())
	{
		m_log->LogStringModule(LEVEL_INFO, _message.GetName(), L"Limit running scripts has reached. MAX running scripts count: %i", m_counter.MaxScriptsCount());
		return;
	}

	ObjectId llFrom = static_cast<__int64>(__int64(_pFrom.GetClientID()) << 32 | _pFrom.GetChildID() );
	ObjectId llTo   = 0;
	CAtlString sLogString;
	if (m_redirect.find(llFrom,llTo))
	{
		CAtlString sTemplate = L" Has redirected from %I64i to %I64i";
		sLogString.Format(sTemplate,llFrom, llTo);
		_message.SetName(L"TA2SG_RunScriptRedirected");
		CLIENT_ADDRESS to_address;
		to_address.SetAsQWORD(llTo);
		m_log->LogStringModule(LEVEL_INFO, _message.GetName(), sLogString.GetString());
		m_router->SendFromTo(_message, _pFrom, to_address);
	}
	else
	{
		sLogString = L" script has run";
		m_log->LogStringModule(LEVEL_INFO, _message.GetName()/*L"SG2TA_RUN_SCRIPT"*/, sLogString.GetString());
		_message.Name = L"TA2D_RUNSCRIPT";
		//m_router.SendToAny(_message);
		//m_router.SendFromToAny(_message, _pFrom);
		CLIENT_ADDRESS to_address = CLIENT_ADDRESS::Any();
		m_router->SendFromTo(_message, const_cast<CLIENT_ADDRESS&>(_pFrom), to_address);
	}
}

void CTransitAgent::SG2TARedirectEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	//ObjectId llFrom = 0;
	//if (CParam* pScriptID = _message.ParamByName(L"ScriptID"))
	//	llFrom = pScriptID->AsInt64();
	ObjectId llFrom = _message.SafeReadParam(L"ScriptID", CMessage::CheckedType::Int64, 0).AsInt64();
	if (!llFrom)
		return;
	ObjectId llTo = _pFrom.GetAsQWORD();
	// logging
	CAtlString sLogString;
	CAtlString sTemplate = L" SG2TA_RUN_SCRIPT messages from %I64i will be redirected to %I64i ";
	sLogString.Format(sTemplate,llFrom, llTo);
	m_log->LogStringModule(LEVEL_INFO, L"SG2TA_RunScriptRedirect", sLogString.GetString());

	m_redirect.add(llFrom,llTo);
}

//static void OnTimeSlotsTimeoutCompleted()
//{
//	CTransitAgent::GetInstance()->BuildTrunkGroup();
//}

void CTransitAgent::RequestTSCompletedHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	m_log->LogString(LEVEL_INFO, L"Incoming message: %s, clientId: %d", _message.GetName(), _pFrom.GetClientID());

	CLIENT_ADDRESS ts_address;
	ts_address.SetAsQWORD(_pFrom.GetClientID());
	ts_address.ChildID = 0;

	bool success = _monitorStack.FreeFile(/**_pFrom*/ts_address);
	m_log->LogString(LEVEL_INFO, L"Monitor stack free file: %s", success ? L"true" : L"false");
}

void CTransitAgent::MakeCallConfirmedEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	//ObjectId llFrom = 0;
	//std::wstring callID;
	//if (CParam* pCallID = _message.ParamByName(L"CallID"))
	//{
	//	llFrom = pCallID->AsInt64();
	//	callID = pCallID->AsWideStr();
	//}

	ObjectId llFrom = _message.SafeReadParam(L"CallID", CMessage::CheckedType::Int64, 0).AsInt64();
	std::wstring callID = Utils::toStr(llFrom);


	//singleton_auto_pointer <CEnumerator> counter;
	m_counter.AddOutcoming(llFrom);

	//CAtlString sLogString;
	//CAtlString sTemplate = TEXT("Incoming message: %s, ")
	//	TEXT("CallID: %s ");
	//		TEXT("BNumMask: %s ");
	//sLogString.Format(sTemplate, _message.GetName(), callID.c_str());
	//m_log->LogString(LEVEL_INFO, sLogString.GetString());
	m_log->LogString(LEVEL_INFO, L"Incoming message: %s, CallID: %s", _message.GetName(), callID.c_str());
	


}

void CTransitAgent::CallEndEventHandler/*MakeCallEndedEventHandler*/(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	/*********************
	msg: 'Name: CALL_ENDED; CallID = 0x000067CB-00000003'
	*********************/

	//ObjectId llFrom = 0;
	//std::wstring callID;
	//if (CParam* pCallID = _message.ParamByName(L"CallID"))
	//{
	//	llFrom = pCallID->AsInt64();
	//	callID = pCallID->AsWideStr();
	//}
	ObjectId llFrom = _message.SafeReadParam(L"CallID", CMessage::CheckedType::Int64, 0).AsInt64();
	std::wstring callID = Utils::toStr(llFrom);


	//singleton_auto_pointer <CEnumerator> counter;
	if (m_counter.RemoveIncoming(llFrom))
	{
		m_log->LogString(LEVEL_INFO, L"Incoming message: %s, CallID: %s, total incoming calls: %i", _message.GetName(), callID.c_str(), m_counter.GetIncoming().size());
	}
	else if (m_counter.RemoveOutcoming(llFrom))
	{
		m_log->LogString(LEVEL_INFO, L"Incoming message: %s, CallID: %s, total outcoming calls: %i", _message.GetName(), callID.c_str(), m_counter.GetOutcoming().size());
	}
	else
	{
		m_log->LogString(LEVEL_INFO, L"Incoming message: %s, ERROR: cannot find call by CallID: %s", _message.GetName(), callID.c_str());
	}
	
}

//void CTransitAgent::CallEndEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
//{
//	/*********************
//	msg: 'Name: CALL_ENDED; CallID = 0x000067CB-00000003'
//	*********************/
//	ObjectId llFrom = 0;
//	if (CParam* pCallID = _message.ParamByName(L"CallID"))
//		llFrom = pCallID->AsInt64();
//
//	CAtlString sTemplate = TEXT("Incoming message: %s, ")
//		TEXT("CallID: %I64i, ");
//	CAtlString sLogString;
//	sLogString.Format(sTemplate, _message.GetName(), llFrom);
//	singleton_auto_pointer <CEnumerator> counter;
//	if (!counter->RemoveIncoming(llFrom))
//		sTemplate += L"ERROR: cannot find incoming call";
//
//	//CallIDList::const_iterator cit = std::find(m_incoming.begin(), m_incoming.end(), llFrom);
//	//if (cit != m_incoming.end())
//	//{
//	//	//success
//	//	m_incoming.erase(cit);
//	//}
//	//else
//	//{
//	//	sTemplate += L"ERROR: cannot find incoming call";
//	//}
//
//	m_log->LogString(LEVEL_INFO, sLogString.GetString());
//}

void CTransitAgent::D2ANYScriptStatusNoticeHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	/**************************************
	msg: 'Name: D2ANY_ScriptStatusNotice; ScriptID = 0x000067CA-000F759F; CallID = 0x000067CB-00000006; 
	A = "toto"; B = "IS3"; MonitorDisplayedName = "PlayWav"; Host = "ANDREY_TEST"; 
	CreationTimeStamp = 0x01CF7FE0-9A4DEDD0; Status = "scTerminated"; ExitTimeStamp = 0x01CF7FE0-9ADC4B20; 
	CPU100nsTicks = 0x00000000-00000000'
	***/

	//singleton_auto_pointer <CEnumerator> counter;

	//ObjectId llscriptID = 0;
	CAtlString /*statusNotice, scriptID, */status(L"Seccessful run");
	//if (CParam* param = _message.ParamByName(L"ScriptID"))
	//{
	//	scriptID = param->AsString();
	//	llscriptID = param->AsInt64();
	//}

	ObjectId llscriptID = _message.SafeReadParam(L"ScriptID", CMessage::CheckedType::Int64, 0).AsInt64();
	CAtlString scriptID = Utils::toStr(llscriptID).c_str();

	CAtlString statusNotice = _message.SafeReadParam(L"ScriptID", CMessage::CheckedType::String, L"").AsWideStr().c_str();
	//if (CParam* param = _message.ParamByName(L"Status"))
	//	statusNotice = param->AsString();

	if (!statusNotice.CompareNoCase(L"scRunning"))
	{
		if (m_counter.AddScript(llscriptID))
			status = L"Limit running scripts has reached";
	}
	if (!statusNotice.CompareNoCase(L"scTerminated"))
	{
		if (m_counter.RemoveScript(llscriptID))
			status = L"cannot find call by ScriptID";
	}

	CAtlString sTemplate = TEXT("Incoming message: %s: ")
		TEXT("%s, ")
		TEXT("ScriptID: %s, ")
		TEXT("ClientID: %I64i, ")
		TEXT("Status: %s, ")
		TEXT("Total scripts: %i ");

	ObjectId llclient = static_cast<ObjectId>(llscriptID);
	//llclient = llclient << 32;
	llclient = llclient >> 32;

	CAtlString sLogString;
	sLogString.Format(sTemplate, status.GetString(), _message.GetName(), scriptID.GetString(), llclient, statusNotice.GetString(), m_counter.GetScripts().size()/*m_scripts.size()*/);
	m_log->LogString(LEVEL_INFO, sLogString.GetString());

}

void CTransitAgent::LIC2TALicenseEventHandler(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	//int _max_incoming  = DEFAULT_MAX_INCOMING, 
	//	_max_scripts   = DEFAULT_MAX_SCRIPTS,
	//	_max_outcoming = DEFAULT_MAX_OUTCOMING;

	//time_t _exp_time = DEFAULT_EXPIRATION_TIME;

	//if (CParam* param = _message.ParamByName(L"max_incoming"))
	//	_max_incoming = param->AsInt();
	//if (CParam* param = _message.ParamByName(L"max_scripts"))
	//	_max_scripts = param->AsInt();
	//if (CParam* param = _message.ParamByName(L"max_outcoming"))
	//	_max_outcoming = param->AsInt();
	//if (CParam* param = _message.ParamByName(L"expiration_time"))
	//	_exp_time = param->AsInt();

	int _max_incoming  = _message.SafeReadParam(L"max_incoming" , CMessage::CheckedType::Int, DEFAULT_MAX_INCOMING).AsInt(),
		_max_scripts   = _message.SafeReadParam(L"max_scripts"  , CMessage::CheckedType::Int, DEFAULT_MAX_SCRIPTS).AsInt(),
		_max_outcoming = _message.SafeReadParam(L"max_outcoming", CMessage::CheckedType::Int, DEFAULT_MAX_OUTCOMING).AsInt();

	time_t _exp_time = _message.SafeReadParam(L"expiration_time", CMessage::CheckedType::Int, DEFAULT_EXPIRATION_TIME).AsInt();



	//std::string encKeyString, encVectorString, encodedData;

	//if (CParam* param = _message.ParamByName(L"encKeyString"))
	//	encKeyString = param->AsStr();

	//if (CParam* param = _message.ParamByName(L"encVectorString"))
	//	encVectorString = param->AsStr();

	//if (CParam* param = _message.ParamByName(L"encodedData"))
	//	encodedData = param->AsStr();

	std::string encKeyString = _message.SafeReadParam(L"encKeyString", CMessage::CheckedType::String, L"").AsStr(),
		encVectorString = _message.SafeReadParam(L"encVectorString", CMessage::CheckedType::String, L"").AsStr(),
		encodedData = _message.SafeReadParam(L"encodedData", CMessage::CheckedType::String, L"").AsStr();


	std::vector<byte> vKey    ( encKeyString    .begin(), encKeyString    .end());
	std::vector<byte> vVector ( encVectorString .begin(), encVectorString .end());

	CAesDecoder AesDecoder(vKey);
	//Decoder should use the same Key and Initialization Vector
	string decoded = AesDecoder.Decode(encodedData, vVector);



	//singleton_auto_pointer <CEnumerator> counter;
	m_counter.SetLicenceParam(_max_incoming, _max_scripts, _max_outcoming, _exp_time);

	CAtlString sTemplate = TEXT("Incoming message: %s ")
		TEXT("max_incoming: %i, ")
		TEXT("max_scripts: %i, ")
		TEXT("max_outcoming: %i ")
		TEXT("expiration_time: %i ");;

	CAtlString sLogString;
	sLogString.Format(sTemplate, _message.GetName(), _max_incoming, _max_scripts, _max_outcoming, _exp_time);
	m_log->LogString(LEVEL_INFO, sLogString.GetString());
}

/******************************* eof *************************************/