/************************************************************************/
/* Name     : TransitAgent\enumarator.cpp                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Jun 2014                                               */
/************************************************************************/
#include "stdafx.h"
#include "enumerator.h"

//CEnumerator::CallIDList   CEnumerator::m_incoming;
//CEnumerator::ScriptIDList CEnumerator::m_scripts;
//CEnumerator::CallIDList   CEnumerator::m_outcoming;
//
//int    CEnumerator::m_max_incoming    = DEFAULT_MAX_INCOMING;
//int    CEnumerator::m_max_scripts     = DEFAULT_MAX_SCRIPTS;
//int    CEnumerator::m_max_outcoming   = DEFAULT_MAX_OUTCOMING;
//time_t CEnumerator::m_expiration_time = DEFAULT_EXPIRATION_TIME;

//boost::mutex CEnumerator::m_numerator_mutex;

CEnumerator::CEnumerator()
{
	m_max_incoming = DEFAULT_MAX_INCOMING;
	m_max_scripts = DEFAULT_MAX_SCRIPTS;
	m_max_outcoming = DEFAULT_MAX_OUTCOMING;
	m_expiration_time = DEFAULT_EXPIRATION_TIME;
}

CEnumerator::~CEnumerator()
{}


bool CEnumerator::AddIncoming(const ObjectId& _callID)
{
	boost::mutex::scoped_lock lock(m_incoming_mutex);
	if (CanMakeIncomingCall())
	{
		m_incoming.push_back(_callID);
		return true;
	}
	return false;
}

bool CEnumerator::AddScript(const ObjectId& _callID)
{
	boost::mutex::scoped_lock lock(m_scripts_mutex);
	if (CanRunScript())
	{
		m_scripts.push_back(_callID);
		return true;
	}
	return false;
}

bool CEnumerator::AddOutcoming(const ObjectId& _callID)
{
	boost::mutex::scoped_lock lock(m_outcoming_mutex);
	if (CanMakeOutcomingCall())
	{
		m_outcoming.push_back(_callID);
		return true;
	}
	return false;
}

bool CEnumerator::CanMakeIncomingCall()
{
	return m_incoming.size() < m_max_incoming;
}

bool CEnumerator::CanRunScript()
{
	return m_scripts.size() < m_max_scripts;
}

bool CEnumerator::CanMakeOutcomingCall()
{
	return m_outcoming.size() < m_max_outcoming;
}

bool CEnumerator::RemoveIncoming(const ObjectId& _callID)
{
	boost::mutex::scoped_lock lock(m_incoming_mutex);
	CallIDList::const_iterator cit = std::find(m_incoming.begin(), m_incoming.end(), _callID);
	if (cit != m_incoming.end())
	{
		//success
		m_incoming.erase(cit);
		return true;
	}
	return false;

}
bool CEnumerator::RemoveScript(const ObjectId& _scriptID)
{
	boost::mutex::scoped_lock lock(m_scripts_mutex);
	ScriptIDList::const_iterator cit = std::find(m_scripts.begin(), m_scripts.end(), _scriptID);
	if (cit != m_scripts.end())
	{
		m_scripts.erase(cit);
		return true;
	}

	return false;
}

int CEnumerator::RemoveScripts(const ObjectId& _dispatcher)
{
	boost::mutex::scoped_lock lock(m_scripts_mutex);
	int iHasDisconnectedScriptsCount = 0;
	ScriptIDList new_list;

	for (ScriptIDList::const_iterator cit = m_scripts.begin(); cit != m_scripts.end(); ++cit)
	{
		//ObjectId llFrom = static_cast<__int64>(__int64(_pFrom->GetClientID()) << 32 | _pFrom->GetChildID());
		ObjectId llclient = static_cast<ObjectId>(*cit);
		//llclient = llclient << 32;
		llclient = llclient >> 32;
		if (_dispatcher != llclient)
			new_list.push_back(static_cast<ObjectId>(*cit));
		else
			++iHasDisconnectedScriptsCount;
	}

	m_scripts.swap(new_list);

	return iHasDisconnectedScriptsCount;
}

bool CEnumerator::RemoveOutcoming(const ObjectId& _callID)
{
	boost::mutex::scoped_lock lock(m_outcoming_mutex);
	CallIDList::const_iterator cit = std::find(m_outcoming.begin(), m_outcoming.end(), _callID);
	if (cit != m_outcoming.end())
	{
		m_outcoming.erase(cit);
		return true;
	}

	return false;
}

void CEnumerator::SetLicenceParam(const int& _max_inc, const int& _max_scripts, const int& _max_out, const time_t& _exp_time )
{
	boost::mutex::scoped_lock lock1(m_outcoming_mutex);
	boost::mutex::scoped_lock lock2(m_incoming_mutex);
	boost::mutex::scoped_lock lock3(m_scripts_mutex);
	m_max_incoming    = _max_inc;
	m_max_scripts     = _max_scripts;
	m_max_outcoming   = _max_out;
	m_expiration_time = _exp_time;
}

bool CEnumerator::HasLicenceExpired()
{
	return (m_expiration_time > time((time_t *)0));
}

/******************************* eof *************************************/