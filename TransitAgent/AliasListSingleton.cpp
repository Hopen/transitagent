/************************************************************************/
/* Name     : TransitAgent\AliasListSingleton.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 08 Apr 2014                                               */
/************************************************************************/
#include "stdafx.h"
//#include "ce_xml.hpp"
#include "utils.h"
#include "AliasListSingleton.h"

CAliasListSingleton::CAliasListSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder)
	: CCompositeSingleton{ log , tmpFileReadFolder }
{
	m_pAliasList = std::make_shared<CompositeAction<CAliasListSingleton>>(this, std::wstring(CProcessHelper::GetCurrentModuleName()) + L".config");
}

CAliasListSingleton::~CAliasListSingleton()
{

}

void CAliasListSingleton::LoadList(const core::config_node& pAliasListRoot, std::wstring fileConfig, IList::SPtr _elem)
{
	//LiteXML::TElem		mask;
	core::configuration config(fileConfig);
	//for(mask = pAliasListRoot.begin(); mask != pAliasListRoot.end(); mask++)
	for (const auto& child : pAliasListRoot)
	{
		std::wstring sName = config.safe_get_config_attr<std::wstring>(child.second, L"name", L"");//mask.attr(L"name");
		std::wstring sPath = config.safe_get_config_attr<std::wstring>(child.second, L"path", L"");//mask.attr(L"path");
		IList::SPtr Alias(new CAliasList(sName, sPath));
		_elem->add(Alias);
	}
}

void CAliasListSingleton::LoadAliasList(const core::config_node& pAliasListRoot, std::wstring fileConfig)
{
	LoadList(pAliasListRoot, fileConfig, m_pAliasList);
}

void CAliasListSingleton::LoadAliasList(const core::config_node& pAliasListRoot, std::wstring fileConfig, IList::SPtr _elem)
{
	LoadList(pAliasListRoot, fileConfig, _elem);
}

void CAliasListSingleton::load(const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& /*blackListGroups*/)
{
	core::configuration config(fileConfig);
	LoadAliasList (config.get_config_node(L"ScriptAliases"), fileConfig, _elem);
}

/******************************* eof *************************************/