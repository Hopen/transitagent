/************************************************************************/
/* Name     : TransitAgent\CustomCfgSingleton.h                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 16 Dec 2010                                               */
/************************************************************************/
#pragma once
#include "CompositeSingleton.h"
//#include "ce_xml.hpp"
#include "..\Composite\CustomCfg.h"

class CCustomCfgSingleton : public CCompositeSingleton
{
public:
	CCustomCfgSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder);
	~CCustomCfgSingleton();

	void LoadCustomCfg(const core::config_node& pCustomCfgRoot, std::wstring fileConfig);
	void LoadCustomCfg(const core::config_node& pCustomCfgRoot, std::wstring fileConfig, IList::SPtr _elem);
	void load(const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& _blackListGroups);

	IList::SPtr GetCustomConfig(){return m_pCustomCfg;}

private:
	void LoadList(const core::config_node& pCustomCfgRoot, std::wstring fileConfig, IList::SPtr _elem);
};


/******************************* eof *************************************/