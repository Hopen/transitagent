/************************************************************************/
/* Name     : TransitAgent\TransitAgentModule.h                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 16 Aug 2010                                               */
/************************************************************************/


#include "stdafx.h"
#include "resource.h"
#include "TransitAgent_i.h"
#include "TransitAgent.h"
#include "Log/SystemLog.h"
#include "Configuration/SystemConfiguration.h"
#include "WinAPI/mdump.h"
#include "WinAPI/ProcessHelper.h"

#include <stdio.h>

//singleton_auto_pointer<MiniDumper> g_MiniDump;
//CXmlConfigSingleton* CXmlConfigSingleton ::_self=NULL;
//int CXmlConfigSingleton::_refcount = 0;

class CTransitAgentModule : public CAtlServiceModuleT< CTransitAgentModule, IDS_SERVICENAME >
{
public :
	DECLARE_LIBID(LIBID_TransitAgentLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_TRANSITAGENT, "{BBD84C44-19AB-440B-A780-F41D6DA4BAB5}")
	HRESULT InitializeSecurity() throw()
	{
		// TODO : Call CoInitializeSecurity and provide the appropriate security settings for 
		// your service
		// Suggested - PKT Level Authentication, 
		// Impersonation Level of RPC_C_IMP_LEVEL_IDENTIFY 
		// and an appropiate Non NULL Security Descriptor.

		return S_OK;
	}
};

CTransitAgentModule _AtlModule;
LPCTSTR c_lpszModuleName = _T("TransitAgent.exe");

void RedirectTest(CTransitAgent* pAgent)
{
	/************************************************************************/
	/* 
	Name: SG2TA_RunScriptRedirect; ScriptID = 0x0004C1E5-00087934
	*/
	/************************************************************************/
	CMessage redirect(L"SG2TA_RunScriptRedirect");
	redirect[L"ScriptID"] = 0x0001BBE900034426;

	CLIENT_ADDRESS _from;
	_from.SetAsQWORD((__int64)0x00063C40 << 32);
	CLIENT_ADDRESS _to;
	//bool blDeliverError = false;
	pAgent->SG2TARedirectEventHandler(redirect,_from,_to);
}

void RunScriptTest(CTransitAgent* pAgent)
{
	/************************************************************************/
	/* 
	Name: SG2TA_RUN_SCRIPT; DbgMsgStep = 1; DbgMsgCOM = 1; DbgMsgStepDisasm = 0; 
	DbgMsgExcDisasm = 1; FileName = Unknown Type 0; DebugFlag = 1; 
	OfflineFlag = 1; RunRequestID = 0xF2CF8A02-001B3BD0; ScrGenAddr = 0xF2CF8BCF-00000000; 
	ScrGenHost = "\MS-IVRTEST"; ScrGenUser = "MS-IVRTEST\Administrator"; MonitorDisplayedName = "XML3.sc3"
	*/
	/************************************************************************/
	CMessage runscript(L"SG2TA_RUN_SCRIPT");
	CLIENT_ADDRESS _from;
	_from.SetAsQWORD(0x0001BBE900034426); // scriptid from RedirectTest()
	CLIENT_ADDRESS _to;
	//bool blDeliverError = false;
	pAgent->SG2TARunScriptEventHandler(runscript,_from,_to);

}

void DebugTest (CTransitAgent* pAgent)
{
 /************************************************************************/
 /* 
 'Name: SG2TA_DEBUG_SCRIPT; DbgMsgStep = 1; DbgMsgCOM = 1; DbgMsgStepDisasm = 0; 
 DbgMsgExcDisasm = 1; FileName = Unknown Type 0; DebugFlag = 1; 
 OfflineFlag = true; RunRequestID = 0x7D9CA2A2-0024EA28; ANumMask = "9055505661"; 
 BNumMask = "%"; RequestID = 0x7D9CA2A2-0024EA28; ScrGenAddr = 0x7D9CA2C1-00000000; 
 ScrGenHost = "\MTT-IVR001"; ScrGenUser = "MTT-IVR001\Administrator"; MonitorDisplayedName = "run.sc3"'
 */
 /************************************************************************/

	CMessage debuging(L"SG2TA_DEBUG_SCRIPT");
	debuging[L"RequestID"] = L"0x7D9CA2A2-0024EA28";
	debuging[L"RunRequestID"] = L"0x7D9CA2A2-0024EA28";
	debuging[L"ANumMask"] = L"9055505661";
	debuging[L"BNumMask"] = L"%";

	CLIENT_ADDRESS _from;
	CLIENT_ADDRESS _to;
	bool blDeliverError = false;
	pAgent->SG2TADebugRunEventHandler(debuging,_from, _to/*,blDeliverError*/);
	debuging[L"RequestID"] = L"0x7D9CA2A2-0024EA29";
	pAgent->SG2TADebugRunEventHandler(debuging,_from, _to/*,blDeliverError*/);

	/************************************************************************/
	/* 
	'Name: SG2TA_DEBUG_SCRIPT_CANCEL; RequestID = 0x7D9CA2A2-0024EA29
	*/
	/************************************************************************/
	debuging.SetName(L"SG2TA_DEBUG_SCRIPT_CANCEL");
	pAgent->SG2TADebugCancelEventHandler(debuging,_from, _to/*,blDeliverError*/);
	//pAgent->AppStatusChange(_from.GetClientID(),CS_DISCONNECTED);
}

void OfferedTest(CTransitAgent* pAgent)
{
	/************************************************************************/
	/* 
	Name: OFFERED; InitialMessage = "OFFERED"; Host = "DR-IVR801"; 
	Board = "DTI1"; TimeSlot = 4; A = "9031392837"; B = "07042d9629475619"; 
	SigInfo = "0x0601100702600109010A020100040A03107040D269927465910A07031309139382730801801D038090A33F0A84937063213607995200"; 
	CallID = 0x000392EA-0004D3E7

	*/
	/************************************************************************/
	// testing
	CMessage offered(L"OFFERED");
	//CMessage offered(L"MCC2TA_NEWCHAT");
	
	//CMessage offered(L"MCC2TA_NEWEMAIL");
	//CMessage offered(L"SIP2TA_SMPP_DELIVER_SM");
	//offered[ L"InitialMessage" ] = L"OFFERED";
	//offered[ L"Host"           ] = L"TELSERV1";
	//offered[ L"Board"          ] = L"DTI1";
	//offered[ L"TimeSlot"       ] = 15;
	//offered[L"CustomerCTN"] = L"9067837625";
	//offered[L"ChannelID"] = L"WEB";
	offered[ L"A"              ] = L"4957813195";
	offered[ L"B"              ] = L"6611";
	


	//offered[ L"A"              ] = L"200@10.22.16.161";
	//offered[ L"B"			   ] = L"12345678901:2102721066123123";

	//offered[L"A"] = L"asterisk@192.168.151.84";
	//offered[L"B"] = L"901@172.21.242.226";

	//offered[ L"Orig_Address"   ] = L"9067837625";
	//offered[ L"Dst_Address"    ] = L"068125";
	//offered[ L"From"           ] = L"ComferenceRoom2@vimpel.tst";
	//offered[ L"To"             ] = L"OfferedTest";
	//offered[ L"SigInfo"        ] = L"0x0601100702200109010A020100040684906018520F0A070313097638675208018003047D0291811D038090A33F0A83973041150992995200";
	//offered[ L"CallID"         ] = 0x0001FD3763B5BA0E;
	//offered[ L"MCCID"          ] = 5;

	//offered[ L"EsmClass"       ] = L"64";// for MM
	//offered[ L"EsmClass"       ] = L"0";


	CLIENT_ADDRESS _from;
	CLIENT_ADDRESS _to;
	bool blDeliverError = false;
	pAgent->OfferedEventHandler(offered,_from, _to/*,blDeliverError*/);
}

void CallStatusTest(CTransitAgent* pAgent)
{
	/************************************************************************/
	/* 
	Name: TS2ANY_CALLSTATUS; StatusText = "INCOMING"; 
	StatusDescription = "unanswred incoming call"; Host = "TELSERV1"; 
	StatusCode = 0x00000000-00000000; Board = "DTI4"; TimeSlot = 0; 
	A = "9162299203"; B = "4955395972"; OldNativeStatus = "CST_NULL"; 
	NewNativeStatus = "CST_OFFERED"; BindToScriptID = 0x00000000-00000000; 
	CallID = 0x0001FD37-63B5BA0E; StatusEnterTime = "18:29:56"; 
	StatusEnterTimestamp = 0x01CB8FDA-471CBFA0'
	*/
	/************************************************************************/
	CMessage call_status(L"TS2ANY_CALLSTATUS");
	call_status[L"StatusText"] = L"INCOMING";
	call_status[L"Board"     ] = L"DTI4";
	call_status[L"TimeSlot"  ] = 0;

	CLIENT_ADDRESS _from;
	CLIENT_ADDRESS _to;
	bool blDeliverError = false;
	pAgent->CallStatusEventHandler(call_status, _from, _to/*,blDeliverError*/);
}

void TimeSlotStatusTest(CTransitAgent* pAgent)
{
	/************************************************************************/
	/*                                                                      
	Name: TS2ANY_TIMESLOTSTATUS; StatusCode = 0x00000000-00000000; 
	StatusText = "IDLE"; StatusDescription = ""; 
	Host = "MTT-IVR001"; InService = true; Board = "DTI2"; TimeSlot = 4; 
	Blocked = false; Busy = false
	*/
	/************************************************************************/
	std::wstring sConfigFileName = CProcessHelper::GetCurrentModuleName() + L".config";
	pAgent->LoadTrunkList(sConfigFileName);


	CMessage TimeSlotStatus(L"TS2ANY_TIMESLOTSTATUS");
	TimeSlotStatus[L"StatusCode"       ] = 0x0000000000000000;
	TimeSlotStatus[L"StatusText"       ] = L"IDLE";
	TimeSlotStatus[L"StatusDescription"] = L"";
	TimeSlotStatus[L"Host"             ] = L"TEST";
	TimeSlotStatus[L"InService"        ] = true;
	TimeSlotStatus[L"Board"            ] = L"IPT2";
	TimeSlotStatus[L"TimeSlot"         ] = 4;
	TimeSlotStatus[L"Blocked"          ] = false;

	CLIENT_ADDRESS _from;
	//_from.SetAsQWORD((__int64)0xc0063C40 << 32);
	_from.SetAsQWORD(777389111909);
	CLIENT_ADDRESS _to;
	bool blDeliverError = false;

	pAgent->TimeSlotStatusEventHandler(TimeSlotStatus,_from, _to/*,blDeliverError*/);

	_to.SetAsQWORD(444566551113);

	TimeSlotStatus[L"Host"             ] = L"TGSIP";
	TimeSlotStatus[L"Board"            ] = L"DTI3";
	TimeSlotStatus[L"TimeSlot"         ] = 5;
	//_from.SetAsQWORD((__int64)0xc0063C41 << 32);
	_from.SetAsQWORD(777389111910);
	pAgent->TimeSlotStatusEventHandler(TimeSlotStatus,_from, _to/*,blDeliverError*/);

	TimeSlotStatus[L"Host"             ] = L"TGSIP";
	TimeSlotStatus[L"Board"            ] = L"DTI3";
	TimeSlotStatus[L"TimeSlot"         ] = 6;
	//_from.SetAsQWORD((__int64)0xc0063C42 << 32);
	_from.SetAsQWORD(777389111911);
	pAgent->TimeSlotStatusEventHandler(TimeSlotStatus,_from, _to/*,blDeliverError*/);

	_from.SetAsQWORD(777389111909);
	pAgent->RequestTSCompletedHandler(TimeSlotStatus, _from, _to/*,blDeliverError*/);

	//pAgent->RequestTSCompletedHandler(TimeSlotStatus, &_from, &_to/*,blDeliverError*/);

	//pAgent->BuildTrunkGroup();

}

void MakeCallTest(CTransitAgent* pAgent)
{
	/************************************************************************/
	/*  BY GROUP
	Name: MAKE_CALL; OriginationNumberType = "National"; 
	DestinationNumberType = "National"; OriginationPresentationRestrictions = "Restricted"; 
	Direction = "Forward"; A = "8003330515"; B = "9164657896"; 
	TimeOut = 60; Group = "default_trunkgroup"; Parity = "Both"; 
	DestinationAddress = 0x00000002-00000000; CallbackID =0x00000000-00000006; 
	bSendMessageToTelServer = 1; ScriptID = 0x0001BBE9-00034426
	*/
	/************************************************************************/

	CMessage MakeCall(L"MAKE_CALL");
	MakeCall[L"OriginationNumberType"] = L"National";
	MakeCall[L"DestinationNumberType"] = L"National";
	MakeCall[L"OriginationPresentationRestrictions"] = L"Restricted";
	MakeCall[L"Direction"] = L"Reverse";
	//MakeCall[L"A"] = L"77773844614";
	MakeCall[L"A"] = L"sip:15099@beeline.ru";
	MakeCall[L"B"] = L"77773844591";
	MakeCall[L"TimeOut"] = 60;
	//MakeCall[L"Group"] = L"MOSCOW.DEFAULT";
	MakeCall[L"Group"] = L"TGSIP";
	MakeCall[L"Parity"] = L"Both";
	MakeCall[L"DestinationAddress"] = 0x0000000200000000;
	MakeCall[L"CallbackID"] = 0x0000000000000006;
	MakeCall[L"bSendMessageToTelServer"] = 1;
	MakeCall[L"ScriptID"] = 0x0001BBE900034426;

	CLIENT_ADDRESS _from;
	_from.SetAsQWORD((__int64)0x00063C40 << 32);
	CLIENT_ADDRESS _to;
	bool blDeliverError = false;

	pAgent->MakeCallEventHandler(MakeCall, _from, _to/*,blDeliverError*/);
	//pAgent->MakeCallEventHandler(MakeCall, &_from, &_to/*,blDeliverError*/);
	//pAgent->MakeCallEventHandler(MakeCall, &_from, &_to/*,blDeliverError*/);
	//pAgent->MakeCallEventHandler(MakeCall, &_from, &_to/*,blDeliverError*/);

	///************************************************************************/
	///* BY TIMESLOT
	//Name: MAKE_CALL; A = "8003330515"; B = "9164657896"; 
	//CallbackID = 0x00000000-00000006; 
	//DestinationAddress= 0x00000002-00000000; 
	//DestinationNumberType = "National"; OriginationNumberType = "National"; 
	//OriginationPresentationRestrictions = "Restricted"; ScriptID = 0x0001BBE9-00034426; 
	//TimeOut = 60; Timeslots = "DTI[1,2,3](0-29)"; bSendMessageToTelServer = 1
	//*/
	///************************************************************************/
	//MakeCall.Remove(L"Group");
	//MakeCall[L"Timeslots"] = L"DTI[1,2,3](0-29)";
	//MakeCall[L"Method"]    = L"Random";
	//MakeCall[L"Direction"] = L"Reverse";
	//MakeCall[L"Parity"]    = L"Both";
	//pAgent->MakeCallEventHandler(MakeCall,&_from, &_to/*,blDeliverError*/);

}


//namespace boostex
//{
//
//	class unique_lock_ex:unique_lock<boost::mutex>
//	{
//	public:
//		unique_lock_ex(boost::mutex& mutex):unique_lock(mutex)
//		{
//			m_log->LogStringModule(LEVEL_INFO, L"MUTEX", L"lock");
//		}
//		~unique_lock_ex()
//		{
//			m_log->LogStringModule(LEVEL_INFO, L"MUTEX", L"unlock");
//		}
//
//	private:
//		singleton_auto_pointer<CSystemLog> m_log;
//		//typedef unique_lock_ex<Mutex> scoped_lock_ex;
//	};
//
//};

void DispCrashTest(CTransitAgent* pAgent)
{
	//ObjectId llFrom = static_cast<__int64>(__int64(_pFrom->GetClientID()) << 32 | _pFrom->GetChildID() );
	//singleton_auto_pointer <CEnumerator> counter;
	CEnumerator counter;
	CLIENT_ADDRESS _from;
	_from.SetAsQWORD((__int64)0x00063C40 /*<< 32*/);

	_from.SetChildID((__int64)0x000AAAAA);
	counter.AddScript(_from.GetAsQWORD());

	_from.SetChildID((__int64)0x000BBBBB);
	counter.AddScript(_from.GetAsQWORD());

	_from.SetChildID((__int64)0x000CCCCC);
	counter.AddScript(_from.GetAsQWORD());

	int count = counter.RemoveScripts(_from.GetClientID());
	int test = count;

	CAtlString sLogString;
	CAtlString sTemplate = L"  %i scripts finished by terminating DISPATCHER ( %I64i )";
	sLogString.Format(sTemplate, count, _from.GetAsQWORD());
	pAgent->m_log->LogStringModule(LEVEL_INFO, L"CS_DISCONNECTED", sLogString.GetString());
}

void DS2ANYNoticeTest(CTransitAgent* pAgent)
{
	/**************************************
	msg: 'Name: D2ANY_ScriptStatusNotice; ScriptID = 0x000067CA-000F759F; CallID = 0x000067CB-00000006;
	A = "toto"; B = "IS3"; MonitorDisplayedName = "PlayWav"; Host = "ANDREY_TEST";
	CreationTimeStamp = 0x01CF7FE0-9A4DEDD0; Status = "scTerminated"; ExitTimeStamp = 0x01CF7FE0-9ADC4B20;
	CPU100nsTicks = 0x00000000-00000000'
	***/

	CMessage StatusNotice(L"D2ANY_ScriptStatusNotice");
	StatusNotice[L"CallID"] = 0x000067CB00000006;
	StatusNotice[L"ScriptID"] = 0x000067CA000F759F;
	StatusNotice[L"Status"] = L"scTerminated";
	StatusNotice[L"A"] = L"77773844614";
	StatusNotice[L"B"] = L"77773844591";

	CLIENT_ADDRESS _from;
	_from.SetAsQWORD((__int64)0x00063C40 << 32);
	CLIENT_ADDRESS _to;
	bool blDeliverError = false;

	pAgent->D2ANYScriptStatusNoticeHandler(StatusNotice, _from, _to/*,blDeliverError*/);
}


void LICENCETest(CTransitAgent* pAgent)
{
	CMessage licence(L"LICENCE_MESSAGE");
	licence[ L"max_incoming"  ] = DEFAULT_MAX_INCOMING;
	licence[L"max_outcoming"] = 0;// DEFAULT_MAX_OUTCOMING;
	licence[ L"max_scripts"   ] = DEFAULT_MAX_SCRIPTS;

	CLIENT_ADDRESS _from;
	_from.SetAsQWORD((__int64)0x00063C40 << 32);
	CLIENT_ADDRESS _to;
	bool blDeliverError = false;

	pAgent->LIC2TALicenseEventHandler(licence, _from, _to/*,blDeliverError*/);
}

template<typename Function, typename... Args>
int64_t do_profiling(Function&& _func, const Args&...args)
{
	using namespace std::chrono;
	auto start = high_resolution_clock::now();
	_func(args...);
	return duration_cast<milliseconds>(high_resolution_clock::now() - start).count();
}

extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, 
								LPTSTR lpCmdLine, int nShowCmd)
{
	try
	{
			::CoInitialize(NULL);;

			singleton_auto_pointer<MiniDumper> g_MiniDump;
			singleton_auto_pointer<CSystemLog> systemLog;
			std::wstring sConfigFileName = CProcessHelper::GetCurrentModuleName() + L".config";

			auto pInst = std::make_unique<CTransitAgent>(CSystemLog::GetInstance(), sConfigFileName);

			//OfferedTest(pInst.get());
			//TimeSlotStatusTest(pInst);
			//for (int i = 0; i < 1000000; ++i)
			//	MakeCallTest(pInst);
			//TimeSlotStatusTest(pInst.get());
			//CallStatusTest(pInst);
			//MakeCallTest(pInst.get());
			//RedirectTest(pInst);
			//RunScriptTest(pInst);
			//DS2ANYNoticeTest(pInst);
			MSG msg;

			while (Sleep(5000), true)
			{
				if (PeekMessage(&msg,0,0,0,PM_NOREMOVE))
					if (!GetMessage(&msg,0,0,0))
						break;
			}

			//CTransitAgent::ReleaseInstance();
			::CoUninitialize();
			return 0;
		//} 
	}
	catch(core::configuration_error& _exception)
	{
		return -1;
	}
	catch (...)
	{
		return -1;
	}


	return _AtlModule.WinMain(nShowCmd);
}

/******************************* eof *************************************/