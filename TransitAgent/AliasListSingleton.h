/************************************************************************/
/* Name     : TransitAgent\AliasListSingleton.h                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 08 Apr 2014                                               */
/************************************************************************/
#pragma once
#include "CompositeSingleton.h"
#include "..\Composite\AliasList.h"

class CAliasListSingleton : public CCompositeSingleton
{
public:
	CAliasListSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder);
	~CAliasListSingleton();

	void LoadAliasList(const core::config_node& pAliasListRoot, std::wstring fileConfig);
	void LoadAliasList(const core::config_node& pAliasListRoot, std::wstring fileConfig, IList::SPtr _elem);
	void load(const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& _blackListGroups);

	IList::SPtr GetAliasList(){return m_pAliasList;}

private:
	void LoadList(const core::config_node& pAliasListRoot, std::wstring fileConfig, IList::SPtr _elem);
};


/******************************* eof *************************************/