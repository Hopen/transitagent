/************************************************************************/
/* Name     : CDRLog\Timer.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 19 Feb 2015                                               */
/************************************************************************/
#pragma once

#include <boost/thread.hpp>
#include <boost/asio.hpp>
//#include "singleton.h"

class CTimer
{
public:
	using SimpleCallbackFunc = std::function<void()>;

	CTimer(boost::shared_ptr<boost::asio::io_service>_io/*boost::asio::io_service& _io*/, SimpleCallbackFunc _callback, boost::posix_time::seconds _checkTime);
	~CTimer();
	void CancelTimer();
private:
	void Loop();

private:
	boost::asio::strand m_strand;
	SimpleCallbackFunc m_callBackFunc;
	/*boost::posix_time::milliseconds*/boost::posix_time::seconds m_checkTime;
	boost::asio::deadline_timer m_timer;
};

class CPulseTimer
{
public:
	//typedef void(*SimpleCallbackFunc)();
	using SimpleCallbackFunc = std::function<void()>;

	CPulseTimer(boost::shared_ptr<boost::asio::io_service>_io/*boost::asio::io_service& _io*/, SimpleCallbackFunc _callback, boost::posix_time::seconds _checkTime);
	~CPulseTimer();
	void CancelTimer();
private:
	void Loop();

private:
	boost::asio::strand m_strand;
	SimpleCallbackFunc m_callBackFunc;
	boost::posix_time::seconds m_checkTime;
	boost::asio::deadline_timer m_timer;
};


//class CSystemLog;
//
//class CBeholder : public singleton < CBeholder >
//{
//	friend class singleton < CBeholder >;
//
//public:
//	void InitTimers(CSystemLog *log);
//
//private:
//	boost::shared_ptr<CTimer> m_timer1;
//	boost::shared_ptr<CTimer> m_timer2;
//	boost::shared_ptr<CTimer> m_timer3;
//
//	boost::asio::io_service m_io;
//};

/******************************* eof *************************************/