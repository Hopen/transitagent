/************************************************************************/
/* Name     : TransitAgent\CustomCfgSingleton.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 16 Dec 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "CustomCfgSingleton.h"

CCustomCfgSingleton::CCustomCfgSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder)
	: CCompositeSingleton{ log , tmpFileReadFolder }
{
	m_pCustomCfg = std::make_shared<CompositeAction<CCustomCfgSingleton>>(this, std::wstring(CProcessHelper::GetCurrentModuleName()) + L".config");
}

CCustomCfgSingleton::~CCustomCfgSingleton()
{

}

void CCustomCfgSingleton::LoadList(const core::config_node& pCustomCfgRoot, std::wstring fileConfig, IList::SPtr _elem)
{
	//LiteXML::TElem		mask;
	//for(mask = pCustomCfgRoot.begin(); mask != pCustomCfgRoot.end(); mask++)
	core::configuration config(fileConfig);
	for (const auto& child : pCustomCfgRoot)
	{
		std::wstring sLentoapply = config.safe_get_config_attr<std::wstring>(child.second, L"lentoapply", L"");//mask.attr(L"lentoapply");
		std::wstring sPrefix     = config.safe_get_config_attr<std::wstring>(child.second, L"value", L"");//mask.attr(L"value");
		IList::SPtr Prefix(new CCustomCfg(sLentoapply, sPrefix));
		_elem->add(Prefix);
	}
}

void CCustomCfgSingleton::LoadCustomCfg(const core::config_node& pCustomCfgRoot, std::wstring fileConfig)
{
	LoadList(pCustomCfgRoot, fileConfig, m_pCustomCfg);
}

void CCustomCfgSingleton::LoadCustomCfg(const core::config_node& pCustomCfgRoot, std::wstring fileConfig, IList::SPtr _elem)
{
	LoadList(pCustomCfgRoot, fileConfig, _elem);
}

void CCustomCfgSingleton::load(const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& /*blackListGroups*/)
{
	core::configuration config(fileConfig);
	LoadCustomCfg (config.get_config_node(L"CustomConfig"), fileConfig, _elem);
}

/******************************* eof *************************************/