/************************************************************************/
/* Name     : Composite\ManagetMessages.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 07 Sep 2010                                               */
/************************************************************************/
#pragma once
#include <string>
#include <sstream> 
#include <functional>

namespace Utils
{
	template<class T>
	std::wstring toStr(T aa)
	{
		std::wostringstream out;
		out << aa;
		return out.str();
	}

	template<class T>
	T toNum(std::wstring _in)
	{
		T num(0);
		std::wistringstream inn(_in);
		inn >> num;
		return num;
	}

	template<class T>
	T toNumX(std::wstring _in)
	{
		T num;
		if (_in.empty())
			num = 0;
		else
			std::wstringstream(_in) >> std::hex >> num;
		return num;
	}

	template <class T>
	T wtoi(std::wstring in)
	{
		T ret = 0;
		bool bTwos = false;
		if (in[0] == '-')
			bTwos = true;
		for (unsigned int i = bTwos ? 1 : 0; i<in.size(); ++i)
		{
			ret *= 10;
			ret += (in[i] - L'0');
		}
		if (bTwos)
			ret = 0 - ret;
		return ret;
	}

	////////////////////////////////////////////////////////////////////////////////
	//service templates for trim operations

	template<class TStr,class TTrimComp>
	TStr trimr(const TStr& str,TTrimComp is_trim_char)
	{
		TStr::const_iterator i;

		for(i=str.end()-1;
			i>=str.begin() && is_trim_char(*i);
			--i);

		return TStr(str.begin(),++i);
	}

	template<class TStr,class TTrimComp>
	TStr triml(const TStr& str,TTrimComp is_trim_char)
	{
		TStr::const_iterator i;

		for(i=str.begin();i!=str.end() && is_trim_char(*i);++i);

		return TStr(i,str.end());
	}

	template<class TStr,class TTrimComp>
	TStr trim(const TStr& str,TTrimComp is_trim_char)
	{
		return triml(trimr(str,is_trim_char),is_trim_char);
	}


	inline std::wstring trimr(const std::wstring& str, const WCHAR& trim_char)
	{return trimr(str,std::bind1st(std::equal_to<WCHAR>(),trim_char));}

	inline std::wstring triml(const std::wstring& str, const WCHAR& trim_char)
	{return triml(str,std::bind1st(std::equal_to<WCHAR>(),trim_char));}

	inline std::wstring trim(const std::wstring& str, const WCHAR& trim_char)
	{return trim(str,std::bind1st(std::equal_to<WCHAR>(),trim_char));}

	extern inline int GetFile(const std::wstring& uri, WIN32_FIND_DATAW* pDataOut)
	{
		HANDLE hf;

		int stat = 1;
		hf = FindFirstFileW((uri).c_str(), pDataOut);
		if (hf != INVALID_HANDLE_VALUE)
		{
			do
			{
				if(pDataOut->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					continue;

				stat = 0; // Successful!
				break;
			}
			while (FindNextFileW(hf,pDataOut)!=0);
			FindClose(hf);
		}
		return stat;
	}

	extern inline std::wstring GetFileNameFromURI(const std::wstring& _uri)
	{
		int pos_left  = _uri.rfind(L'/');
		int pos_rigth = _uri.rfind(L'\\');

		int pos = (pos_left>pos_rigth)?pos_left:pos_rigth;

		std::wstring sName(_uri);
		if (pos >= 0) 
		{
			sName = _uri.substr(pos+1,_uri.length() - 1); // without "\"
		}

		return sName;
	}
	extern inline std::wstring GetFolderNameFromURI(const std::wstring& _uri)
	{
		int pos_left  = _uri.rfind(L'/');
		int pos_rigth = _uri.rfind(L'\\');

		int pos = (pos_left>pos_rigth)?pos_left:pos_rigth;

		std::wstring sName(_uri);
		if (pos >= 0) 
		{
			sName = _uri.substr(0,pos+1); // with "\"
		}

		return sName;
	}

}

/******************************* eof *************************************/