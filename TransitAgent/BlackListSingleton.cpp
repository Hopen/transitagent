/************************************************************************/
/* Name     : TransitAgent\BlackListSingleton.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 18 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"
//#include "ce_xml.hpp"

#include "Patterns/string_functions.h"

#include "utils.h"
#include "BlackListSingleton.h"

CBlackListSingleton::CBlackListSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder, CCustomCfgSingleton* customCfgSingleton)
	: CCompositeSingleton{ log , tmpFileReadFolder }
	, _customCfgSingleton{ customCfgSingleton }
{}

CBlackListSingleton::~CBlackListSingleton()
{}

void CBlackListSingleton::LoadList(rapidxml::xml_node<> *blacklistNode, std::wstring fileConfig, IList::SPtr _elem)
{
	if (!blacklistNode)
	{
		return;
	}

	auto rootFolder = Utils::GetFolderNameFromURI(_elem->GetScriptName());
	for (rapidxml::xml_node<> *node = blacklistNode->first_node(); node; node = node->next_sibling())
	{
		if (boost::to_lower_copy(std::string(node->name())) == "include")
		{
			std::wstring uri;
			auto fileAttr = node->first_attribute("file", 0, false);
			if (!fileAttr)
			{
				continue;
			}
			auto fileName = stow(fileAttr->value());
			if (!AddFolder(fileName, rootFolder, uri))
			{
				continue;
			}

			bool bFileAccept = true;
			std::wstring configFileName = uri;
			if (!m_backup->FileValid(uri))
			{
				bFileAccept = false;
				configFileName = m_backup->LoadBackup(uri);
			}
			if (configFileName.empty())
				continue;

			rapidxml::xml_document<> doc;
			rapidxml::file<> f(wtos(configFileName).c_str());
			doc.parse<0>(f.data());
			rapidxml::xml_node<> *configurationNode = doc.first_node("configuration", 0, false);
			if (!configurationNode)
			{
				continue;
			}

			IList::SPtr pBlackList(new CompositeAction<CBlackListSingleton>(this, uri));
			_elem->add(pBlackList);
			LoadList(configurationNode->first_node("BlackList", 0, false), configFileName, pBlackList);
			if (bFileAccept)
				m_backup->Save(configFileName, false);

			continue;
		}

		auto originAttr = node->first_attribute("Origination", 0, false);
		if (!originAttr)
		{
			continue;
		}
		std::wstring sOrigination = stow(originAttr->value());
		CBlackList* pBL = new CBlackList(sOrigination, L"");
		pBL->AddCustomConfig(_customCfgSingleton->GetCustomConfig().get());
		IList::SPtr Caller(pBL);

		_elem->add(Caller);
	}
}

//void CBlackListSingleton::LoadBlackList(rapidxml::xml_node<> *blacklistNode, std::wstring fileConfig)
//{
//	LoadList(blacklistNode, fileConfig, m_pBlackList);
//}

void CBlackListSingleton::LoadBlackList(rapidxml::xml_node<> *blacklistNode, std::wstring fileConfig, IList::SPtr _elem)
{
	LoadList(blacklistNode, fileConfig, _elem);
}

void CBlackListSingleton::LoadBlackLists(
	rapidxml::xml_node<> *managedBacklistNode,
	std::wstring fileConfig,
	const std::wstring& _root_folder,
	IList::BlackListGroups& groups)
{
	if (!managedBacklistNode)
	{
		return;
	}

	std::map<std::wstring, std::wstring> attrFiles;
	for (rapidxml::xml_node<> *node = managedBacklistNode->first_node(); node; node = node->next_sibling())
	{
		std::wstring sFileURI;

		auto fileAttr = node->first_attribute("file", 0, false);
		if (!fileAttr)
		{
			continue;
		}
		auto fileName = stow(fileAttr->value());
		if (AddFolder(fileName,_root_folder,sFileURI))
			attrFiles[stow(node->name())] = sFileURI;
	}

	// extract black list files
	for (std::map<std::wstring, std::wstring>::const_iterator cit = attrFiles.begin();
		cit!=attrFiles.end();
		++cit)
	{
		bool bFileAccept = true;
		std::wstring uri = cit->second;
		std::wstring configFileName = uri;
		if (!m_backup->FileValid(uri))
		{
			bFileAccept = false;
			configFileName = m_backup->LoadBackup(uri);
		}
		if (configFileName.empty())
			continue;
		
		rapidxml::xml_document<> doc;
		rapidxml::file<> f(wtos(configFileName).c_str());
		doc.parse<0>(f.data());
		rapidxml::xml_node<> *configurationNode = doc.first_node("configuration", 0, false);
		if (!configurationNode)
		{
			return;
		}

		IList::SPtr pBlackList(new CompositeAction<CBlackListSingleton>(this, uri));
		LoadList(configurationNode->first_node("BlackList", 0, false), configFileName, pBlackList);
		groups[cit->first]=pBlackList;
		if (bFileAccept)
			m_backup->Save(configFileName, false);
	}
	attrFiles.clear();
}


//
//void CBlackListSingleton::LoadBlackList(const core::config_node& pBlackListRoot, std::wstring fileConfig)
//{
//	//LoadList(pBlackListRoot, fileConfig, m_pBlackList);
//}
//
//void CBlackListSingleton::LoadBlackList(const core::config_node& pBlackListRoot, std::wstring fileConfig, IList::SPtr _elem)
//{
//	//LoadList(pBlackListRoot, fileConfig, _elem);
//}

void CBlackListSingleton::load(const std::wstring& fileConfig,  IList::SPtr _elem, IList::BlackListGroups& blackListGroups)
{
	rapidxml::xml_document<> doc;
	rapidxml::file<> f(wtos(fileConfig).c_str());
	doc.parse<0>(f.data());
	rapidxml::xml_node<> *configurationNode = doc.first_node("configuration", 0, false);
	if (!configurationNode)
	{
		return;
	}

	LoadBlackList(configurationNode->first_node("BlackList", 0, false), fileConfig, _elem);
	auto node = configurationNode->first_node("ManagedBlackList", 0, false);
	if (!node)
	{
		return;
	}
	LoadBlackLists(node, fileConfig, Utils::GetFolderNameFromURI(_elem->GetScriptName()), blackListGroups);
}

//void CBlackListSingleton::LoadBlackLists(
//	const core::config_node& pBlackListRoot, 
//	std::wstring fileConfig, 
//	const std::wstring& _root_folder,
//	IList::BlackListGroups& groups)
//{
	//std::map<std::wstring, std::wstring> attrFiles;
	////core::configuration config(fileConfig);
	//for (const auto& child : pBlackListRoot)
	//{
	//	std::wstring sFileURI;
	//	if (AddFolder(config.safe_get_config_attr<std::wstring>(child.second, L"file", L""),_root_folder,sFileURI))
	//		attrFiles[child.first] = sFileURI;
	//}

	//// extract black list files
	//for (std::map<std::wstring, std::wstring>::const_iterator cit = attrFiles.begin();
	//	cit!=attrFiles.end();
	//	++cit)
	//{
	//	bool bFileAccept = true;
	//	std::wstring uri = cit->second;
	//	std::wstring config_file_name = uri;
	//	if (!m_backup->FileValid(uri))
	//	{
	//		bFileAccept = false;
	//		config_file_name = m_backup->LoadBackup(uri);
	//	}
	//	if (config_file_name.empty())
	//		continue;
	//	
	//	//core::configuration config(config_file_name);

	//	IList::SPtr pBlackList(new CompositeAction<CBlackListSingleton>(this, uri));
	//	LoadList(config.get_config_node(L"BlackList"), config_file_name, pBlackList);
	//	groups[cit->first]=pBlackList;
	//	if (bFileAccept)
	//		m_backup->Save(config_file_name,false);
	//}
	//attrFiles.clear();
//}

/******************************* eof *************************************/