/************************************************************************/
/* Name     : TransitAgent\PhoneMasksSingleton.h                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 23 Aug 2010                                               */
/************************************************************************/
#pragma once
//#include "singleton.h"
#include "CompositeSingleton.h"
#include "CustomCfgSingleton.h"
#include "AliasListSingleton.h"
#include "..\Composite\PhoneMasks.h"

class CPhoneMasksSingleton : public CCompositeSingleton
{
public:
	CPhoneMasksSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder, CCustomCfgSingleton* customCfgSingleton, CAliasListSingleton* aliasListSingleton);
	~CPhoneMasksSingleton();

	void LoadPhoneMasks(
		const core::config_node& pPhoneMasksRoot, 
		std::wstring fileConfig, 
		IList::SPtr _elem,
		const IList::BlackListGroups& _blackListGroups);

	void LoadPhoneMasks(
		const core::config_node& pPhoneMasksRoot, 
		std::wstring fileConfig,
		const IList::BlackListGroups& _blackListGroups);

	void load (const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& _blackListGroups);

private:
	void LoadMasks(
		const core::config_node& pPhoneMasksRoot,
		std::wstring fileConfig,
		IList::SPtr _elem,
		const IList::BlackListGroups& _blackListGroups);

private:
	CCustomCfgSingleton* _customCfgSingleton{};
	CAliasListSingleton* _aliasListSingleton{};

};

/******************************* eof *************************************/