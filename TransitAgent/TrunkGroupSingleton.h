/************************************************************************/
/* Name     : TransitAgent\TruncGroupSingleton.h                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 8 Nov 2010                                                */
/************************************************************************/
#pragma once
#include "CompositeSingleton.h"

class CTrunkGroupSingleton: public CCompositeSingleton
{

};


/******************************* eof *************************************/