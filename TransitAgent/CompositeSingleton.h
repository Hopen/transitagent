/************************************************************************/
/* Name     : TransitAgent\CompositeSingleton.h                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 26 Aug 2010                                               */
/************************************************************************/
#pragma once
#include "Patterns/singleton.h"
#include "WinAPI/ProcessHelper.h"	
#include "Log/SystemLog.h"
#include "..\Composite\Composite.h"
#include "backuper.h"

class CCompositeSingleton
{
	class CSaver
	{
	public:
		CSaver(std::wstring* pString):m_pString(pString)
		{
			if (m_pString)
				m_sValue = *m_pString;
		}
		~CSaver()
		{
			if (m_pString)
				*m_pString = m_sValue;
		}
	private:
		std::wstring* m_pString;
		std::wstring  m_sValue;
	};

public:
	CCompositeSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder);
	virtual ~CCompositeSingleton() {};

	void load(const std::wstring&, IList::SPtr _elem, IList::BlackListGroups& _blackListGroups)
	{
		throw std::runtime_error("CCompositeSingleton: Can't load abstract class");
	}

	IList::SPtr GetInstance();
	bool AddFolder(const std::wstring& _filename, const std::wstring& _root, std::wstring& uri);
	StringSet& GetFoldersList() { return m_backup->GetFoldersList(); };
	bool IsItOurFile(const std::wstring& _uri);

	void refresh(const std::wstring&, IList::SPtr, IList::BlackListGroups& blackListGrpups);

	void CheckRemovedFile(std::function <void(const std::wstring&)> call_back);

private:
	boost::optional<std::wstring> CopyToTmp(const std::wstring& _filename) const;

protected:
	CSystemLog* _log{};
private:
	//mutable std::mutex _tmpFileReadMutex;
	mutable std::mutex _refreshMutex;
	std::wstring _tmpFileReadFolder;

protected:
	IList::SPtr m_pManagetMessages;
	IList::SPtr m_pPhoneMasks;
	IList::SPtr m_pCustomCfg;
	IList::SPtr m_pAliasList;
	IList::SPtr m_pProxyList;

	IList::SPtr ExtactBlackList(const std::wstring& _blacklists, const IList::BlackListGroups& _blackListGroups);
	//IList* GetCustomConfig();
	//IList* GetAliasList();

	std::auto_ptr<CBackUper> m_backup;

private:
	IList::SPtr m_pAgent;
	std::wstring m_sRootFolder;
};

/******************************* eof *************************************/