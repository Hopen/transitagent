/************************************************************************/
/* Name     : TransitAgent\enumarator.h                                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Jun 2014                                               */
/************************************************************************/
#pragma once

//#include <singleton.h>
#include <list>
#include <boost/thread.hpp>
#include "RunScriptRedirectCollector.h"


const int DEFAULT_MAX_INCOMING = 10000000;
const int DEFAULT_MAX_OUTCOMING = 10000000;
const int DEFAULT_MAX_SCRIPTS = 10000000;
const int DEFAULT_EXPIRATION_TIME = 0;

class CEnumerator //: public singleton <CEnumerator>
{
private:
	//friend class singleton<CEnumerator>;

public:
	CEnumerator();
	virtual ~CEnumerator();

private:
	typedef std::list <ObjectId> CallIDList;
	typedef CallIDList ScriptIDList;
	/*static*/ CallIDList   m_incoming;
	/*static*/ ScriptIDList m_scripts;
	/*static*/ CallIDList   m_outcoming;

	/*static*/ int    m_max_incoming;
	/*static*/ int    m_max_outcoming;
	/*static*/ int    m_max_scripts;
	/*static*/ time_t m_expiration_time;

	boost::mutex m_outcoming_mutex;
	boost::mutex m_incoming_mutex;
	boost::mutex m_scripts_mutex;

public:
	/*static*/ CallIDList   GetIncoming () { return m_incoming  ;}
	/*static*/ ScriptIDList GetScripts  () { return m_scripts   ;}
	/*static*/ CallIDList   GetOutcoming() { return m_outcoming ;}

	/*static*/ bool AddIncoming  (const ObjectId& _callID);
	/*static*/ bool AddScript(const ObjectId& _scriptID);
	/*static*/ bool AddOutcoming(const ObjectId& _callID);

	/*static*/ bool RemoveIncoming(const ObjectId& _callID);
	/*static*/ bool RemoveScript(const ObjectId& _scriptID);
	/*static*/ int  RemoveScripts(const ObjectId& _dispatcher);
	/*static*/ bool RemoveOutcoming(const ObjectId& _callID);

	/*static*/ int MaxIncomingCount  () { return m_max_incoming  ; }
	/*static*/ int MaxOutcomingCount () { return m_max_outcoming ; }
	/*static*/ int MaxScriptsCount   () { return m_max_scripts   ; }

	/*static*/ bool CanMakeIncomingCall();
	/*static*/ bool CanMakeOutcomingCall();
	/*static*/ bool CanRunScript();
	/*static*/ bool HasLicenceExpired();

	/*static*/ void SetLicenceParam(const int& _max_inc, const int& _max_scripts, const int& _max_out, const time_t& _exp_time);
};
/******************************* eof *************************************/