/************************************************************************/
/* Name     : TransitAgent\IPOBoundSingleton.h                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 13 Nov 2014                                               */
/************************************************************************/
#pragma once
#include "CompositeSingleton.h"
#include "..\Composite\IPOBound.h"

class CIPOBoundSingleton : public CCompositeSingleton
{
public:
	CIPOBoundSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder);
	~CIPOBoundSingleton();

	void LoadProxyList(const core::config_node& pProxyListRoot, std::wstring fileConfig);
	void LoadProxyList(const core::config_node& pProxyListRoot, std::wstring fileConfig, IList::SPtr _elem);
	void load(const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& _blackListGroups);

	IList::SPtr GetInstance(){ return m_pProxyList; }
	IList::SPtr GetIPOBoundByName(const std::wstring& _outboundName);

private:
	void LoadList(const core::config_node& pProxyListRoot, std::wstring fileConfig, IList::SPtr _elem);

private:
	typedef std::map<std::wstring, IList::SPtr> ProxyListGroup;
	ProxyListGroup m_groups;
};

/******************************* eof *************************************/