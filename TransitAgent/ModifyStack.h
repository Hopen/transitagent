/************************************************************************/
/* Name     : TransitAgent\ModifyStack.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 28 Set 2010                                               */
/************************************************************************/
#pragma once

#include <vector>
#include <algorithm>
#include <set>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread\lock_types.hpp>

#include "..\Composite\Composite.h"
#include "..\Trunk\Group.h"
#include "Log/SystemLog.h"
#include "Timer.h"

namespace boostex
{

	class unique_lock_ex : boost::unique_lock<boost::mutex>
	{
	public:
		unique_lock_ex(boost::mutex& mutex, LPCTSTR _module):unique_lock(mutex)
		{
			m_name = _module;
			m_log->LogStringModule(LEVEL_FINEST, L"MUTEX lock", _module);
		}
		~unique_lock_ex()
		{
			m_log->LogStringModule(LEVEL_FINEST, L"MUTEX unlock", m_name.c_str());
		}

	private:
		singleton_auto_pointer<CSystemLog> m_log;
		std::wstring m_name;
	};

};

using TransiteCallbackFunc = std::function <void()>;
using SimpleCallbackFunc = std::function <void(const std::wstring&)>;

class CMonitorStack
{
	class CFileNameMatch
	{
	public:
		CFileNameMatch(const std::wstring& _uri):m_sURI(_uri.c_str())
		{

		}
		bool operator()(std::wstring _filename)
		{
			if (!m_sURI.CompareNoCase(_filename.c_str()))
				return true;
			return false;
		}
	private:
		CAtlString m_sURI;
	};

	typedef	std::set<CLIENT_ADDRESS>   AddressUniqList;
	class CTimeSlotsLoadBeholder
	{
		mutable std::mutex m_mutex;
	public:
		void LockFile(const std::wstring& _filename)
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			for (AddressUniqList::const_iterator cit=m_servers.begin();cit!=m_servers.end();++cit)
			{
				m_connections.push_back(TConnection(_filename,*cit));
			}
		}
		bool UnlockFile(const CLIENT_ADDRESS& _addr)
		{
			bool success = false;
			std::lock_guard<std::mutex> lock(m_mutex);
			for (Connections::const_iterator cit = m_connections.begin();cit!=m_connections.end();++cit)
			{
				if (cit->server == _addr)
				{
					m_connections.erase(cit);
					success = true;
					break;
				}
			}

			return success;
		}

		void AddServer(const CLIENT_ADDRESS& _addr)
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			m_servers.insert(_addr);
		}
		bool isNewServer(const CLIENT_ADDRESS& _addr)const
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			auto cit = m_servers.find(_addr);
			return cit == m_servers.end();
		}
		void DelServer(const CLIENT_ADDRESS& _addr)
		{
			UnlockFile(_addr);

			std::lock_guard<std::mutex> lock(m_mutex);
			m_servers.erase(_addr);
		}
		bool IsFileLockForTimeSlotRequest(const std::wstring& _filename)
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			for (UINT i=0;i<m_connections.size();++i)
			{
				if (m_connections[i].filename == _filename)
					return true;
			}
			return false;
		}

		void clear()
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			m_servers.clear();
			m_connections.clear();
		}
	private:
		struct TConnection
		{
			TConnection(const std::wstring _filename, const CLIENT_ADDRESS& _addr):filename(_filename),server(_addr)
			{

			}
			std::wstring   filename;
			CLIENT_ADDRESS server;
		};

		typedef std::vector<TConnection> Connections;
		Connections m_connections;
		AddressUniqList m_servers;
	};

public:
	CMonitorStack(CSystemLog* log);
	~CMonitorStack();

	void Init(
		IList::updatefunc updateList,
		IList::updatefunc updateGroup,
		TransiteCallbackFunc transitCallback,
		unsigned int uHttpCheckTimeOutSec);

	void Loop1();
	void Loop2();

	void AddFileName(std::wstring _filename);

	void AddServer(const CLIENT_ADDRESS& _addr);
	void DelServer(const CLIENT_ADDRESS& _addr);
	bool isNewServer(const CLIENT_ADDRESS& _addr);
	bool FreeFile(const CLIENT_ADDRESS& _addr);

private:
	bool IsVerifyFile(const std::wstring& _uri);
	bool IsLockForTimeSlotsRequest(const std::wstring& _filename);

	void ClearServers();

private:
	CSystemLog* m_log{};
	int counter;

	typedef std::vector <std::wstring> FileList;

	boost::shared_ptr<CTimer> m_timer1, m_timer2;
	boost::shared_ptr<boost::asio::io_service> m_io;


	boost::mutex m_mutex;
	bool m_bActive;


	FileList m_files;
	IList::updatefunc m_updateListCallbackFunc;
	IList::updatefunc m_updateTruncGroupCallbackFunc;

	TransiteCallbackFunc m_transitCallBack;

	unsigned int m_uHttpCheckTimeOutSec;


	CTimeSlotsLoadBeholder m_tsbeholder;
};

/******************************* eof *************************************/