/************************************************************************/
/* Name     : TransitAgent\CompositeSingleton.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 26 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"

#include <mutex>
#include <boost/filesystem.hpp>

//#include "ce_xml.hpp"
#include "utils.h"
#include "CompositeSingleton.h"
#include "BlackListSingleton.h"
#include "CustomCfgSingleton.h"
#include "TransitAgent.h"
#include "AliasListSingleton.h"

CCompositeSingleton::CCompositeSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder)
	: _log{ log }
	, _tmpFileReadFolder{ tmpFileReadFolder }
{
	m_pAgent = std::make_shared<CompositeAction<CCompositeSingleton>>(this, std::wstring(L""));
	m_backup = std::auto_ptr<CBackUper>(CBackUper::GetInstance());
};

IList::SPtr CCompositeSingleton::GetInstance()
{
	//m_log = std::auto_ptr<CSystemLog>(CSystemLog::GetInstance());
	m_pAgent->add(m_pCustomCfg);
	//m_pAgent->add(m_pBlackList);
	m_pAgent->add(m_pManagetMessages);
	m_pAgent->add(m_pPhoneMasks);
	m_pAgent->add(m_pProxyList);
	return m_pAgent;
}

static IList::SPtr GetBlackList(const std::wstring& _name, const IList::BlackListGroups& _blackListGroups)
{
	auto cit = _blackListGroups.find(_name);
	if (cit != _blackListGroups.end())
		return cit->second;
	return IList::SPtr((IList*)(NULL));
}

IList::SPtr CCompositeSingleton::ExtactBlackList(const std::wstring& _blacklists, const IList::BlackListGroups& _blackListGroups)
{
	IList::SPtr ParentBlackList(new CompositeAction<CCompositeSingleton>(this, std::wstring(L"")));
	LPCWSTR p = _blacklists.c_str(), q;
	std::wstring sParamName, sParamVal;
	int k = 0;

	while (*p)
	{
		q = wcschr(p, L';');
		if (!q)
		{
			sParamVal = Utils::trim(std::wstring(p),L';');
		}
		else
		{
			sParamVal = Utils::trim(std::wstring(p, q - p),L';');
		}
		if (!sParamVal.empty())
		{
			if (IList::SPtr pBlackList = GetBlackList(sParamVal, _blackListGroups))
				ParentBlackList->add(pBlackList);
		}
		if (!q)
		{
			break;
		}
		p = q + 1;
	}
	return ParentBlackList;
	//return IList::SPtr((IList*)NULL);
}
bool CCompositeSingleton::AddFolder(const std::wstring& _filename, const std::wstring& _root, std::wstring& uri)
{
	return m_backup->AddFolder(_filename, _root, uri);
}

bool CCompositeSingleton::IsItOurFile(const std::wstring& _uri)
{
	return m_backup->Find(_uri);
	
}

void CCompositeSingleton::refresh(const std::wstring& _filename, IList::SPtr pParent, IList::BlackListGroups& blackListGrpups)
{
	std::lock_guard<std::mutex> lock(_refreshMutex);

	auto tmpFile = CopyToTmp(_filename);
	if (!tmpFile.is_initialized())
		return;

	std::wstring configFileName{ *tmpFile };

	bool bFileAccept = true;

	_log->LogString(LEVEL_INFO, L"Validate config: %s", configFileName);

	if (!m_backup->FileValid(configFileName))
	{
		bFileAccept = false;
		auto cachedFileName = m_backup->LoadBackup(_filename);
		if (cachedFileName.empty())
		{
			_log->LogString(LEVEL_INFO, L"Error caching config: %s", _filename);
		}
		configFileName = cachedFileName;
	}
	if (configFileName.empty())
	{
		_log->LogString(LEVEL_WARNING, L"Error refreshing config: %s", _filename);
		return;
	}

	if (bFileAccept)
	{
		_log->LogString(LEVEL_INFO, L"Config validated: %s", configFileName);
		m_backup->Save(configFileName, false);
	}

	pParent->clear();

	try
	{
		_log->LogString(LEVEL_INFO, L"Load: %s", configFileName);
		pParent->load(configFileName, pParent, blackListGrpups);
	}
	catch (core::configuration_error& e)
	{
		_log->LogString(LEVEL_WARNING, L"Failed to load: %s", configFileName);
	}
}

void CCompositeSingleton::CheckRemovedFile(std::function <void (const std::wstring&)> call_back)
{
		StringSet sFileList = m_backup->GetFilesList();
		for (StringSet::const_iterator cit = sFileList.begin();cit!=sFileList.end();++cit)
		{
			std::wstring _uri = *cit;
			if (_uri.find(L"http://") != 0)
				continue;
			if (m_backup->MatchFiles(_uri))
			{
				//CTransitAgent* pInst = CTransitAgent::GetInstance();
				//if (pInst)
				//	pInst->Modify(_uri);

				call_back(_uri);
			}
	
		}
}

//IList* CCompositeSingleton::GetCustomConfig()
//{
//	return CCustomCfgSingleton::GetCustomConfig().get();
//}
//
//IList* CCompositeSingleton::GetAliasList()
//{
//	return CAliasListSingleton::GetAliasList().get();
//}

boost::optional<std::wstring> CCompositeSingleton::CopyToTmp(const std::wstring& _filename) const
{
	WCHAR sNewURI[1024];
	PathCombineW(sNewURI, sNewURI, _tmpFileReadFolder.c_str());
	PathCombineW(sNewURI, sNewURI, Utils::GetFileNameFromURI(_filename).c_str());

	//std::lock_guard<std::mutex> lock(_tmpFileReadMutex);
	_log->LogString(LEVEL_INFO, L"Moving config to file: %s", sNewURI);
	try
	{
		boost::filesystem::copy_file(_filename, sNewURI, boost::filesystem::copy_option::overwrite_if_exists);
	}
	catch (const std::exception& error)
	{
		_log->LogString(LEVEL_WARNING, L"Failed to move config to file: '%s', error: %s", sNewURI, stow(error.what()));
	}

	if (!boost::filesystem::exists(boost::filesystem::path(sNewURI)))
	{
		return boost::none;
	}

	_log->LogString(LEVEL_INFO, L"Moving config to file: %s successful", sNewURI);

	return sNewURI;
}
/******************************* eof *************************************/