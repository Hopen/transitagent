/************************************************************************/
/* Name     : TransitAgent\RunScriptRedirectCollector.h                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 25 Nov 2010                                               */
/************************************************************************/

#pragma once
#include <map>

typedef __int64 ObjectId;

class CRedirectCollector
{
public:
	void add(const ObjectId& _from, const ObjectId& _to)
	{
		m_collector[_from] = _to;
	}
	bool find(const ObjectId& _from, ObjectId& _to)
	{
		RedirectList::const_iterator cit = m_collector.find(_from);
		if (cit != m_collector.end())
		{
			_to = cit->second;
			return true;
		}
		return false;

	}
private:
	typedef std::map<ObjectId,ObjectId> RedirectList;

	RedirectList m_collector;
};

/******************************* eof *************************************/