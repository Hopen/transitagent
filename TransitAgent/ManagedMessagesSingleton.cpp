/************************************************************************/
/* Name     : TransitAgent\ManagetMessagesSingleton.cpp                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 23 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"
//#include "ce_xml.hpp"
#include "utils.h"
#include "ManagedMessagesSingleton.h"
//#include "BlackListSingleton.h"

CManagedMessagesSingleton::CManagedMessagesSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder)
	: CCompositeSingleton{ log , tmpFileReadFolder }
{
	m_pManagetMessages = std::make_shared<CompositeAction<CManagedMessagesSingleton>>(this, std::wstring(CProcessHelper::GetCurrentModuleName()) + L".config");
}

CManagedMessagesSingleton::~CManagedMessagesSingleton()
{}

void CManagedMessagesSingleton::LoadMasks(
	const core::config_node& pManagetMessagesRoot, 
	std::wstring fileConfig, 
	IList::SPtr _elem,
	const IList::BlackListGroups& _blackListGroups)
{
	//for(mask = pManagetMessagesRoot.begin(); mask != pManagetMessagesRoot.end(); mask++)

	core::configuration config(fileConfig);
	for (const auto& child : pManagetMessagesRoot)
	{
		//if (!mask.name().compare(L"include"))
		if (!child.first.compare(L"include") || !child.first.compare(L"<xmlcomment>"))
		{
			// skip including file
			continue;
		}

		std::wstring sMessage              = config.safe_get_config_attr<std::wstring>(child.second, L"Name", L"");//mask.attr( L"Name"                 );
		std::wstring sScript               = config.safe_get_config_attr<std::wstring>(child.second, L"ScriptName", L"");//mask.attr( L"ScriptName"           );
		std::wstring sBlackList            = config.safe_get_config_attr<std::wstring>(child.second, L"blacklist", L"");//mask.attr( L"blacklist"            );
		std::wstring sMonitorDisplayedName = config.safe_get_config_attr<std::wstring>(child.second, L"MonitorDisplayedName", L"");//mask.attr( L"MonitorDisplayedName" );

		CManagedMessages *pMM = new CManagedMessages(sMessage, sScript, L"", sMonitorDisplayedName);

		//LiteXML::TElem		mask2;
		for (const auto& child2 : child.second)
		{
			pMM->AddParam(
				config.safe_get_config_attr<std::wstring>(child2.second, L"Name", L""),
				config.safe_get_config_attr<std::wstring>(child2.second, L"value", L"")
				);
			
		}
		//for (mask2 = mask.begin(); mask2 != mask.end(); mask2++)
		//{
		//	pMM->AddParam(mask2.attr(L"Name"), mask2.attr(L"value"));
		//}
		if (!sBlackList.empty())
		{
			pMM->AddBlackList(ExtactBlackList(sBlackList, _blackListGroups));
		}
	
		IList::SPtr Caller(pMM);
		//m_pManagetMessages->add(Caller);
		_elem->add(Caller);

	}
}

void CManagedMessagesSingleton::LoadManagedMessages(
	const core::config_node& pManagetMessagesRoot, 
	std::wstring fileConfig, 
	IList::SPtr _elem,
	const IList::BlackListGroups& _blackListGroups)
{
	LoadMasks(pManagetMessagesRoot, fileConfig, _elem, _blackListGroups);

	core::configuration config(fileConfig);
	//LiteXML::TElem		mask;
	std::wstring sCurrentRootFolder = Utils::GetFolderNameFromURI(_elem->GetScriptName());// = m_backup->GetLocalRootFolder();
	if (sCurrentRootFolder.empty())
		sCurrentRootFolder = Utils::GetFolderNameFromURI(static_cast<std::wstring>(CProcessHelper::GetCurrentModuleName()));
	//for(mask = pManagetMessagesRoot.begin(); mask != pManagetMessagesRoot.end(); mask++)
	for (const auto& child : pManagetMessagesRoot)
	{
		//if (!mask.name().compare(L"include"))
		if (!child.first.compare(L"include"))
		{
			std::wstring sFileURI, config_file;
			if (AddFolder(/*mask.attr(L"file")*/config.safe_get_config_attr<std::wstring>(child.second, L"file", L""),sCurrentRootFolder, sFileURI))
			{
				config_file = sFileURI;
				bool bFileAccept = true;
				if (!m_backup->FileValid(sFileURI))
				{
					bFileAccept = false;
					config_file = m_backup->LoadBackup(sFileURI).empty();
				}
				if (!bFileAccept && config_file.empty())
					continue;

				//CXmlConfigSingleton::LoadFromFile(ATL::CString(sFileURI.c_str()));
				core::configuration newConfig(config_file);
				IList::SPtr child(new CompositeAction<CManagedMessagesSingleton>(this, sFileURI));
				_elem->add(child);
				LoadManagedMessages(newConfig.get_config_node(L"ManagedMessages"), config_file,child, _blackListGroups);
				if (bFileAccept)
					m_backup->Save(config_file,false);
			}
		}

	}
}

void CManagedMessagesSingleton::LoadManagedMessages(
	const core::config_node& pManagetMessagesRoot, 
	std::wstring fileConfig, 
	const IList::BlackListGroups& blackListGroups)
{
	LoadManagedMessages(pManagetMessagesRoot, fileConfig, m_pManagetMessages, blackListGroups);
}

void CManagedMessagesSingleton::load(const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& blackListGroups)
{
	//m_log->LogString(LEVEL_INFO, L"Load managed messages list");
	core::configuration config(fileConfig);
	LoadManagedMessages(config.get_config_node(L"ManagedMessages"), fileConfig, _elem, blackListGroups);
}

/******************************* eof *************************************/