/************************************************************************/
/* Name     : TransitAgent\TrunkGroupCollector.h                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 12 Oct 2010                                               */
/************************************************************************/
#pragma once
#include <vector>
#include "Patterns/singleton.h"
#include "..\Trunk\Group.h"
#include "IPOBoundSingleton.h"

#include "backuper.h"
class CTrunkGroupCollector
{
public:
	CTrunkGroupCollector(CIPOBoundSingleton* iPOBoundSingleton);
	~CTrunkGroupCollector();

	void LoadTrunkGroup(const core::config_node& pTrunkGroupRoot, std::wstring fileConfig);
	CAbstractTrunk/*CTrunkGroup*/::ITrunkGroup GetInstance(){ return m_groups; }
	void AddTimeSlot(const std::wstring& sGroupName, 
		                    const std::wstring& sBoard,
		                    const std::wstring& sTimeSlot,
		                    const ULONGLONG& ullAddress);
	bool refresh(const std::wstring& _filename, CAbstractTrunk/*CTrunkGroup*/::ITrunkGroup pParent, CAbstractTrunk::truck_group_callback call_back);

private:
	void LoadTrunkGroup(const core::config_node& pTrunkGroupRoot, std::wstring fileConfig, CAbstractTrunk/*CTrunkGroup*/::ITrunkGroup pElem);

	void LoadGroup(const core::config_node& pTrunkGroupRoot, std::wstring fileConfig, CAbstractTrunk/*CTrunkGroup*/::ITrunkGroup pElem);
	CBackUper* m_backup;
	bool AddFolder(const std::wstring& _filename, const std::wstring& _root, std::wstring& uri);

private:
	CIPOBoundSingleton* _iPOBoundSingleton{};
	CAbstractTrunk::ITrunkGroup m_groups;
	std::mutex m_mutex;
};


/******************************* eof *************************************/