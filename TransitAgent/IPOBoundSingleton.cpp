/************************************************************************/
/* Name     : TransitAgent\IPOBoundSingleton.cpp                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 13 Nov 2014                                               */
/************************************************************************/
#include "stdafx.h"
//#include "ce_xml.hpp"
#include "utils.h"
#include "IPOBoundSingleton.h"

static E_SORT_METHOD MethodByName(const std::wstring& _method)
{
	const CAtlString M_CIRCLE = L"Circle";
	const CAtlString M_LINEAR = L"Linear";
	const CAtlString M_RANDOM = L"Random";

	if (!M_RANDOM.CompareNoCase(_method.c_str()))
		return SM_RANDOM;
	if (!M_CIRCLE.CompareNoCase(_method.c_str()))
		return SM_CIRCLE;

	return SM_LINEAR;
}

CIPOBoundSingleton::CIPOBoundSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder)
	: CCompositeSingleton{ log , tmpFileReadFolder }
{
	m_pProxyList = std::make_shared<CompositeAction<CIPOBoundSingleton>>(this, std::wstring(CProcessHelper::GetCurrentModuleName()) + L".config");
}

CIPOBoundSingleton::~CIPOBoundSingleton()
{

}


//void CIPOBoundSingleton::LoadList(LiteXML::TElem pProxyListRoot, IList::SPtr _elem, const std::wstring& sName, const std::wstring& sMethod)
//{
//	LiteXML::TElem		mask;
//	for (mask = pProxyListRoot.begin(); mask != pProxyListRoot.end(); mask++)
//	{
//		std::wstring sIp       = mask.attr(L"Ip");
//		std::wstring sPort     = mask.attr(L"Port");
//		std::wstring sProtocol = mask.attr(L"Protocol");
//
//		IList::SPtr Proxy(new CIPOBound(sIp, sPort, sProtocol, sName, sMethod));
//		_elem->add(Proxy);
//	}
//}

void CIPOBoundSingleton::LoadList(const core::config_node& pProxyListRoot, std::wstring fileConfig, IList::SPtr _elem)
{
	//LiteXML::TElem		mask;
	core::configuration config(fileConfig);
	for (const auto& child : pProxyListRoot)
	//for (mask = pProxyListRoot.begin(); mask != pProxyListRoot.end(); mask++)
	{
		std::wstring sIp       = config.safe_get_config_attr<std::wstring>(child.second, L"Ip", L""); //mask.attr(L"Ip");
		std::wstring sPort     = config.safe_get_config_attr<std::wstring>(child.second, L"Port", L""); //mask.attr(L"Port");
		std::wstring sProtocol = config.safe_get_config_attr<std::wstring>(child.second, L"Protocol", L""); //mask.attr(L"Protocol");

		IList::SPtr Proxy(new CIPOBound(sIp, sPort, sProtocol));
		_elem->add(Proxy);
	}
}

//void CIPOBoundSingleton::LoadProxyList(LiteXML::TElem pProxyListRoot, const std::wstring& sName, const std::wstring& sMethod)
//{
//	LoadList(pProxyListRoot, m_pProxyList, sName, sMethod);
//}
//
//void CIPOBoundSingleton::LoadProxyList(LiteXML::TElem pProxyListRoot, IList::SPtr _elem, const std::wstring& sName, const std::wstring& sMethod)
//{
//	LoadList(pProxyListRoot, _elem, sName, sMethod);
//}

void CIPOBoundSingleton::load(const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& blackListGroups)
{
	//LoadProxyList(CXmlConfigSingleton::instance().subnode(L"IPOutBound"), _elem);
	//LiteXML::TElem		mask;
	//for (mask = CXmlConfigSingleton::instance().begin(); mask != CXmlConfigSingleton::instance().end(); mask++)
	//{
	//	if (CAtlString(mask.name().c_str()).CompareNoCase(L"IPOutBound"))
	//		continue;
	//	std::wstring sName = mask.attr(L"name");
	//	std::wstring sMethod = mask.attr(L"method");

	//	LoadProxyList(mask, sName, sMethod);
	//}
	m_groups.clear();
	core::configuration config(fileConfig);
	LoadProxyList(config.get_root_node(), fileConfig);
}


void CIPOBoundSingleton::LoadProxyList(const core::config_node& pProxyListRoot, std::wstring fileConfig)
{
	//LiteXML::TElem	elem;
	core::configuration config(fileConfig);
	//for (elem = pProxyListRoot.begin(); elem != pProxyListRoot.end(); elem++)
	for (const auto& child : pProxyListRoot)
	{
		if (CAtlString(child.first.c_str()).CompareNoCase(L"IPOutBound"))
			continue;
		std::wstring sName = config.safe_get_config_attr<std::wstring>(child.second, L"Name", L""); //elem.attr(L"Name");
		std::wstring sMethod = config.safe_get_config_attr<std::wstring>(child.second, L"Method", L""); //elem.attr(L"Method"); // ToDo!!!

		IList::SPtr pProxyList(new CompositeAction<CIPOBoundSingleton>(this, std::wstring(CProcessHelper::GetCurrentModuleName()) + L".config", MethodByName(sMethod)));

		LoadProxyList(child.second, fileConfig, pProxyList);
		pProxyList->sort();
		
		m_groups[sName] = pProxyList;
	}
}

void CIPOBoundSingleton::LoadProxyList(const core::config_node& pProxyListRoot, std::wstring fileConfig, IList::SPtr _elem)
{
	LoadList(pProxyListRoot, fileConfig, _elem);
}

IList::SPtr CIPOBoundSingleton::GetIPOBoundByName(const std::wstring& _outboundName)
{
	ProxyListGroup::const_iterator cit = m_groups.find(_outboundName);
	if (cit != m_groups.end())
		return cit->second;
	return IList::SPtr((IList*)(NULL));
}

//void CIPOBoundSingleton::OrderList(const std::wstring& sMethod, IList::SPtr _rootElem)
//{
//	if (!M_RANDOM.CompareNoCase(sMethod))
//		std::random_shuffle(_rootElem->  .begin(), m_ActiveTimeSlots.end());
//}
/******************************* eof *************************************/