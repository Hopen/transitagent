/************************************************************************/
/* Name     : TransitAgent\DebugListCollector.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 28 Oct 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "DebugListCollector.h"

CDebugListCollector::CDebugListCollector()
{
	m_pDebugList = std::make_shared<CompositeAction<DummyLoader>>(&dummyLoader, L"");
}

void CDebugListCollector::AddList(const std::wstring& _requestid, const std::wstring& _runrequestid, const std::wstring& _origination, const std::wstring& _destination, const CMessage& _message, CLIENT_ADDRESS * _from/*, std::wstring& sErrDescript */)
{
	IList::SPtr _elem(new CDebugList(_origination,_destination,_requestid,_runrequestid,_message,_from));
	m_pDebugList->add(_elem);
}

void CDebugListCollector::RemoveListByID(const std::wstring& _requestid)
{
	m_pDebugList->remove(_requestid);
}

void CDebugListCollector::RemoveListByAddress(const DWORD& _address)
{
	m_pDebugList->remove(_address);
}

void CDebugListCollector::Remove(CDebugList* pItem)
{
	RemoveListByAddress(pItem->GetAddress());
}

void CDebugListCollector::load(const std::wstring& /*fileConfig*/, IList::SPtr /*_elem*/, IList::BlackListGroups& blackListGroups)
{
	throw std::runtime_error("CDebugListCollector::load: Non implemented");
}

void CDebugListCollector::refresh(const std::wstring& /*fileConfig*/, IList::SPtr /*_elem*/, IList::BlackListGroups& blackListGroups)
{
	throw std::runtime_error("CDebugListCollector::refresh: Non implemented");
}

/******************************* eof *************************************/