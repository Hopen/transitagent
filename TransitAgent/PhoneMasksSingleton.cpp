/************************************************************************/
/* Name     : TransitAgent\PhoneMasksSingleton.cpp                      */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 23 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"
//#include "ce_xml.hpp"
#include "utils.h"
#include "PhoneMasksSingleton.h"

CPhoneMasksSingleton::CPhoneMasksSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder, CCustomCfgSingleton* customCfgSingleton, CAliasListSingleton* aliasListSingleton)
	: CCompositeSingleton{ log , tmpFileReadFolder }
	, _customCfgSingleton{ customCfgSingleton }
	, _aliasListSingleton{ aliasListSingleton }
{
	m_pPhoneMasks = std::make_shared<CompositeAction<CPhoneMasksSingleton>>(this, std::wstring(CProcessHelper::GetCurrentModuleName()) + L".config");
}

CPhoneMasksSingleton::~CPhoneMasksSingleton()
{

}

void CPhoneMasksSingleton::LoadMasks(
	const core::config_node& pPhoneMasksRoot, 
	std::wstring fileConfig, 
	IList::SPtr _elem,
	const IList::BlackListGroups& _blackListGroups)
{
	//LiteXML::TElem		mask;
	//for(mask = pPhoneMasksRoot.begin(); mask != pPhoneMasksRoot.end(); mask++)
	core::configuration config(fileConfig);
	for (const auto& child : pPhoneMasksRoot)
	{
		//if (!mask.name().compare(L"include"))
		if (!child.first.compare(L"include") || !child.first.compare(L"<xmlcomment>"))
		{
			// skip including file
			continue;
		}

		std::wstring sOrigination;
		std::wstring sDestination;
		std::wstring sScript;
		std::wstring sMonitorDisplayedName;
		std::wstring sBlackList;
		std::wstring sRegion;

		IList::ParamsList params;
		if (child.second.empty())
		{
			continue;
		}

		auto cit = child.second.begin();
		for (const auto& item : cit->second)
		{
			if (item.first == L"Origination")
			{
				sOrigination = item.second.get_value(L"");
			}
			else if (item.first == L"Destination")
			{
				sDestination = item.second.get_value(L"");
			}
			else if (item.first == L"ScriptName")
			{
				sScript = item.second.get_value(L"");
			}
			else if (item.first == L"MonitorDisplayedName")
			{
				sMonitorDisplayedName = item.second.get_value(L"");
			}
			else if (item.first == L"blacklist")
			{
				sBlackList = item.second.get_value(L"");
			}
			else if (item.first == L"RegionName")
			{
				sRegion = item.second.get_value(L"");
			}
			else
			{
				params.emplace(item.first, item.second.get_value(L""));
			}
		}

		//std::wstring sOrigination			= config.safe_get_config_attr<std::wstring>(child.second, L"Origination", L"");
		//std::wstring sDestination			= config.safe_get_config_attr<std::wstring>(child.second, L"Destination", L"");
		//std::wstring sScript				= config.safe_get_config_attr<std::wstring>(child.second, L"ScriptName", L"");
		//std::wstring sMonitorDisplayedName	= config.safe_get_config_attr<std::wstring>(child.second, L"MonitorDisplayedName", L"");
		//std::wstring sBlackList				= config.safe_get_config_attr<std::wstring>(child.second, L"blacklist",L"");
		//std::wstring sRegion				= config.safe_get_config_attr<std::wstring>(child.second, L"RegionName", L"");

		CPhoneMasks* pPM = NULL;
		if (!sRegion.empty())
		{
			pPM = new CPhoneMasksRegion(sRegion, sOrigination, sDestination, sMonitorDisplayedName, std::move(params));
		}
		else
		{
			pPM = new CPhoneMasks(sScript, sOrigination, sDestination, sMonitorDisplayedName, std::move(params));
		}

		if (!pPM)
			continue;
		
		if (!sBlackList.empty())
		{
			pPM->AddBlackList(ExtactBlackList(sBlackList, _blackListGroups));
		}

		pPM->AddCustomConfig(_customCfgSingleton->GetCustomConfig().get());
		pPM->AddAliasList(_aliasListSingleton->GetAliasList().get());

		IList::SPtr Caller(pPM);
		//m_pPhoneMasks->add(Caller);
		_elem->add(Caller);
	}
}

void CPhoneMasksSingleton::LoadPhoneMasks(
	const core::config_node& pPhoneMasksRoot, 
	std::wstring fileConfig, 
	IList::SPtr _elem,
	const IList::BlackListGroups& _blackListGroups)
{
	LoadMasks(pPhoneMasksRoot, fileConfig, _elem, _blackListGroups);

	//LiteXML::TElem		mask;
	core::configuration config(fileConfig);
	std::wstring sCurrentRootFolder = Utils::GetFolderNameFromURI(_elem->GetScriptName());// = m_backup->GetLocalRootFolder();
	if (sCurrentRootFolder.empty()) // default root folder
		sCurrentRootFolder = Utils::GetFolderNameFromURI(static_cast<std::wstring>(CProcessHelper::GetCurrentModuleName()));

	//for(mask = pPhoneMasksRoot.begin(); mask != pPhoneMasksRoot.end(); mask++)
	for (const auto& child : pPhoneMasksRoot)
	{
		//if (!mask.name().compare(L"include"))
		if (!child.first.compare(L"include"))
		{
			//std::wstring sFileName = AddFileName(mask.attr(L"file"));
			std::wstring sFileURI, config_file;
			if (AddFolder(/*mask.attr(L"file")*/config.safe_get_config_attr<std::wstring>(child.second, L"file", L""),sCurrentRootFolder,sFileURI))
			{
				config_file = sFileURI;
				bool bFileAccept = true;
				if (!m_backup->FileValid(sFileURI))
				{
					bFileAccept = false;
					config_file = m_backup->LoadBackup(sFileURI);
				}
				if (!bFileAccept && config_file.empty())
					continue;

				//CXmlConfigSingleton::LoadFromFile(ATL::CString(sFileURI.c_str()));
				core::configuration newConfig(config_file);

				IList::SPtr child(new CompositeAction<CPhoneMasksSingleton>(this, sFileURI));
				_elem->add(child);
				LoadPhoneMasks(newConfig.get_config_node(L"PhoneMasks"), config_file, child, _blackListGroups);
				if (bFileAccept)
					m_backup->Save(config_file,false);
			}
		}

	}
}

void CPhoneMasksSingleton::LoadPhoneMasks(
	const core::config_node& pPhoneMasksRoot, 
	std::wstring fileConfig, 
	const IList::BlackListGroups& _blackListGroups)
{
	//m_log->LogString(LEVEL_INFO, L"Load phone masks list");
	LoadPhoneMasks(pPhoneMasksRoot, fileConfig, m_pPhoneMasks, _blackListGroups);
}

void CPhoneMasksSingleton::load(const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& _blackListGroups)
{
	core::configuration config(fileConfig);
	LoadPhoneMasks(config.get_config_node(L"PhoneMasks"), fileConfig, _elem, _blackListGroups);
}

/******************************* eof *************************************/