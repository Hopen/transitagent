/************************************************************************/
/* Name     : TransitAgent\ModifyStack.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 27 May 2015                                               */
/************************************************************************/
#include "stdafx.h"

#include "ModifyStack.h"
#include "utils.h"

const boost::posix_time::seconds timer1(30);


#define MAXRETRIES  5
#define RETRYDELAY  250


CMonitorStack::CMonitorStack(CSystemLog* log)
	: m_log{ log }
	, m_bActive(false)
	, m_io(new boost::asio::io_service())
{
}

void CMonitorStack::Init(
	IList::updatefunc updateList,
	IList::updatefunc updateGroup,
	TransiteCallbackFunc transitCallback,
	unsigned int uHttpCheckTimeOutSec)
{
	m_updateListCallbackFunc = updateList;
	m_updateTruncGroupCallbackFunc = updateGroup;
	m_transitCallBack = transitCallback;
	m_uHttpCheckTimeOutSec = uHttpCheckTimeOutSec;

	// INIT TIMERS
	m_timer1.reset(new CTimer(m_io, [this]() {this->Loop1(); }, timer1));
	m_timer2.reset(new CTimer(m_io, [this]() {this->Loop2(); }, boost::posix_time::seconds(uHttpCheckTimeOutSec)));
	boost::thread t(boost::bind(&boost::asio::io_service::run, m_io));

	m_bActive = true;
}


CMonitorStack::~CMonitorStack()
{
	m_io->stop();
	m_timer1->CancelTimer();
	m_timer2->CancelTimer();
	m_timer1.reset();
	m_timer2.reset();
}

bool CMonitorStack::IsVerifyFile(const std::wstring& _uri)
{
	m_log->LogStringModule(LEVEL_INFO, L"CMonitorStack", L"Verify: %s", _uri.c_str());
	if (::PathFileExists(_uri.c_str()) == FALSE)
	{
		m_log->LogStringModule(LEVEL_WARNING, L"CMonitorStack", L"file does not exist");
		return true; // why true?
	}

	if (::GetFileAttributes(_uri.c_str()) & FILE_ATTRIBUTE_READONLY)
	{
		m_log->LogStringModule(LEVEL_WARNING, L"CMonitorStack", L"cannot read attributes file");
		return false;
	}

	HANDLE  hFile = INVALID_HANDLE_VALUE;
	DWORD   dwRetries = 0;
	BOOL    bSuccess = FALSE;
	DWORD   dwErr = 0;

	do
	{
		hFile = CreateFile(_uri.c_str(),
			GENERIC_READ,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
		if (INVALID_HANDLE_VALUE == hFile)
		{
			dwErr = GetLastError();

			if (ERROR_SHARING_VIOLATION == dwErr)
			{
				dwRetries += 1;
				Sleep(RETRYDELAY);
				continue;
			}
			else
			{
				// An error occurred.
				break;
			}
		}

		::CloseHandle(hFile);
		bSuccess = TRUE;
		break;
	} while (dwRetries < MAXRETRIES);

	if (bSuccess)
	{
		m_log->LogStringModule(LEVEL_INFO, L"CMonitorStack", L"Succeeded verifying file");
		return true;
	}

	m_log->LogStringModule(LEVEL_WARNING, L"CMonitorStack", L"Verify: false; GetLastError: %i", GetLastError());
	return false;
}

bool CMonitorStack::IsLockForTimeSlotsRequest(const std::wstring& _filename)
{
	//return m_tsbeholder.IsFileLockForTimeSlotRequest(_filename);
	if (m_tsbeholder.IsFileLockForTimeSlotRequest(_filename))
	{
		m_log->LogStringModule(LEVEL_FINEST, L"CMonitorStack", L"_filename: %s IsFileLockForTimeSlotRequest", _filename);
		return true;
	}

	return false;
}

void CMonitorStack::AddFileName(std::wstring _filename)
{
	CFileNameMatch functor(_filename);
	boostex::unique_lock_ex lock(m_mutex, L"push_stack");
	m_log->LogStringModule(LEVEL_FINEST, L"CMonitorStack", L"m_files size: %i", m_files.size());
	for (unsigned i = 0; i<m_files.size(); ++i)
	{
		m_log->LogStringModule(LEVEL_FINEST, L"CMonitorStack", L"monitoring filename: %s", m_files[i].c_str());
	}
	if (find_if(m_files.begin(), m_files.end(), functor) == m_files.end())
	{
		m_files.push_back(_filename);
	}
}

void CMonitorStack::AddServer(const CLIENT_ADDRESS& _addr)
{
	m_tsbeholder.AddServer(_addr);
}

void CMonitorStack::DelServer(const CLIENT_ADDRESS& _addr)
{
	m_tsbeholder.DelServer(_addr);
}

bool CMonitorStack::isNewServer(const CLIENT_ADDRESS& _addr)
{
	return m_tsbeholder.isNewServer(_addr);
}

//bool CMonitorStack::RequestServer(const CLIENT_ADDRESS& _addr)
//{
//	if (!m_tsbeholder.IsServerLockedForRequestCompleted(_addr))
//	{
//		m_log->LogStringModule(LEVEL_FINEST, L"CMonitorStack", L"lock server for request: 0x%p", _addr.GetClientID());
//		m_tsbeholder.LockServerForRequest(_addr);
//		return true;
//	}
//	return false;
//}
//
//void CMonitorStack::UnlockRequest(const CLIENT_ADDRESS& _addr)
//{
//	m_log->LogStringModule(LEVEL_FINEST, L"CMonitorStack", L"unlock server: 0x%p", _addr.GetClientID());
//	m_tsbeholder.UnlockFileRequest(_addr);
//}

bool CMonitorStack::FreeFile(const CLIENT_ADDRESS& _addr)
{
	return m_tsbeholder.UnlockFile(_addr);
}

void CMonitorStack::ClearServers()
{
	m_tsbeholder.clear();
}

void CMonitorStack::Loop1()
{
	if (!m_bActive)
	{
		m_log->LogStringModule(LEVEL_FINEST, L"CMonitorStack", L"Loop1 unactive");
		return;
	}
	m_log->LogStringModule(LEVEL_FINEST, L"CMonitorStack", L"========================Tick===================");
	if (m_files.size())
	{
		boostex::unique_lock_ex lock(m_mutex, L"Loop1");
		FileList newList;
		for (unsigned int i = 0; i<m_files.size(); ++i)
		{
			std::wstring sFileName = m_files[i];

			if (!IsVerifyFile(sFileName))
			{
				newList.push_back(sFileName);
				continue;
			}
			if (IsLockForTimeSlotsRequest(sFileName))
			{
				newList.push_back(sFileName);
				continue;
			}
			m_updateListCallbackFunc(sFileName);
			if (m_updateTruncGroupCallbackFunc(sFileName))
			{
				m_tsbeholder.LockFile(sFileName);
			}
		}
		m_files = newList;
	}
}

void CMonitorStack::Loop2()
{
	if (!m_bActive)
		return;
	m_transitCallBack();
}


/******************************* eof *************************************/