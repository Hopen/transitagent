/************************************************************************/
/* Name     : TransitAgent\backuper.h                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Oct 2010                                               */
/************************************************************************/
#pragma once

#include "Patterns/singleton.h"
#include "Log/SystemLog.h"
#include "..\Composite\Composite.h"
#include "client.h"

class CBackUper:public singleton <CBackUper>
{
	friend class singleton<CBackUper>;
public:
	CBackUper();
	~CBackUper();
	void Save(const std::wstring& _uri, bool bCheck = true);
	bool FileValid(const std::wstring& _uri);
	std::wstring LoadBackup(const std::wstring& _uri);
	bool AddFolder(const std::wstring& _filename, const std::wstring& _root, std::wstring& uri);
	StringSet& GetFoldersList(){return m_folders  ;};
	StringSet& GetFilesList  (){return m_filenames;};
	bool Find(const std::wstring& _uri);
	bool HTTPGet(const std::wstring& uri, std::wstring& sFileText);
	bool MatchFiles(const std::wstring& uri);
	void GetHostAndParam(const std::wstring& _uri, std::wstring& _host, std::wstring& _params);
	//void MatchHTTPFiles();
	//std::wstring GetLocalRootFolder()const;

private:
	//std::wstring GetNameFromURI(
	//	const std::wstring& _uri, 
	//	bool bFileName = true) // otherwise it's folder name
	//	const;

	std::wstring m_sCacheFolder;
	singleton_auto_pointer<CSystemLog> m_log;

	StringSet m_filenames;
	StringSet m_folders;
	std::wstring m_sFileText;
	std::wstring m_sFileTextName;

};

/******************************* eof *************************************/