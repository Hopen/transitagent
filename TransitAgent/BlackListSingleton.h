/************************************************************************/
/* Name     : TransitAgent\BlackListSingleton.h                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 18 Aug 2010                                               */
/************************************************************************/
#pragma once

#include <3dparty/rapidxml/rapidxml.hpp>
#include <3dparty/rapidxml/rapidxml_utils.hpp>

//#include "singleton.h"
#include "CompositeSingleton.h"
#include "CustomCfgSingleton.h"
#include "..\Composite\BlackList.h"

class CBlackListSingleton: public CCompositeSingleton
{
public:
	CBlackListSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder, CCustomCfgSingleton* customCfgSingleton);
	~CBlackListSingleton();

	//void LoadBlackList(rapidxml::xml_node<> *blacklistNode, std::wstring fileConfig);
	void LoadBlackList(rapidxml::xml_node<> *blacklistNode, std::wstring fileConfig, IList::SPtr _elem);
	void LoadBlackLists(
		rapidxml::xml_node<> *managedBacklistNode,
		std::wstring fileConfig,
		const std::wstring& _root_folder,
		IList::BlackListGroups& groups);


	//void LoadBlackList(const core::config_node& pBlackListRoot, std::wstring fileConfig);
	//void LoadBlackList(const core::config_node& pBlackListRoot, std::wstring fileConfig, IList::SPtr _elem);
	//void LoadBlackLists(
	//	const core::config_node& pBlackListRoot, 
	//	std::wstring fileConfig, 
	//	const std::wstring& _root_folder,
	//	IList::BlackListGroups& groups);
	void load(const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& _blackListGroups);

private:
	//void LoadList(const core::config_node& pBlackListRoot, std::wstring fileConfig, IList::SPtr _elem);
	void LoadList(rapidxml::xml_node<> *blacklistNode, std::wstring fileConfig, IList::SPtr _elem);
private:
	CCustomCfgSingleton* _customCfgSingleton{};
};


/******************************* eof *************************************/