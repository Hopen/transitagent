/************************************************************************/
/* Name     : TransitAgent\TransitAgent.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 16 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "boost/bind.hpp"
#include <boost/filesystem.hpp>

#include <3dparty/rapidxml/rapidxml.hpp>
#include <3dparty/rapidxml/rapidxml_utils.hpp>


#include "utils.h"
#include "Configuration/SystemConfiguration.h"
#include "TransitAgent.h"
#include "Router/router_compatibility.h"

CTransitAgent::CTransitAgent(CSystemLog* log, const std::wstring& sConfigFileName)
	: m_log{ log }
	, _monitorStack{ log }
	, m_io(new boost::asio::io_service())
	, m_blFirstConnect(true)
{
	m_uCallFailCounter = 0;
	m_uTrunkFailCounter = 0;
	m_uMaxTrunkGroupFail = 0;
	m_log->LogString(LEVEL_INFO, L"Transit agent created...");
	m_log->LogString(LEVEL_INFO, L"Load config: %s", sConfigFileName);

	SystemConfig settings;

	std::wstring tmpFileReadFolder;
	try
	{
		tmpFileReadFolder = settings->get_config_value<std::wstring>(_T("TmpFileReadFolder"));
		boost::filesystem::create_directory(tmpFileReadFolder);
	}
	catch (...)
	{
		tmpFileReadFolder = CProcessHelper::GetCurrentFolderName() + L"\\tmp\\";
		m_log->LogString(LEVEL_WARNING, L"Exception reading TmpFileReadFolder config key");
	}

	m_log->LogString(LEVEL_INFO, L"init CompositeSingleton");
	_compositeSingleton = std::make_unique<CCompositeSingleton>(m_log, tmpFileReadFolder);

	_customCfgSingleton = std::make_unique<CCustomCfgSingleton>(m_log, tmpFileReadFolder);
	_aliasListSingleton = std::make_unique<CAliasListSingleton>(m_log, tmpFileReadFolder);

	_blackListSingleton = std::make_unique<CBlackListSingleton>(m_log, tmpFileReadFolder, _customCfgSingleton.get());
	_managedMessagesSingleton = std::make_unique<CManagedMessagesSingleton>(m_log, tmpFileReadFolder);
	_phoneMasksSingleton = std::make_unique<CPhoneMasksSingleton>(m_log, tmpFileReadFolder, _customCfgSingleton.get(), _aliasListSingleton.get());
	_iPOBoundSingleton = std::make_unique<CIPOBoundSingleton>(m_log, tmpFileReadFolder);
	_trunkGroupCollector = std::make_unique<CTrunkGroupCollector>(_iPOBoundSingleton.get());

	m_log->LogString(LEVEL_INFO, L"Load bound list");
	LoadBoundList(sConfigFileName);

	m_log->LogString(LEVEL_INFO, L"Load trunk group");
	LoadTrunkList(sConfigFileName);

	m_log->LogString(LEVEL_INFO, L"Load config lists");
	
	mCompositeScipts = LoadMasks(sConfigFileName);

	std::wstring newConfigName = sConfigFileName;

	_compositeSingleton->AddFolder(sConfigFileName,CProcessHelper::GetCurrentModuleName(), newConfigName);

	//m_pList        = CCompositeSingleton ::GetInstance();
	m_pTrunkGroups = _trunkGroupCollector->GetInstance();

	_debugListCollector = std::make_unique<CDebugListCollector>();
	m_pDebugList   = _debugListCollector->GetInstance();

	AddWatchFolders(_compositeSingleton->GetFoldersList());

	m_changeFileConnector = m_monitor.AddModifyHandler(boost::bind(&CTransitAgent::OnModify, this, _1, _2));

	m_router = std::make_unique<CRouterManager>();
	CRouterManager::ConnectHandler chandler = boost::bind(&CTransitAgent::RouterConnectHandler, this);
	m_router->RegisterConnectHandler(chandler);
	CRouterManager::DeliverErrorHandler ehandler = boost::bind(&CTransitAgent::RouterDeliverErrorHandler, this, _1, _2, _3);
	m_router->RegisterDeliverErrorHandler(ehandler);

	CRouterManager::StatusChangeHandler status_handler = boost::bind(&CTransitAgent::AppStatusChange, this, _1, _2);
	m_router->RegisterStatusHandler(status_handler);

	SubscribeOnEvents();


	unsigned int uHTTPTimeOut = 60;
	try
	{
		uHTTPTimeOut = settings->get_config_value<int>(_T("HTTPCheckTimeOut"));
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading HTTPCheckTimeOut config key");
	}
	
	try
	{
		m_uMaxTrunkGroupFail = settings->get_config_value<int>(_T("MaximumTrunkGroupFail"));
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading MaximumTrunkGroupFail config key");
	}

	_monitorStack.Init(
		[&](const std::wstring& _filename)
		{
			return UpdateList(_filename);
		}, 
		[&](const std::wstring& _filename)
		{
			return UpdateTruncGroup(_filename);
		},	
		[composite = _compositeSingleton.get(), self = this]()
		{
			composite->CheckRemovedFile(
				[&](const std::wstring& _uri)
				{
					self->Modify(_uri.c_str());
				}
			);

		}
		, uHTTPTimeOut);

	m_log->LogString(LEVEL_INFO, L"Monitor thread stack started");
}

CTransitAgent::~CTransitAgent()
{
	//m_io.stop();
	m_io->stop();
	if (m_timer.get())
	{
		m_timer->CancelTimer();
		m_timer.reset();
	}
	m_changeFileConnector.disconnect();
}

void CTransitAgent::SubscribeOnEvents()
{
	SUBSCRIBE_MESSAGE(*m_router, L"OFFERED"                              , CTransitAgent::OfferedEventHandler           );
	SUBSCRIBE_MESSAGE(*m_router, L"TS2ANY_TIMESLOTSTATUS"                , CTransitAgent::TimeSlotStatusEventHandler    );
	SUBSCRIBE_MESSAGE(*m_router, L"MAKE_CALL"                            , CTransitAgent::MakeCallEventHandler          );
	SUBSCRIBE_MESSAGE(*m_router, L"SIP2TA_SMPP_DELIVER_SM"               , CTransitAgent::OfferedEventHandler           );
	SUBSCRIBE_MESSAGE(*m_router, L"MCC2TA_NEWEMAIL"                      , CTransitAgent::OfferedEventHandler           );
	SUBSCRIBE_MESSAGE(*m_router, L"MCC2TA_NEWCHAT"                       , CTransitAgent::OfferedEventHandler           );
	SUBSCRIBE_MESSAGE(*m_router, L"SG2TA_DEBUG_SCRIPT"                   , CTransitAgent::SG2TADebugRunEventHandler     );
	SUBSCRIBE_MESSAGE(*m_router, L"SG2TA_DEBUG_SCRIPT_CANCEL"            , CTransitAgent::SG2TADebugCancelEventHandler  );
	SUBSCRIBE_MESSAGE(*m_router, L"SG2TA_RUN_SCRIPT"                     , CTransitAgent::SG2TARunScriptEventHandler    );
	SUBSCRIBE_MESSAGE(*m_router, L"ANY2TA_RUN_SCRIPT"                    , CTransitAgent::SG2TARunScriptEventHandler    );
	SUBSCRIBE_MESSAGE(*m_router, L"SG2TA_RunScriptRedirect"              , CTransitAgent::SG2TARedirectEventHandler     );
	SUBSCRIBE_MESSAGE(*m_router, L"ANY2TS_RequestTimslotsStateCompleted", CTransitAgent::RequestTSCompletedHandler);
	SUBSCRIBE_MESSAGE(*m_router, L"ANY2TS_RequestTimeslotsStateCompleted", CTransitAgent::RequestTSCompletedHandler);
	SUBSCRIBE_MESSAGE(*m_router, L"MAKE_CALL_CONFIRMED"                  , CTransitAgent::MakeCallConfirmedEventHandler );
	SUBSCRIBE_MESSAGE(*m_router, L"CALL_ENDED"                           , CTransitAgent::CallEndEventHandler           );
}

IList::SPtr CTransitAgent::LoadMasks(const std::wstring& aConfigFileName) const
{
	IList::SPtr list = std::make_shared<CompositeAction<CCompositeSingleton>>(_compositeSingleton.get(), std::wstring(L""));
	list->add(LoadCustomCfg2(aConfigFileName));
	list->add(LoadAliasList2(aConfigFileName));

	IList::SPtr blackList;
	IList::BlackListGroups blackListGroups;
	std::tie(blackList, blackListGroups) = LoadBlackList2(aConfigFileName);

	list->add(blackList);
	list->add(LoadMessageList2(aConfigFileName, blackListGroups));
	list->add(LoadPhoneList2(aConfigFileName, blackListGroups));

	return list;
}

//void CTransitAgent::LoadBlackList(std::wstring _file)
//{
//	try {
//		core::configuration config(_file);
//		_blackListSingleton->LoadBlackList(config.get_config_node(L"BlackList"), _file);
//		_blackListSingleton->LoadBlackLists(
//			config.get_config_node(L"ManagedBlackList"), 
//			_file, 
//			Utils::GetFolderNameFromURI(static_cast<std::wstring>(CProcessHelper::GetCurrentModuleName())),
//			mBlackListGroups);
//	}
//	catch (core::configuration_error& e)
//	{
//		m_log->LogString(LEVEL_WARNING, L"Exception when loading BlackList: %s", stow(e.what()).c_str());
//	}
//}

std::pair<IList::SPtr, IList::BlackListGroups> CTransitAgent::LoadBlackList2(const std::wstring& _file) const
{
	IList::SPtr blackCfg = std::make_shared<CompositeAction<CBlackListSingleton>>(_blackListSingleton.get(), _file);
	IList::BlackListGroups blackListGroups;
	try 
	{
		rapidxml::xml_document<> doc;
		rapidxml::file<> f(wtos(_file).c_str());
		doc.parse<0>(f.data());
		rapidxml::xml_node<> *configurationNode = doc.first_node("configuration", 0, false);

		_blackListSingleton->LoadBlackList(configurationNode->first_node("BlackList", 0, false), _file, blackCfg);
		_blackListSingleton->LoadBlackLists(
			configurationNode->first_node("ManagedBlackList", 0, false),
			_file, 
			Utils::GetFolderNameFromURI(static_cast<std::wstring>(CProcessHelper::GetCurrentModuleName())),
			blackListGroups);
	}
	catch (std::exception& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading BlackList: %s", stow(e.what()).c_str());
	}



	//IList::SPtr blackCfg = std::make_shared<CompositeAction<CBlackListSingleton>>(_blackListSingleton.get(), _file);
	//IList::BlackListGroups blackListGroups;
	//try {
	//	core::configuration config(_file);
	//	_blackListSingleton->LoadBlackList(config.get_config_node(L"BlackList"), _file, blackCfg);
	//	_blackListSingleton->LoadBlackLists(
	//		config.get_config_node(L"ManagedBlackList"), 
	//		_file, 
	//		Utils::GetFolderNameFromURI(static_cast<std::wstring>(CProcessHelper::GetCurrentModuleName())),
	//		blackListGroups);
	//}
	//catch (core::configuration_error& e)
	//{
	//	m_log->LogString(LEVEL_WARNING, L"Exception when loading BlackList: %s", stow(e.what()).c_str());
	//}

	return std::make_pair(blackCfg, blackListGroups);
}

void CTransitAgent::LoadMessageList(std::wstring _file)
{
	try {
		core::configuration config(_file);
		_managedMessagesSingleton->LoadManagedMessages(config.get_config_node(L"ManagedMessages"), _file, mBlackListGroups);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading ManagedMessages: %s", stow(e.what()).c_str());
	}
}

IList::SPtr CTransitAgent::LoadMessageList2(const std::wstring& _file, const IList::BlackListGroups& _blackListGroups) const
{
	IList::SPtr massageCfg = std::make_shared<CompositeAction<CManagedMessagesSingleton>>(_managedMessagesSingleton.get(), _file);
	try 
	{
		core::configuration config(_file);
		_managedMessagesSingleton->LoadManagedMessages(config.get_config_node(L"ManagedMessages"), _file, massageCfg, _blackListGroups);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading ManagedMessages: %s", stow(e.what()).c_str());
	}

	return massageCfg;
}

void CTransitAgent::LoadPhoneList(std::wstring _file)
{
	try {
		core::configuration config(_file);
		_phoneMasksSingleton->LoadPhoneMasks(config.get_config_node(L"PhoneMasks"), _file, mBlackListGroups);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading PhoneMasks: %s", stow(e.what()).c_str());
	}
}

IList::SPtr CTransitAgent::LoadPhoneList2(const std::wstring& _file, const IList::BlackListGroups& _blackListGroups) const
{
	IList::SPtr phoneCfg = std::make_shared<CompositeAction<CPhoneMasksSingleton>>(_phoneMasksSingleton.get(), _file);
	try 
	{
		core::configuration config(_file);
		_phoneMasksSingleton->LoadPhoneMasks(
			config.get_config_node(L"PhoneMasks"), 
			_file, 
			phoneCfg,
			_blackListGroups);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading PhoneMasks: %s", stow(e.what()).c_str());
	}

	return phoneCfg;
}


void CTransitAgent::LoadTrunkList(std::wstring _file)
{
	try {
		core::configuration config(_file);
		// we're got race conditions with TS2ANY_TIMESLOTSTATUS messages when started
		boostex::unique_lock_ex lock(m_initMutex, L"LoadTrunkList");
		_trunkGroupCollector->LoadTrunkGroup(config.get_config_node(L"TrunkGroups"), _file);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading TrunkGroups: %s", stow(e.what()).c_str());
		//throw;
	}
}

IList::SPtr CTransitAgent::LoadCustomCfg2(const std::wstring& _file) const
{
	IList::SPtr customCfg = std::make_shared<CompositeAction<CCustomCfgSingleton>>(_customCfgSingleton.get(), _file);
	try 
	{
		core::configuration config(_file);
		_customCfgSingleton->LoadCustomCfg(config.get_config_node(L"CustomConfig"), _file, customCfg);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading CustomConfig: %s", stow(e.what()).c_str());
	}
	return customCfg;
}

void CTransitAgent::LoadCustomCfg(std::wstring _file)
{
	try {
		core::configuration config(_file);
		_customCfgSingleton->LoadCustomCfg(config.get_config_node(L"CustomConfig"), _file);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading CustomConfig: %s", stow(e.what()).c_str());
	}
}

void CTransitAgent::AddWatchFolders(const StringSet& _filelist)
{
	for (StringSet::const_iterator cit = _filelist.begin();cit!=_filelist.end();++cit)
		m_monitor.AddWatchFolder((*cit).c_str());
}

void CTransitAgent::LoadAliasList(std::wstring _file)
{
	try {
		core::configuration config(_file);
		_aliasListSingleton->LoadAliasList(config.get_config_node(L"ScriptAliases"), _file);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading ScriptAliases: %s", stow(e.what()).c_str());
	}
}

IList::SPtr CTransitAgent::LoadAliasList2(const std::wstring& _file) const
{
	IList::SPtr aliasCfg = std::make_shared<CompositeAction<CAliasListSingleton>>(_aliasListSingleton.get(), _file);
	try 
	{
		core::configuration config(_file);
		_aliasListSingleton->LoadAliasList(config.get_config_node(L"ScriptAliases"), _file, aliasCfg);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading ScriptAliases: %s", stow(e.what()).c_str());
	}
	return aliasCfg;
}


void CTransitAgent::LoadBoundList(std::wstring _file)
{
	try
	{
		core::configuration config(_file);
		_iPOBoundSingleton->LoadProxyList(config.get_root_node(), _file);
	}
	catch (core::configuration_error& e)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception when loading BoundList: %s", stow(e.what()).c_str());
	}
}

/***************  HANDLERS *************/
void CTransitAgent::OnRename(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName, LPCTSTR _lpszNewName)
{

}

void CTransitAgent::OnCreate(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName)
{

}

void CTransitAgent::OnModify(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName)
{
	WCHAR szBuf[1024];
	PathCombineW(szBuf, szBuf, _lpszFolderName);
	PathCombineW(szBuf, szBuf, _lpszFileName);
	if (!_compositeSingleton->IsItOurFile(wcslen(szBuf)?szBuf:_lpszFileName))
		return;

	m_log->LogString(LEVEL_WARNING, L"File %s has changed", wcslen(szBuf)?szBuf:_lpszFileName);
	_monitorStack.AddFileName(wcslen(szBuf) ? szBuf : _lpszFileName);
}

void CTransitAgent::OnDelete(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName)
{

}

void CTransitAgent::BuildTrunkGroup()
{
	boost::unique_lock<boost::shared_mutex> mylock(m_groupMutex);
	m_pTrunkGroups->BuildList();
	m_log->LogStringModule(LEVEL_INFO, L"TransitAgent Core", m_pTrunkGroups->Info().c_str());
}

void CTransitAgent::RouterConnectHandler()
{
	m_log->LogStringModule(LEVEL_INFO, L"TransitAgent Core", L"Router has connected");
	if(m_blFirstConnect)
	{
		m_blFirstConnect = false;
		std::wstring sConfigFileName = CProcessHelper::GetCurrentModuleName() + L".config";
		UpdateTruncGroup(sConfigFileName);
	}
	else
	{
		//m_timer.expires_from_now(boost::posix_time::seconds(30)); //SKIV
		//m_timer.async_wait(m_strand.wrap(boost::bind(&CTransitAgent::SendRequestTimslotsMessage, this)));
		m_io->stop();
		m_io.reset(new boost::asio::io_service());
		m_timer.reset(new CPulseTimer(m_io, [&]() {SendRequestTimslotsMessage(); }, boost::posix_time::seconds(30)));
		boost::thread t(boost::bind(&boost::asio::io_service::run, m_io));
	}
}

void CTransitAgent::SendRequestTimslotsMessage()
{
	boostex::unique_lock_ex lock(m_initMutex, L"SendRequestTimeslotsMessage");
	m_log->LogStringModule(LEVEL_INFO, L"TransitAgent Core", L"Requesting timeslots");
	m_router->SendToAll(CMessage(L"ANY2TS_RequestTimslotsState"));
}

void CTransitAgent::SendRequestTimslotsMessageFromTo(const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
{
	boostex::unique_lock_ex lock(m_initMutex, L"SendRequestTimeslotsMessage");
	m_log->LogStringModule(LEVEL_INFO, L"TransitAgent Core", L"Requesting timeslots from 0x%p", _pTo.GetClientID());
	m_router->SendFromTo(CMessage(L"ANY2TS_RequestTimeslotsState"), _pFrom, _pTo);
}


void CTransitAgent::RouterDeliverErrorHandler(const CMessage& msg, const CLIENT_ADDRESS&, const CLIENT_ADDRESS&)
{
	m_log->LogStringModule(LEVEL_FINE, L"Router client", msg.Dump().c_str());
	if ((msg == L"ANY2TS_RequestTimeslotsState") || (msg == L"ANY2TS_RequestTimslotsState"))
	{
		//m_timer.expires_from_now(boost::posix_time::seconds(20)); //SKIV
		//m_timer.async_wait(m_strand.wrap(boost::bind(&CTransitAgent::SendRequestTimslotsMessage, this)));
		m_io->stop();
		m_io.reset(new boost::asio::io_service());
		m_timer.reset(new CPulseTimer(m_io, [&]() {SendRequestTimslotsMessage(); }, boost::posix_time::seconds(20)));
		boost::thread t(boost::bind(&boost::asio::io_service::run, m_io));
	}
	else if((msg == L"MAKE_CALL") && (m_uCallFailCounter++ > 10) )
	{
		m_uCallFailCounter = 0;
		SendRequestTimslotsMessage();
	}
}

void CTransitAgent::Modify(const std::wstring& _uri)
{
	OnModify(L"",_uri.c_str());
}

void CTransitAgent::AppStatusChange(DWORD dwClient, core::ClientStatus nStatus)
{
	if(nStatus == core::ClientStatus::CS_DISCONNECTED)
	{
		try
		{
			_debugListCollector->RemoveListByAddress(dwClient);

			//singleton_auto_pointer <CEnumerator> counter;
			int count = m_counter.RemoveScripts(dwClient);

			CAtlString sLogString;
			CAtlString sTemplate = L"  %i scripts finished by terminating DISPATCHER ( %i )";
			sLogString.Format(sTemplate, count, dwClient);

			m_log->LogStringModule(LEVEL_INFO, L"CS_DISCONNECTED", sLogString.GetString());

			CLIENT_ADDRESS ts_address;
			ts_address.SetAsQWORD(dwClient);
			ts_address.ChildID = 0;

			_monitorStack.DelServer(ts_address);
		}
		catch (...)
		{

		}
	}

}

static __int64 FormatTimeStamp(const SYSTEMTIME& systime)
{
	FILETIME fileTime;
	::SystemTimeToFileTime(&systime, &fileTime);
	return *((__int64 *)&fileTime);
}

static  CString FormatTime(const SYSTEMTIME& systime)
{
	CString strResult;
	strResult.Format(_T("%02i-%02i-%04i %02i:%02i:%02i"), 
		systime.wDay, systime.wMonth, systime.wYear,
		systime.wHour, systime.wMinute, systime.wSecond);

	return strResult;
}

CMessage CTransitAgent::AlarmMsg(const std::wstring& sSubModule, const std::wstring& sSubSystem, const std::wstring& _error_name, const std::wstring& _description, const std::wstring& _probErrorCause, const int& iError)
{
	SYSTEMTIME eventTime, importanceTime;
	::GetSystemTime(&eventTime);
	ZeroMemory(&importanceTime, sizeof(importanceTime));
	importanceTime.wMinute = 2;

	COleDateTime odtNow = COleDateTime::GetCurrentTime();
	SYSTEMTIME formatTime;
	odtNow.GetAsSystemTime(formatTime);

	CMessage message;
	message.Name = L"SYSTEM_ALARM";
	message[L"AlarmID"]    = (__int64(rand()) << 32) | rand();;
	message[L"Severity"]    = L"Major";

	message[L"ActivationTime"]  = FormatTime(formatTime).GetString();
	message[L"ActivationTimestamp"] = FormatTimeStamp(eventTime);

	message[L"ImportanceTime"]  = FormatTime(importanceTime).GetString();
	message[L"ImportanceTimestamp"] = importanceTime.wMinute * 60 * 1000; 

	message[L"Machine"]    = CProcessHelper::GetComputerName();
	message[L"Module"]     = L"TransitAgent";
	message[L"SubModule"]  = sSubModule.c_str();
	message[L"SubSystem"]   = sSubSystem.c_str();
	message[L"ErrorCode"]   = iError;
	message[L"ErrorName"]   = _error_name.c_str();
	message[L"ErrorDescription"]  = _description.c_str();
	message[L"ProbErrorCause"] = _probErrorCause.c_str();

	return message;
}

bool CTransitAgent::UpdateList(const std::wstring& _filename)
{
	//boost::unique_lock<boost::shared_mutex> mylock(m_listMutex);
	//mCompositeScipts->refresh(_filename, CCompositeSingleton::refresh, mCompositeScipts, mBlackListGroups);

	CoInitialize(NULL);
	mCompositeScipts->refresh(_filename, mCompositeScipts, mBlackListGroups);

	//WCHAR sNewURI[1024];
	//PathCombineW(sNewURI, sNewURI, mTmpFileReadFolder.c_str());
	//PathCombineW(sNewURI, sNewURI, Utils::GetFileNameFromURI(_filename).c_str());

	//std::lock_guard<std::mutex> lock(mTmpFileReadMutex);
	//m_log->LogString(LEVEL_INFO, L"Moving config to file: %s", sNewURI);
	//try
	//{
	//	boost::filesystem::copy_file(_filename, sNewURI, boost::filesystem::copy_option::overwrite_if_exists);
	//}
	//catch (const std::exception& error)
	//{
	//	m_log->LogString(LEVEL_WARNING, L"Failed to move config to file: '%s', error: %s", sNewURI, stow(error.what()));
	//}

	//if (boost::filesystem::exists(boost::filesystem::path(sNewURI)))
	//{
	//	mCompositeScipts->refresh(sNewURI, mCompositeScipts, mBlackListGroups);
	//}

	CoUninitialize();

	return true;
}

bool CTransitAgent::UpdateTruncGroup(const std::wstring& _filename)
{
	boost::unique_lock<boost::shared_mutex> mylock(m_groupMutex);
	m_log->LogString(LEVEL_INFO, L"Load trunk group");

	bool result = false;

	try
	{
		result = m_pTrunkGroups->refresh(
			_filename, 
			[trankGroupSingleton = _trunkGroupCollector.get()](const std::wstring& filename, auto group, auto callback)
			{
				return trankGroupSingleton->refresh(filename, group, callback);
			},
			m_pTrunkGroups, 
			[&]() 
			{
				SendRequestTimslotsMessage(); 
			});
	}
	catch (core::configuration_error& error)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception while read TrunkGroup xml: %s", stow(error.what()));
	}
	catch (std::runtime_error& error)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception while read TrunkGroup: %s", stow(error.what()));
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Unknown exception while read TrunkGroup");
	}

	return result;
}

/******************************* eof *************************************/