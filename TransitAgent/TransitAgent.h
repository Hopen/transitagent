/************************************************************************/
/* Name     : TransitAgent\TransitAgent.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 16 Aug 2010                                               */
/************************************************************************/
#pragma once

#include <mutex>

#include "Patterns\singleton.h"
#include "Router/router_manager.h"
#include "..\Composite\Composite.h"
//#include "Thread.h"
#include "FileMonitor.h"
#include "ModifyStack.h"
#include "TrunkGroupCollector.h"
#include "DebugListCollector.h"
#include "RunScriptRedirectCollector.h"
#include "enumerator.h"

#include "BlackListSingleton.h"
#include "ManagedMessagesSingleton.h"
#include "PhoneMasksSingleton.h"
#include "CustomCfgSingleton.h"
#include "AliasListSingleton.h"
#include "IPOBoundSingleton.h"

#define REGION_PARAM L"$$REGION$$"

#define LODWORD(X)				DWORD(X & 0xFFFFFFFF)
#define HIDWORD(X)				DWORD(X >> 32)


class CTransitAgent
{
	class EMakeCallRejected
	{
	private:
		CMessage	m_Msg;

	public:
		EMakeCallRejected(
			__int64		callbackID,
			LONGLONG	llErrCode,
			PCWSTR		wsErrCodeText,
			PCWSTR		wsErrCodeDescr)
			: m_Msg(L"MAKE_CALL_REJECTED")
		{
			m_Msg[L"CallbackID"]		= callbackID;
			m_Msg[L"ReasonCode"]		= llErrCode;
			m_Msg[L"ReasonText"]        = wsErrCodeText;
			m_Msg[L"ReasonDescription"] = wsErrCodeDescr;
		}
		CMessage	Msg() { return m_Msg; }
	};

public:

	void SendRequestTimslotsMessage();
	void SendRequestTimslotsMessageFromTo(const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void BuildTrunkGroup();
	void Modify(const std::wstring& _uri);

	bool UpdateList(const std::wstring& _filename);
	bool UpdateTruncGroup(const std::wstring& _filename);

private:
	void LoadBlackList  (std::wstring _file);
	void LoadMessageList(std::wstring _file);
	void LoadPhoneList  (std::wstring _file);
	void LoadTrunkList  (std::wstring _file);
	void LoadCustomCfg  (std::wstring _file);
	void LoadAliasList  (std::wstring _file);
	void LoadBoundList  (std::wstring _file);

	std::pair<IList::SPtr, IList::BlackListGroups> LoadBlackList2(const std::wstring& _file) const;
	IList::SPtr LoadMessageList2(const std::wstring& _file, const IList::BlackListGroups& _blackListGroups) const;
	IList::SPtr LoadPhoneList2(const std::wstring& _file, const IList::BlackListGroups& _blackListGroups) const;
	IList::SPtr LoadCustomCfg2(const std::wstring& _file) const;
	IList::SPtr LoadAliasList2(const std::wstring& _file) const;

	void AddWatchFolders(const StringSet& _filelist);

	// handlers
	void OnRename(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName, LPCTSTR _lpszNewName);
	void OnCreate(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName);
	void OnModify(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName);
	void OnDelete(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName);

	void RouterConnectHandler();
	void RouterDeliverErrorHandler(const CMessage& , const CLIENT_ADDRESS&, const CLIENT_ADDRESS&);

	IList::SPtr LoadMasks(const std::wstring& aConfigFileName) const;
public:
	CTransitAgent(CSystemLog* log, const std::wstring& sConfigFileName);
	virtual ~CTransitAgent();

private:
	CMessage AlarmMsg(const std::wstring& sSubModule, const std::wstring& sSubSystem, const std::wstring& _error_name, const std::wstring& _description, const std::wstring& _probErrorCause, const int& iError);

	//void CreateSomeThreads(const int& count);
	//void StopAllThreads();

public:
	CSystemLog* m_log;
	CMonitorStack _monitorStack;

private:
	//CRouterManager m_router;
	std::unique_ptr<CRouterManager> m_router;

	//IList::SPtr m_pList;
	IList::SPtr mCompositeScipts;

	IList::BlackListGroups mBlackListGroups;

	//CTrunkGroup::ITrunkGroup m_pTrunkGroups;
	CAbstractTrunk::ITrunkGroup m_pTrunkGroups;
	IList::SPtr m_pDebugList;
	//IList::SPtr m_pProxyList
	boost::mutex m_initMutex;
	//boost::shared_mutex m_listMutex;
	boost::shared_mutex m_groupMutex;
	CRedirectCollector m_redirect;

	bool m_blFirstConnect;

	CFileMonitor m_monitor;
	CFileMonitor::Connector m_changeFileConnector;

	//std::auto_ptr<CMonitorStack> m_stack;
	//boost::asio::io_service m_io;

	CEnumerator m_counter;
	UINT m_uCallFailCounter;
	UINT m_uTrunkFailCounter;
	UINT m_uMaxTrunkGroupFail;

	//
	//boost::asio::strand m_strand;
	//boost::asio::deadline_timer m_timer;
	boost::shared_ptr<CPulseTimer> m_timer;
	boost::shared_ptr<boost::asio::io_service> m_io;

	std::unique_ptr<CCompositeSingleton> _compositeSingleton;
	std::unique_ptr<CTrunkGroupCollector> _trunkGroupCollector;
	std::unique_ptr<CDebugListCollector> _debugListCollector;
	std::unique_ptr<CBlackListSingleton> _blackListSingleton;
	std::unique_ptr<CManagedMessagesSingleton> _managedMessagesSingleton;
	std::unique_ptr<CPhoneMasksSingleton> _phoneMasksSingleton;
	std::unique_ptr<CCustomCfgSingleton> _customCfgSingleton;
	std::unique_ptr<CAliasListSingleton> _aliasListSingleton;
	std::unique_ptr<CIPOBoundSingleton> _iPOBoundSingleton;
	
	
	
	

//public:
//	class CTransitThread;
//	typedef boost::shared_ptr<CTransitThread> ThreadPointer;
//	typedef std::list <ThreadPointer> ThreadContainer;
//private:
//	ThreadContainer m_threads;

public:
	//void OfferedEventHandler_test(CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo)
	//{
	//	OfferedEventHandler(_message, _pFrom, _pTo, blDeliverError);
	//}

	template <class TMessage>
	IList* GetScript(TMessage&& msg) { return mCompositeScipts->GetScript(std::forward<TMessage>(msg)); }

	
protected:
	// handlers
	void OfferedEventHandler            (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void TimeSlotStatusEventHandler     (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void MakeCallEventHandler           (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	//void SIP2TAEventHandler             (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void SG2TADebugRunEventHandler      (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void SG2TADebugDebugEventHandler    (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo, bool& blDeliverError);
	void SG2TADebugCancelEventHandler   (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void SG2TARunScriptEventHandler     (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void CallStatusEventHandler         (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void SG2TARedirectEventHandler      (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void RequestTSCompletedHandler      (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void CallEndEventHandler            (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void MakeCallConfirmedEventHandler  (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void MakeCallEndedEventHandler      (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void D2ANYScriptStatusNoticeHandler (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);
	void LIC2TALicenseEventHandler      (CMessage& _message, const CLIENT_ADDRESS& _pFrom, const CLIENT_ADDRESS& _pTo);

	void SubscribeOnEvents();
	void AppStatusChange(DWORD dwClient, core::ClientStatus	nStatus);

	friend void OfferedTest       (CTransitAgent* pAgent);
	friend void TimeSlotStatusTest(CTransitAgent* pAgent);
	friend void MakeCallTest      (CTransitAgent* pAgent);
	friend void DebugTest         (CTransitAgent* pAgent);
	friend void CallStatusTest    (CTransitAgent* pAgent);
	friend void RedirectTest      (CTransitAgent* pAgent);
	friend void RunScriptTest     (CTransitAgent* pAgent);
	friend void DispCrashTest     (CTransitAgent* pAgent);
	friend void DS2ANYNoticeTest  (CTransitAgent* pAgent);
	friend void LICENCETest       (CTransitAgent* pAgent);

public:

};


/******************************* eof *************************************/