/************************************************************************/
/* Name     : TransitAgent\ManagedMessagesSingleton.h                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 23 Aug 2010                                               */
/************************************************************************/
#pragma once
//#include "singleton.h"
#include "CompositeSingleton.h"
#include "..\Composite\ManagetMessages.h"

class CManagedMessagesSingleton : public CCompositeSingleton
{
public:
	CManagedMessagesSingleton(CSystemLog* log, const std::wstring& tmpFileReadFolder);
	~CManagedMessagesSingleton();

	void LoadManagedMessages(
		const core::config_node& pManagetMessagesRoot, 
		std::wstring fileConfig, 
		IList::SPtr _elem,
		const IList::BlackListGroups& _blackListGroups);

	void LoadManagedMessages(
		const core::config_node& pManagetMessagesRoot, 
		std::wstring fileConfig,
		const IList::BlackListGroups& _blackListGroups);

	void load (const std::wstring& fileConfig, IList::SPtr _elem, IList::BlackListGroups& _blackListGroups);

private:
	void LoadMasks(
		const core::config_node& pManagetMessagesRoot,
		std::wstring fileConfig,
		IList::SPtr _elem,
		const IList::BlackListGroups& _blackListGroups);
};


/******************************* eof *************************************/