/************************************************************************/
/* Name     : TransitAgent\TrunkGroupCollector.cpp                      */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 12 Oct 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "utils.h"
#include "WinAPI/ProcessHelper.h"
#include "TrunkGroupCollector.h"
#include "TransitAgent.h"
#include "IPOBoundSingleton.h"

CTrunkGroupCollector::CTrunkGroupCollector(CIPOBoundSingleton* iPOBoundSingleton)
	: _iPOBoundSingleton{ iPOBoundSingleton }
{
	m_groups = boost::make_shared<CParentTrunk>(std::wstring(CProcessHelper::GetCurrentModuleName()) + L".config");
	m_backup = CBackUper::GetInstance();
}

CTrunkGroupCollector::~CTrunkGroupCollector()
{
}

void CTrunkGroupCollector::LoadGroup(const core::config_node& pTrunkGroupRoot, std::wstring fileConfig, CAbstractTrunk /*CTrunkGroup*/::ITrunkGroup pElem)
{
	CParentTrunk * parent = dynamic_cast <CParentTrunk*> ( pElem.get());
	if (!parent)
		return;

	core::configuration config(fileConfig);
	for (const auto& child : pTrunkGroupRoot)
	{
		if (!child.first.compare(L"include") || !child.first.compare(L"<xmlcomment>"))
		{
			continue;
		}

		std::wstring sMaxLines = config.safe_get_config_attr<std::wstring>(child.second, L"Max", L"");

		unsigned int uMaxLines = sMaxLines.empty()? 999999 : Utils::toNum<unsigned int>(sMaxLines);

		CAbstractTrunk::ITrunkGroup new_elem = CAbstractTrunk::CreateGroup(child.first,
			config.safe_get_config_attr<std::wstring>(child.second, L"Timeslots", L""), //mask.attr(L"Timeslots"),
			config.safe_get_config_attr<std::wstring>(child.second, L"Method", L""), //mask.attr(L"Method"),
			config.safe_get_config_attr<std::wstring>(child.second, L"Direction", L""), //mask.attr(L"Direction"),
			config.safe_get_config_attr<std::wstring>(child.second, L"Parity", L""), //mask.attr(L"Parity"),
			uMaxLines);
		
		std::wstring sIpoutbound = config.safe_get_config_attr<std::wstring>(child.second, L"Ipoutbound", L"");// mask.attr(L"Ipoutbound");
		if (!sIpoutbound.empty())
		{
			new_elem->AddProxyList(_iPOBoundSingleton->GetIPOBoundByName(sIpoutbound));
		}
		parent->Add(new_elem);
	}
}

void CTrunkGroupCollector::LoadTrunkGroup(const core::config_node& pTrunkGroupRoot, std::wstring fileConfig)
{
	LoadTrunkGroup(pTrunkGroupRoot, fileConfig, m_groups);
}

void CTrunkGroupCollector::LoadTrunkGroup(const core::config_node& pTrunkGroupRoot, std::wstring fileConfig, CAbstractTrunk/*CTrunkGroup*/::ITrunkGroup pElem)
{
	CParentTrunk * parent = dynamic_cast <CParentTrunk*> (pElem.get());
	if (!parent)
		return;

	LoadGroup(pTrunkGroupRoot, fileConfig, pElem);
	std::wstring sCurrentRootFolder = Utils::GetFolderNameFromURI(parent->GetScriptName());;
	if (sCurrentRootFolder.empty()) // default root folder
		sCurrentRootFolder = Utils::GetFolderNameFromURI(static_cast<std::wstring>(CProcessHelper::GetCurrentModuleName()));

	core::configuration config(fileConfig);
	for (const auto& child : pTrunkGroupRoot)
	{
		if (!child.first.compare(L"include"))
		{
			std::wstring sFileURI, config_file;
			if (AddFolder(/*mask.attr(L"file")*/config.safe_get_config_attr<std::wstring>(child.second, L"file", L""),sCurrentRootFolder, sFileURI))
			{
				config_file = sFileURI;
				bool bFileAccept = true;
				if (!m_backup->FileValid(sFileURI))
				{
					bFileAccept = false;
					config_file = m_backup->LoadBackup(sFileURI);
				}
				if (!bFileAccept && config_file.empty())
					continue;

				core::configuration newConfig(config_file);
				CAbstractTrunk/*CTrunkGroup*/::ITrunkGroup child(new CParentTrunk(sFileURI));
				parent->Add(child);
				LoadTrunkGroup(newConfig.get_config_node(L"TrunkGroups"), config_file,child);
				if (bFileAccept)
					m_backup->Save(config_file,false);

			}
		}
	}
}

void CTrunkGroupCollector::AddTimeSlot(const std::wstring& _groupname, const std::wstring& sBoard, const std::wstring& sTimeSlot, const ULONGLONG& ullAddress)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if (!m_groups->AddDTI(_groupname, sBoard, sTimeSlot, ullAddress))
	{
		CParentTrunk * parent = dynamic_cast<CParentTrunk*>(m_groups.get());
		if (parent)
		{
			parent->AddNewGroupWithDefaultParams(_groupname, sBoard, sTimeSlot, ullAddress);
		}
	}
}

bool CTrunkGroupCollector::AddFolder(const std::wstring& _filename, const std::wstring& _root, std::wstring& uri)
{
	return m_backup->AddFolder(_filename, _root, uri);
}

bool CTrunkGroupCollector::refresh(const std::wstring& _filename, CAbstractTrunk/*CTrunkGroup*/::ITrunkGroup pParent, CAbstractTrunk::truck_group_callback call_back)
{
	CParentTrunk * parent = dynamic_cast <CParentTrunk*> (pParent.get());
	if (!parent)
		return false;

	std::wstring sRootFileName = CProcessHelper::GetCurrentModuleName() + L".config"; 
	std::wstring config_file = sRootFileName;
	bool bFileAccept = true;
	if (!m_backup->FileValid(sRootFileName))
	{
		bFileAccept = false;
		config_file = m_backup->LoadBackup(sRootFileName);
	}
	if (!bFileAccept && config_file.empty())
		return false;

	core::configuration newConfig(config_file);
	parent->Clear();

	LoadTrunkGroup(newConfig.get_config_node(L"TrunkGroups"), config_file, m_groups);

	if (bFileAccept)
		m_backup->Save(config_file,false);

	call_back();
	
	return true;
}

/******************************* eof *************************************/