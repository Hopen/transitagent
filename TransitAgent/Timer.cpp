/************************************************************************/
/* Name     : CDRLog\Timer.cpp                                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 19 Feb 2015                                               */
/************************************************************************/
#include "stdafx.h"
#include "Timer.h"
//#include "ConfigurationSettings.h"
//#include "logger.h"
//#include "FileRecorder.h"
//#include "FtpClient.h"

const int CHECK_TIME  = 30000;
const int EXPIREDTIME = 30000;

CTimer::CTimer(boost::shared_ptr<boost::asio::io_service>_io/*boost::asio::io_service& _io*/, SimpleCallbackFunc _callback, boost::posix_time::seconds _checkTime)
	: m_strand(*_io),
	m_checkTime(_checkTime),
	m_timer(*_io, m_checkTime),
	m_callBackFunc(_callback)
{
	m_timer.async_wait(m_strand.wrap(boost::bind(&CTimer::Loop, this)));
}

void CTimer::Loop()
{
	m_callBackFunc();

	m_timer.expires_at(m_timer.expires_at() + m_checkTime);
	m_timer.async_wait(m_strand.wrap(boost::bind(&CTimer::Loop, this)));

}

CTimer::~CTimer()
{
	
}

void CTimer::CancelTimer()
{
	m_timer.cancel();
}



CPulseTimer::CPulseTimer(boost::shared_ptr<boost::asio::io_service>_io/*boost::asio::io_service& _io*/, SimpleCallbackFunc _callback, boost::posix_time::seconds _checkTime)
	: m_strand(*_io),
	m_checkTime(_checkTime),
	m_timer(*_io, m_checkTime),
	m_callBackFunc(_callback)
{
	m_timer.async_wait(m_strand.wrap(boost::bind(&CPulseTimer::Loop, this)));
}

void CPulseTimer::Loop()
{
	m_callBackFunc();
}

CPulseTimer::~CPulseTimer()
{
	CancelTimer();
}

void CPulseTimer::CancelTimer()
{
	m_timer.cancel();
}

//void CBeholder::InitTimers(CSystemLog *log)
//{
//	int iFileRecorderTimer = EXPIREDTIME,
//		iFtpClientTimer    = EXPIREDTIME;
//
//	CConfigurationSettings settings;
//	
//	try
//	{
//		iFileRecorderTimer = settings[_T("TimeDelay")].ToInt();
//	}
//	catch (...)
//	{
//		log->LogString(LEVEL_WARNING, L"Exception reading \"TimeDelay\" config key");
//	}
//
//	try
//	{
//		iFtpClientTimer = settings[_T("Timer")].ToInt();
//	}
//	catch (...)
//	{
//		log->LogString(LEVEL_WARNING, L"Exception reading \"Timer\" config key");
//	}
//
//
//	m_timer1.reset(new CTimer(m_io, CFileRecorder::OnTimer1Expired, iFileRecorderTimer));
//	m_timer2.reset(new CTimer(m_io, CFileRecorder::OnTimer2Expired, CHECK_TIME));
//	//m_timer3.reset(new CTimer(m_io, CFtpClient   ::OnTimerExpired , iFtpClientTimer));
//
//	boost::thread t(boost::bind(&boost::asio::io_service::run, &m_io));
//}



/******************************* eof *************************************/