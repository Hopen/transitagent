#include "stdafx.h"
#include <boost/test/unit_test.hpp>

#include "MessageFactory.h"
#include "IdGenerator.h"
#include "ChatRouterClient.h"

BOOST_AUTO_TEST_SUITE(offered_tests)

BOOST_AUTO_TEST_CASE(ShouldRunScriptWithEmptyMask)
{
	singleton_auto_pointer<CSystemLog> log;
	log->LogString(LEVEL_INFO, L"Start ShouldRunScriptWithEmptyMask...");

	auto aNum = CIdGenerator::GetInstance()->makeSId();
	auto bNum = CIdGenerator::GetInstance()->makeSId();

	std::wstring fileName = L"C:\\IS3\\Scripts\\FreeswitchTest\\Unknown.sc3";

	MessageContainer msgList;

	msgList.push_back(CreateOffered(aNum, bNum));

	MyRouterClient router_client;

	router_client.Expect(L"TA2D_RUNSCRIPT",
		[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
	{
		BOOST_CHECK(_msg[L"FileName"].AsWideStr() == fileName);
	});

	BOOST_CHECK(router_client.Start(std::move(msgList), log->GetInstance()) == true);

	std::this_thread::sleep_for(std::chrono::seconds(3));
}

BOOST_AUTO_TEST_CASE(ShouldRunScriptSeveralMask)
{
	singleton_auto_pointer<CSystemLog> log;
	log->LogString(LEVEL_INFO, L"Start ShouldRunScriptSeveralMask...");

	MyRouterClient router_client;

	auto test = [&](const std::wstring& aNum, const std::wstring& bNum, const std::wstring& aFileName)
	{
		MessageContainer msgList{ CreateOffered(aNum, bNum) };

		log->LogString(LEVEL_INFO, L"Offered test: aNum: %s, bNum: %s, FileName: %s", aNum, bNum, aFileName);

		router_client.Expect(L"TA2D_RUNSCRIPT",
			[aFileName](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			auto fileName = _msg[L"FileName"].AsWideStr();
			BOOST_CHECK(fileName == aFileName);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(1));
	};

	//test(
	//	L"9055505661@bee.vimpelcom.ru",
	//	L"15682@bee.vimpelcom.ru",
	//	L"C:\\IS3\\Beeline\\SS7OperatorController\\Script\\UserConnect.sc3");
	
	//test(L"788", L"065106201111", L"test5.ccxml");
	test(L"9037451526", L"4999204162", L"PAM_1_listener_SIP.sc3");
	//test(L"123", L"+065106201111", L"test5.ccxml");
	//test(L"123", L"065106201112", L"test6.ccxml");
	//test(L"123", L"065106201122", L"test2.ccxml");
	//test(L"123", L"0651062011111", L"test1.ccxml");
	//test(L"123", L"0651062011211", L"test3.ccxml");

	//test(
	//	L"123",
	//	L"12345678901:2102721066123123",
	//	L"C:\\IS3\\scripts\\DigitalFinance\\DigitalFinance.sc3");

	//test(
	//	L"123",
	//	L"12345678901@172.19.58.6:21027210019999",
	//	L"C:\\IS3\\scripts\\abc\\abc.sc3");
}

BOOST_AUTO_TEST_SUITE_END()