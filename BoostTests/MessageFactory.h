#pragma once

#include "Base.h"

inline CMessage CreateOffered(const std::wstring& aA, const std::wstring& aB)
{
	/************************************************************************/
	/*
	Name: OFFERED; InitialMessage = "OFFERED"; Host = "DR-IVR801";
	Board = "DTI1"; TimeSlot = 4; A = "9031392837"; B = "07042d9629475619";
	SigInfo = "0x0601100702600109010A020100040A03107040D269927465910A07031309139382730801801D038090A33F0A84937063213607995200";
	CallID = 0x000392EA-0004D3E7

	*/
	/************************************************************************/

	CMessage msg(L"OFFERED");

	msg[L"A"] = aA;
	msg[L"B"] = aB;

	return msg;
}

