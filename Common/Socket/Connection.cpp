/************************************************************************/
/* Name     : MVEON\Connection.cpp                                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/


#include "stdafx.h"
#include "Base.h"
#include "Connection.h"
#include "MessageParser.h"
#include "ParseMessageStatus.h"
//#include "MessageCollector.h"
#include "IdGenerator.h"

#include "tgbot/net/HttpParser.h"

const int TCP_PACK_SIZE = 2048;

std::atomic<unsigned int> connection::_count = 0;

void connection::startListening()
{
	boost::asio::spawn(_strand,
		[self = shared_from_this()/*, &collector*/](boost::asio::yield_context yield)
	{
		try
		{
			std::string in_data;
			boost::system::error_code errorCode;
			boost::asio::streambuf buff_response;

			self->_timer.expires_from_now(std::chrono::seconds(30));

			size_t bytes_transferred = 0;
			do
			{
				char reply_[TCP_PACK_SIZE] = {};
				int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
				if (errorCode)
				{
					throw errorCode;
				}

				bytes_transferred += len;

				in_data.append(reply_, len);
			} while (in_data.find("\r\n\r\n") == std::string::npos);


			size_t num_additional_bytes = bytes_transferred/*in_data.size()*/ - (in_data.find("\r\n\r\n") + strlen("\r\n\r\n"));
			TgBot::HttpParser::HeadersMap headers;
			//std::wstring serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(in_data, headers));

			TgBot::HttpParser::getInstance().parseResponse(in_data, headers);

			auto header_it = headers.find("content-length");
			if (header_it != headers.end())
			{
				auto content_length = stoull(header_it->second);
				if (content_length > num_additional_bytes)
				{
					try
					{
						do
						{
							char reply_[TCP_PACK_SIZE] = {}; //ToDo: change buffer size in depandence on content-length
							int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
							if (errorCode)
							{
								throw errorCode;
							}

							num_additional_bytes += len;

							in_data.append(reply_, len);

						} while (content_length > num_additional_bytes);
					}
					catch (boost::system::error_code& error)
					{
						if (error != boost::asio::error::eof)
							self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
					}
					catch (std::runtime_error & error)
					{
						self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
					}
					catch (...)
					{
						self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
					}
				}
			}
			else
			{
				//throw (std::runtime_error((boost::format("Invalid http header: %s") % in_data.c_str()).str()));
			}

			std::wstring answer(stow(in_data));

			self->LogStringModule(LEVEL_FINEST, L"Read: %s", answer.c_str());
			std::string serverResponse = TgBot::HttpParser::getInstance().parseResponse(wtos(answer), headers);

			auto method = headers.find("method")->second;

			std::string error;
			if (method != "GET"
				&& serverResponse.empty())
			{
				error = "Parameters in request body or queryString are malformed";
			}

			std::string response;
			if (error.empty())
			{
				response = self->_handler(std::move(serverResponse), headers.find("status")->second, method);
			}
			else
			{
				response = TgBot::HttpParser::getInstance().generateResponse(error, "text/plain", 400, "Bad Request");
			}

			self->LogStringModule(LEVEL_FINEST, L"Write: %s", stow(response.c_str()));
			boost::asio::async_write(self->_socket, boost::asio::buffer(response, response.size()), yield);
			self->LogStringModule(LEVEL_FINEST, L"Write OK");

			self->_socket.close();

		}
		catch (std::exception& e)
		{
			self->LogStringModule(LEVEL_FINEST, L"Exception: %s", stow(e.what()).c_str());
			self->_socket.close();
			self->_timer.cancel();
		}
		catch (...)
		{
			self->LogStringModule(LEVEL_WARNING, L"Connection failed with error: unhandled exception caught");
		}

	});

	boost::asio::spawn(_strand,
		[self = shared_from_this()/*, &collector*/](boost::asio::yield_context yield)
	{
		while (self->_socket.is_open())
		{
			boost::system::error_code ignored_ec;
			self->_timer.async_wait(yield[ignored_ec]);
			if (self->_timer.expires_from_now() <= std::chrono::seconds(0))
			{
				self->LogStringModule(LEVEL_FINEST, L"Socket timeout");
				if (self->_socket.is_open())
				{
					self->_socket.close();
					self->LogStringModule(LEVEL_FINEST, L"Close socket by timeout");
				}

			}
		}
	});
}

//void connection::Send(CMessage msg)
//{
//	_send(std::move(CMessageParser::ToString(std::move(msg))));
//}
//
//void connection::Send(CMessage msg, std::wstring arrayName, MessageList list)
//{
//	_send(std::move(CMessageParser::ToString(std::move(msg), std::move(arrayName), std::move(list))));
//}
//
//void connection::_send(std::wstring response)
//{
//	boost::asio::spawn(_strand,
//		[self = shared_from_this(), response = std::move(response)](boost::asio::yield_context yield)
//	{
//		try
//		{
//			if (self->_socket.is_open())
//			{
//				self->LogStringModule(LEVEL_FINEST, L"Write: %s", response.c_str());
//				boost::asio::async_write(self->_socket, boost::asio::buffer(wtos(response), response.size()), yield);
//			}
//		}
//		catch (std::exception& e)
//		{
//			self->LogStringModule(LEVEL_FINEST, L"Exception: %s", stow(e.what()).c_str());
//
//			self->_socket.close();
//			self->_timer.cancel();
//		}
//	});
//}



/******************************* eof *************************************/