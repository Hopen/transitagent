/************************************************************************/
/* Name     : MVEON_TESTS\Socket\MakeSslClientToVeon.h                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Feb 2017                                               */
/************************************************************************/

#pragma once
#include "Base.h"

#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include "boost/asio/ssl.hpp"

#include "tgbot/net/httpparser.h"
#include "tgbot/tgtypeparser.h"
#include "Log/SystemLog.h"

#include "SslSpawnClient.h"

//class CSystemLog;

namespace ssl_veon_client
{
	struct TConnectionParams
	{
		TConnectionParams()
			: _response(std::make_shared<std::string>())
		{
		}

		std::string _uri;
		std::string _port;
		std::string _request;
		std::string _request_uri;
		std::shared_ptr<std::string> _response;
		//std::string _sertificate;
		std::string _token;
		int _iServerTimeout;
		uint32_t  _clientId;
		std::string _partyId;
		TgBot::HTTP_METHOD _method;
	};

	//bool MakeClient(boost::asio::io_service* io_service, TConnectionParams& aParams, CSystemLog* aLog);
	const int TCP_PACK_SIZE = 1024;

	template <class TParams>
	bool MakeClient(boost::asio::io_service* io_service, TParams&& params, CSystemLog *pLog)
	{
		boost::asio::spawn(*io_service, [io_service = io_service, params = std::forward<TParams>(params), pLog = pLog](boost::asio::yield_context yield)
		{
			auto LogStringModule = [pLog = pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
			{
				if (!pLog)
					return;
				pLog->LogStringModule(level, L"client", formatMessage, std::forward<decltype(args)>(args)...);
			};

			auto read_complete_predicate = [&](int data_len)
			{
				if (!data_len)
					return true;
				return 	data_len < TCP_PACK_SIZE;
			};

			try
			{
				LogStringModule(LEVEL_INFO, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());

				boost::asio::ip::tcp::resolver resolver(*io_service);
				//LogStringModule(LEVEL_INFO, L"12");
				boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
				//LogStringModule(LEVEL_INFO, L"13");
				boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
				//LogStringModule(LEVEL_INFO, L"14");
				boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
				//LogStringModule(LEVEL_INFO, L"15");

				boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
				//ctx.load_verify_file(/*params._sertificate*/"D:/Programming/Projects/MCHAT/Obj/PEM/MMBot_home.pem ");
				ctx.set_default_verify_paths();
				//LogStringModule(LEVEL_INFO, L"16");

				boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket(*io_service, ctx);

				//connect(_socket.lowest_layer(), iterator);
				//LogStringModule(LEVEL_INFO, L"17");

				boost::system::error_code errorCode;

				boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);

				if (errorCode)
				{
					LogStringModule(LEVEL_INFO, L"errorCode: %s", errorCode.message().c_str());
					throw errorCode;
				}

				_socket.set_verify_mode(boost::asio::ssl::verify_none);
				_socket.set_verify_callback([&](bool preverified,
					boost::asio::ssl::verify_context& ctx)
				{
					// The verify callback can be used to check whether the certificate that is
					// being presented is valid for the peer. For example, RFC 2818 describes
					// the steps involved in doing this for HTTPS. Consult the OpenSSL
					// documentation for more details. Note that the callback is called once
					// for each certificate in the certificate chain, starting from the root
					// certificate authority.

					// In this example we will simply print the certificate's subject name.
					char subject_name[256];
					X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
					X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

					std::cout << subject_name << std::endl;

					LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");

					return preverified;
				});
				//LogStringModule(LEVEL_INFO, L"18");


				_socket.async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);

				//LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(errorCode.message()));

				//LogStringModule(LEVEL_INFO, L"19");

				if (errorCode)
				{
					LogStringModule(LEVEL_INFO, L"errorCode: %s", errorCode.message().c_str());
					throw errorCode;
				}

				//LogStringModule(LEVEL_INFO, L"19_1");

				std::vector<TgBot::HttpReqArg> args;
				if (!params._request.empty())
				{
					args.push_back(TgBot::HttpReqArg("jsondata", params._request));
				}

				std::string httpRequest = TgBot::HttpParser::getInstance().generateRequest(params._request_uri, args, params._method);

				LogStringModule(LEVEL_FINEST, L"Write: %s", stow(httpRequest).c_str());

				//LogStringModule(LEVEL_INFO, L"19_2");

				_socket.async_write_some(boost::asio::buffer(httpRequest), yield[errorCode]);

				//LogStringModule(LEVEL_INFO, L"20");

				if (errorCode)
				{
					throw errorCode;
				}

				std::string in_data;
				int len = 0;

				do
				{
					//LogStringModule(LEVEL_INFO, L"21");

					char reply_[TCP_PACK_SIZE] = {};
					_socket.async_read_some(boost::asio::buffer(reply_), yield);
					if (errorCode)
					{
						throw errorCode;
					}

					len = strlen(reply_);

					in_data.append(reply_, len < TCP_PACK_SIZE ? len : TCP_PACK_SIZE);

				} while (!read_complete_predicate(len));

				LogStringModule(LEVEL_FINEST, L"Read: %s", stow(in_data).c_str());
				
				if (std::is_lvalue_reference<TParams>::value && params._response)
				{
					
					*(std::remove_const_t<TParams>(params)._response) = in_data;
				}
			}
			catch (boost::system::error_code& error)
			{
				if (error != boost::asio::error::eof)
					//std::cout << error.message() << std::endl;v
					LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
			}
			catch (std::runtime_error & error)
			{
				LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
			}
			catch (...)
			{
				LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
			}


		});

		int test = 0;

		return true;
	}
}

/******************************* eof *************************************/