/************************************************************************/
/* Name     : MVEON\Common\Socket\SpawnClient.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>
#include "tgbot/net/HttpParser.h"
#include "SpawnClient.h"
#include "MessageParser.h"
#include "Log/SystemLog.h"

namespace spawn_client
{
	const int TCP_PACK_SIZE = 1024;
	static unsigned int _count = 0;

	bool makeClient(boost::asio::io_service* io_service, TConnectionClientParams& params, CSystemLog * pLog, bool bPut/* = false*/)
	{
		boost::asio::spawn(*io_service, [io_service = io_service, &params, pLog = pLog, bPut = bPut](boost::asio::yield_context yield)
		{
			std::wstring clientName = L"Client " + std::to_wstring(++_count);

			auto LogStringModule = [pLog, clientName = std::move(clientName)](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
			{
				pLog->LogStringModule(level, clientName.c_str(), formatMessage, std::forward<decltype(args)>(args)...);
			};

			auto read_complete_predicate = [&](int data_len)
			{
				if (!data_len)
					return true;
				return 	data_len < TCP_PACK_SIZE;
			};

			boost::asio::steady_timer timer(*io_service);
			boost::system::error_code errorCode;

			//singleton_auto_pointer<CMessageCollector> collector;

			try
			{
				//const auto &requestMsg = params._msg;

				{

					// makeClient
					LogStringModule(LEVEL_INFO, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());

					boost::asio::ip::tcp::resolver resolver(*io_service);
					boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
					boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
					boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();


					boost::asio::ip::tcp::socket _socket(*io_service);
					boost::system::error_code errorCode;

					boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);

					if (errorCode)
					{
						LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
						throw errorCode;
					}

					//std::wstring request = CMessageParser::ToStringUtf(requestMsg);

					//boost::replace_all(request, "\\", "\\\\");
					//std::string url;

					//requestMsg.CheckParam(L"cid", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
					//ChatId cid = requestMsg.SafeReadParam(L"cid", CMessage::CheckedType::String, L"").AsWideStr();

					//if (requestMsg == L"CHATMESSAGE" || requestMsg == L"END_SESSION" || requestMsg == L"SCRIPTMESSAGE")
					//{
					//	url = "http://" + params._uri + "/chatsessions/" + wtos(cid) + "/chatmessages";
					//}
					//if (requestMsg == L"TRANSFER")
					//{
					//	bPut = true;
					//	url = "http://" + params._uri + "/chatsessions/" + wtos(cid) + "/operator";
					//}
					//if (requestMsg == L"SESSION_STATUS")
					//{
					//	bPut = true;
					//	url = "http://" + params._uri + "/chatsessions/" + wtos(cid);
					//}

					//std::vector<TgBot::HttpReqArg> args;
					//args.push_back(TgBot::HttpReqArg("jsondata", request));

					//std::string cookie, expires;

					//if (params._needCookie)
					//{
					//	cookie = utils::toStr<char>(params._clientId);
					//}

					//std::string httpRequest = TgBot::HttpParser::getInstance().generateRequest(params._request_uri, args, /*cookie, expires,*/ false, bPut);

					LogStringModule(LEVEL_FINEST, L"Write: %s", params._request.c_str());

					std::string in_data;
					int len = 0;

					_socket.async_write_some(boost::asio::buffer(params._request), yield[errorCode]);

					if (errorCode)
					{
						throw errorCode;
					}

					try
					{
						do
						{
							char reply_[TCP_PACK_SIZE] = {};
							_socket.async_read_some(boost::asio::buffer(reply_), yield);
							if (errorCode)
							{
								throw errorCode;
							}

							len = strlen(reply_);

							in_data.append(reply_, len < TCP_PACK_SIZE ? len : TCP_PACK_SIZE);

						} while (!read_complete_predicate(len));
					}
					catch (boost::system::error_code& error)
					{
						if (error != boost::asio::error::eof)
							LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
					}
					catch (std::runtime_error & error)
					{
						LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
					}
					catch (...)
					{
						LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
					}


					LogStringModule(LEVEL_FINEST, L"Read: %s", in_data.c_str());

					params._response = in_data;

					if (in_data == "500")
					{
						//ToDo: Error answer here
					}

				}
			}
			catch (boost::system::error_code& error)
			{
				if (error != boost::asio::error::eof)
					LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
			}
			catch (std::runtime_error & error)
			{
				LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
			}
			catch (...)
			{
				LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
			}

		});


		int test = 0;
		return false;
	}
}



/******************************* eof *************************************/