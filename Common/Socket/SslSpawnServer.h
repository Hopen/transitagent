/************************************************************************/
/* Name     : MVEON\Common\Socket\SslSpawnServer.h                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Feb 2017                                               */
/************************************************************************/

#pragma once

#include <string>
#include <functional>
#include "Router/router_compatibility.h"
#include "Base.h"

namespace ssl_spawn_server
{
	struct TConnectionServerParams
	{
		std::string _uri;
		std::string _port;
		std::string _token;
		int _iServerTimeout;
		std::string _sertificate;
		std::string _private_key;
		uint32_t  _clientId;
		std::string _server_uri;
		std::string _server_port;
		ChatTextMessageHandler_s _handler;
	};

	bool makeMySslServer(boost::asio::io_service *io_service, TConnectionServerParams&& params, CSystemLog *pLog);
}
/******************************* eof *************************************/