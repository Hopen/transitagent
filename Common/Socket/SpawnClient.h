/************************************************************************/
/* Name     : MVEON\Common\Socket\SpawnClient.h                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/

#pragma once

#include <string>
#include <functional>
#include "Router/router_compatibility.h"
#include "Base.h"

class CSystemLog;

namespace spawn_client
{
	struct TConnectionClientParams
	{
		std::string _uri;
		std::string _port;
		int _iClientTimeout;
		uint32_t  _clientId;

		std::string _request;
		std::string _request_uri;
		std::string _response;
		//CMessage    _msg;
		bool _needCookie;
	};

	bool makeClient(boost::asio::io_service* io_service, TConnectionClientParams& params, CSystemLog * pLog, bool bPut = false);
}

/******************************* eof *************************************/