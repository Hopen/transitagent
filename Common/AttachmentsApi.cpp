/************************************************************************/
/* Name     : MVEON\AttachmentsApi.cpp                                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 12 Feb 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "AttachmentsApi.h"
#include "utils.h"

namespace attachments
{
	AttachmentsList extractAttachments(const CMessage& reqMsg)
	{
		AttachmentsList attachments;
		if (reqMsg.HasParam(L"attachments"))
		{
			auto messages = reqMsg[L"attachments"].AsMessagesVector();
			for (const auto& msg : messages)
			{
				std::wstring url = utils::toLower(msg[L"url"].AsWideStr());
				int size = utils::toNum<int, wchar_t>(msg[L"fileSize"].AsWideStr());
				std::wstring name = utils::toLower(msg[L"name"].AsWideStr());

				attachments.emplace_back(url, name, size);
			}

			//CMessage newAttachMsg = reqMsg[L"attachments"].AsMessage();
			//for (auto cit = newAttachMsg.begin(); cit != newAttachMsg.end(); ++cit)
			//{
			//	CMessage param = cit->second.AsMessage();

			//	std::wstring url = param[L"url"].AsWideStr();
			//	int size = utils::toNum<int, wchar_t>(param[L"size"].AsWideStr());
			//	std::wstring name = param[L"name"].AsWideStr();

			//	attachments.emplace_back(url, name, size);
			//}
		}

		return attachments;
	}

	static std::wstring makeUri(
		const ChatId& cid,
		const std::wstring& aInitString,
		const std::wstring& aAttachName,
		const char endSimbol)
	{
		auto initString = utils::toLower(aInitString);
		auto attachName = utils::toLower(aAttachName);

		std::wstring uri = initString;
		if (initString[initString.length() - 1] != (endSimbol))
			uri += endSimbol;

		uri += cid;
		uri += endSimbol;
		uri += attachName;
		return uri;
	}

	static std::wstring makeUri(
		const ChatId& cid,
		const std::wstring& aDisk,
		const std::wstring& aFolder,
		const std::wstring& aAttachName,
		const char endSimbol)
	{
		auto disk = utils::toLower(aDisk);
		auto folder = utils::toLower(aFolder);

		std::wstring uri = disk;
		uri += L":/" + folder;

		return makeUri(cid, uri, aAttachName, endSimbol);
	}

	std::wstring GetUri(
		const ChatId& cid,
		const std::wstring& sAttachmentString,
		const std::wstring& attachName)
	{
		return makeUri(cid, sAttachmentString, attachName, '/');
	}

	std::wstring GetLocalUri(
		const ChatId& cid,
		const std::wstring& aAttachmentDisk,
		const std::wstring& aAttachmentFolder,
		const std::wstring& aAttachName)
	{
		return makeUri(cid, aAttachmentDisk, aAttachmentFolder, aAttachName, '\\');
	}

	std::pair<std::string, size_t> GetFileSource(const std::wstring& aFileName)
	{
		auto fileName = utils::toLower(aFileName);

		std::string sSource;

		std::ifstream _in(fileName, std::ios::binary);
		DWORD   dwDataSize = 0;
		if (_in.seekg(0, std::ios::end))
		{
			dwDataSize = static_cast<DWORD>(_in.tellg());
		}
		std::string buff(dwDataSize, 0);
		if (dwDataSize && _in.seekg(0, std::ios::beg))
		{
			std::copy(std::istreambuf_iterator< char>(_in),
				std::istreambuf_iterator< char >(),
				buff.begin());

			sSource = buff;
		}
		_in.close();

		return std::make_pair(sSource, dwDataSize);
	}

	std::pair<std::string, size_t> readAttachment2(
		const ChatId& cid,
		const std::wstring& aFileName,
		const std::wstring& aAttachmentDisk,
		const std::wstring& aAttachmentFolder)
	{
		auto fileName = utils::toLower(aFileName);
		auto attachmentFolder = utils::toLower(aAttachmentFolder);

		std::wstring localPath = GetLocalUri(cid, aAttachmentDisk, attachmentFolder, fileName);
		return GetFileSource(localPath);
	}

	std::string readAttachment(
		const std::wstring& aAttachPath,
		const std::wstring& aAttachSourceName,
		const std::wstring& aAttachmentString,
		const std::wstring& aAttachmentFolder)
	{
		std::string sSource;

		auto attachPath = utils::toLower(aAttachPath);
		auto attachSourceName = utils::toLower(aAttachSourceName);
		auto attachmentString = utils::toLower(aAttachmentString);
		auto attachmentFolder = utils::toLower(aAttachmentFolder);

		std::wstring localPath;
		int pos = attachPath.find(attachmentString);
		if (pos >= 0)
		{
			localPath = attachPath.substr(pos + attachmentString.length(), attachPath.length());
		}
		if (!localPath.empty())
		{
			if (attachmentFolder[attachmentFolder.length() - 1] == ('\\'))
			{
				localPath = attachmentFolder + localPath;
			}
			else
			{
				localPath = attachmentFolder + L"\\" + localPath;
			}

			try
			{
				std::ifstream _in(localPath.c_str(), std::ios::binary);
				DWORD   dwDataSize = 0;
				if (_in.seekg(0, std::ios::end))
				{
					dwDataSize = static_cast<DWORD>(_in.tellg());
				}
				std::string buff(dwDataSize, 0);
				if (dwDataSize && _in.seekg(0, std::ios::beg))
				{
					std::copy(std::istreambuf_iterator< char>(_in),
						std::istreambuf_iterator< char >(),
						buff.begin());

					sSource = buff;
				}
				_in.close();
			}
			catch (std::runtime_error &e)
			{
				sSource = (boost::format("Exception when reading \"%s\", what: %s") % localPath.c_str() % e.what()).str();
			}
			catch (...)
			{
				sSource = (boost::format("Exception when reading \"%s\"") % localPath.c_str()).str();
			}
		}

		return sSource;
	}

	std::wstring CreateGUID()
	{
		boost::uuids::random_generator gen;
		auto callUUID = gen();
		return boost::lexical_cast<std::wstring>(callUUID);
	}

	TAttachmentsParam saveAttachment(ChatId cid,
		const std::wstring& aAttachmentDisk,
		const std::wstring& aAttachmentFolder,
		const std::wstring& aAttachmentString,
		const std::string& aContent,
		const std::wstring& aSaveAttachName,
		const std::wstring& aAttachName)
	{
		auto attachmentDisk = utils::toLower(aAttachmentDisk);
		auto attachmentFolder = utils::toLower(aAttachmentFolder);
		auto attachmentString = utils::toLower(aAttachmentString);
		auto saveAttachName = utils::toLower(aSaveAttachName);
		aAttachName;

		std::wstring localPath = GetLocalUri(cid, attachmentDisk, attachmentFolder, saveAttachName);
		std::wstring fileUrl = GetUri(cid, attachmentString, saveAttachName);

		auto folder = boost::filesystem::path(localPath).parent_path();

		if (!boost::filesystem::exists(folder) && !boost::filesystem::create_directories(folder))
		{
			throw(std::runtime_error((boost::format("Cannot create \"%s\" folder") % folder.c_str()).str()));
		}
		else
		{
			std::fstream ofs(localPath, std::fstream::ios_base::out | std::fstream::ios_base::binary);
			ofs.write(aContent.c_str(), aContent.size());
			ofs.close();
		}

		return TAttachmentsParam(fileUrl, aAttachName, aContent.size());
	}

	std::wstring getLocalUrlByRemote(
		const std::wstring& aAttachPath, 
		const std::wstring& aAttachmentString,
		const std::wstring& aAttachmentDisk,
		const std::wstring& aAttachmentFolder)
	{
		auto attachPath = utils::toLower(aAttachPath);
		auto attachmentDisk = utils::toLower(aAttachmentDisk);
		auto attachmentString = utils::toLower(aAttachmentString);
		auto attachmentFolder = utils::toLower(aAttachmentFolder);

		int pos = attachPath.find(attachmentString);

		if (pos < 0)
		{
			throw std::runtime_error("Invalid AttachmentString");
		}

		std::wstring localPath(attachPath.cbegin() + pos + attachmentString.size(), attachPath.cend());

		if (!localPath.empty() 
			&& localPath[0] != '\\' 
			&& localPath[0] != '/')
		{
			localPath = L"\\" + localPath;
		}
		
		return attachmentDisk + L":/" + attachmentFolder + localPath;
	}

	std::wstring getFtpUrlByServerUrl(
		const std::wstring& aServerLocation,
		const std::wstring& aAttachmentFolder,
		const std::wstring& aFtpFolder,
		const std::wstring& aFtpHostName)
	{
		std::wstring serverLocation = utils::toLower(aServerLocation);
		std::wstring attachmentFolder = utils::toLower(aAttachmentFolder);

		std::wstring ftpHostName = L"http://" + aFtpHostName + L"/" + aFtpFolder;

		int pos = serverLocation.find(attachmentFolder);

		if (pos < 0)
		{
			throw std::runtime_error("Invalid AttachmentString");
		}

		std::wstring localPath(serverLocation.cbegin() + pos + attachmentFolder.size(), serverLocation.cend());

		if (!localPath.empty()
			&& localPath[0] != '\\'
			&& localPath[0] != '/')
		{
			localPath = L"\\" + localPath;
		}

		return ftpHostName + localPath;
	}
}

/******************************* eof *************************************/