///************************************************************************/
///* Name     : MVEON\ParseMessageStatus.h                                */
///* Author   : Andrey Alekseev                                           */
///* Project  : Multimedia                                                */
///* Company  : Expert Solutions                                          */
///* Date     : 22 Jan 2017                                               */
///************************************************************************/
//#pragma once
//#include "Base.h"
//
//enum class E_PARSE_ERROR
//{
//	ER_UNKNOWN = 0,
//	ER_INVALID_URI,
//	ER_INVALID_PARTNERS,
//	ER_PARTNERS_REQUIRED,
//	ER_INVALID_METHOD,
//	ER_INVALID_HEADER
//};
//
//class ParseException : public std::exception
//{
//public:
//	explicit ParseException(E_PARSE_ERROR aError)
//		: std::exception(ErrorToString(aError))
//		, mError(aError)
//	{
//	}
//
//	explicit ParseException(E_PARSE_ERROR aError, char const* const aMessage)
//		: std::exception(aMessage)
//		, mError(aError)
//	{
//	}
//
//	int GetError()const
//	{
//		return ErrorToInt(mError);
//	}
//private:
//	static constexpr int ErrorToInt(E_PARSE_ERROR aError)
//	{
//		return (aError == E_PARSE_ERROR::ER_INVALID_URI ? 404
//			: (aError == E_PARSE_ERROR::ER_INVALID_PARTNERS ? 404
//			: (aError == E_PARSE_ERROR::ER_PARTNERS_REQUIRED ? 404
//			: (aError == E_PARSE_ERROR::ER_INVALID_METHOD ? 405
//			: (aError == E_PARSE_ERROR::ER_INVALID_HEADER ? 404
//			: 404)))));
//	}
//	static constexpr char* ErrorToString(E_PARSE_ERROR aError)
//	{
//		return (aError == E_PARSE_ERROR::ER_INVALID_URI ? "Invalid uri format" 
//			: (aError == E_PARSE_ERROR::ER_INVALID_PARTNERS ? "Not Found" 
//			: (aError == E_PARSE_ERROR::ER_PARTNERS_REQUIRED ? "Uri \"partners\" param required"
//			: (aError == E_PARSE_ERROR::ER_INVALID_METHOD ? "Method Not Allowed"
//			: (aError == E_PARSE_ERROR::ER_INVALID_HEADER ? "Invalid http header status"
//			: "Unknown")))));
//	}
//
//private:
//	E_PARSE_ERROR mError;
//};
//
//
//TMessageStatus ParseMessageChatStatus(std::string status);
//
//TMessageStatus ParseMessageVeonStatus(const std::string& aPartyId, const std::string& aStatus, const std::string& aHttpMethod);
//
///******************************* eof *************************************/