/************************************************************************/
/* Name     : MVEON\ParseMessageStatus.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 22 Jan 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include "ParseMessageStatus.h"

TMessageStatus ParseMessageChatStatus(std::string status)
{
	TMessageStatus result;

	int pos = status.find("/chatsessions");
	if (pos < 0)
		throw ParseException(E_PARSE_ERROR::ER_INVALID_HEADER);

	result.methodName = L"M2MCC_START_SESSION";

	//pos += strLen(L"/chatsessions") - 1;
	pos += 13;
	status = status.substr(pos, status.size() - pos);

	pos = status.find("/");
	if (pos != std::string::npos)
	{
		status = status.substr(pos + 1, status.size() - pos - 1);

		pos = status.find("/");
		if (pos >= 0)
		{
			std::string chatId = status.substr(0, pos);
			result.chatId = stow(chatId);
			result.methodName = L"M2MCC_CHATMESSAGE";
		}
		else if (status.find("?timestamp") != std::string::npos)
		{
			result.methodName = L"M2MCC_GET_SESSION_STATUS";
			pos = status.find("?timestamp");

			std::wstring chatId = stow(status).substr(0, pos);
			result.chatId = chatId;
			//pos += strLen(L"?timestamp");
			pos += 11;
			result.timeStamp = stow(status.substr(pos, status.size() - pos));
		}
		else
		{
			result.methodName = L"M2MCC_GET_ONLY_SESSION_STATUS";
			//pos = status.find("?timestamp");

			std::wstring chatId = stow(status).substr(0, pos);
			result.chatId = chatId;
			////pos += strLen(L"?timestamp");
			//pos += 11;
			//result.timeStamp = stow(status.substr(pos, status.size() - pos));
		}
	}

	return result;
}


template <class T, std::size_t size>
constexpr std::size_t strLen(T(&)[size])
{
	return size;
}

int FindString(const std::string& aRequest, char* aFindString, E_PARSE_ERROR aError, int aStartPos = 0)
{
	int pos = aRequest.find(aFindString, aStartPos);
	if (pos < 0)
	{
		throw ParseException(aError);
	}
	return pos;
}

void CheckStatus(const std::string& aPartyId, const std::string& aStatus)
{
	if (aStatus[aStatus.size() - 1] == '/')
	{
		throw ParseException(E_PARSE_ERROR::ER_INVALID_URI);
	}

	int pos = FindString(aStatus, "partners/", E_PARSE_ERROR::ER_PARTNERS_REQUIRED);

	pos += 9; // strlen("partners/")

	int pos2 = FindString(aStatus, "/", E_PARSE_ERROR::ER_INVALID_HEADER, pos);

	auto partyId = aStatus.substr(pos, pos2 - pos);
	if (partyId != aPartyId)
	{
		throw ParseException(E_PARSE_ERROR::ER_INVALID_PARTNERS);
	}
}

TMessageStatus ParseMessageVeonStatus(const std::string& aPartyId, const std::string& aStatus, const std::string& aHttpMethod)
{
	TMessageStatus result;

	std::string status(aStatus);

	CheckStatus(aPartyId, status);

	result.methodName = VEON_START_SESSION;

	int pos = FindString(status, "/conversations", E_PARSE_ERROR::ER_INVALID_HEADER);

	//pos += strLen("/conversations") - 1;
	pos += 14;
	status = status.substr(pos, status.size() - pos);

	pos = status.find("/");
	if (pos != std::string::npos)
	{
		int pos2 = status.find("/", pos + 1);
		if (pos2 == std::string::npos)
		{
			result.chatId = stow(status.substr(pos + 1, status.size()));

			result.methodName = VEON_UPDATE_SESSION;

			if (aHttpMethod == "DELETE"
				|| aHttpMethod == "PUT"
				|| aHttpMethod == "POST"
				|| aHttpMethod == "GET")
			{
				throw ParseException(E_PARSE_ERROR::ER_INVALID_METHOD);
			}
		}
		else
		{
			result.chatId = stow(status.substr(pos + 1, pos2 - pos - 1));
			status = status.substr(pos2 + 1, status.size() - pos2 - 1);

			pos = status.find("participants/");
			if (pos >= 0)
			{
				//pos += strLen("/participants") - 1;
				pos += 13;
				result.methodName = VEON_UPDATE_CUSTOMER;
				result.userId = stow(status.substr(pos, status.size() - pos));
			}
			else
			{
				pos = status.find("messages");
				if (pos >= 0)
				{
					result.methodName = VEON_MESSAGE;
					if (aHttpMethod == "DELETE" 
						|| aHttpMethod == "PATCH"
						|| aHttpMethod == "PUT")
					{
						throw ParseException(E_PARSE_ERROR::ER_INVALID_METHOD);
					}
				}
				else
				{
					throw ParseException(E_PARSE_ERROR::ER_INVALID_URI);
				}
			}
		}

	}
	else if (aHttpMethod == "DELETE"
		|| aHttpMethod == "PUT"
		|| aHttpMethod == "PATCH"
		|| aHttpMethod == "GET")
	{
		throw ParseException(E_PARSE_ERROR::ER_INVALID_METHOD);
	}

	return result;
}
/******************************* eof *************************************/