#pragma once
#include "windows.h"

namespace Threading
{

class CThread
{
public:
	CThread();
	virtual ~CThread();

	virtual void Start();
	virtual void Stop();

protected:
	virtual void Process() = 0;

protected:
	static unsigned __stdcall threadFunction(void * pData);

	HANDLE	m_hThread;
	HANDLE	m_hExitEvent;
};

}