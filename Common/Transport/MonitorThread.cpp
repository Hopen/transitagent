#include "StdAfx.h"
#include ".\monitorthread.h"
#include <algorithm>

CMonitorThread::CMonitorThread()
{
}

CMonitorThread::~CMonitorThread(void)
{
}

void CMonitorThread::Process()
{
	::CoInitialize(NULL);//, COINIT_MULTITHREADED);
	while(::WaitForSingleObject(m_hExitEvent, 1000) == WAIT_TIMEOUT)
	{
		std::lock_guard<std::mutex> lock(m_guardSection);

		for(int i = 0 ; i < m_files.size(); i++)
		{
			if(m_files[i]->m_blErase == false && m_files[i]->VerifyFile())
			{
				try
				{
					m_files[i]->m_blErase = m_files[i]->ExecuteAction();
				}
				catch(...)
				{
				}
			}
		}

		for(int i = 0; i < m_files.size(); i++)
		{
			if(m_files[i]->m_blErase == true)
			{
				delete m_files[i];
				m_files.erase(m_files.begin() + i);
				i--;
			}
		}

		for(int i = 0; i < m_files.size(); i++)
		{
			if(m_files[i]->m_blErase)
				m_pLog->LogString(LEVEL_INFO, _T("Job for file %s not cleared"), m_files[i]->m_strFileName);
		}

		m_pLog->LogString(LEVEL_FINEST, _T("Pending file operations: %i"), m_files.size());
	}
	::CoUninitialize();
}

void CMonitorThread::AddMonitor(IMonitorAction * _fa)
{
	std::lock_guard<std::mutex> lock(m_guardSection);

	m_files.push_back(_fa);
}

bool IMonitorAction::VerifyFile()
{
	if(::PathFileExists(m_strFileName.c_str()) == FALSE)
		return true;

	if(::GetFileAttributes(m_strFileName.c_str()) & FILE_ATTRIBUTE_READONLY)
		return false;

	HANDLE hFile = ::CreateFile(m_strFileName.c_str(),
		DELETE | GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (hFile != INVALID_HANDLE_VALUE)
	{ 
		::CloseHandle(hFile); 
		return true;
	}

	return false;
}
