#pragma once
#include "MonitorThread.h"
#import	 "c:\is3\Transport\ISTransferService.exe" no_namespace named_guids
//#include "ContentManager.h"
//#include "Settings.h"
//#include "SettingVector.h"
#include <boost/signals.hpp>
#include "locked_vector.h"
//#include "conversion.h"

class CJobInfo
{
public:
	long		m_jobId;
	FILETIME	m_replicationTime;
	DWORD		m_dwClientId;
	DWORD		m_dwChildId;
	std::wstring		m_strDestination;
	int			m_status;
	std::wstring m_strTemporary;
	bool		m_blDownload;
	bool		m_blConfig;
	
	std::wstring mCommitUrl;
	std::wstring mMessageId;
	std::wstring mFileName;
	size_t mFileSize;

public:
	CJobInfo();
	CJobInfo(
		long _jobId, 
		FILETIME	_replicationTime, 
		DWORD	_dwClientId, 
		DWORD _dwChildId, 
		LPCTSTR _strDestination, 
		LPCTSTR _lpszTemporary, 
		bool _blDownload,
		const std::wstring& aCommitUrl,
		const std::wstring& aMessageId,
		const std::wstring& aFileName,
		size_t aFileSize);

	CJobInfo(
		long _jobId, 
		DWORD	_dwClientId, 
		DWORD _dwChildId, 
		LPCTSTR _strDestination, 
		bool _blDownload);

	CJobInfo(const CJobInfo& _job);
	~CJobInfo() { }

	CJobInfo& operator = (const CJobInfo& _job)
	{
		m_jobId								= _job.m_jobId;
		m_replicationTime.dwHighDateTime	= _job.m_replicationTime.dwHighDateTime;
		m_replicationTime.dwLowDateTime		= _job.m_replicationTime.dwLowDateTime;
		m_dwChildId							= _job.m_dwChildId;
		m_dwClientId						= _job.m_dwClientId;
		m_strDestination					= _job.m_strDestination;
		m_strTemporary						= _job.m_strTemporary;
		m_status							= _job.m_status;
		m_blDownload						= _job.m_blDownload;
		m_blConfig							= _job.m_blConfig;
		mCommitUrl							= _job.mCommitUrl;
		mMessageId							= _job.mMessageId;
		mFileName							= _job.mFileName;
		mFileSize							= _job.mFileSize;
		return *this;
	}

	CJobInfo& operator = (const std::wstring& _str);
	operator std::wstring() const;

	bool operator == (int _nJobId) const { return m_jobId == _nJobId; }
};


class CTransportManager
{
public:
	typedef boost::signals::connection							Connector;
	typedef boost::signal<void (int)>::slot_function_type		JobHandler;

	typedef long JobId;

private:
	singleton_auto_pointer<CSystemLog>		m_pLog;
	singleton_auto_pointer<CMonitorThread>	m_monitor;
	IStream *								m_pCopyManagerStream;
	//ATL::CCriticalSection&					m_lock;
	std::mutex m_lock;

	boost::signal<void (int)>				m_jobCompleteSignal;
	boost::signal<void (int)>				m_jobErrorSignal;

	locked_vector<long>						m_jobs;

	class CTransportMonitor : public IMonitorAction
	{
		CTransportManager *		m_pManager;

	public:
		CTransportMonitor(CTransportManager * _pManager) : IMonitorAction(_T("Transport Manager")), m_pManager(_pManager)
		{
		}

		virtual bool VerifyFile() { return true; };
		virtual	bool ExecuteAction();
	};

	void OnJobError(JobId _nJobId);
	void OnJobComplete(JobId _nJobId);
	void SplitPath(LPCTSTR _lpszPath, std::wstring& server, std::wstring& path);
	
	bool functionJobStatusWatcher(IInServ * _transport, int _nJobId);

	void				tryFileDownload(LPCTSTR lpszSource, LPCTSTR lpszTempPath, DWORD _dwClientId, DWORD _dwChildId, FILETIME& _replicationTime, LPCTSTR _lpszDestination);
	CComPtr<IInServ>	recreateInstance();

public:
	CTransportManager();
	~CTransportManager();


	JobId 		AddFileUpload(LPCTSTR lpszSource, LPCTSTR lpszAddress, LPCTSTR lpszLogin, LPCTSTR lpszPassword, int iPort);
	JobId 		AddFileDownload(LPCTSTR lpszSource, LPCTSTR lpszDestination, LPCTSTR lpszLogin, LPCTSTR lpszPassword, int iPort);

	void		MonitorJobProcess();
	void		AddJobToMonitor(long _nJobId)				{ m_jobs.add(_nJobId); }

	Connector	AddJobCompleteHandler(JobHandler _handler)	{ return m_jobCompleteSignal.connect(_handler); }
	Connector	AddJobErrorHandler(JobHandler _handler)		{ return m_jobErrorSignal.connect(_handler); }
};
