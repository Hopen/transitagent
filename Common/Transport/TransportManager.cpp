#include "StdAfx.h"
#include <boost/bind.hpp>
#include "TransportManager.h"
//#include "ContentManager.h"
//#include "Locker.h"
//#include "Scheduler.h"
//#include "ConvertAction.h"
#include <MMReg.h>
//#include "ReplicationEvent.h"
//#include "ReplicaAction.h"
//#undef AddJob

CJobInfo::CJobInfo()
{
}

CJobInfo::CJobInfo(
	long _jobId, 
	FILETIME _replicationTime, 
	DWORD _dwClientId, 
	DWORD _dwChildId, 
	LPCTSTR _strDestination, 
	LPCTSTR _lpszTemporary, 
	bool _blDownload,
	const std::wstring& aCommitUrl,
	const std::wstring& aMessageId,
	const std::wstring& aFileName,
	size_t aFileSize)
	: m_jobId(_jobId)
	, m_replicationTime(_replicationTime)
	, m_dwClientId(_dwClientId)
	, m_dwChildId(_dwChildId)
	, m_strDestination(_strDestination)
	, m_strTemporary(_lpszTemporary)
	, m_blDownload(_blDownload)
	, m_blConfig(false)
	, mCommitUrl(aCommitUrl)
	, mMessageId(aMessageId)
	, mFileName(aFileName)
	, mFileSize(aFileSize)
{
}

CJobInfo::CJobInfo(const CJobInfo& _job)
{
	*this = _job;
}

CJobInfo::operator std::wstring() const
{
	std::wstring strResult = (boost::wformat(L"%i,%i,%i,%i,%i,%s,%s,%s") 
		% m_jobId 
		% m_replicationTime.dwHighDateTime
		% m_replicationTime.dwLowDateTime
		% m_dwClientId 
		% m_dwChildId 
		% (m_blDownload == true ? L"true" : L"false")
		% m_strTemporary.c_str()
		% m_strDestination.c_str()
		).str();

	return strResult;
}


CJobInfo& CJobInfo::operator = (const std::wstring& _str)
{
	TCHAR szPath[MAX_PATH], szTempPath[MAX_PATH], szDownload[MAX_PATH];

	if(_stscanf(_str.c_str(), _T("%i,%i,%i,%i,%i,%[^,],%[^,],%[^\r\n]"), &m_jobId, &m_replicationTime.dwHighDateTime, 
		&m_replicationTime.dwLowDateTime, &m_dwClientId, &m_dwChildId, szDownload, szTempPath, szPath) != 8)
		throw std::runtime_error("Not enough arguments provided");

	m_strDestination	= szPath;
	m_strTemporary		= szTempPath;
	m_blDownload		= _tcsicmp(_T("true"), szDownload) == 0;
	return *this;
}

CTransportManager::CTransportManager()
{
	try
	{
		CComPtr<IInServ> transfer = recreateInstance();
		/*
		HRESULT hr = ::CoMarshalInterThreadInterfaceInStream(__uuidof(IInServ), transfer, &m_pCopyManagerStream);
		if(!SUCCEEDED(hr))
			throw _com_error(hr);
		*/
	}
	catch(_com_error& error)
	{
		m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to connect to IS Transport Service. Error code: 0x%08x. %s"), error.Error(), error.ErrorMessage());
		throw;
	}

	m_monitor->AddMonitor(new CTransportMonitor(this));
	m_monitor->Start();
}

CTransportManager::~CTransportManager(void)
{
}

void CTransportManager::SplitPath(LPCTSTR _lpszPath, std::wstring& server, std::wstring& path)
{
	std::wstring strPath(_lpszPath);

	int size = strlen("http://");
	int pos = strPath.find(L"http://");
	if (pos < 0)
	{
		pos = strPath.find(L"ftp://");
		size = strlen("ftp://");
	}

	if (pos >= 0)
	{
		server = std::wstring(strPath.cbegin() + pos + size, strPath.cend());
	}
	else
	{
		throw std::exception("Invalid URL");
	}

	int pos1 = server.find(L"/");
	int pos2 = server.find(L"\\");
	if (pos1 < 0 && pos2 < 0)
	{
		std::exception("Invalid URL");
	}

	if (pos1 > 0 && pos2 < 0)
	{
		pos = pos1;
	}
	else if (pos1 < 0 && pos2 > 0)
	{
		pos = pos2;
	}
	else if (pos1 > 0 && pos2 > 0)
	{
		pos = pos1 < pos2 ? pos1 : pos2;
	}
	else
	{
		std::exception("Invalid URL");
	}

	path = std::wstring(server.cbegin() + pos + 1, server.cend());
	server = std::wstring(server.cbegin(), server.cbegin() + pos);

	//if (strPath.Left(7) == _T("http://"))
	//{
	//	server = strPath.Mid(7);
	//}
	//else if (strPath.Left(6) == _T("ftp://"))
	//{
	//	server = strPath.Mid(6);
	//}
	//else
	//	throw std::exception("Invalid URL");

	//server.Trim(_T("/\\"));
	//int nIndex = server.Find(_T("/"));
	//if (nIndex == -1)
	//	throw std::exception("Invalid URL");

	//path = server.Mid(nIndex);
	//server = server.Left(nIndex);
	//server.Trim(_T("/\\"));
	//path.Trim(_T("/\\"));
}

//CTransportManager::JobId CTransportManager::AddFileDownload(LPCTSTR lpszSource, LPCTSTR lpszTempPath, LPCTSTR lpszLogin, LPCTSTR lpszPassword, int iPort)
//{
//	std::lock_guard<std::mutex> lock(m_lock);
//	long lJobId = -1;
//	m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSFER SERVICE"),_T("Starting download of [%s] to [%s]"), lpszSource, lpszTempPath);
//	int counter = 2;
//	try
//	{
//		CComPtr<IInServ> transferService = recreateInstance();
//
//		do 
//		{
//			try
//			{
//				std::wstring strPath, strServer;
//				SplitPath(lpszSource, strServer, strPath);
//				DWORD dwPort = 80;
//				
//				int pos = strServer.find(L":");
//				if(pos >= 0)
//				{
//					auto port = std::wstring(strServer.cbegin() + pos, strServer.cend());
//					dwPort = _ttoi(port.c_str());
//					strServer = std::wstring(strServer.cbegin(), strServer.cbegin() + pos);
//				}
//
//				lJobId = transferService->AddJob(stFtp, mdToServer, _bstr_t(strServer.c_str()), _bstr_t(lpszLogin), _bstr_t(lpszPassword), iPort, 1024 * 512, 1000);
//
//				transferService->AddFileToJob(lJobId, _bstr_t(strPath.c_str()), _bstr_t(lpszTempPath), FALSE);
//				transferService->JobStart(lJobId);
//				m_jobs.add(lJobId);
//				m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSFER SERVICE"), _T("JobID: %i. Download started successfully"), lJobId);
//				break;
//			}
//			catch(_com_error& _error)
//			{
//				m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to start transfer: HRESULT: 0x%08x, %s"), _error.Error(), (LPCTSTR)_error.Description());
//				transferService = recreateInstance();
//				counter--;
//			}
//			catch(...)
//			{
//				m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to start transfer: Unknown exception"));
//				throw;
//			}
//		} while(counter);
//	}
//	catch(_com_error& error)
//	{
//		m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to connect to IS Transport Service. Error code: 0x%08x. %s"), error.Error(), error.ErrorMessage());
//	}
//	return lJobId;
//}

CTransportManager::JobId CTransportManager::AddFileDownload(LPCTSTR lpszSource, LPCTSTR lpszTempPath, LPCTSTR lpszLogin, LPCTSTR lpszPassword, int iPort)
{
	long lJobId = -1;
	m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSFER SERVICE"), _T("Starting download of [%s] to [%s]"), lpszSource, lpszTempPath);
	int counter = 2;
	try
	{
		CComPtr<IInServ> transferService = recreateInstance();
		do
		{
			try
			{

				std::wstring strPath, strServer;
				SplitPath(lpszSource, strServer, strPath);
				lJobId = transferService->AddJob(stFtp, mdToClient, _bstr_t(strServer.c_str()), _bstr_t(lpszLogin), _bstr_t(lpszPassword), iPort, 1024 * 512, 1000);
				transferService->AddFileToJob(lJobId, _bstr_t(lpszTempPath), _bstr_t(strPath.c_str()), FALSE);
				transferService->JobStart(lJobId);
				m_jobs.add(lJobId);
				m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSFER SERVICE"), _T("JobID: %i. Download started successfully"), lJobId);
				break;
			}
			catch (_com_error& _error)
			{
				m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to start transfer: HRESULT: 0x%08x, %s"), _error.Error(), (LPCTSTR)_error.Description());
				recreateInstance();
				counter--;
			}
			catch (...)
			{
				break;
			}
		} while (counter);
	}
	catch (_com_error& error)
	{
		m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to connect to IS Transport Service. Error code: 0x%08x. %s"), error.Error(), error.ErrorMessage());
	}

	return lJobId;
}

void CTransportManager::tryFileDownload(LPCTSTR lpszSource, LPCTSTR lpszTempPath, DWORD _dwClientId, DWORD _dwChildId, FILETIME& _replicationTime, LPCTSTR _lpszDestination)
{
}

CTransportManager::JobId CTransportManager::AddFileUpload(LPCTSTR lpszSource, LPCTSTR lpszAddress, LPCTSTR lpszLogin, LPCTSTR lpszPassword, int iPort)
{
	long lJobId = -1;
	m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSFER SERVICE"), _T("Starting upload of [%s] to [%s]"), lpszSource, lpszAddress);
	int counter = 2;
	try
	{
		CComPtr<IInServ> transferService = recreateInstance();
		do
		{
			try
			{

				std::wstring strPath, strServer;
				SplitPath(lpszAddress, strServer, strPath);
				lJobId = transferService->AddJob(stFtp, mdToServer, _bstr_t(strServer.c_str()), _bstr_t(lpszLogin), _bstr_t(lpszPassword), iPort, 1024 * 512, 1000);
				transferService->AddFileToJob(lJobId, _bstr_t(lpszSource), _bstr_t(strPath.c_str()), FALSE);
				transferService->JobStart(lJobId);
				m_jobs.add(lJobId);
				m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSFER SERVICE"), _T("JobID: %i. Upload started successfully"), lJobId);
				break;
			}
			catch (_com_error& _error)
			{
				m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to start transfer: HRESULT: 0x%08x, %s"), _error.Error(), (LPCTSTR)_error.Description());
				recreateInstance();
				counter--;
			}
			catch (...)
			{
				break;
			}
		} while (counter);
	}
	catch (_com_error& error)
	{
		m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to connect to IS Transport Service. Error code: 0x%08x. %s"), error.Error(), error.ErrorMessage());
	}

	return lJobId;
}

//CTransportManager::JobId CTransportManager::AddFileUpload(LPCTSTR lpszSource, LPCTSTR lpszDestination, int iPort)
//{
//	long lJobId = -1;
//	m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSFER SERVICE"),_T("Starting upload of [%s] to [%s]"), lpszSource, lpszDestination);
//	int counter = 2;
//	try
//	{
//		CComPtr<IInServ> transferService = recreateInstance();
//		do 
//		{
//			try
//			{
//
//				CString strPath, strServer;
//				SplitPath(lpszDestination, strServer, strPath);
//				lJobId = transferService->AddJob(stFtp, mdToServer, _bstr_t(strServer), _bstr_t(""), _bstr_t(""), iPort, 1024 * 512, 1000);
//				transferService->AddFileToJob(lJobId, _bstr_t(lpszSource), _bstr_t(strPath), FALSE);
//				transferService->JobStart(lJobId);
//				m_jobs.add(lJobId);
//				m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSFER SERVICE"), _T("JobID: %i. Upload started successfully"), lJobId);
//				break;
//			}
//			catch(_com_error& _error)
//			{
//				m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to start transfer: HRESULT: 0x%08x, %s"), _error.Error(), (LPCTSTR)_error.Description());
//				recreateInstance();
//				counter--;
//			}
//			catch(...)
//			{
//				break;
//			}
//		} while(counter);
//	}
//	catch(_com_error& error)
//	{
//		m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to connect to IS Transport Service. Error code: 0x%08x. %s"), error.Error(), error.ErrorMessage());
//	}
//
//	return lJobId;
//}

bool CTransportManager::functionJobStatusWatcher(IInServ * _transport, int _nJobId)
{
	tagJobState state = _transport->GetJobState(_nJobId);

	switch(state)
	{
	case jsUnknown:
	case jsError:
	case jsAbort:
		OnJobError(_nJobId);
		_transport->JobStop(_nJobId);
		return true;
		break;
	case jsProcess:
	case jsInit:
		break;
	case jsPause:
		_transport->JobContinue(_nJobId);
		break;
	case jsSleep:
		break;
	case jsComplete:
		OnJobComplete(_nJobId);
		_transport->JobStop(_nJobId);
		return true;
		break;
	}

	return false;
}

void CTransportManager::MonitorJobProcess()
{
	try
	{
		CComPtr<IInServ> transferService = recreateInstance();
		std::lock_guard<std::mutex> lock(m_lock);
		m_jobs.remove_if(boost::bind(&CTransportManager::functionJobStatusWatcher, this, transferService, _1));
	}
	catch(_com_error& error)
	{
		m_pLog->LogStringModule(LEVEL_SEVERE, _T("TRANSFER SERVICE"), _T("Failed to connect to IS Transport Service. Error code: 0x%08x. %s"), error.Error(), error.ErrorMessage());
	}
}

CComPtr<IInServ> CTransportManager::recreateInstance()
{
	CComPtr<IInServ> transferService;
	HRESULT hr = transferService.CoCreateInstance(L"AtlServ.CoInServ");

	if(!SUCCEEDED(hr))
		throw _com_error(hr);

	return transferService;
}

void CTransportManager::OnJobError(JobId _nJobId)
{
	try
	{
		m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSPORT SERVICE"), _T("Job %i failed to download"), _nJobId);
		m_jobErrorSignal(_nJobId);
	}
	catch(...)
	{
	}
}

void CTransportManager::OnJobComplete(JobId _nJobId)
{
	try
	{
		m_pLog->LogStringModule(LEVEL_INFO, _T("TRANSPORT SERVICE"), _T("Download job %i completed"), _nJobId);
		m_jobCompleteSignal(_nJobId);
	}
	catch(...)
	{
	}
}

bool CTransportManager::CTransportMonitor::ExecuteAction()
{
	m_pManager->MonitorJobProcess();
	return false;
}