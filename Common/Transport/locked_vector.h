#pragma once
#include <vector>
#include <mutex>
#include <algorithm>

template<class T>
class locked_vector
{
	std::vector<T>			m_data;
	std::mutex	m_access;
public:
	locked_vector()		{ }
	~locked_vector()	{ }

	void add(const T& _data)
	{
		std::lock_guard<std::mutex> lock(m_access);
		m_data.push_back(_data);
	}

	void remove(const T& _data)
	{
		std::lock_guard<std::mutex> lock(m_access);
		std::vector<T>::iterator i = std::find(m_data.begin(), m_data.end(), _data);
		if(i != m_data.end())
			m_data.erase(i);
	}

	template<class Function>
	void for_each(Function f)
	{
		std::lock_guard<std::mutex> lock(m_access);
		std::for_each(m_data.begin(), m_data.end(), f);
	}

	template<class Function>
	void remove_if(Function f)
	{
		std::lock_guard<std::mutex> lock(m_access);
		std::vector<T>::iterator new_end = std::remove_if(m_data.begin(), m_data.end(), f);
		m_data.erase(new_end, m_data.end());
	}
};