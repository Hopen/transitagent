#pragma once
#include "thread.h"
//#include "SysObj.h"
#include <vector>
#include <mutex>
//#include <logger.h>
#include "Patterns\singleton.h"
#include "Log\SystemLog.h"

class IMonitorAction
{
	friend class CMonitorThread;

protected:
	std::wstring m_strFileName;

public:
	IMonitorAction(const std::wstring& _lpszFileName) :m_strFileName(_lpszFileName), m_blErase(false)	
	{}
	virtual ~IMonitorAction()																
	{}

	virtual bool VerifyFile();
	virtual	bool ExecuteAction()	= 0;

public:
	bool	m_blErase;
};

class CFileDeleteAction : public IMonitorAction
{
public:
	CFileDeleteAction(LPCTSTR _lpszFileName) : IMonitorAction(_lpszFileName)
	{
	}

	virtual bool ExecuteAction()
	{
		::DeleteFile(m_strFileName.c_str());// == TRUE;
		return true;
	}
};


class CMonitorThread : public Threading::CThread, public singleton<CMonitorThread>
{
	friend class singleton<CMonitorThread>;

	//SysObj::CCriticalSection			m_guardSection;
	std::mutex m_guardSection;
	
	std::vector<IMonitorAction *>		m_files;
	SystemLog m_pLog;

protected:
	CMonitorThread();
	~CMonitorThread();

public:
	virtual void AddMonitor(IMonitorAction * _fa);

protected:
	virtual void Process();
};
