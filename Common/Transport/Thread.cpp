#include "StdAfx.h"
#include <process.h>
#include ".\thread.h"

namespace Threading
{

CThread::CThread(void) : m_hThread(INVALID_HANDLE_VALUE)
{
	m_hExitEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
}

CThread::~CThread(void)
{
}

void CThread::Start()
{
	if(m_hThread != INVALID_HANDLE_VALUE)
		return;
	unsigned threadId;
	m_hThread = (HANDLE)::_beginthreadex(NULL, 0, &CThread::threadFunction, this, 0, &threadId);
}

void CThread::Stop()
{
	::SetEvent(m_hExitEvent);

	::WaitForSingleObject(m_hThread, INFINITE);
	::CloseHandle(m_hThread);
}

unsigned __stdcall CThread::threadFunction(void * pData)
{
	CThread * pThread = (CThread *)pData;

	pThread->Process();

	return 0;
}

}