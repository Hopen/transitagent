/************************************************************************/
/* Name     : Common\m2mcc.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 04 Jan 2017                                               */
/************************************************************************/

#pragma once

#include "Base.h"

namespace m2mcc
{
	CMessage Create_M2MCC_CHATMESSAGE_plain_text(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const std::wstring& aContent,
		const std::wstring& timeStampNow = core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

	CMessage Create_M2MCC_CHATMESSAGE_file_list(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const MessageVector& aAttachments,
		const std::wstring& timeStampNow = core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

	CMessage Create_M2MCC_DELETE_SESSION(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		const std::wstring& timeStampNow = core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

	CMessage Create_M2MCC_START_SESSION(const std::wstring& aCid,
		const std::wstring& aMsisdn,
		//const std::wstring& aStartDateTime,
		const std::wstring& aCustomerName,
		const std::wstring& aTopicName,
		const std::wstring& aSplitName,
		const std::wstring& aChannel,
		E_CHANNEL_TYPE aChannelType,
		const std::wstring& aMarketCode,
		const std::wstring& aSupportPartId,
		const std::wstring& aTimeStampNow = core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

	CMessage Create_M2MCC_GET_SESSION_STATUS(const ChatId& aCid);

	CMessage Create_M2MCC_GET_ONLY_SESSION_STATUS(const ChatId& aCid);

	CMessage Create_M2MCC_TECHNICAL_RESET_TIMER(
		int aTimeout);
}

/******************************* eof *************************************/