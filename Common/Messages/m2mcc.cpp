/************************************************************************/
/* Name     : Common\m2mcc.cpp                                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 04 Jan 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include "m2mcc.h"
#include "utils.h"

namespace m2mcc
{
	static CMessage Create_M2MCC_CHATMESSAGE(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		bool aText,
		ENUM_MESSAGE_CODE aCode,
		const std::wstring& aContent,
		const MessageVector& aAttachments,
		const std::wstring& aTimeStampNow
		)
	{
		CMessage msg(L"M2MCC_CHATMESSAGE");
		msg[L"id"] = aMessageId;
		msg[L"cid"] = aCid;
		msg[L"code"] = static_cast<int>(aCode);
		msg[L"timestamp"] = aTimeStampNow;
		msg[L"content"] = aContent;

		if (aText)
		{
			msg[L"contentType"] = L"TEXT";
		}
		else
		{
			msg[L"contentType"] = L"FILE";
			msg[L"attachments"] = core::to_raw_data(aAttachments);
		}

		return msg;
	}

	CMessage Create_M2MCC_CHATMESSAGE_plain_text(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const std::wstring& aContent,
		const std::wstring& aTimeStampNow)
	{
		return Create_M2MCC_CHATMESSAGE(
			aMessageId,
			aCid,
			true,
			aCode,
			aContent,
			{},
			aTimeStampNow);
	}

	CMessage Create_M2MCC_CHATMESSAGE_file_list(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const MessageVector& aAttachments,
		const std::wstring& aTimeStampNow)
	{
		return Create_M2MCC_CHATMESSAGE(
			aMessageId,
			aCid,
			false,
			aCode,
			L"File in attachment",
			aAttachments,
			aTimeStampNow);
	}

	CMessage Create_M2MCC_DELETE_SESSION(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		const std::wstring& timeStampNow)
	{
		CMessage msg(L"M2MCC_DELETE_SESSION");
		msg[L"id"] = aMessageId;
		msg[L"cid"] = aCid;
		msg[L"contentType"] = L"TEXT";
		msg[L"content"] = L"Session closed";
		msg[L"timestamp"] = timeStampNow;
		msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_SESSION_CLOSED_BY_ABONENT);

		return msg;
	}

	CMessage Create_M2MCC_START_SESSION(
		const std::wstring& aCid,
		const std::wstring& aMsisdn,
		//const std::wstring& aStartDateTime,
		const std::wstring& aCustomerName,
		const std::wstring& aTopicName,
		const std::wstring& aSplitName,
		const std::wstring& aChannel,
		E_CHANNEL_TYPE aChannelType,
		const std::wstring& aMarketCode,
		const std::wstring& aSupportPartId,
		const std::wstring& aTimeStampNow)
	{
		CMessage msg(M2MCC_START_SESSION);
		msg[L"cid"] = aCid;
		//msg[L"startDateTime"] = aStartDateTime;

		CMessage customer;
		customer[L"number"] = aMsisdn;
		customer[L"name"] = aCustomerName;

		CMessage topic;
		topic[L"name"] = aTopicName;
		topic[L"split"] = aSplitName;

		msg[L"customer"] = customer;
		msg[L"topic"] = topic;
		msg[L"channel"] = aChannel;
		msg[L"channelType"] = static_cast<int>(aChannelType);
		msg[L"marketCode"] = aMarketCode;
		msg[L"participantId"] = aSupportPartId;
		msg[L"timestamp"] = aTimeStampNow;

		return msg;
	}

	CMessage Create_M2MCC_GET_SESSION_STATUS(const ChatId& aCid)
	{
		CMessage msg(L"M2MCC_GET_SESSION_STATUS");
		msg[L"cid"] = aCid;

		return msg;
	}

	CMessage Create_M2MCC_GET_ONLY_SESSION_STATUS(const ChatId& aCid)
	{
		CMessage msg(L"M2MCC_GET_ONLY_SESSION_STATUS");
		msg[L"cid"] = aCid;

		return msg;
	}
	
	CMessage Create_M2MCC_TECHNICAL_RESET_TIMER(
		int aTimeout)
	{
		CMessage msg(L"M2MCC_TECHNICAL_RESET_TIMER");
		msg[L"ChatExpiredTime"] = aTimeout;

		return msg;
	}
}

/******************************* eof *************************************/