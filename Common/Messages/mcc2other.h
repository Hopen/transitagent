/************************************************************************/
/* Name     : Common\mcc2other.h                                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 07 Jan 2017                                               */
/************************************************************************/

#pragma once

#include "Base.h"

namespace mcc2other
{
	CMessage Create_ANY2SM_BeginSession(
		const std::wstring& aContact,
		const std::wstring& aChannelId,
		E_CHANNEL_TYPE aChannelType,
		const std::wstring& aSplit,
		const std::wstring& aIpAddress);

	CMessage Create_MCP2ICCP_OAM_replyToMessage_PlainText(
		const std::wstring& aRequestId,
		const std::wstring& aMessageId,
		const std::wstring& aText);

	CMessage Create_MCP2ICCP_OAM_replyToMessage_FileList(
		const std::wstring& aRequestId,
		const std::wstring& aMessageId,
		const std::wstring& aUri,
		const std::wstring& aName,
		const std::size_t aSize);

	CMessage Create_MCP2ICCP_OAM_receiveMessageRequest(
		const std::wstring& aRequestId,
		const std::wstring& aAgentId,
		const std::wstring& aAgentName);

	CMessage Create_MCP2ICCP_OAM_receiveMessageRequestAck_submessage(
		const std::wstring& aMessageId,
		const SessionId& aSid,
		const MCCID aMccid,
		const std::wstring& aContent,
		const std::wstring& aContentType,
		const std::wstring& aChannelId,
		E_CHANNEL_TYPE aChannelType,
		const std::wstring& aChannelName,
		const std::wstring& aMessageReceiver,
		const std::wstring& aTopicName,
		MessageVector&& aAttachments,
		const std::wstring& aTimestamp = core::support::time_to_string<std::wstring>(core::support::now()),
		bool aMakeUrl = false);

	CMessage Create_ANY2SM_BeginSessionAck(
		int aSessionExist,
		SessionScriptId aSsid,
		SessionId aSid);

	CMessage Create_SM2ANY_SessionDeleted(
		ENUM_MESSAGE_CODE aStatus,
		const std::wstring& aReason);

	CMessage Create_MCP2ICCP_OAM_receiveMessage(
		MCCID aMccid,
		const SessionId& aSid,
		const std::wstring& aContent,
		const std::wstring& aChannelId,
		E_CHANNEL_TYPE aChannelType,
		MessageVector && aAttachments,
		const std::wstring& aTitle,
		const std::wstring& aChannelName,
		const std::wstring& aMessageId);

	CMessage Create_S2MCC_SessionTransfer(
		const std::wstring& aEwt,
		const std::wstring& aMessage);
}

/******************************* eof *************************************/