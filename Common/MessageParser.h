/************************************************************************/
/* Name     : MCHAT\MessageParser.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Jul 2016                                               */
/************************************************************************/
#pragma once

#include <boost/property_tree/json_parser.hpp>
#include "Router/router_compatibility.h"
#include "utils.h"

//ToDo: make std::string functions
namespace CMessageParser
{
	std::wstring ToStringUnicode(const CMessage& msg);
	std::wstring ToStringUtf(const boost::property_tree::wptree& aTree);
	std::wstring ToStringUtf(const CMessage& msg);

	CMessage ToMessage(const std::wstring& aData);
	boost::property_tree::wptree ToTree(const std::wstring& aData);
	boost::property_tree::ptree ToTree(const std::string& aData);

}
/******************************* eof *************************************/
