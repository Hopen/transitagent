/************************************************************************/
/* Name     : MultHttpServer\Base.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Project  : MultHttpServer                                            */
/* Company  : Expert Solutions                                          */
/* Date     : 24 Jun 2018                                               */
/************************************************************************/
#pragma once

#include <memory>
#include <boost/thread.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/coroutine/asymmetric_coroutine.hpp>
#include <boost/coroutine/symmetric_coroutine.hpp>
#include <boost/format.hpp>

#include "Router/router_compatibility.h"

using ObjectId = __int64;

using ChatTextMessageHandler_test = std::function<void(const CMessage& msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)>;

using MessageContainer = std::list<CMessage>;

const unsigned int SAVE_TIME_OUT = 30;

template <class TChatId>
struct TMessageStatus_t
{
	TChatId chatId;
	std::wstring methodName;
	std::wstring timeStamp;
};

using MCCID = unsigned __int64;
namespace Base
{
	template <class TChatId>
	using ChatTextMessageHandler = std::function<std::wstring(const CMessage& msg, TMessageStatus_t<TChatId>& status)>;
	using ChatTextMessageHandler_s = std::function<std::wstring(const CMessage& msg, const std::string& status)>;
	using ServerRequestMessageHandler = std::function<std::string(const CMessage& msg)>;
}


/******************************* eof *************************************/