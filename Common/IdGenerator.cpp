/************************************************************************/
/* Name     : MCHAT\IdGenaretor.h                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Jul 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include "IdGenerator.h"

CIdGenerator::CIdGenerator() : _gen(_rd())
{

}

int64_t CIdGenerator::makeId()
{
	std::uniform_int_distribution<int64_t> dis;

	return dis(_gen);
}

std::wstring CIdGenerator::makeSId()
{
	return std::to_wstring(makeId());
}

int64_t CIdGenerator::makeId(unsigned int clientId)
{ 
	int64_t mccid = clientId;
	mccid = (mccid << 32) | (makeId() >> 32);

	return mccid;
}

std::wstring CIdGenerator::makeSId(unsigned int clientId)
{
	return std::to_wstring(makeId(clientId));
}

/******************************* eof *************************************/