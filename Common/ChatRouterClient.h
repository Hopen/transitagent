/************************************************************************/
/* Name     : AllHeaders.h                                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Jun 2018                                               */
/************************************************************************/

#pragma once
#include "myrouterclient.h"
#include "SpawnClient.h"

struct SpawnClientSetup
{
	using TConnectionParams = spawn_client::TConnectionClientParams;

	template <class ... TArgs>
	static bool MakeClient(TArgs&& ... aArgs)
	{
		return spawn_client::makeClient(std::forward<TArgs>(aArgs)...);
	}
};

using MyRouterClient = CMyRouterClient2<SpawnClientSetup>;


/******************************* eof *************************************/