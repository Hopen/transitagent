/************************************************************************/
/* Name     : MVEON\AttachmentsApi.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 12 Feb 2017                                               */
/************************************************************************/
#pragma once
#include "Base.h"

namespace attachments
{
	struct TAttachmentsParam
	{
		std::wstring Url;
		std::wstring SourceName;
		int FileSize;

		TAttachmentsParam(const std::wstring& _url, const std::wstring& _sourceName, int size)
			:Url(_url), SourceName(_sourceName), FileSize(size)
		{}
	};

	using AttachmentsList = std::vector<TAttachmentsParam>;

	std::wstring CreateGUID();
	std::wstring GetUri(
		const ChatId& cid,
		const std::wstring& sAttachmentString,
		const std::wstring& attachName);

	std::wstring GetLocalUri(
		const ChatId& cid,
		const std::wstring& sAttachmentDisk,
		const std::wstring& sAttachmentFolder,
		const std::wstring& attachName);

	TAttachmentsParam saveAttachment(ChatId cid,
		const std::wstring& sAttachmentDisk,
		const std::wstring& sAttachmentFolder,
		const std::wstring& sAttachmentString,
		const std::string& sContent,
		const std::wstring& aSaveAttachName,
		const std::wstring& attachName);

	AttachmentsList extractAttachments(const CMessage& reqMsg);

	std::pair<std::string, size_t> readAttachment2(
		const ChatId& cid,
		const std::wstring& aFileName,
		const std::wstring& aAttachmentDisk,
		const std::wstring& aAttachmentFolder);

	std::string readAttachment(
		const std::wstring& attachPath,
		const std::wstring& attachSourceName,
		const std::wstring& sAttachmentString,
		const std::wstring& sAttachmentFolder);

	std::pair<std::string, size_t> GetFileSource(const std::wstring& aFileName);
	
	std::wstring getLocalUrlByRemote(
		const std::wstring& attachPath,
		const std::wstring& sAttachmentString,
		const std::wstring& sAttachmentDisk,
		const std::wstring& sAttachmentFolder);

	std::wstring getFtpUrlByServerUrl(
		const std::wstring& aServerLocation,
		const std::wstring& aAttachmentFolder,
		const std::wstring& aFtpFolder,
		const std::wstring& aFtpHostName);

}

/******************************* eof *************************************/