/************************************************************************/
/* Name     : Common\CheckVeonMessages.h                                */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 03 Jan 2018                                               */
/************************************************************************/

#pragma once

#include "Base.h"

namespace veon_checkmessages
{
	template <class TFunction>
	static bool CheckMessageParams(const CMessage& aMsg, std::string& outErrorMsg, TFunction aCheckedFunction)
	{
		bool bOK = true;

		// Check chat message
		try
		{
			aCheckedFunction(aMsg);
		}
		catch (const std::runtime_error& e)
		{
			bOK = false;
			outErrorMsg = e.what();
		}

		return bOK;
	}

	static bool CheckChatMessageParams(const CMessage& aMsg, std::string& outErrorMsg, bool aIsToCWS)
	{
		return CheckMessageParams(aMsg, outErrorMsg, [aIsToCWS](const CMessage& aMsg)
		{
			if (!aIsToCWS)
			{
				aMsg.CheckParam(L"messageId", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			}
			aMsg.CheckParam(L"sender", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"timestamp", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			//aMsg.CheckParam(L"name", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		});
	}

	static bool CheckMultimediaMessageParams(const CMessage& aMsg, std::string& outErrorMsg)
	{
		return CheckMessageParams(aMsg, outErrorMsg, [](const CMessage& aMsg)
		{
			aMsg.CheckParam(L"multimediaMessageId", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"templateId", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"elements", CMessage::CheckedType::RawData, CMessage::ParamType::Mandatory);
		});
	}

	static bool CheckPayloadContentParams(const CMessage& aMsg, std::string& outErrorMsg)
	{
		return CheckMessageParams(aMsg, outErrorMsg, [](const CMessage& aMsg)
		{
			aMsg.CheckParam(L"type", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"key", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"payload", CMessage::CheckedType::RawData, CMessage::ParamType::Mandatory);
		});
	}

	static void CheckPartRole(const std::wstring& aRole)
	{
		if (StringToRole(aRole) == E_PART_ROLE::R_UNKNOWN)
			throw std::runtime_error("Bad Request");
	}

	static void CheckPartState(const std::wstring& aState)
	{
		if (StringToParticipantState(aState) == E_PART_STATE::PS_UNKNOWN)
			throw std::runtime_error("Bad Request");
	}

	static bool CheckStartVeonMessageParams(const CMessage& aMsg, std::string& outErrorMsg)
	{
		return CheckMessageParams(aMsg, outErrorMsg, [](const CMessage& aMsg)
		{
			aMsg.CheckParam(L"conversationId", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"state", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"topic", CMessage::CheckedType::String, CMessage::ParamType::Optional);
			aMsg.CheckParam(L"startDateTime", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"participants", CMessage::CheckedType::RawData, CMessage::ParamType::Mandatory);
		});
	}
	
	static bool CheckParticipantsMessageParams(const CMessage& aMsg, std::string& outErrorMsg)
	{
		return CheckMessageParams(aMsg, outErrorMsg, [](const CMessage& aMsg)
		{
			aMsg.CheckParam(L"participantId", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"role", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"state", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
			aMsg.CheckParam(L"displayName", CMessage::CheckedType::String, CMessage::ParamType::Optional);
			aMsg.CheckParam(L"attributes", CMessage::CheckedType::RawData, CMessage::ParamType::Optional);

			CheckPartState(aMsg.SafeReadParam(L"state", CMessage::CheckedType::String, L"").AsWideStr());
			CheckPartRole(aMsg.SafeReadParam(L"role", CMessage::CheckedType::String, L"").AsWideStr());
		});
	}

	static bool CheckParticipantsAttributesParams(const CMessage& aMsg, std::string& outErrorMsg)
	{
		return CheckMessageParams(aMsg, outErrorMsg, [](const CMessage& aMsg)
		{
			aMsg.CheckParam(L"msisdn", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		});
	}

}
/******************************* eof *************************************/