/************************************************************************/
/* Name     : Common\CreateVeonMessages.h                               */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 30 Dec 2017                                               */
/************************************************************************/

#pragma once

#include "Base.h"
#include "IdGenerator.h"

namespace veon_messages
{
	static inline CMessage MakeConversation(
		const std::wstring& aConvId,
		const std::wstring& aSupportPartId,
		const std::wstring& aMsisdn,
		E_PART_ROLE aRole,
		E_PART_STATE aState)
	{
		CMessage msg(L"Conversation");

		msg[L"conversationId"] = aConvId;

		CMessage participant1(L"Participant");
		participant1[L"participantId"] = CIdGenerator::GetInstance()->makeSId();
		participant1[L"role"] = RoleToString(aRole);
		participant1[L"state"] = ParticipantStateToString(aState);
		participant1[L"displayName"] = L"Homer Simpson";

		if (!aMsisdn.empty())
		{
			CMessage attribute1(L"Attribute");
			attribute1[L"msisdn"] = aMsisdn;
			participant1[L"attributes"] = attribute1;
		}

		CMessage participant2(L"Participant");
		participant2[L"participantId"] = aSupportPartId;
		participant2[L"role"] = RoleToString(E_PART_ROLE::R_SUPPORT);
		participant2[L"state"] = ParticipantStateToString(E_PART_STATE::PS_PENDING);
		participant2[L"displayName"] = L"Homer Simpson";

		//CMessage attribute2(L"Attribute");
		//attribute2[L"empty"] = 0;
		//participant2[L"attributes"] = attribute2;

		std::list<CMessage> participants;
		participants.emplace_back(std::move(participant1));
		participants.emplace_back(std::move(participant2));

		msg[L"participants"] = core::to_raw_data(participants);

		msg[L"topic"] = L"How to survive in Springfield";
		msg[L"startDateTime"] = core::support::time_to_string<std::wstring>(core::time_type::clock::now());
		msg[L"state"] = ConvStateToString(E_CONV_STATE::CS_ACTIVE);

		return msg;
	}

	static inline CMessage MakeConversation(
		const std::wstring& aConvId,
		const std::wstring& aSupportPartId,
		const std::wstring& aMsisdn)
	{
		return MakeConversation(
			aConvId,
			aSupportPartId,
			aMsisdn,
			E_PART_ROLE::R_USER,
			E_PART_STATE::PS_JOINED);
	}

	static inline CMessage CreateLocalizedString(const std::wstring& aTextMessage, const std::wstring& aLocalization)
	{
		CMessage msg(L"LocalizedString");
		msg[aLocalization] = aTextMessage;
		return msg;
	}

	static inline CMessage CreatePlaintTextMessage(const std::wstring& aTextMessage, const std::wstring& aLocalization)
	{
		CMessage msg(L"PlainText");
		msg[L"plainText"] = CreateLocalizedString(aTextMessage, aLocalization);

		return msg;
	}

	static inline CMessage MakePlainLocalizedTextMessage(
		const std::wstring& aMessageId,
		const std::wstring& aTextMessage,
		const std::wstring& aLocalization)
	{
		CMessage msg(L"PlainText");
		msg[L"elementId"] = aMessageId;
		msg[L"type"] = MsgTypeToString(E_MSG_TYPE::MT_PLAIN);
		msg[L"key"] = MsgKeyToString(E_MSG_KEY::MY_BOTTOM);

		std::list<CMessage> elements;
		elements.emplace_back(CreatePlaintTextMessage(aTextMessage, aLocalization));

		msg[L"payload"] = core::to_raw_data(elements);

		return msg;
	}

	static inline CMessage MakePlainTextMessage(
		const std::wstring& aMessageId,
		const std::wstring& aTextMessage)
	{
		return MakePlainLocalizedTextMessage(aMessageId, aTextMessage, L"ru");
	}

	struct FileListParams
	{
		FileListParams(
			const std::wstring& aUri,
			const std::wstring& aFileName = L"",
			const std::wstring& aFileSize = L"")
			: Uri(aUri)
			, FileName(aFileName)
			, FileSize(aFileSize)
		{}

		std::wstring Uri;
		std::wstring FileName;
		std::wstring FileSize;
	};

	using FileListVector = std::vector<FileListParams>;

	static inline CMessage MakeFileListMessage(
		const std::wstring& aMessageId,
		const FileListVector& aFileListVector)
	{
		CMessage msg(L"FileList");
		msg[L"elementId"] = aMessageId;
		msg[L"type"] = MsgTypeToString(E_MSG_TYPE::MT_FILELIST);
		msg[L"key"] = MsgKeyToString(E_MSG_KEY::MY_BOTTOM);

		std::list<CMessage> elements;
		for (const auto& param : aFileListVector)
		{
			CMessage element;
			element[L"source"] = param.Uri;
			if (!param.FileName.empty())
			{
				element[L"title"] = param.FileName;
			}
			if (!param.FileSize.empty())
			{
				element[L"size"] = param.FileSize;
			}

			elements.emplace_back(std::move(element));
		}

		msg[L"payload"] = core::to_raw_data(elements);

		return msg;
	}

	template <class... TArgs>
	static inline CMessage MakeMultimediaMessage(TArgs&&... aArgs)
	{
		CMessage multiMsg(L"MultimediaMessage");
		multiMsg[L"multimediaMessageId"] = CIdGenerator::GetInstance()->makeSId();
		//multiMsg[L"templateId"] = CIdGenerator::GetInstance()->makeSId();
		multiMsg[L"templateId"] = L"chatMessage";

		std::list<CMessage> elements;
		auto insert = [&](auto&& aParam)
		{
			elements.emplace_back(std::forward<decltype(aParam)>(aParam));
			return true;
		};
		[](...) {}((insert(std::forward<TArgs>(aArgs)))...);

		multiMsg[L"elements"] = core::to_raw_data(elements);

		return multiMsg;
	}

	static inline CMessage MakeVeonMessage(
		const CMessage& aMultiMsg,
		bool aToCWS)
	{
		CMessage msg(L"Message");

		if (!aToCWS)
		{
			msg[L"messageId"] = CIdGenerator::GetInstance()->makeSId();
		}
		msg[L"sender"] = CIdGenerator::GetInstance()->makeSId();
		msg[L"timestamp"] = core::support::time_to_iso_8601_string<std::wstring>(core::time_type::clock::now());
		//msg[L"name"] = MsgNameToString(E_MSG_NAME::MN_PAUSE);
		msg[L"payload"] = aMultiMsg;

		return msg;
	}

	static inline CMessage MakeUpdateConversationMessage(
		const std::wstring& /*aConvId*/,
		const std::wstring& aTopicName,
		const std::wstring& aFeedBack,
		const E_CONV_STATE aState)
	{
		CMessage msg(L"UpdateConversation");

		if (!aTopicName.empty())
		{
			msg[L"topic"] = aTopicName;
		}

		if (!aFeedBack.empty())
		{
			msg[L"feedback"] = aFeedBack;
		}
		
		msg[L"state"] = ConvStateToString(aState);

		return msg;
	}

	static inline CMessage MakeUpdateParticipantMessage(
		const std::wstring& aName,
		E_PART_ROLE aRole,
		E_PART_STATE aState)
	{
		CMessage msg(L"UpdateParticipant");
		msg[L"role"] = RoleToString(aRole);
		msg[L"displayName"] = aName;
		msg[L"state"] = ParticipantStateToString(aState);

		return msg;
	}
}

/******************************* eof *************************************/