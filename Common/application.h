/************************************************************************/
/* Name     : MCC\application.h                                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : Common MCC                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 26 Jul 2016                                               */
/************************************************************************/
#pragma once

int CreateMain(int argc, char *c_argv[]);

/******************************* eof *************************************/