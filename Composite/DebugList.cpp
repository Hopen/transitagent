/************************************************************************/
/* Name     : Composite\DebugList.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 28 Oct 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "DebugList.h"

CDebugList::CDebugList( //const std::wstring& script, 
					    const std::wstring& _origination, 
						const std::wstring& _destination, 
						//const std::wstring& display, 
						const std::wstring& _request, 
						const std::wstring& _runrequest, 
						const CMessage& _message, 
						CLIENT_ADDRESS* _from )
						:CPhoneMasks(L"script_name",_origination,_destination,L"", ParamsList()),
						 m_sRequestID(_request),
						 m_sRunRequestID(_runrequest),
						 m_message(_message),
						 m_pFrom(_from)
{

}



IList* CDebugList::find(const std::wstring& _id)
{
	if (!m_sRequestID.compare(_id))
		return static_cast<IList*>(this);
	return NULL;
}

IList* CDebugList::find(const DWORD& _dwClient)
{
	if (m_pFrom->GetClientID() == _dwClient)
		return static_cast<IList*>(this);
	return NULL;
}



/******************************* eof *************************************/