/************************************************************************/
/* Name     : Composite\PhoneMasks.cpp                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 23 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "PhoneMasks.h"

IList* CPhoneMasks::GetScript(const CMessage& msg)
{
	//if (m_pOwnBlackList)
	//{
	//	if (IList* blacknumber = m_pOwnBlackList->GetScript(msg)) // check for entering in own black list
	//	{
	//		return blacknumber; // we've got it
	//	}
	//}

	std::wstring sOrigination, sDistination;

	sOrigination = msg.SafeReadParam(L"A", CMessage::CheckedType::String, L"").AsWideStr();
	sDistination = msg.SafeReadParam(L"B", CMessage::CheckedType::String, L"").AsWideStr();

	if (sOrigination.empty() && sDistination.empty()) // SIP2TA_SMPP_DELIVER_SM message
	{
		sOrigination = msg.SafeReadParam(L"Orig_Address", CMessage::CheckedType::String, L"").AsWideStr();
		sDistination = msg.SafeReadParam(L"Dst_Address", CMessage::CheckedType::String, L"").AsWideStr();
	}

	if (sOrigination.empty() && sDistination.empty()) // MCC2TA_NEWEMAIL message
	{
		sOrigination = msg.SafeReadParam(L"From", CMessage::CheckedType::String, L"").AsWideStr();
		sDistination = msg.SafeReadParam(L"To", CMessage::CheckedType::String, L"").AsWideStr();
	}

	if (sOrigination.empty() && sDistination.empty()) // MCC2TA_NEWCHAT message
	{
		sOrigination = msg.SafeReadParam(L"CustomerCTN", CMessage::CheckedType::String, L"").AsWideStr();
		sDistination = msg.SafeReadParam(L"ChannelID", CMessage::CheckedType::String, L"").AsWideStr();
	}

	IList* pCustomConfig = NULL;
	if (m_pCustomCfg)
	{
		pCustomConfig = m_pCustomCfg->GetCustomCfg(msg);
	}

	if (pCustomConfig)
	{
		sOrigination = pCustomConfig->GetPrefix() + sOrigination;
	}

	IList* pRecipient = NULL;
	bool bOriginValid = mMatcherOrigin->IsMatch(sOrigination.c_str(), sOrigination.c_str() + sOrigination.size(), false);
	bool bDestinValid = false;
	if (bOriginValid)
	{
		bDestinValid = mMatcherDestin->IsMatch(sDistination.c_str(), sDistination.c_str() + sDistination.size(), false);
	}

	if (bOriginValid && bDestinValid)
	{
		return pRecipient = (IList*)this;
	}

	return pRecipient;
}

bool CPhoneMasks::BetterThanYou(const IList* Matched/*, const std::wstring& sOrigination, const std::wstring& sDistinationst*/)const
{
	
	UINT uMachedOriginRegPartLength = Matched->GetOriginationRegularPartLength();
	UINT uMachedDestinRegPartLength = Matched->GetDestinationRegularPartLength();

	UINT uMachedOriginMaskLength    = Matched->GetOriginationLength();
	UINT uMachedDestinMaskLength    = Matched->GetDestinationLength();
	
	UINT uMachedOriginNotRegPartMaskLength = uMachedOriginMaskLength - uMachedOriginRegPartLength;
	UINT uMachedDestinNotRegPartMaskLength = uMachedDestinMaskLength - uMachedDestinRegPartLength;

	UINT uOriginNotRegPartMaskLength = m_uOriginMaskLength - m_uOriginRegMaskLength;
	UINT uDestinNotRegPartMaskLength = m_uDestinMaskLength - m_uDestinRegMaskLength;

	UINT uMachedOriginQuestionLen = Matched->GetOriginQuestionLen();
	UINT uMachedDestinQuestionLen = Matched->GetDestinQuestionLen();
	UINT uMachedOriginPercentLen  = Matched->GetOriginPercentLen();
	UINT uMachedDestinPercentLen  = Matched->GetDestinPercentLen();

	// match the lengths of destin NOT regular part
	if (uDestinNotRegPartMaskLength - m_uDestinQuestionLen - m_uDestinPercentLen >
		uMachedDestinNotRegPartMaskLength - uMachedDestinQuestionLen - uMachedDestinPercentLen)
		return true;

	if (uDestinNotRegPartMaskLength - m_uDestinQuestionLen - m_uDestinPercentLen <
		uMachedDestinNotRegPartMaskLength - uMachedDestinQuestionLen - uMachedDestinPercentLen)
		return false;

	//destin NOT regul part is the same
	// match the lengths of origin NOT regular part

	if (uOriginNotRegPartMaskLength - m_uOriginQuestionLen - m_uOriginPercentLen >
		uMachedOriginNotRegPartMaskLength - uMachedOriginQuestionLen - uMachedOriginPercentLen)
		return true;

	if (uOriginNotRegPartMaskLength - m_uOriginQuestionLen - m_uOriginPercentLen <
		uMachedOriginNotRegPartMaskLength - uMachedOriginQuestionLen - uMachedOriginPercentLen)
		return false;

	//destin NOT regul part is the same && origin NOT regul part is the same
	// match count of question
	// for destin at first

	if (uDestinNotRegPartMaskLength - m_uDestinPercentLen >
		uMachedDestinNotRegPartMaskLength - uMachedDestinPercentLen)
		return true;

	if (uDestinNotRegPartMaskLength - m_uDestinPercentLen <
		uMachedDestinNotRegPartMaskLength - uMachedDestinPercentLen)
		return false;

	//and now for origin
	if (uOriginNotRegPartMaskLength - m_uOriginPercentLen >
		uMachedOriginNotRegPartMaskLength - uMachedOriginPercentLen)
		return true;

	if (uOriginNotRegPartMaskLength - m_uOriginPercentLen <
		uMachedOriginNotRegPartMaskLength - uMachedOriginPercentLen)
		return false;

	// match percent 
	if (m_uOriginPercentLen && !uMachedOriginPercentLen)
		return false;

	if (!m_uOriginPercentLen && uMachedOriginPercentLen)
		return true;

	if (m_uDestinPercentLen && !uMachedDestinPercentLen)
		return false;

	if (!m_uDestinPercentLen && uMachedDestinPercentLen)
		return true;


	// match the lengths of regular part
	if (m_uDestinRegMaskLength > uMachedDestinRegPartLength)
		return true;

	if (m_uDestinRegMaskLength < uMachedDestinRegPartLength)
		return false;

	if (m_uOriginRegMaskLength > uMachedOriginRegPartLength)
		return true;

	if (m_uOriginRegMaskLength < uMachedOriginRegPartLength)
		return false;

	//// match the lengths of NOT regular part
	//if (uDestinNotRegPartMaskLength - /*(int)m_bDestinNotRegMask*/m_uDestinPercentQuestionLen > 
	//	uMachedDestinNotRegPartMaskLength - uMachedDestinPercentQuestionLen/*(int)bMachedDestinNotRegMask?1:0*/)
	//	return true;

	//if (uDestinNotRegPartMaskLength - /*(int)m_bDestinNotRegMask*/m_uDestinPercentQuestionLen < uMachedDestinNotRegPartMaskLength - uMachedDestinPercentQuestionLen/*(int)bMachedDestinNotRegMask*/)
	//	return false;


	//if (uOriginNotRegPartMaskLength - m_uOriginPercentQuestionLen/*(int)m_bOriginNotRegMask*/ > uMachedOriginNotRegPartMaskLength - uMachedOriginPercentQuestionLen/*(int)bMachedOriginNotRegMask*/)
	//	return true;

	//if (uOriginNotRegPartMaskLength - m_uOriginPercentQuestionLen/*(int)m_bOriginNotRegMask*/ < uMachedOriginNotRegPartMaskLength - uMachedOriginPercentQuestionLen/*(int)bMachedOriginNotRegMask*/)
	//	return false;

	//if (/*m_bOriginNotRegMask*/m_uOriginPercentQuestionLen && !uMachedOriginPercentQuestionLen/*bMachedOriginNotRegMask*/)
	//	return false;

	//if (!/*m_bOriginNotRegMask*/m_uOriginPercentQuestionLen && uMachedOriginPercentQuestionLen/*bMachedOriginNotRegMask*/)
	//	return true;

	//if (/*m_bDestinNotRegMask*/m_uDestinPercentQuestionLen && !uMachedDestinPercentQuestionLen/*bMachedDestinNotRegMask*/)
	//	return false;

	//if (!/*m_bDestinNotRegMask*/m_uDestinPercentQuestionLen && uMachedDestinPercentQuestionLen/*bMachedDestinNotRegMask*/)
	//	return true;

	//// match the lengths of regular part
	//if (m_uDestinRegMaskLength > uMachedDestinRegPartLength)
	//	return true;

	//if (m_uDestinRegMaskLength < uMachedDestinRegPartLength)
	//	return false;

	//if (m_uOriginRegMaskLength > uMachedOriginRegPartLength)
	//	return true;

	//if (m_uOriginRegMaskLength < uMachedOriginRegPartLength)
	//	return false;

	// incredible, but we've got absolutely identical masks 
	return false;
}

void CPhoneMasks::AddBlackList(SPtr _blacklist)
{
	m_pOwnBlackList = _blacklist;
}


void CPhoneMasks::AddCustomConfig(IList* _customcfg)
{
	m_pCustomCfg = _customcfg;
}

void CPhoneMasks::AddAliasList(IList* _aliaslist)
{
	m_pAliasList = _aliaslist;
}


void CPhoneMasks::refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList)
{
	if (m_pOwnBlackList.get())
	{
		m_pOwnBlackList->refresh(_filename, m_pOwnBlackList, blackList);
	}
}

std::wstring CPhoneMasks::log()const
{
	CAtlString sTemplate = 
		TEXT (" Get in Phonemask by origination mask: %s, ")
		TEXT (" destination mask: %s, ")
		TEXT (" runscript name: %s ");
	CAtlString sLog;
	std::wstring sOrigiantion(m_sOrigination);
	std::wstring sDestination(m_sDestination);
	std::wstring simbolsIn[]  = {L"%" , L""};
	std::wstring simbolsOut[] = {L"%%", L""};
	ReplaceSymbols(sOrigiantion,simbolsIn,simbolsOut);
	ReplaceSymbols(sDestination,simbolsIn,simbolsOut);

	sLog.Format(sTemplate,sOrigiantion.c_str(),sDestination.c_str(),m_sScriptName.c_str());
	return sLog.GetString();
}

std::wstring CPhoneMasks::GetScriptName() const
{
	CAtlString sScriptName = m_sScriptName.c_str();

	if (sScriptName.Find(L"$") != -1)
		sScriptName = sScriptName.Right(sScriptName.GetLength() - 1);

	IList* pAliasList = NULL;
	if (m_pAliasList)
	{
		pAliasList = m_pAliasList->GetAliasList(sScriptName.GetString());
	}

	if (pAliasList)
	{
		sScriptName = pAliasList->GetAliasPath().c_str();
	}

	return sScriptName.GetString();
}



/******************************* eof *************************************/