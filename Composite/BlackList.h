/************************************************************************/
/* Name     : Composite\BlackList.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 17 Aug 2010                                               */
/************************************************************************/
#pragma once
#include "Composite.h"

class CBlackList: public IList
{

public:
	CBlackList(const std::wstring& mask, const std::wstring& sBlackListName)
		: m_uRegMaskLength(0),
		  m_uMaskLength(mask.length()),
		  m_sOrigination(mask),
		  m_uOriginQuestionLen(0),
		  m_uOriginPercentLen(0)
	{
		std::wstring origination;
		ConvertMask(mask,origination,m_uRegMaskLength, m_uOriginQuestionLen, m_uOriginPercentLen);
		m_sName = sBlackListName;
		m_pCustomCfg= NULL;

		mMatcherOrigin = IList::CreateAutomat(mask.c_str());
	}
	virtual E_ACTION GetAction()const{return A_IGNORE;}
	virtual bool BetterThanYou(const IList* Matched)const;
	UINT GetDestinationRegularPartLength()const{return 0;}
	UINT GetDestinationLength           ()const{return 0;}
	UINT GetOriginationRegularPartLength()const{return m_uRegMaskLength;}
	UINT GetOriginationLength           ()const{return m_uMaskLength;}

	UINT GetOriginQuestionLen  ()const { return m_uOriginQuestionLen; }
	UINT GetOriginPercentLen   ()const { return m_uOriginPercentLen; }
	UINT GetDestinQuestionLen  ()const { return 0; }
	UINT GetDestinPercentLen   ()const { return 0; }

	const ParamsList& GetOtherParams() const override
	{
		static ParamsList paramsList{};
		return paramsList;
	}

	std::wstring GetMessageName()const {return std::wstring();}
	std::wstring GetScriptName ()const {return std::wstring();}
	std::wstring GetMonitorName()const {return std::wstring();}

	std::wstring GetPrefix()const {return m_sPrefix;}
	IList* GetCustomCfg(const CMessage& msg){return NULL;}
	IList* GetAliasList(const std::wstring& msg){return NULL;}
	std::wstring GetAliasPath()const {return std::wstring(L"");}
	IList* GetBlackList() { return NULL; }

	void refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList) override { return; }
	IList* find (const std::wstring& _param){return NULL;}
	std::wstring log()const;
	void AddCustomConfig(IList* _customcfg);
protected:
	IList* GetScript(const CMessage& msg);

private:
	std::wstring m_sName;

	UINT m_uRegMaskLength;
	UINT m_uMaskLength;

	UINT m_uOriginQuestionLen;
	UINT m_uOriginPercentLen;

	std::wstring m_sOrigination;
	IList* m_pCustomCfg;
	std::wstring m_sPrefix;

	State<wchar_t> mMatcherOrigin;

};

/******************************* eof *************************************/