/************************************************************************/
/* Name     : Composite\CustomCfg.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 15 Dec 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "CustomCfg.h"

IList* CCustomCfg::GetScript(const CMessage& msg)
{
	//std::wstring sOrigination, sDistination;
	//if (const CParam* pA = msg.ParamByName(L"A"))
	//	sOrigination = pA->AsWideStr();

	//if (sOrigination.empty() && sDistination.empty()) // from SIP2TA_SMPP_DELIVER_SM message
	//{
	//	if (const CParam* pOrigAddress = msg.ParamByName(L"Orig_Address"))
	//		sOrigination = pOrigAddress->AsWideStr();
	//}

	//IList* pRecipient = NULL;
	//if (sOrigination.length() == m_lentoapply)
	//	pRecipient = (IList*)this;

	//return pRecipient;
	return NULL;

}

IList* CCustomCfg::GetCustomCfg(const CMessage& msg)
{
	std::wstring sOrigination/*, sDistination*/;

	sOrigination = msg.SafeReadParam(L"A", CMessage::CheckedType::String, L"").AsWideStr();

	//if (const CParam* pA = msg.ParamByName(L"A"))
	//	sOrigination = pA->AsWideStr();

	if (sOrigination.empty() /*&& sDistination.empty()*/) // from SIP2TA_SMPP_DELIVER_SM message
	{
		//if (const CParam* pOrigAddress = msg.ParamByName(L"Orig_Address"))
		//	sOrigination = pOrigAddress->AsWideStr();
		sOrigination = msg.SafeReadParam(L"Orig_Address", CMessage::CheckedType::String, L"").AsWideStr();
	}

	IList* pRecipient = NULL;
	if (sOrigination.length() == m_lentoapply)
		pRecipient = (IList*)this;

	return pRecipient;
}

/******************************* eof *************************************/