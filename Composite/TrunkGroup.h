///************************************************************************/
///* Name     : Composite\TrunkGroup.h                                    */
///* Author   : Andrey Alekseev                                           */
///* Company  : Forte-CT                                                  */
///* Date     : 8 Nov 2010                                                */
///************************************************************************/
//#pragma once
//#include "Composite.h"
//
//namespace TrunkGroupConsts
//{
//	// methods
//	const CAtlString M_CIRCLE   = L"Circle"    ;
//	const CAtlString M_LINEAR   = L"Linear"    ;
//	const CAtlString M_RANDOM   = L"Random"    ;
//	const CAtlString M_LASTUSED = L"LeastUsed" ;
//	// directions
//	const CAtlString D_FORWARD  = L"Forward"   ;
//	const CAtlString D_REVERSE  = L"Reverse"   ;
//	// parity
//	const CAtlString P_BOTH     = L"Both"      ;
//	const CAtlString P_ODD      = L"Odd"       ;
//	const CAtlString P_EVEN     = L"Even"      ;
//
//}
//
//
//class CTGroup: public IList
//{
//private:
//	struct DTI
//	{
//		DTI(const int& _board, const int& _timeslot)
//			:board  (_board),
//			timeslot(_timeslot),
//			address (0)
//		{
//		}
//
//		DTI(const int& _board, const int& _timeslot, const ULONGLONG&_address)
//			:board   (_board),
//			timeslot(_timeslot),
//			address (_address)
//		{
//		}
//		DTI(const DTI& rhs)
//		{
//			board    = rhs.board;
//			timeslot = rhs.timeslot;
//			address  = rhs.address;
//		}
//		~DTI()
//		{
//			int test = 0;
//		}
//		int board;
//		int timeslot;
//		ULONGLONG address;
//	};
//
//	class NameList:public std::vector<DTI>
//	{
//	public:
//		NameList::iterator Find(const int         & _board, const int         & _timeslot);
//		NameList::iterator Find(const std::wstring& _board, const std::wstring& _timeslot);
//	};
//
//public:
//	class CRemoveUnctiveTimeSlotPredicat
//	{
//	public:
//		bool operator()(CTGroup::DTI& _dti)
//		{
//			if (!_dti.address)
//				return true;
//			return false;
//		}
//	};
//
//	class CRemoveParityTimeSlotPredicat
//	{
//	public:
//		CRemoveParityTimeSlotPredicat(bool _parity_ever = true):m_bParityEver(_parity_ever)
//		{}
//		bool operator()(const CTGroup::DTI& _dti)
//		{
//			return (m_bParityEver)?((_dti.timeslot%2)==1):((_dti.timeslot%2)!=1);
//		}
//	private:
//		bool m_bParityEver;
//	};
//
//public:
//
//	// interface
//	std::wstring log()const;
//	IList* GetScript( const CMessage& msg );
//	E_ACTION GetAction()const{return A_UNKNOWN;}
//
//	bool BetterThanYou(const IList* Matched)const{return false;}
//	UINT GetDestinationRegularPartLength()const{return 0;}
//	UINT GetDestinationLength           ()const{return 0;}
//	UINT GetOriginationRegularPartLength()const{return 0;}
//	UINT GetOriginationLength           ()const{return 0;}
//	bool GetOriginationNotRegMask		()const{return false;}
//	bool GetDestinationNotRegMask		()const{return false;}
//
//	std::wstring GetMessageName()const{return L"";}
//	std::wstring GetScriptName ()const{return L"";}
//	std::wstring GetMonitorName()const{return L"";}
//
//	std::wstring GetPrefix()const {return std::wstring();}
//	IList* GetAliasList(const std::wstring& msg){return NULL;}
//	std::wstring GetAliasPath()const {return std::wstring(L"");}
//
//	void refresh(const std::wstring& _filename, listcallbackfunc callbackfunc, SPtr parent) {return;}
//
//	bool GetAddressByGroupName(
//		const std::wstring& _groupname, // [in]
//		AddressList &List);             // [out]
//
//	bool GetAddressByTimeSlot(
//		const std::wstring& _dti_timeslot, // [in]
//		const std::wstring& _method      , // [in]      
//		const std::wstring& _direction   , // [in]
//		const std::wstring& _parity      , // [in]
//		AddressList &_list);               // [out]
//
//	// typedefs
//	typedef void (*listcallbackfunc)(const std::wstring&, IList*);
//	
//	// ctors
//	CTGroup(const std::wstring& _filename, const std::wstring& _ipoutbound);
//
//	CTGroup(const std::wstring& _name,
//		const std::wstring& _timeslots,
//		const std::wstring& _method, 
//		const std::wstring& _direction, 
//		const std::wstring& _parity, 
//		const std::wstring& _ipoutbound);
//
//
//	CTGroup(const std::wstring& _name,
//		const std::wstring& _dti,
//		const std::wstring& _timeslot,
//		const std::wstring& _method, 
//		const std::wstring& _direction, 
//		const std::wstring& _parity,
//		const std::wstring& _ipoutbound,
//		const ULONGLONG& ullAddress
//		);
//
//	~CTGroup();
//
//private:
//	void GetAddressByParametrs(const NameList& _dti_list,
//		const std::wstring& _method,
//		const std::wstring& _direction,
//		const std::wstring& _parity,
//		AddressList &_address_list);
//
//	std::wstring GetGroupName ()const{return m_sGroupName.GetString();}
//	void LUStepList(const std::wstring& _board, const std::wstring& _timeslot);
//
//	void ExtractTimeSlots(const std::wstring& sTimeSlots, // [in ]
//		NameList& _list);               // [out]
//
//	void BuildList();
//	void StepList ();
//	void StepList (const int& _board,	const int& _timeslot);
//
//	template<class _Pr> inline
//		NameList Remove_if(const NameList& _list, _Pr _Pred);
//
//	void AddDTI(const int&_board,
//		const int&_timeslot,
//		const ULONGLONG& _address);
//
//	void AddDTI(const std::wstring& _board,
//		const std::wstring& _timeslot,
//		const ULONGLONG& _address);
//
//	void      GetDTIAddress(AddressList &List);
//	//ULONGLONG GetDTIAddress(const std::wstring& _board, const std::wstring& _timeslot);
//
//private:
//	CAtlString      m_sGroupName;
//	NameList        m_TimeSlots;
//	NameList		m_ActiveTimeSlots;
//	CAtlString      m_method;
//	CAtlString      m_direction;
//	CAtlString      m_parity;
//	CAtlString      m_ipoutbound;
//	unsigned int    m_Counter;
//};
//
//
///******************************* eof *************************************/