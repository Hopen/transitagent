/************************************************************************/
/* Name     : Composite\IPOBound.cpp                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 10 Nov 2014                                               */
/************************************************************************/
#include "IPOBound.h"

std::wstring CIPOBound::log()const
{
	std::wstring out;
	out  = L"ip=" + m_ip;
	out += L";port=" + m_port;
	out += L";protocol=" + m_protocol;
	out += L";";
	return out;
}

/******************************* eof *************************************/