/************************************************************************/
/* Name     : Composite\IPOBound.h                                      */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 10 Nov 2014                                               */
/************************************************************************/
#pragma once
#include "Composite.h"

//// methods
//const CAtlString M_CIRCLE = L"Circle";
//const CAtlString M_LINEAR = L"Linear";
//const CAtlString M_RANDOM = L"Random";
////const CAtlString M_LASTUSED = L"LeastUsed";


class CIPOBound : public IList
{
public:
	CIPOBound(const std::wstring& _ip,
		const std::wstring& _port,
		const std::wstring& _protocol/*,
		const std::wstring& _name,
		const std::wstring& _method*/) :
		m_ip(_ip), m_port(_port), m_protocol(_protocol)/*, m_name(_name), m_method(_method)*/
	{

	}

	IList* GetScript(const CMessage& msg) { return NULL; }
	IList* GetCustomCfg(const CMessage& msg) { return NULL; }
	std::wstring GetPrefix()const { return std::wstring(L""); }
	IList* GetAliasList(const std::wstring& msg){return NULL;}
	std::wstring GetAliasPath()const { return std::wstring(L""); }
	IList* GetBlackList() { return NULL; }

	const ParamsList& GetOtherParams() const override
	{
		static ParamsList paramsList{};
		return paramsList;
	}

	// other interface
	void refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList) override { return; }

	std::wstring log()const;// { return std::wstring(); }
	E_ACTION GetAction(void) const{ return A_UNKNOWN; }
	bool BetterThanYou(const IList *) const{ return false; }
	UINT GetDestinationRegularPartLength()const{ return 0; }
	UINT GetDestinationLength()const{ return 0; }
	UINT GetOriginationRegularPartLength()const{ return 0; }
	UINT GetOriginationLength()const{ return 0; }
	//bool GetOriginationNotRegMask()const{ return false; }
	//bool GetDestinationNotRegMask()const{ return false; }
	UINT GetOriginQuestionLen           ()const { return 0; }
	UINT GetDestinQuestionLen           ()const { return 0; }
	UINT GetOriginPercentLen            ()const { return 0; }
	UINT GetDestinPercentLen            ()const { return 0; }



	std::wstring GetMessageName()const{ return L""; }
	std::wstring GetScriptName()const{ return L""; }
	std::wstring GetMonitorName()const{ return L""; }
protected:
	//bool GetAddressByGroupName(
	//	const std::wstring& _groupname,  // [in]
	//	AddressList &List){
	//	return false;
	//}// [out]

	//bool GetAddressByTimeSlot(
	//	const std::wstring& _dti_timeslot, // [in]
	//	const std::wstring& _method, // [in]      
	//	const std::wstring& _direction, // [in]
	//	const std::wstring& _parity, // [in]
	//	AddressList &_list){
	//	return false;
	//} // [out]


private:
	std::wstring m_ip;
	std::wstring m_port;
	std::wstring m_protocol;

	//std::wstring m_name;
	//std::wstring m_method;
};

/******************************* eof *************************************/