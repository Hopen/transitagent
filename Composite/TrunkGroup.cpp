///************************************************************************/
///* Name     : Composite\TrunkGroup.cpp                                  */
///* Author   : Andrey Alekseev                                           */
///* Company  : Forte-CT                                                  */
///* Date     : 8 Nov 2010                                                */
///************************************************************************/
//#include "stdafx.h"
//#include "TrunkGroup.h"
//
//const std::wstring ALPH = L"0123456789";
//
//enum ExtractCurrentState
//{
//	ECS_UNKNOWN = 0,
//	ECS_DTI,
//	ECS_TS,
//	ECS_ERROR
//};
//
//IList* CTGroup::GetScript( const CMessage& msg )
//{
//	return NULL;
//}
//
//bool static Convert(const std::wstring& sBoard, const std::wstring& sTimeSlot, int& iBoard, int& iTimeSlot)
//{
//	int pos = sBoard.find(L"DTI");
//	if (pos)
//		return false;
//	std::wstring sDTINum;
//	bool bError = false;
//	for (unsigned int i=3;i<sBoard.size() && !bError;++i)
//	{
//		if (ALPH.find(sBoard[i]) == -1)
//			bError = true;
//		else sDTINum+=sBoard[i];
//	}
//	if (bError)
//		return false;
//
//	iBoard    = _wtoi(sDTINum.c_str());
//	iTimeSlot = _wtoi(sTimeSlot.c_str());
//	return true;
//}
//
//void CTGroup::ExtractTimeSlots(const std::wstring& sTimeSlots, NameList& _list)
//{
//	std::vector<int> dti;
//	std::vector<int> ts;
//	std::wstring sTemp, first_range;
//	bool bEnum  = false;// "true" when we've got enumeration
//	bool bError = false;
//	int iCurrentState = ECS_UNKNOWN;
//	for (unsigned int i=0;i<sTimeSlots.size() && !bError;++i)
//	{
//		WCHAR CurrentChar = sTimeSlots[i];
//		switch (iCurrentState)
//		{
//		case ECS_UNKNOWN:
//			{
//				if (CurrentChar=='[')
//					iCurrentState = ECS_DTI;
//				if (CurrentChar=='(')
//					iCurrentState = ECS_TS;
//				continue;
//				break;
//			}
//		case ECS_DTI:
//		case ECS_TS:
//			{
//				std::vector<int> &rLiast      = (iCurrentState==ECS_DTI)?dti:ts;
//				//WCHAR            &rFinishChar = (iCurrentState==ECS_DTI)?']':')';
//				if(CurrentChar==']' && iCurrentState == ECS_DTI || CurrentChar==')' && iCurrentState == ECS_TS)
//				{
//					if (bEnum)
//					{
//						int range1 = _wtoi(first_range.c_str()),
//							range2 = _wtoi(sTemp.c_str());
//						for (int i=range1;i<=range2;++i)
//						{
//							rLiast.push_back(i);
//						}
//						bEnum = false;
//					}
//					else
//						rLiast.push_back(_wtoi(sTemp.c_str()));
//					sTemp.clear();
//					iCurrentState = ECS_UNKNOWN;
//					continue;
//				}
//				if(CurrentChar==',')
//				{
//					if (bEnum)
//					{
//						int range1 = _wtoi(first_range.c_str()),
//							range2 = _wtoi(sTemp.c_str());
//						for (int i=range1;i<=range2;++i)
//						{
//							rLiast.push_back(i);
//						}
//						bEnum = false;
//					}
//					else
//						rLiast.push_back(_wtoi(sTemp.c_str()));
//					sTemp.clear();
//					continue;
//				}
//				if(CurrentChar=='-' && bEnum || 
//					CurrentChar==',' && bEnum   )
//				{
//					iCurrentState = ECS_ERROR;
//					continue;
//				}
//				if (CurrentChar == '-')
//				{
//					first_range = sTemp;
//					sTemp.clear();
//					bEnum = true;
//					continue;
//				}
//				if (ALPH.find(CurrentChar) == -1)
//				{
//					iCurrentState = ECS_ERROR;
//					continue;
//				}
//				sTemp+=CurrentChar;
//
//				break;
//			}
//
//		case ECS_ERROR:
//			{
//				bError = true;
//			}
//		};
//	}
//
//	if (iCurrentState == ECS_ERROR)
//	{
//		// swearing here
//		return;
//	}
//
//	for (unsigned int i=0;i<dti.size();++i)
//	{
//		for (unsigned int j=0;j<ts.size();++j)
//		{
//			_list.push_back(DTI(dti[i],ts[j]));
//		}
//	}
//}
//
//CTGroup::CTGroup(const std::wstring& _name,
//						 const std::wstring& _timeslots,
//						 const std::wstring& _method, 
//						 const std::wstring& _direction, 
//						 const std::wstring& _parity,
//						 const std::wstring& _ipoutbound)
//						 : m_method   ((!_method   .empty())?_method   .c_str():TrunkGroupConsts::M_CIRCLE ),
//						 m_direction((!_direction.empty())?_direction.c_str():TrunkGroupConsts::D_FORWARD),
//						 m_parity   ((!_parity   .empty())?_parity   .c_str():TrunkGroupConsts::P_BOTH   ),
//						 m_ipoutbound(_ipoutbound.c_str()),
//						 m_sGroupName(_name.c_str()),
//						 m_Counter  (0)
//
//{
//	ExtractTimeSlots(_timeslots,m_TimeSlots);
//};
//
//CTGroup::~CTGroup()
//{
//
//}
//
//CTGroup::NameList::iterator CTGroup::NameList::Find(const int& _board, const int& _timeslot)
//{
//	for (NameList::iterator it = begin();it!=end();++it)
//	{
//		if (it->board == _board && it->timeslot == _timeslot)
//			return it;
//	}
//
//	return end();
//}
//
//
//CTGroup::NameList::iterator CTGroup::NameList::Find(const std::wstring& _board, const std::wstring& _timeslot)
//{
//	int iBoard    = 0, 
//		iTimeSlot = 0;
//
//	if (Convert(_board,_timeslot,iBoard,iTimeSlot))
//		return Find(iBoard,iTimeSlot);
//	return end();
//}
//
//template<class _Pr>
//CTGroup::NameList CTGroup::Remove_if(const NameList& _list, _Pr _Pred)
//{
//	NameList ExistTimeSlots(_list);
//	NameList NewTimeSlots; NewTimeSlots.clear();
//	NameList::iterator it1 = std::remove_if(ExistTimeSlots.begin(),ExistTimeSlots.end(),_Pred);
//	NameList::iterator it2 = ExistTimeSlots.begin();
//	while (it2!=it1)
//	{
//		NewTimeSlots.push_back(*it2);
//		++it2;
//	}
//	return NewTimeSlots;
//}
//
//
//void CTGroup::BuildList()
//{
//	m_ActiveTimeSlots = Remove_if(m_TimeSlots,CTGroup::CRemoveUnctiveTimeSlotPredicat());
//
//	if (!TrunkGroupConsts::M_RANDOM.CompareNoCase(m_method))
//		std::random_shuffle ( m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end() );
//	if (!TrunkGroupConsts::D_REVERSE.CollateNoCase(m_direction))
//		std::reverse(m_ActiveTimeSlots.begin(), m_ActiveTimeSlots.end() );
//
//	if (!TrunkGroupConsts::P_EVEN.CollateNoCase(m_parity))
//		//std::remove_if(m_ActiveTimeSlots.begin(),m_ActiveTimeSlots.end(),CTrunkGroup::CRemoveParityTimeSlotPredicat(true));
//		m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots,CTGroup::CRemoveParityTimeSlotPredicat(true));
//
//	if (!TrunkGroupConsts::P_ODD.CollateNoCase(m_parity))
//		//std::remove_if(m_ActiveTimeSlots.begin(),m_ActiveTimeSlots.end(),CTrunkGroup::CRemoveParityTimeSlotPredicat(false));
//		m_ActiveTimeSlots = Remove_if(m_ActiveTimeSlots,CTGroup::CRemoveParityTimeSlotPredicat(false));
//
//	m_Counter = 0;
//
//}
//
//void CTGroup::StepList()
//{
//	if (!TrunkGroupConsts::M_LASTUSED.CollateNoCase(m_method))
//		return;
//
//	if (!m_ActiveTimeSlots.size() || m_ActiveTimeSlots.size() == 1)
//		return;
//	bool bFirstElem = true;
//	NameList::iterator it = m_ActiveTimeSlots.begin();
//	if (!TrunkGroupConsts::D_REVERSE.CollateNoCase(m_direction))
//	{
//		it = m_ActiveTimeSlots.end();
//		--it;
//		bFirstElem = false;
//	}
//
//	if (!TrunkGroupConsts::M_CIRCLE.CollateNoCase(m_method) || !TrunkGroupConsts::M_RANDOM.CollateNoCase(m_method))
//	{
//		DTI _dti = *it;
//		m_ActiveTimeSlots.erase(it);
//		m_ActiveTimeSlots.insert(bFirstElem?m_ActiveTimeSlots.end():m_ActiveTimeSlots.begin(),_dti);
//	}
//	++m_Counter;
//	if (m_Counter>=m_ActiveTimeSlots.size())
//	{
//		BuildList();
//	}
//}
//
//void CTGroup::GetDTIAddress(AddressList &_list)
//{
//	for (unsigned int i=0;i<m_ActiveTimeSlots.size();++i)
//	{
//		_list.push_back(m_ActiveTimeSlots[i].address);
//	}
//}
//
//bool CTGroup::GetAddressByGroupName(const std::wstring& _groupname, AddressList &_list)
//{
//	if (!m_sGroupName.CompareNoCase(_groupname.c_str()))
//	{
//		GetDTIAddress(_list);
//		StepList(/*false*/);
//		return true;
//	}
//	//for (unsigned int i=0;i<m_childrens.size();++i)
//	//{
//	//	if (m_childrens[i]->GetAddressByGroupName(_groupname,_list))
//	//		return true;
//	//}
//	return false;
//}	
//
//void CTGroup::GetAddressByParametrs(const NameList& _dti_list, const std::wstring& _method, const std::wstring& _direction, const std::wstring& _parity, AddressList &_address_list)
//{
//	if (!(!_method   .empty() && m_method   .CompareNoCase(_method   .c_str()) ||
//		!_direction.empty() && m_direction.CompareNoCase(_direction.c_str()) ||
//		!_parity   .empty() && m_parity   .CompareNoCase(_parity   .c_str())   ) )
//		//if (!m_method   .CompareNoCase(_method   .c_str()) &&
//		//	!m_direction.CompareNoCase(_direction.c_str()) &&
//		//	!m_parity   .CompareNoCase(_parity   .c_str())
//		//	)
//	{
//		for (unsigned int i=0;i<_dti_list.size();++i)
//		{
//			NameList::iterator it = m_ActiveTimeSlots.Find(_dti_list[i].board,_dti_list[i].timeslot);
//			if (it!=m_ActiveTimeSlots.end())
//			{
//				AddressList::const_iterator cit = std::find(_address_list.begin(),_address_list.end(),it->address);
//				if (cit == _address_list.end())
//					_address_list.push_back(it->address);
//			}
//		}
//	}
//	//for (unsigned int i=0;i<m_childrens.size();++i)
//	//{
//	//	m_childrens[i]->GetAddressByParametrs(_dti_list,_method,_direction,_parity,_address_list);
//	//}
//}
//
//bool CTGroup::GetAddressByTimeSlot(const std::wstring& _dti_timeslot, /*const std::wstring& _timeslot,*/ const std::wstring& _method, const std::wstring& _direction, const std::wstring& _parity, AddressList &_list)
//{
//	NameList WaitTimeSlots;
//	ExtractTimeSlots(_dti_timeslot,WaitTimeSlots);
//	if (!WaitTimeSlots.size())
//		return false;
//
//	GetAddressByParametrs(WaitTimeSlots,_method,_direction,_parity,_list );
//	if (_list.size())
//		return true;
//	//// still cannot find. Try to find with default parameters
//	//GetAddressByParametrs(WaitTimeSlots,M_CIRCLE.GetString(),D_FORWARD.GetString(),P_BOTH.GetString(),_list);
//	//if (_list.size())
//	//	return true;
//
//	return false;
//}
//
//std::wstring CTGroup::log()const
//{
//	std::wstring sLogString;
//	return sLogString;
//}
//
//
///******************************* eof *************************************/