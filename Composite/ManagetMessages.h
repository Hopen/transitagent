/************************************************************************/
/* Name     : Composite\ManagetMessages.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 18 Aug 2010                                               */
/************************************************************************/
#pragma once
#include "Composite.h"

class CManagedMessages: public IList
{
private:
	typedef std::map<std::wstring,std::wstring> Parameters;
	Parameters   m_Param;
	std::wstring m_sMonitorDisplayedName;

	std::wstring m_sScriptName; // script name existing means run script action, otherwise ignore
	std::wstring m_sMessage;

	SPtr m_pOwnBlackList;

public:
	CManagedMessages(const std::wstring& message, const std::wstring& script, const std::wstring& mask, const std::wstring& display)
		//: m_uMaskLength(mask.length()),
		  //m_uRegMaskLength(0),
		  //m_sOrigination(mask)
	{
		m_sScriptName = script;
		//std::wstring origination;
		//ConvertMask(mask,origination,m_uRegMaskLength);
		//m_origination = boost::xpressive::wsregex::compile(origination);
		m_sMessage = message;
		m_sMonitorDisplayedName = display;
	}
	virtual E_ACTION GetAction()const{return (m_sScriptName.empty()?A_IGNORE:A_RUNSCRIPT);}
	void AddParam(const std::wstring& _name, const std::wstring& _value);
	bool BetterThanYou(const IList* Matched/*, const std::wstring& sOrigination, const std::wstring& sDistinationst*/)const;
	UINT GetDestinationRegularPartLength()const{return 0;}
	UINT GetDestinationLength           ()const{return 0;}
	UINT GetOriginationRegularPartLength()const{return 0/*m_uRegMaskLength*/;}
	UINT GetOriginationLength           ()const{return 0/*m_uMaskLength*/;}
	//bool GetOriginationNotRegMask		()const{return false;}
	//bool GetDestinationNotRegMask		()const{return false;}
	UINT GetOriginQuestionLen           ()const { return 0; }
	UINT GetDestinQuestionLen           ()const { return 0; }
	UINT GetOriginPercentLen            ()const { return 0; }
	UINT GetDestinPercentLen            ()const { return 0; }



	std::wstring GetMessageName()const {return m_sMessage             ;}
	std::wstring GetScriptName ()const {return m_sScriptName          ;}
	std::wstring GetMonitorName()const {return m_sMonitorDisplayedName;}

	std::wstring GetPrefix()const {return std::wstring();}
	IList* GetCustomCfg(const CMessage& msg){return NULL;}
	IList* GetAliasList(const std::wstring& msg){return NULL;}
	std::wstring GetAliasPath()const {return std::wstring(L"");}
	IList* GetBlackList() { return m_pOwnBlackList.get(); }

	void AddBlackList(SPtr _blacklist);

	void refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList) override;

	IList* find (const std::wstring& _param){return NULL;}
	std::wstring log()const;

protected:
	IList* GetScript(const CMessage& msg/*const std::wstring& sOrigination, const std::wstring& sDistination, const std::wstring& sMessage*/);

	const ParamsList& GetOtherParams() const override
	{
		static ParamsList paramsList{};
		return paramsList;
	}

	//bool GetAddressByGroupName(
	//	const std::wstring& _groupname,  // [in]
	//	AddressList &List){return false;}// [out]

	//bool GetAddressByTimeSlot(
	//	const std::wstring& _dti_timeslot, // [in]
	//	const std::wstring& _method      , // [in]      
	//	const std::wstring& _direction   , // [in]
	//	const std::wstring& _parity      , // [in]
	//	AddressList &_list){return false;} // [out]

};

/******************************* eof *************************************/