/************************************************************************/
/* Name     : Composite\Composite.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 24 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "Composite.h"

#include "CompositeSingleton.h"
#include "AliasListSingleton.h"
#include "BlackListSingleton.h"
#include "CustomCfgSingleton.h"
#include "ManagedMessagesSingleton.h"
#include "PhoneMasksSingleton.h"
#include "DebugListCollector.h"
#include "IPOBoundSingleton.h"

enum E_CONVERT_STATE
{
	CS_NUM = 1,
	CS_REGUL
};

void IList::ConvertMask(const std::wstring& _in, std::wstring& _out, UINT& uRegularPartLength, UINT& uQuestionLen, UINT& uPercentLen)
{
	// extract regular expressions
	std::wstring temp;
	int CurrentState = CS_NUM;

	int iSCount = 0; // counter, which shows how many times we've entered in { }
	for (unsigned int i=0;i<_in.size();++i)
	{
		WCHAR CurrentChar = _in[i];
		switch (CurrentState)
		{
		case CS_NUM:
			{
				if (CurrentChar == '{')
				{
					CurrentState = CS_REGUL;
				}
				else
				{
					if (CurrentChar == '.')
					{
						temp+=L'\\';
					}
					if (CurrentChar == '?')
					{
						++uQuestionLen;
					}
					if (CurrentChar == '%')
					{
						++uPercentLen;
					}
					temp+=CurrentChar;
				}
				break;
			}
		case CS_REGUL:
			{
				if (CurrentChar == '{')
				{
					++iSCount;
				}

				if (CurrentChar == '}')
				{
					if (!iSCount)
					{
						CurrentState = CS_NUM;
					}
					else
					{
						temp+=CurrentChar;
						--iSCount;
					}
				}
				else
				{
					++uRegularPartLength;
					temp+=CurrentChar;
				}
				break;
			}
		};

	}

	
	std::wstring simbolsIn [] = {L"%" , L"?", L""};
	std::wstring simbolsOut[] = {L".*", L".", L""};
	ReplaceSymbols(temp,simbolsIn,simbolsOut);

	_out = temp;
}

void IList::ReplaceSymbols(std::wstring& sExpr, std::wstring simbolsIn[],std::wstring simbolsOut[])const
{
	ATLASSERT(sizeof(simbolsOut) == sizeof(simbolsIn));

	std::wstring *pSim = simbolsIn;
	unsigned int i = 0;
	while (pSim && !pSim->empty())
	{
		int pos = -1;
		while ((pos = sExpr.find(*pSim,++pos))>=0)
		{
			sExpr.replace(pos,pSim->size(),simbolsOut[i]);
			pos+=simbolsOut[i].size()-1;
		}

		++i;
		++pSim;
	}
}

State<wchar_t> IList::CreateAutomat(const wchar_t* aFilter)
{
	return Factory::CreateAutomat(aFilter);
}

template<class Factory>
IList* CompositeAction<Factory>::GetScript(const CMessage& msg)
{
	std::lock_guard<std::mutex> mylock(mUpdateMutex);

	IList* pRet = NULL; // recipient
	for (auto& sptr : children_)
	{
		if (IList * pRecipient = sptr->GetScript(msg))
		{
			if (pRecipient->GetAction() == A_IGNORE) // entire in black list
				return (pRet = pRecipient);

			if (pRecipient->GetAction() == A_UNKNOWN) // error, cannot be unknown
			{
				// log error !!!
				continue;
			}

			if (!pRet) // just got first match
			{
				if (!pRecipient->GetMessageName().empty()) // entire in ManagetMessages
					return pRecipient; // found it

				pRet = pRecipient;
				continue;
			}

			// entire in ManagetMessages
			if (!pRecipient->GetMessageName().empty() && pRet->GetMessageName().empty())
			{
				return pRecipient; // found it
			}

			if (pRecipient->GetMessageName().empty() && !pRet->GetMessageName().empty())
			{
				continue;
			}

			if (pRecipient->BetterThanYou(pRet))
				pRet = pRecipient;
		}
	}
	return pRet;
}

template<class Factory>
void CompositeAction<Factory>::refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList)
{
	if (!m_sFileName.compare(_filename))
	{
		auto temp = std::make_shared<CompositeAction<Factory>>(_factory, _filename, this->m_method);
		_factory->refresh(_filename, temp, blackList);

		std::lock_guard<std::mutex> mylock(mUpdateMutex);
		this->children_.swap(temp->children_);
		this->m_method = temp->m_method;
		
		return; // children has already changed
	}
	for (auto cit = children_.begin(); cit != children_.end(); ++cit)
	{
		(*cit).get()->refresh(_filename, *cit, blackList);
	}
}

template<class Factory>
IList* CompositeAction<Factory>::GetCustomCfg(const CMessage& msg)
{
	for (std::list<SPtr>::iterator it = children_.begin(); it!=children_.end(); ++it)
	{
		SPtr& sptr = *it;
		if (IList * pRecipient = sptr->GetCustomCfg(msg))
			return pRecipient;
	}
	return NULL;
}

template<class Factory>
IList* CompositeAction<Factory>::GetAliasList(const std::wstring& name)
{
	for (std::list<SPtr>::iterator it = children_.begin(); it!=children_.end(); ++it)
	{
		SPtr& sptr = *it;
		if (IList * pRecipient = sptr->GetAliasList(name))
			return pRecipient;
	}
	return NULL;
}

template<class Factory>
IList* CompositeAction<Factory>::find(const std::wstring& _param)
{
	return NULL;
}

template<class Factory>
void CompositeAction<Factory>::step()
{
	if (m_method == SM_CIRCLE)
	{
		IList::SPtr first = children_.front();
		children_.pop_front();
		children_.push_back(first);
	}
}

template<class Factory>
void CompositeAction<Factory>::sort()
{
	if (m_method == SM_RANDOM)
	{ 
		std::vector<IList::SPtr> tmpVector(children_.begin(), children_.end());
		std::random_shuffle(tmpVector.begin(), tmpVector.end());
		std::copy(tmpVector.begin(), tmpVector.end(), children_.begin());
	}
}

template<class Factory>
IList::SPtr CompositeAction<Factory>::get_front()const
{
	return children_.front();
}


template class CompositeAction<CCompositeSingleton>;
template class CompositeAction<CDebugListCollector>;
template class CompositeAction<CAliasListSingleton>;
template class CompositeAction<CBlackListSingleton>;
template class CompositeAction<CCustomCfgSingleton>;
template class CompositeAction<CManagedMessagesSingleton>;
template class CompositeAction<CPhoneMasksSingleton>;
template class CompositeAction<DummyLoader>;
template class CompositeAction<CIPOBoundSingleton>;

/******************************* eof *************************************/