/************************************************************************/
/* Name     : Composite\DebugList.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 28 Oct 2010                                               */
/************************************************************************/
#pragma once
//#include "protocol.h"
#include "PhoneMasks.h"


class CDebugList:public CPhoneMasks
{
public:
	CDebugList(
		//const std::wstring& script, 
		const std::wstring& _origination, 
		const std::wstring& _destination, 
		//const std::wstring& display,
		const std::wstring& _request,
		const std::wstring& _runrequest,
		const CMessage& _message,
		CLIENT_ADDRESS* _from
		);

	CMessage&    GetDebugMessage(){return m_message;}

	// interface
	std::wstring GetMessageName()const {return std::wstring();}
	std::wstring GetScriptName ()const {return std::wstring();}
	std::wstring GetMonitorName()const {return std::wstring();}
	IList* GetAliasList(const std::wstring& msg){return NULL;}
	std::wstring GetAliasPath()const {return std::wstring(L"");}
	IList* GetBlackList() { return NULL; }

	//IList::SPtr FindByRequestID(const std::wstring& _id);
	IList* find (const std::wstring& _param);
	IList* find (const DWORD& _dwClient);

	DWORD GetAddress()const{return m_pFrom->GetClientID();}

private:
	CMessage        m_message;
	CLIENT_ADDRESS* m_pFrom;
	std::wstring    m_sRequestID;
	std::wstring    m_sRunRequestID;
};

/******************************* eof *************************************/