/************************************************************************/
/* Name     : Composite\PhoneMasks.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 23 Aug 2010                                               */
/************************************************************************/
#pragma once
#include "Composite.h"

class CPhoneMasks: public IList
{
protected:
	std::wstring m_sScriptName; // script name existing means run script action, otherwise ignore
	//boost::xpressive::wsregex m_origination;
	//boost::xpressive::wsregex m_destination;
	//boost::wregex m_origination;
	//boost::wregex m_destination;
	std::wstring m_sMonitorDisplayedName;

	UINT m_uDestinRegMaskLength;
	UINT m_uOriginRegMaskLength;
	UINT m_uDestinMaskLength;
	UINT m_uOriginMaskLength;

	//bool m_bOriginNotRegMask;
	//bool m_bDestinNotRegMask;
	UINT m_uOriginQuestionLen;
	UINT m_uDestinQuestionLen;
	UINT m_uOriginPercentLen;
	UINT m_uDestinPercentLen;

	std::wstring m_sOrigination;
	std::wstring m_sDestination;

	SPtr m_pOwnBlackList;
	IList* m_pCustomCfg;
	std::wstring m_sPrefix;
	IList* m_pAliasList;

	ParamsList m_params;

	State<wchar_t> mMatcherOrigin;
	State<wchar_t> mMatcherDestin;


public:


	CPhoneMasks(
		const std::wstring& script, 
		const std::wstring& _origination, 
		const std::wstring& _destination, 
		const std::wstring& display,
		ParamsList&& _params)
		: m_uDestinMaskLength (_destination.length()),
		  m_uOriginMaskLength (_origination.length()),
		  m_uDestinRegMaskLength(0),
		  m_uOriginRegMaskLength(0),
		  m_sOrigination(_origination),
		  m_sDestination(_destination),
		  //m_bOriginNotRegMask(false),
		  //m_bDestinNotRegMask(false)
		m_uOriginQuestionLen(0),
		m_uDestinQuestionLen(0),
		m_uOriginPercentLen(0),
		m_uDestinPercentLen(0),
		m_params(std::move(_params))
	{
		std::wstring origination, destination;
		ConvertMask(_origination,origination,m_uOriginRegMaskLength, m_uOriginQuestionLen, m_uOriginPercentLen);
		ConvertMask(_destination,destination,m_uDestinRegMaskLength, m_uDestinQuestionLen, m_uDestinPercentLen);
		m_sScriptName = script;
		//m_origination = boost::xpressive::wsregex::compile(origination);
		//m_destination = boost::xpressive::wsregex::compile(destination);
		//m_origination = boost::wregex(origination);
		//m_destination = boost::wregex(destination);
		m_sMonitorDisplayedName = display;
		m_pCustomCfg = NULL;

		mMatcherOrigin = IList::CreateAutomat(_origination.c_str());
		mMatcherDestin = IList::CreateAutomat(_destination.c_str());
	}
	virtual E_ACTION GetAction()const{return A_RUNSCRIPT;}
	virtual bool BetterThanYou(const IList* Matched/*, const std::wstring& sOrigination, const std::wstring& sDistinationst*/)const;
	UINT GetDestinationRegularPartLength()const{return m_uDestinRegMaskLength; }
	UINT GetDestinationLength           ()const{return m_uDestinMaskLength;    }
	UINT GetOriginationRegularPartLength()const{return m_uOriginRegMaskLength; }
	UINT GetOriginationLength           ()const{return m_uOriginMaskLength;    }
	//bool GetOriginationNotRegMask		()const{return m_bOriginNotRegMask;}
	//bool GetDestinationNotRegMask		()const{return m_bDestinNotRegMask;}

	UINT GetOriginQuestionLen           ()const { return m_uOriginQuestionLen; }
	UINT GetDestinQuestionLen           ()const { return m_uDestinQuestionLen; }
	UINT GetOriginPercentLen            ()const { return m_uOriginPercentLen; }
	UINT GetDestinPercentLen            ()const { return m_uDestinPercentLen; }


	std::wstring GetMessageName()const {return std::wstring()         ;}
	std::wstring GetScriptName ()const ;//{return m_sScriptName          ;}
	std::wstring GetMonitorName()const {return m_sMonitorDisplayedName;}

	std::wstring GetPrefix()const {return m_sPrefix;}
	IList* GetCustomCfg(const CMessage& msg){return NULL;}
	IList* GetAliasList(const std::wstring& msg){return NULL;}
	std::wstring GetAliasPath()const {return std::wstring(L"");}
	IList* GetBlackList() { return m_pOwnBlackList.get(); }

	void AddBlackList(SPtr _blacklist);
	void AddCustomConfig(IList* _customcfg);
	void AddAliasList(IList* _aliaslist);

	void refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList) override;

	IList* find (const std::wstring& _param){return NULL;}
	std::wstring log()const;

protected:
	//IList* GetScript2(const CMessage& msg);
	IList* GetScript(const CMessage& msg);

	const ParamsList& GetOtherParams() const override
	{
		return m_params;
	}

	//bool GetAddressByGroupName(
	//	const std::wstring& _groupname,  // [in]
	//	AddressList &List){return false;}// [out]

	//bool GetAddressByTimeSlot(
	//	const std::wstring& _dti_timeslot, // [in]
	//	const std::wstring& _method      , // [in]      
	//	const std::wstring& _direction   , // [in]
	//	const std::wstring& _parity      , // [in]
	//	AddressList &_list){return false;} // [out]

};

class CPhoneMasksRegion : public CPhoneMasks
{
public:
	CPhoneMasksRegion(
		const std::wstring& script, 
		const std::wstring& _origination, 
		const std::wstring& _destination, 
		const std::wstring& display,
		ParamsList&& _params)
	: CPhoneMasks(script, _origination, _destination, display, std::move(_params))
	{

	}
	virtual E_ACTION GetAction()const{ return A_REGION; }
};

/******************************* eof *************************************/