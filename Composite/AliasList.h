/************************************************************************/
/* Name     : Composite\AliasList.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 08 Apr 2014                                               */
/************************************************************************/
#pragma once
#include "Composite.h"

class CAliasList: public IList
{
public:
	CAliasList(const std::wstring& _name, const std::wstring& _path):m_name(_name),m_path(_path)
	{

	}

	IList* GetScript(const CMessage& msg) {return NULL;}
	IList* GetCustomCfg(const CMessage& msg) {return NULL;}
	std::wstring GetPrefix()const {return std::wstring(L"");}
	IList* GetAliasList(const std::wstring& msg);//{return NULL;}
	std::wstring GetAliasPath()const {return m_path;};
	IList* GetBlackList() { return NULL; }
	const ParamsList& GetOtherParams() const override
	{
		static ParamsList paramsList{};
		return paramsList;
	}

	// other interface
	void refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList) override { return; }

	std::wstring log()const{return std::wstring();}
	E_ACTION GetAction(void) const{return A_UNKNOWN;}
	bool BetterThanYou(const IList *) const{return false;}
	UINT GetDestinationRegularPartLength()const{return 0;}
	UINT GetDestinationLength           ()const{return 0;}
	UINT GetOriginationRegularPartLength()const{return 0;}
	UINT GetOriginationLength           ()const{return 0;}
	//bool GetOriginationNotRegMask		()const{return false;}
	//bool GetDestinationNotRegMask		()const{return false;}
	UINT GetOriginQuestionLen           ()const { return 0; }
	UINT GetDestinQuestionLen           ()const { return 0; }
	UINT GetOriginPercentLen            ()const { return 0; }
	UINT GetDestinPercentLen            ()const { return 0; }



	std::wstring GetMessageName()const{return L"";}
	std::wstring GetScriptName ()const{return L"";}
	std::wstring GetMonitorName()const{return L"";}
protected:
	bool GetAddressByGroupName(
		const std::wstring& _groupname,  // [in]
		AddressList &List){return false;}// [out]

	bool GetAddressByTimeSlot(
		const std::wstring& _dti_timeslot, // [in]
		const std::wstring& _method      , // [in]      
		const std::wstring& _direction   , // [in]
		const std::wstring& _parity      , // [in]
		AddressList &_list){return false;} // [out]


private:
	std::wstring m_name;
	std::wstring m_path;
};

/******************************* eof *************************************/