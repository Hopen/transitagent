/************************************************************************/
/* Name     : Composite\Composite.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 17 Aug 2010                                               */
/************************************************************************/
#pragma once

#include <list>
#include <vector>
#include <set>
#include <memory>
#include <mutex>
#include <boost/shared_ptr.hpp>
//#include <boost/xpressive/xpressive.hpp>
#include <boost/regex/v4/regex.hpp>
#include <boost/foreach.hpp>
//#include "EvtModel.h"
#include "Router/router_compatibility.h"
//#include "ce_xml.hpp"
#include "Parser.h"


typedef std::vector<std::wstring> StringList;
typedef std::set   <std::wstring> StringSet;

enum E_ACTION
{
	A_UNKNOWN = 0,
	A_IGNORE,
	A_RUNSCRIPT,
	A_REGION
};

enum E_SORT_METHOD
{
	SM_LINEAR = 0,
	SM_CIRCLE,
	SM_RANDOM,
	SM_LASTUSED
};

//class CCompositeSingleton;

class IList
{
public:
	using ParamsList = std::map<std::wstring, std::wstring>;

public:

	using SPtr = std::shared_ptr<IList>;
	using BlackListGroups = std::map<std::wstring, IList::SPtr>;

	using updatefunc = std::function<bool(const std::wstring&)>;

	typedef std::vector<ULONGLONG>  AddressList;

	virtual IList* GetScript( const CMessage& msg ) = 0;
	virtual const ParamsList& GetOtherParams() const = 0;

	virtual void add(const SPtr&) {
		throw std::runtime_error("IList: Can't add to a leaf");
	}

	virtual void remove(const SPtr&){
		throw std::runtime_error("IList: Can't remove from a leaf");
	}

	virtual void remove(const std::wstring&){
		
	}
	virtual void remove(const DWORD&){

	}

	virtual IList* find(const std::wstring& _filename)
	{
		throw std::runtime_error("IList: Can't find from a leaf");
		return NULL;
	}

	virtual IList* find(const DWORD& _filename)
	{
		return NULL;
	}

	virtual void refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList)
	{
		throw std::runtime_error("IList: Can't refresh from a leaf");
	}

	virtual void clear()
	{
		throw std::runtime_error("IList: Can't clear from a leaf");
	}

	virtual void load(const std::wstring& fileName, IList::SPtr _elem, IList::BlackListGroups& blackList)
	{
		throw std::runtime_error("IList: Can't load from a leaf");
	}

	virtual void step(/*IList::SPtr _elem*/) {
		throw std::runtime_error("IList: Can't step from a leaf");
	}

	virtual void sort() {
		throw std::runtime_error("IList: Can't sort from a leaf");
	}

	virtual IList::SPtr get_front()const{
		throw std::runtime_error("IList: Can't get front from a leaf");
	}

	virtual std::wstring log()const = 0;

	void ConvertMask(const std::wstring& _in, std::wstring& _out, UINT& uRegularPartLength, UINT& uQuestionLen, UINT& uPercentLen);

	State<wchar_t> CreateAutomat(const wchar_t* aFilter);

	virtual ~IList()
	{
		int test = 0;
	};

	// interface
	virtual E_ACTION GetAction()const = 0;
	virtual bool BetterThanYou(const IList* Matched)const = 0;
	virtual UINT GetDestinationRegularPartLength()const = 0;
	virtual UINT GetDestinationLength           ()const = 0;
	virtual UINT GetOriginationRegularPartLength()const = 0;
	virtual UINT GetOriginationLength           ()const = 0;

	virtual UINT GetOriginQuestionLen           ()const = 0;
	virtual UINT GetDestinQuestionLen           ()const = 0;
	virtual UINT GetOriginPercentLen            ()const = 0;
	virtual UINT GetDestinPercentLen            ()const = 0;


	virtual std::wstring GetMessageName()const = 0;
	virtual std::wstring GetScriptName ()const = 0;
	virtual std::wstring GetMonitorName()const = 0;

	virtual IList* GetCustomCfg(const CMessage& msg)=0;
	virtual std::wstring GetPrefix()const=0;
	virtual IList* GetAliasList(const std::wstring& msg)=0;
	virtual std::wstring GetAliasPath()const=0;
	virtual IList* GetBlackList() = 0;
protected:
	void ReplaceSymbols(std::wstring& sExpr, std::wstring simbolsIn[],std::wstring simbolsOut[])const;
};

template <class Factory>
class CompositeAction: public IList
{
//public:
	//using SingletonFactory = Factory;

public:
	CompositeAction(Factory* factory, const std::wstring& _filename, E_SORT_METHOD _method = SM_LINEAR)
		: _factory{ factory }
		, m_sFileName(_filename)
		, m_method(_method)
	{
	}

	void add(const SPtr& sptr) override {
		children_.push_back(sptr);
	}

	void remove(const SPtr& sptr) override {
		children_.remove(sptr);
	}

	void remove(const std::wstring& _param) override
	{
		std::list<SPtr> new_children;
		BOOST_FOREACH(SPtr& sptr, children_)
		{
			if (!sptr->find(_param))
				new_children.push_back(sptr);
		}
		children_.clear();
		children_ = new_children;
	}

	void remove(const DWORD& _param) override
	{
		std::list<SPtr> new_children;
		BOOST_FOREACH(SPtr& sptr, children_)
		{
			if (!sptr->find(_param))
				new_children.push_back(sptr);
		}
		children_.clear();
		children_ = new_children;
	}

	void replace(const SPtr& oldValue, const SPtr& newValue) {
		std::replace(children_.begin(), children_.end(), oldValue, newValue);
	}

	virtual IList* find (const std::wstring& _param);

	void refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList) override;

	void clear()override
	{
		children_.clear();
	}

	void load(const std::wstring& fileName, IList::SPtr _elem, IList::BlackListGroups& blackListGroups) override
	{
		_factory->load(fileName, _elem, blackListGroups);
	}

	std::wstring log() const override
	{
		return std::wstring(L"");
	}

	void step() override;
	void sort() override;

	IList::SPtr get_front()const override;
	virtual IList* GetScript(const CMessage& msg) override;

	const ParamsList& GetOtherParams() const override
	{
		static ParamsList params;
		return params;
	}

	virtual E_ACTION GetAction()const override {return A_UNKNOWN;}
	virtual bool BetterThanYou(const IList* Matched) const {return false;}

	UINT GetDestinationRegularPartLength()const{return 0;}
	UINT GetDestinationLength           ()const{return 0;}
	UINT GetOriginationRegularPartLength()const{return 0;}
	UINT GetOriginationLength           ()const{return 0;}
	UINT GetOriginQuestionLen           ()const { return 0; }
	UINT GetDestinQuestionLen           ()const { return 0; }
	UINT GetOriginPercentLen            ()const { return 0; }
	UINT GetDestinPercentLen            ()const { return 0; }

	std::wstring GetMessageName()const {return std::wstring();}
	std::wstring GetScriptName ()const {return m_sFileName;}
	std::wstring GetMonitorName()const {return std::wstring();}

	IList* GetCustomCfg(const CMessage& msg);
	std::wstring GetPrefix()const {return std::wstring();}
	IList* GetAliasList(const std::wstring& msg);
	std::wstring GetAliasPath()const {return std::wstring();}
	IList* GetBlackList() { return NULL; }
private:
	Factory* _factory;
	std::list<SPtr> children_;
	
	std::mutex mUpdateMutex;

protected:
	std::wstring  m_sFileName;
	E_SORT_METHOD m_method;	
};

struct DummyLoader
{
	void load(const std::wstring&, IList::SPtr, IList::BlackListGroups& blackListGroups) {}
	void refresh(const std::wstring&, IList::SPtr, IList::BlackListGroups& blackListGroups) {}
};

/******************************* eof *************************************/