/************************************************************************/
/* Name     : Composite\AliasList.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 08 Apr 2014                                               */
/************************************************************************/

#include "stdafx.h"
#include "AliasList.h"

IList* CAliasList::GetAliasList(const std::wstring& sName)
{
	IList* pRecipient = NULL;
	if (sName == m_name)
		pRecipient = (IList*)this;

	return pRecipient;
}

/******************************* eof *************************************/