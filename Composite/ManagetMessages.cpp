/************************************************************************/
/* Name     : Composite\ManagetMessages.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 18 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "ManagetMessages.h"
#include "sv_strutils.h"

//using namespace boost;//::xpressive;

IList* CManagedMessages::GetScript(const CMessage& msg)
{
	IList* pRecipient = NULL;

	std::wstring sMessage(msg.GetName());

	if (m_sMessage.compare(sMessage))
	{
		return /*SPtr*/(pRecipient);
	}

	for (Parameters::const_iterator cit = m_Param.begin(); cit != m_Param.end(); ++cit)
	{
		std::wstring sName  = cit->first;
		std::wstring sValue = cit->second;

		std::wstring paramValue = msg.SafeReadParam(sName, CMessage::CheckedType::String, L"").AsWideStr();
		if (CAtlString(paramValue.c_str()).CompareNoCase(sValue.c_str())) // not match
			return pRecipient;
	}

	// matched here

	//if (m_pOwnBlackList)
	//{
	//	if (IList* blacknumber = m_pOwnBlackList->GetScript(msg)) // check for entering in own black list
	//	{
	//		return blacknumber; // we've got it
	//	}
	//}

	pRecipient = static_cast<IList*>(const_cast<CManagedMessages*>(this));

	return /*SPtr*/(pRecipient);
}

void CManagedMessages::AddParam(const std::wstring& _name, const std::wstring& _value)
{
	m_Param[_name] = _value;//boost::xpressive::wsregex::compile(_value);
}

bool CManagedMessages::BetterThanYou(const IList* Matched/*, const std::wstring& sOrigination, const std::wstring& sDistinationst*/)const
{
	//UINT uMachedOriginRegPartLength = Matched->GetOriginationRegularPartLength();
	//UINT uMachedOriginMaskLength    = Matched->GetOriginationLength();

	//// match the lengths of NOT regular part
	//if (m_uMaskLength - m_uRegMaskLength > uMachedOriginMaskLength - uMachedOriginRegPartLength)
	//	return true;

	//if (m_uMaskLength - m_uRegMaskLength < uMachedOriginMaskLength - uMachedOriginRegPartLength)
	//	return false;

	//// match the lengths of regular part
	//if (m_uRegMaskLength > uMachedOriginRegPartLength)
	//	return true;

	//if (m_uRegMaskLength < uMachedOriginRegPartLength)
	//	return false;

	return false;
}

void CManagedMessages::AddBlackList(SPtr _blacklist)
{
	m_pOwnBlackList = _blacklist;
}

void CManagedMessages::refresh(const std::wstring& _filename, SPtr& parent, IList::BlackListGroups& blackList)
{
	if (m_pOwnBlackList.get())
	{
		m_pOwnBlackList->refresh(_filename, m_pOwnBlackList, blackList);
	}
}

std::wstring CManagedMessages::log()const
{
	std::wstring sLog = format_wstring(L"Get in ManagedMessages by name : %s, action: %s, runscript name: %s ",
		m_sMessage.c_str(), 
		m_sScriptName.empty() ? L"IGNORE" : L"RUNSCRIPT", 
		m_sScriptName.c_str());

	for (Parameters::const_iterator cit = m_Param.begin(); cit != m_Param.end(); ++cit)
	{
		std::wstring sName = cit->first;
		std::wstring sValue = cit->second;

		sLog += format_wstring(L", [%s = %s]", sName.c_str(), sValue.c_str());
	}

	return sLog;

	//CAtlString sTemplate = 
	//	TEXT (" Get in ManagedMessages by name : %s, ")
	//	TEXT (" action: %s,")
	//	TEXT (" runscript name: %s ");

	//CAtlString sLog;
	//sLog.Format(sTemplate,m_sMessage.c_str(),m_sScriptName.empty()?L"IGNORE":L"RUNSCRIPT",m_sScriptName.c_str());
	//return sLog.GetString();
}


/******************************* eof *************************************/