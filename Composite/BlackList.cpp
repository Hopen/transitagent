/************************************************************************/
/* Name     : Composite\BlackList.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 17 Aug 2010                                               */
/************************************************************************/
#include "stdafx.h"
#include "BlackList.h"

IList* CBlackList::GetScript(const CMessage& msg)
{
	std::wstring sOrigination;
	sOrigination = msg.SafeReadParam(L"A", CMessage::CheckedType::String, L"").AsWideStr();
	if (sOrigination.empty()) // from SIP2TA_SMPP_DELIVER_SM message
	{
		sOrigination = msg.SafeReadParam(L"Orig_Address", CMessage::CheckedType::String, L"").AsWideStr();
	}

	if (sOrigination.empty()) // MCC2TA_NEWEMAIL message
	{
		sOrigination = msg.SafeReadParam(L"From", CMessage::CheckedType::String, L"").AsWideStr();
	}

	IList* pCustomConfig = NULL;
	if (m_pCustomCfg)
	{
		pCustomConfig = m_pCustomCfg->GetCustomCfg(msg);
	}

	if (pCustomConfig)
	{
		sOrigination = pCustomConfig->GetPrefix() + sOrigination;
	}

	IList* pRecipient = NULL;
	bool bOriginValid = mMatcherOrigin->IsMatch(sOrigination.c_str(), sOrigination.c_str() + sOrigination.size(), false);
	if (bOriginValid)
	{
		return pRecipient = (IList*)this;
	}

	return pRecipient;
}

bool CBlackList::BetterThanYou(const IList* Matched)const
{
	UINT uMachedOriginRegPartLength = Matched->GetOriginationRegularPartLength();
	UINT uMachedOriginMaskLength = Matched->GetOriginationLength();
	UINT uMachedOriginQuestionLen   = Matched->GetOriginQuestionLen();
	UINT uMachedOriginPercentLen    = Matched->GetOriginPercentLen();

	UINT uMachedOriginNotRegPartMaskLength = uMachedOriginMaskLength - uMachedOriginRegPartLength;

	UINT uOriginNotRegPartMaskLength = m_uMaskLength - m_uRegMaskLength;

	// match the lengths of NOT regular part
	if (uOriginNotRegPartMaskLength - m_uOriginQuestionLen - m_uOriginPercentLen > 
		uMachedOriginNotRegPartMaskLength - uMachedOriginQuestionLen - uMachedOriginPercentLen)
		return true;

	if (uOriginNotRegPartMaskLength - m_uOriginQuestionLen - m_uOriginPercentLen <
		uMachedOriginNotRegPartMaskLength - uMachedOriginQuestionLen - uMachedOriginPercentLen)
		return false;

	if (uOriginNotRegPartMaskLength - m_uOriginPercentLen >
		uMachedOriginNotRegPartMaskLength - uMachedOriginPercentLen)
		return true;

	if (uOriginNotRegPartMaskLength - m_uOriginPercentLen <
		uMachedOriginNotRegPartMaskLength - uMachedOriginPercentLen)
		return false;

	if (m_uOriginPercentLen && !uMachedOriginPercentLen)
		return false;

	if (!m_uOriginPercentLen && uMachedOriginPercentLen)
		return true;

	// match the lengths of regular part
	if (m_uRegMaskLength > uMachedOriginRegPartLength)
		return true;

	if (m_uRegMaskLength < uMachedOriginRegPartLength)
		return false;

	return false;
}
std::wstring CBlackList::log()const
{
	CAtlString sTemplate = 
		TEXT (" Get in Blacklist by origination mask: %s, ")
	    TEXT (" Call'll be ignored ");
	CAtlString sLog;

	std::wstring sOrigiantion(m_sOrigination);
	std::wstring simbolsIn[]  = {L"%" , L""};
	std::wstring simbolsOut[] = {L"%%", L""};
	ReplaceSymbols(sOrigiantion,simbolsIn,simbolsOut);

	sLog.Format(sTemplate,sOrigiantion.c_str());
	return sLog.GetString();
}

void CBlackList::AddCustomConfig(IList* _customcfg)
{
	m_pCustomCfg = _customcfg;
}

/******************************* eof *************************************/