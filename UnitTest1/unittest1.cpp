#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\TransitAgent\TransitAgent.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

//std::wstring CSystemConfiguration::s_module_name = L"TransitAgent.exe";

namespace UnitTest1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(offered_test)
		{
			// TODO: Your test code here
			std::unique_ptr<CTransitAgent> pInst = std::make_unique<CTransitAgent>(L"D:\\Programming\\Projects\\TransitAgent\\Obj\\Test.TransitAgent.exe.config");

			/*IList* script = pInst->GetScript(make_offered(L"4957813195", L"6611"));
			Assert::AreEqual(int(script->GetAction()), int(A_IGNORE));


			script = pInst->GetScript(make_offered(L"4957813191", L"6611"));
			Assert::AreEqual(script->GetScriptName(), std::wstring(L"MakeCall661.sc3"));

			script = pInst->GetScript(make_offered(L"123", L"065106201111"));
			Assert::AreEqual(script->GetScriptName(), std::wstring(L"test5.ccxml"));

			script = pInst->GetScript(make_offered(L"123", L"065106201112"));
			Assert::AreEqual(script->GetScriptName(), std::wstring(L"test6.ccxml"));

			script = pInst->GetScript(make_offered(L"123", L"065106201122"));
			Assert::AreEqual(script->GetScriptName(), std::wstring(L"test2.ccxml"));
		
			script = pInst->GetScript(make_offered(L"123", L"0651062011111"));
			Assert::AreEqual(script->GetScriptName(), std::wstring(L"test1.ccxml"));

			script = pInst->GetScript(make_offered(L"123", L"0651062011211"));
			Assert::AreEqual(script->GetScriptName(), std::wstring(L"test3.ccxml"));*/

			IList* script = pInst->GetScript(make_offered(L"9055505661@bee.vimpelcom.ru", L"15682@bee.vimpelcom.ru"));
			Assert::AreEqual(script->GetScriptName(), std::wstring(L"C:\\IS3\\Beeline\\SS7OperatorController\\Script\\UserConnect.sc3"));

		}

		TEST_METHOD(SMPP_DELIVER_test)
		{
			std::unique_ptr<CTransitAgent> pInst = std::make_unique<CTransitAgent>(L"D:\\Programming\\Projects\\TransitAgent\\Obj\\Test.TransitAgent.exe.config");

			IList* script = pInst->GetScript(make_SIP2TA_SMPP_DELIVER_SM(L"79067503448", L"150003"));
			Assert::AreEqual(script->GetScriptName(), std::wstring(L"c:\\is3\\scripts\\mm\\mm_in2_test.sc3"));

		}

		TEST_METHOD(black_list_get_script)
		{
			std::unique_ptr<CTransitAgent> pInst = std::make_unique<CTransitAgent>(L"D:\\Programming\\Projects\\TransitAgent\\Obj\\Test.TransitAgent.exe.config");

			IList* script = pInst->GetScript(make_MCC2TA_NEWEMAIL(L"MAILER-DAEMON%beekas-09@beeline.ru", L"internet@beeline.ru"));
			//Assert::AreEqual(script->GetScriptName(), std::wstring(L"c:\\is3\\scripts\\mm\\mm_in2_test.sc3"));
			Assert::AreEqual(static_cast<int>(script->GetAction()), static_cast<int>(A_IGNORE));

		}

		CMessage make_offered(const std::wstring& _a, const std::wstring& _b);
		CMessage make_SIP2TA_SMPP_DELIVER_SM(const std::wstring& _a, const std::wstring& _b);
		CMessage make_MCC2TA_NEWEMAIL(const std::wstring& _a, const std::wstring& _b);
	};

}

CMessage UnitTest1::UnitTest1::make_offered(const std::wstring& _a, const std::wstring& _b)
{


	/************************************************************************/
	/*
	Name: OFFERED; InitialMessage = "OFFERED"; Host = "DR-IVR801";
	Board = "DTI1"; TimeSlot = 4; A = "9031392837"; B = "07042d9629475619";
	SigInfo = "0x0601100702600109010A020100040A03107040D269927465910A07031309139382730801801D038090A33F0A84937063213607995200";
	CallID = 0x000392EA-0004D3E7

	*/
	/************************************************************************/
	// testing
	CMessage offered(L"OFFERED");
	//CMessage offered(L"MCC2TA_NEWEMAIL");
	//CMessage offered(L"SIP2TA_SMPP_DELIVER_SM");
	offered[L"InitialMessage"] = L"OFFERED";
	offered[L"Host"] = L"TELSERV1";
	offered[L"Board"] = L"DTI1";
	offered[L"TimeSlot"] = 15;
	offered[L"A"] = _a;
	offered[L"B"] = _b;

	//offered[ L"A"              ] = L"200@10.22.16.161";
	//offered[ L"B"			   ] = L"79135560632@10.22.16.161:5050";

	//offered[L"A"] = L"asterisk@192.168.151.84";
	//offered[L"B"] = L"901@172.21.242.226";

	//offered[ L"Orig_Address"   ] = L"9067837625";
	//offered[ L"Dst_Address"    ] = L"068125";
	//offered[ L"From"           ] = L"ComferenceRoom2@vimpel.tst";
	//offered[ L"To"             ] = L"OfferedTest";
	offered[L"SigInfo"] = L"0x0601100702200109010A020100040684906018520F0A070313097638675208018003047D0291811D038090A33F0A83973041150992995200";
	offered[L"CallID"] = 0x0001FD3763B5BA0E;
	offered[L"MCCID"] = 5;

	//offered[ L"EsmClass"       ] = L"64";// for MM
	offered[L"EsmClass"] = L"0";

	return offered;
	
}


CMessage UnitTest1::UnitTest1::make_SIP2TA_SMPP_DELIVER_SM(const std::wstring& _a, const std::wstring& _b)
{


	/************************************************************************/
	/*
	Name: SIP2TA_SMPP_DELIVER_SM; OriginationAddress = "79067503448"; 
	Orig_Address = "79067503448"; Orig_NPI = 1; Orig_TON = 1; DestinationAddress = "150003"; 
	Dst_Address = "150003"; Dst_NPI = 1; Dst_TON = 3; 
	MessageText = "Fffffffffffffffccccccvvbhhjjkkkbvcdddfhjbvcfdffgvvvcxxddfgg��������"; 
	ServiceType = ""; ESMClass = 64; ProtocolId = 0; PriorityFlag = 1; 
	ScheduledDelivery = ""; ValidityPeriod = ""; RegisteredDelivery = 0; 
	ReplaceIfPresent = 0; DataCoding = 8; sm_def_message_id = 0; 
	SequenceNumber = 9; CommandStatus = 0; 
	OptionalPart = "0x020c0002005d020e000103020f000101"; 
	InitialMessage = "SMPP_DELIVER_SM"; Session = "MCC_RX"; NeedResponse = 0
	*/
	/************************************************************************/
	// testing
	CMessage offered(L"SIP2TA_SMPP_DELIVER_SM");
	offered[L"OriginationAddress"] = _a;
	offered[L"Orig_Address"] = _a;
	offered[L"Orig_NPI"] = 1;
	offered[L"Orig_TON"] = 1;
	offered[L"DestinationAddress"] = _b;
	offered[L"Dst_Address"] = _b;
	offered[L"Dst_NPI"] = 1;
	offered[L"Dst_TON"] = 1;
	offered[L"MessageText"] = L"Fffffffffffffffccccccvvbhhjjkkkbvcdddfhjbvcfdffgvvvcxxddfgg��������";
	offered[L"ServiceType"] = L"";
	offered[L"ESMClass"] = 64;
	offered[L"ProtocolId"] = 0;
	offered[L"PriorityFlag"] = 1;
	offered[L"ScheduledDelivery"] = L"";
	offered[L"ValidityPeriod"] = L"";
	offered[L"RegisteredDelivery"] = 0;
	offered[L"ReplaceIfPresent"] = 0;
	offered[L"DataCoding"] = 8;
	offered[L"sm_def_message_id"] = 0;
	offered[L"SequenceNumber"] = 9;
	offered[L"CommandStatus"] = 0;
	offered[L"OptionalPart"] = L"0x020c0002005d020e000103020f000101";
	offered[L"InitialMessage"] = L"SMPP_DELIVER_SM";
	offered[L"Session"] = L"MCC_RX";
	offered[L"NeedResponse"] = 0;



	return offered;

}

CMessage UnitTest1::UnitTest1::make_MCC2TA_NEWEMAIL(const std::wstring & _a, const std::wstring & _b)
{
	/* 
	Name: MCC2TA_NEWEMAIL; MessageUrl = "http://ms-mult001/Attachments/563579097410710252/inbox/text.plain"; 
	Senddate = 05.08.2016 10:54:23; MCCID = 0x07D23C34-00002AEC; CC = ""; Host = "MS-MULT001"; 
	Type = "Email"; From = "MAILER-DAEMON%beekas-09@beeline.ru"; To = "internet@beeline.ru"; 
	ToList = "internet@beeline.ru;"; 
	Subject = "DELIVERY FAILURE: 550 Message was not accepted -- invalid mailbox. Local mailbox firelady8@mail.ru is unavailable: user not found"...; 
	Attach1Size = 0x00000000-000013A1;	ReceiveDate = 05.08.2016 10:54:26; 
	AttachCount = 0x00000000-00000002; 
	Attach0Url = "http://ms-mult001/Attachments/563579097410710252/inbox/C.DTF"; 
	Attach0Type= 0x00000000-00000001; Attach0Name = "C.DTF"; 
	Attach0Size = 0x00000000-000001D1; 
	Attach1Url = "http://ms-mult001/Attachments/563579097410710252/inbox/msg.eml";
	Attach1Type = 0x00000000-00000001; Attach1Name = "msg.eml"
	*/

	CMessage offered(L"MCC2TA_NEWEMAIL");
	offered[L"MessageUrl"] = L"http://ms-mult001/Attachments/563579097410710252/inbox/text.plain";
	offered[L"Senddate"] = L"05.08.2016 10:54:23";
	offered[L"MCCID"] = 0x07D23C3400002AEC;
	offered[L"Host"] = L"MS-MULT001";
	offered[L"Type"] = L"Email";
	offered[L"From"] = _a;
	offered[L"To"] = _b;
	offered[L"ToList"] = L"internet@beeline.ru";
	offered[L"Subject"] = L"DELIVERY FAILURE: 550 Message was not accepted -- invalid mailbox. Local mailbox firelady8@mail.ru is unavailable: user not found";

	return offered;
}


